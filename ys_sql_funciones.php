<?php
//**************************************************
//                     CONEXION
//**************************************************
function YConnect()
 {
 $ys_dbconn = mysqli_connect(YDB_LOCAL, YDB_USER, YDB_PASS, YDB_BASE);
 if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  exit();
  }
 return $ys_dbconn;
 }

function YQuery($ys_query, $ys_rows=0)
 {
 $yconn = YConnect();
 $ys_ret = mysqli_query($yconn, $ys_query) or die(mysqli_error($yconn));

 //--- Si $ys_rows==1, devolver la cantidad de l�neas
 if($ys_rows==1) {$ys_ret = mysqli_affected_rows($yconn);}

 mysqli_close($yconn);

 //--- Devuelve el Resource ID o la cantidad de l�neas
 return $ys_ret;
 }

function YDelete($ys_tabla, $ys_cond)
 {
 $ys_ret = 0;

 $SQL = "";
 $SQL .= "DELETE";
 $SQL .= " FROM";
 $SQL .= " ".$ys_tabla;
 $SQL .= " WHERE";
 $SQL .= " ".$ys_cond;
 $ys_ret = YQuery($SQL, 1);

 //--- Devuelve la cantidad de l�neas
 return $ys_ret;
 }

function ys_searchdata($ys_tabla="", $ys_condicion="", $ys_campo="", $ys_0Campo_1Contar_2AutoNum_3Max=YSEARCH_CAMPO)
 {
 //--- Si el par�metro $ys_count es YSEARCH_CAMPO (por defecto),
 //--- El valor solicitado de cualquier tabla.

 //--- Si el par�metro $ys_count es YSEARCH_CONTAR,
 //--- se ignora el valor de $ys_campo y se devuelve el conteo de registros seg�n la condici�n dada.

 //--- ALTERNATIVA PARA CONTAR REPETIDOS EN MYSQL
 //--- SELECT [campo], COUNT(*) Total
 //--- FROM [tabla]
 //--- GROUP BY [campo]
 //--- HAVING COUNT(*) > 1

 //--- Si el par�metro $ys_count es YSEARCH_AUTONUM,
 //--- se ignora el valor de $ys_campo y de $ys_condicion, y se devuelve el �ltimo Autonum�rico registrado.

 //--- Si el par�metro $ys_count es YSEARCH_MAX,
 //--- se devuelve el mayor valor de $ys_campo en base a $ys_condicion.

 $SQL = "";
 $SQL .= "SELECT";
 if($ys_0Campo_1Contar_2AutoNum_3Max==YSEARCH_CAMPO)   {$SQL .= " ".$ys_campo;}
 if($ys_0Campo_1Contar_2AutoNum_3Max==YSEARCH_CONTAR)  {$SQL .= " COUNT(*) AS YCount";}
 if($ys_0Campo_1Contar_2AutoNum_3Max==YSEARCH_AUTONUM) //--- Determina el nombre del campo autonum�rico.
  {
  //--- Toma el nombre de la columna de AutoNum de acuerdo a la tabla indicada.
  $ys_autonum = ys_searchdata("information_schema.columns", "TABLE_SCHEMA='".YDB_BASE."' AND TABLE_NAME='".$ys_tabla."' AND EXTRA='auto_increment'", "column_name", YSEARCH_CAMPO);
  $SQL .= " MAX(".$ys_autonum.") AS YCount";
  }

 if($ys_0Campo_1Contar_2AutoNum_3Max==YSEARCH_MAX) //--- Determina el valor m�ximo del campo dado.
  {
  //--- Toma el M�ximo del campo indicado.
  $SQL .= " MAX(".$ys_campo.") AS YCount";
  }

 $SQL .= " FROM";
 $SQL .= " ".$ys_tabla;

 if($ys_0Campo_1Contar_2AutoNum_3Max!=YSEARCH_AUTONUM)
  {
  $SQL .= " WHERE";
  $SQL .= " ".$ys_condicion;
  }
 $SQL .= ";";
 $yconn = YConnect();

 //--- Si se desea saber la cantidad de registros, tomar la variable "YCount".
 $ys_ret = "";

 //--- Realizar la consulta
 if(!mysqli_connect_errno())
  {$ys_rs = mysqli_query($yconn, $SQL);}
 else
  {echo mysqli_connect_error(); exit();}

 if(mysqli_error($yconn)!="")
  {echo mysqli_error($yconn); exit();}

 //--- Obtiene la cantidad de l�neas afectadas por la consulta
 $ys_rows = mysqli_num_rows($ys_rs);

 //--- Si hay registros, tomar el valor del campo solicitado.
 if($ys_rs && $ys_rows > 0)
  {
  $ys_file = mysqli_fetch_array($ys_rs);
  if($ys_0Campo_1Contar_2AutoNum_3Max==YSEARCH_CONTAR  ||
     $ys_0Campo_1Contar_2AutoNum_3Max==YSEARCH_AUTONUM ||
     $ys_0Campo_1Contar_2AutoNum_3Max==YSEARCH_MAX)
   {$ys_ret = intval($ys_file["YCount"]);}
  else
   {$ys_ret = $ys_file[$ys_campo];}
  }

 //--- Cerrar la conexi�n.
 mysqli_close($yconn);

 //--- Si la b�squeda es para Contar, Autonum�rico o M�ximo y no se recibe ning�n valor, devolver cero (0).
 if($ys_0Campo_1Contar_2AutoNum_3Max==YSEARCH_CONTAR  ||
    $ys_0Campo_1Contar_2AutoNum_3Max==YSEARCH_AUTONUM ||
    $ys_0Campo_1Contar_2AutoNum_3Max==YSEARCH_MAX)
  {if($ys_ret=="") {$ys_ret=0;}}

 return $ys_ret;
 }

//**************************************************
//          OBTENER SCRIPT ACTUAL DEL URL
//**************************************************
function ys_get_script()
 {
 $ys_retval = "";
 $ys_retval = basename(stristr($_SERVER['SCRIPT_FILENAME'], substr("lu_", 0, -1)));
 return $ys_retval;
 }

//**************************************************
//          OBTENER DIRECCION ACTUAL URL
//**************************************************
function ys_get_url()
 {
 $ys_retval = "";
 $ys_retval = basename(stristr($_SERVER['REQUEST_URI'], substr("lu_", 0, -1)));
 return $ys_retval;
 }

//**************************************************
//    CONTADOR DE LECTURA Y DESCARGA DE ENLACES
//**************************************************
//--- Incrementar contador de descargas o enlaces.
function ys_sumarcontador($LNK_Codigo)
 {
 $SQL    = "";
 //--- Incrementar los contador de lecturas y descargas
 $SQL .= "UPDATE ";
 $SQL .= " y_links";
 $SQL .= " SET";
 $SQL .= " LNK_Count = LNK_Count+1";
 $SQL .= " WHERE";
 $SQL .= " LNK_Emp = '".$_SESSION["EMP_COD"]."'";
 $SQL .= " AND";
 $SQL .= " LNK_Codigo = '".$LNK_Codigo."'";
 YQuery($SQL);
 }

//--- Obtener cuenta de contador gen�rico
function ys_getcount($LNK_Codigo)
 {
 $ys_ret    = "";
 $ys_ret = ys_searchdata("links", "LNK_Emp = '".$_SESSION["EMP_COD"]."' AND LNK_Codigo = '".$LNK_Codigo."'", "LNK_Count", YSEARCH_CAMPO);
 return $ys_ret;
 }

//**************************************************
//          RECARGAR VARIABLES DE SESION
//**************************************************
function ys_reload_vars()
 {
 //--- Obtiene AutoNumerico y Balance del Usuario actual.
 $_SESSION["UNUM"] = ys_searchdata($ys_tabla=TB_USUARIOS, $ys_condicion="USU_Login='".$_SESSION["ULOG"]."'", $ys_campo="USU_AutoNum", $ys_0Campo_1Contar_2AutoNum_3Max=YSEARCH_CAMPO);
 $_SESSION["UBAL"] = ys_searchdata($ys_tabla=TB_USUARIOS, $ys_condicion="USU_Login='".$_SESSION["ULOG"]."'", $ys_campo="USU_Balance", $ys_0Campo_1Contar_2AutoNum_3Max=YSEARCH_CAMPO);
 }


//**************************************************
//                FUNCIONES VARIAS
//**************************************************

//--- Devuelve ys_count caracteres a la derecha de ys_string
function ys_right($ys_string, $ys_count)
 {return substr($ys_string, ($ys_count*(-1)));}

//--- Devuelve ys_count caracteres a la izquierda de ys_string
function ys_left($ys_string, $ys_count)
 {return substr($ys_string, 0, $ys_count);}


function YFecha($ys_fecha, $ys_tobd)
{
//--- Convierte el valor de "Fecha" entre el formato
//--- dd/mm/aaaa (Pantalla y Reportes) al formato
//--- "aaaammdd" para bases de datos.
$cFecha="";
$cFecha=trim($ys_fecha);

if($ys_tobd)
 {
 //--- Convertir para base de datos "aaaaddmm"

 //--- Verificar que venga con formato dd/mm/aaaa
 if(substr_count($cFecha, "/")==2)
  {
  $cDia = substr($cFecha, 0, 2);
  $cMes = substr($cFecha, 3, 2);
  $cAno = substr($cFecha, 6, 4);
  $ys_ret = $cAno.$cMes.$cDia;
  }
 else
  {
  $ys_ret = $cFecha;
  }
 }
else
 {
 //--- Convertir desde base de datos "dd/mm/aaaa"

 //--- Viene con formato aaaaddmm
 if(substr_count($cFecha, "/")==0)
  {
  $cDia = substr($cFecha, 6, 2);
  $cMes = substr($cFecha, 4, 2);
  $cAno = substr($cFecha, 0, 4);
  $ys_ret = $cDia."/".$cMes."/".$cAno;
  }
 else
  {
  $ys_ret = $cFecha;
  }
 }
return $ys_ret;
}

//**************************************************
//           ADMINISTRAR LA CONFIGURACION
//**************************************************
function ys_getconfig($ys_seccion="", $ys_codigo="", $ys_tipo="C")
 {
 $ys_ret    = "";
 //--- Tipos:  D - Descripci�n / C - Caracter / N - Num�rico / T - Texto
 $ys_tipo_ret = "CFG_Valor".$ys_tipo;
 $ys_ret = ys_searchdata(TB_CONFIG, "CFG_Seccion='".$ys_seccion."' AND CFG_Codigo='".$ys_codigo."'", $ys_tipo_ret, YSEARCH_CAMPO);
 if(mb_detect_encoding($ys_ret)=="UTF-8") {$ys_ret=utf8_decode($ys_ret);}
 return $ys_ret;
 }

function ys_setconfig($ys_seccion="", $ys_codigo="", $ys_tipo="C")
 {
 //--- Tipos:  D - Descripci�n / C - Caracter / N - Num�rico / T - Texto
 $ys_tipo_ret = "CFG_Valor".$ys_tipo;
 return ys_searchdata(TB_CONFIG, "CFG_Seccion='".$ys_seccion."' AND CFG_Codigo='".$ys_codigo."'", $ys_tipo_ret, YSEARCH_CAMPO);
 }

function ys_date($ys_0cor_1lar = 0)
 {
 //--- Variables internas
 $ys_dia    = "";  //--- Nombre corto del d�a (Sun Mon Tue Wed Thu Fri Sat)
 $ys_mes    = "";  //--- Nombre corto del mes (Jan Feb Mar Apr Jun Jul Aug Sep Oct Nov Dec)
 $ys_ano    = "";  //--- Ano en n�meros (1976)
 $ys_retval = "";  //--- Valor devuelto en formato Corto (por defecto): "Lun, 13/Sep/1976" - Largo: "Lunes, 13 de Septiembre de 1976"

 //--- Obtenci�n de los valores
 $ys_dia    = date('D');
 $ys_mes    = date('M');
 $ys_ano    = date('Y');

 //--- Traducci�n del d�a de la semana
 if ($ys_dia == "Sun")     {$ys_dia = "Domingo";}
 elseif ($ys_dia == "Mon") {$ys_dia = "Lunes";}
 elseif ($ys_dia == "Tue") {$ys_dia = "Martes";}
 elseif ($ys_dia == "Wed") {$ys_dia = "Mi�rcoles";}
 elseif ($ys_dia == "Thu") {$ys_dia = "Jueves";}
 elseif ($ys_dia == "Fri") {$ys_dia = "Viernes";}
 elseif ($ys_dia == "Sat") {$ys_dia = "S�bado";}
 if($ys_0cor_1lar == 0) {$ys_dia = ys_left($ys_dia, 3);}
 $ys_dia += ", ";

 //--- Traducci�n del mes del a�o
 if ($ys_mes == "Jan")     {$ys_mes = "Enero";}
 elseif ($ys_mes == "Feb") {$ys_mes = "Febrero";}
 elseif ($ys_mes == "Mar") {$ys_mes = "Marzo";}
 elseif ($ys_mes == "Apr") {$ys_mes = "Abril";}
 elseif ($ys_mes == "May") {$ys_mes = "Mayo";}
 elseif ($ys_mes == "Jun") {$ys_mes = "Junio";}
 elseif ($ys_mes == "Jul") {$ys_mes = "Julio";}
 elseif ($ys_mes == "Aug") {$ys_mes = "Agosto";}
 elseif ($ys_mes == "Sep") {$ys_mes = "Septiembre";}
 elseif ($ys_mes == "Oct") {$ys_mes = "Octubre";}
 elseif ($ys_mes == "Nov") {$ys_mes = "Noviembre";}
 elseif ($ys_mes == "Dec") {$ys_mes = "Diciembre";}
 if($ys_0cor_1lar == 0) {$ys_mes = ys_left($ys_mes, 3);}

 //--- Construye la cadena que ser� devuelta
 $ys_retval = $ys_dia.date('d')." de ".$ys_mes." de ".$ys_ano;
 return $ys_retval;
 }


function YJust($ys_cad, $ys_long, $ys_0der_1izq)
 {
 //--- Justifica la Cadena ys_cad a la derecha o
 //--- a la izquierda (determinado por $ys_0der_1izq)
 //--- seg�n la longitud ys_long.
 //--- Usa las constantes STR_PAD_LEFT y STR_PAD_RIGHT
 $ys_retval = "";
 $ys_retval = str_pad($ys_cad, $ys_long, '&nbsp;', ($ys_0der_1izq ? STR_PAD_LEFT : STR_PAD_RIGHT));
 return $ys_retval;
 }


//--- MOSTRAR TEXTOS DE LAS PAGINAS
function ys_contenido($ys_pagina)
 {
 $ys_rs   = "";
 $ys_echo = "";

 //--- Recorrer el contenido
 $SQL = "";
 $SQL .= "SELECT";
 $SQL .= " *";
 $SQL .= " FROM";
 $SQL .= " textos";
 $SQL .= " WHERE";
 $SQL .= " TXT_Pagina='".$ys_pagina."'";
 $SQL .= ";";
 $ys_rs=YQuery($SQL,"1");

 if($ys_rs)
  {
  $ys_rs= YQuery($SQL);

  while ($ys_file = mysqli_fetch_assoc($ys_rs))
   {
   //--- Se encontr� texto para esta p�gina
   if($ys_file["TXT_Primero"])
    {
    //--- Es el texto SIN imagen
    $ys_echo .= "<div align='center'>";
    $ys_echo .= "\n";
    $ys_echo .= "<center>";
    $ys_echo .= "\n";
    $ys_echo .= "<table border='0' cellspacing='0' style='font-family: Verdana; font-size: 8pt; border-top: 1px solid rgb(255,0,0); border-bottom: 1px solid rgb(255,0,0)' width='1000' cellpadding='5'>";
    $ys_echo .= "\n";
    $ys_echo .= "<tr>";
    $ys_echo .= "\n";
    $ys_echo .= "<td valign='top'>";
    $ys_echo .= "\n";
    $ys_echo .= "<span style='font-family: Verdana; font-size: 13pt; color: rgb(255,0,0)'>".$ys_file["TXT_Titulo"]."</span>";
    $ys_echo .= "\n";
    $ys_echo .= stripslashes($ys_file["TXT_Contenido"]);
    $ys_echo .= "\n";
    $ys_echo .= "</td>";
    $ys_echo .= "\n";
    $ys_echo .= "</tr>";
    $ys_echo .= "\n";
    $ys_echo .= "</table>";
    $ys_echo .= "\n";
    $ys_echo .= "</center>";
    $ys_echo .= "\n";
    $ys_echo .= "</div>";
    $ys_echo .= "\n";
    }
   else
    {
    //--- Es el texto CON imagen
    $ys_echo .= "<div align='center'>";
    $ys_echo .= "\n";
    $ys_echo .= "<center>";
    $ys_echo .= "\n";
    $ys_echo .= "<table border='0' cellspacing='0' style='font-family: Verdana; font-size: 8pt; border-bottom: 1px solid rgb(255,0,0)' width='1000' cellpadding='5'>";
    $ys_echo .= "\n";
    $ys_echo .= "<tr>";
    $ys_echo .= "\n";
    $ys_echo .= "<td width='300' valign='top' style='text-align: center'><img src='images/".$ys_file["TXT_Imagen"]."' width='290' height='120'></td>";
    $ys_echo .= "\n";
    $ys_echo .= "<td width='700' valign='top'><span style='font-family: Verdana; font-size: 13pt; color: rgb(255,0,0)'>".$ys_file["TXT_Titulo"]."</span>";
    $ys_echo .= "\n";
    $ys_echo .= stripslashes($ys_file["TXT_Contenido"]);
    $ys_echo .= "\n";
    $ys_echo .= "</td>";
    $ys_echo .= "\n";
    $ys_echo .= "</tr>";
    $ys_echo .= "\n";
    $ys_echo .= "</table>";
    $ys_echo .= "\n";
    $ys_echo .= "</center>";
    $ys_echo .= "\n";
    $ys_echo .= "</div>";
    $ys_echo .= "\n";
    }
   }

  } //--- if $ys_rs
 else
  {
  //--- �ERROR! NO se encontr� texto para esta p�gina
  $ys_echo .= "<div align='center'>";
  $ys_echo .= "\n";
  $ys_echo .= "<center>";
  $ys_echo .= "\n";
  $ys_echo .= "<table border='0' cellspacing='0' style='font-family: Verdana; font-size: 8pt; text-align: center; border-top: 1px solid rgb(255,0,0); border-bottom: 1px solid rgb(255,0,0)' width='1000' cellpadding='5'>";
  $ys_echo .= "\n";
  $ys_echo .= "<tr>";
  $ys_echo .= "\n";
  $ys_echo .= "<td valign='top'>";
  $ys_echo .= "\n";
  $ys_echo .= "<span style='font-family: Verdana; font-size: 13pt; color: rgb(255,0,0)'>En estos momentos no tenemos informaci�n disponible en este m�dulo.</span><br>No deje de visitarnos para conocer sobre nuestros productos y servicios.";
  $ys_echo .= "\n";
  $ys_echo .= "<p><input type='submit' value='Aceptar' name='ys_enviar'></p>";
  $ys_echo .= "\n";
  $ys_echo .= "</td>";
  $ys_echo .= "\n";
  $ys_echo .= "</tr>";
  $ys_echo .= "\n";
  $ys_echo .= "</table>";
  $ys_echo .= "\n";
  $ys_echo .= "</center>";
  $ys_echo .= "\n";
  $ys_echo .= "</div>";
  $ys_echo .= "\n";
  }

 echo $ys_echo;
 }

//**************************************************
//            CONTAR VISITAS A LA PAGINA
//**************************************************
function ys_visitas($ys_show, $ys_numusu)
 {
 $ys_echo         = "";
 $SQL             = "";
 $ys_autonum      = 0;
 $ys_remote_addr  = getenv('REMOTE_ADDR');
 $ys_remote_host  = gethostbyaddr(getenv('REMOTE_ADDR'));
 $ys_remote_port  = getenv('REMOTE_PORT');
 $ys_user_agent   = getenv('HTTP_USER_AGENT');
 $ys_http_referer = getenv('HTTP_REFERER');
 if(!$ys_show)
  {
  //--- No se mostrar�n las estad�sticas. Se sumar� una visita.
  //--- Si existe la IP, sumar la visita. Si no existe la IP, crear el registro con una visita.
  $ys_autonum = ys_searchdata(TB_VISITAS, "VIS_REMOTE_ADDR='".$ys_remote_addr."'", "VIS_AutoNum", YSEARCH_CAMPO);
  if($ys_autonum)
   {
   //--- Actualizar registro de visita desde esta IP.
   //--- Se anotar�n los �ltimos datos registrados en cada campo.
   $SQL  = "";
   $SQL .= "UPDATE ";
   $SQL .= " y_visitas";
   $SQL .= " SET";
   $SQL .= " VIS_Fecha           ='". YGetDate()       ."'".",";
   $SQL .= " VIS_Hora            ='". YGetTime()       ."'".",";
   $SQL .= " VIS_REMOTE_ADDR     ='". $ys_remote_addr  ."'".",";
   $SQL .= " VIS_REMOTE_HOST     ='". $ys_remote_host  ."'".",";
   $SQL .= " VIS_REMOTE_PORT     =" . $ys_remote_port      .",";
   $SQL .= " VIS_HTTP_USER_AGENT ='". $ys_user_agent   ."'".",";
   $SQL .= " VIS_HTTP_REFERER    ='". $ys_http_referer ."'".",";
   $SQL .= " VIS_Contador        = VIS_Contador+1"         .",";
   $SQL .= " VIS_Movil           =" .WEB_ISMOBILE              ;
   $SQL .= " WHERE";
   $SQL .= " VIS_AutoNum=".$ys_autonum;
   YQuery($SQL);
   }
  else
   {
   //---- Grabar el registro de primera visita desde esta IP.
   $SQL = "";
   $SQL .= "INSERT INTO";
   $SQL .= " ".TB_VISITAS;
   $SQL .= " VALUES";
   $SQL .= " (";
   $SQL .= " 0"                      .",";
   $SQL .= " '".YGetDate()       ."'".",";
   $SQL .= " '".YGetTime()       ."'".",";
   $SQL .= " '".$ys_remote_addr  ."'".",";
   $SQL .= " '".$ys_remote_host  ."'".",";
   $SQL .= " " .$ys_remote_port      .",";
   $SQL .= " '".$ys_user_agent   ."'".",";
   $SQL .= " '".$ys_http_referer ."'".",";
   $SQL .= " 1"                      .",";
   $SQL .= " " .WEB_ISMOBILE             ;
   $SQL .= " )";
   YQuery($SQL);
   }
  }
 else
  {
  //--- Se mostrar�n los datos de las visitas.
  $ys_echo = "";
  $ys_echo .= "Visitas: ".ys_searchdata(TB_VISITAS, "VIS_AutoNum<>0", "SUM(VIS_Contador)", YSEARCH_CAMPO);
  $ys_echo .= " - ";
  $ys_echo .= "Usuario: ";
  if(!isset($_SESSION["USU_Numero"]))
   {$ys_echo .= "N/D";}
  else
   {$ys_echo .= ys_searchdata(TB_VISITAS_LOGIN, "VIS_NumUsuario=".$_SESSION["USU_Numero"], "", YSEARCH_CONTAR);}
  $ys_echo .= " - ";
  $ys_echo .= "I.P. (".$ys_remote_addr."): ".ys_searchdata(TB_VISITAS, "VIS_REMOTE_ADDR='".$ys_remote_addr."'", "VIS_Contador", YSEARCH_CAMPO);
  $ys_echo .= " - ";
  $ys_echo .= "M�vil: ".ys_searchdata(TB_VISITAS, "VIS_Movil", "", YSEARCH_CONTAR);
  return $ys_echo;
  }

 //--- Si el usuario hizo Login, agregar registro en visitas_login.
 if($ys_numusu<>0)
  {
  $SQL = "";
  $SQL .= "INSERT INTO";
  $SQL .= " ".TB_VISITAS_LOGIN;
  $SQL .= " VALUES";
  $SQL .= " (";
  $SQL .= " 0"                      .",";
  $SQL .= " '".YGetDate()       ."'".",";
  $SQL .= " '".YGetTime()       ."'".",";
  $SQL .= " " .$ys_numusu               ;
  $SQL .= " )";
  YQuery($SQL);
  }
 }

//--- Obtiene fecha actual.
function YGetDate()
 {return date("Ymd", getdate()[0]);}

//--- Obtiene hora actual.
function YGetTime()
 {return date("H:i:s", getdate()[0]);}

function ys_getpais($ys_get)
 {
 //--- Valores de ys_get
 //--- 1 - IP.
 //--- 2 - Codigo ISO del pa�s.
 //--- 3 - Nombre del pa�s en Min�sculas.
 //--- 4 - Nombre del pa�s en MAYUSCULAS.
 //--- 5 - C�digo telef�nico del pa�s. Si no existe se devuelve "00".

 $ys_ret = "N/D";
 $ys_ip  = getenv('REMOTE_ADDR');
 $ys_ip_long = ip2long($ys_ip);
			 if ($ys_ip_long == -1 || $ys_ip_long === FALSE)
  {
  $ys_ret = 0;
  }
 else
  {
  $ys_ip_dec = sprintf("%u", ip2long($ys_ip));
  if($ys_get==1) {$ys_ret = $ys_ip;}
  if($ys_get==2) {$ys_ret = ys_searchdata(TB_LOCATION, $ys_ip_dec.">= LOC_IPIni AND ".$ys_ip_dec."<=LOC_IPFin", "LOC_Pais", YSEARCH_CAMPO);}
  }

 if($ys_get==3) {$ys_ret = ys_getconfig($ys_seccion="pais", $ys_codigo=$_SESSION["PAISCOD"], $ys_tipo="C");}
 if($ys_get==4) {$ys_ret = ys_getconfig($ys_seccion="pais", $ys_codigo=$_SESSION["PAISCOD"], $ys_tipo="T");}
 if($ys_get==5)
  {
  $ys_ret = ys_getconfig($ys_seccion="paiscodtel", $ys_codigo=$_SESSION["PAISCOD"], $ys_tipo="C");
  if($ys_ret=="" || $ys_ret=="-") {$ys_ret ="00";}
  }

 return $ys_ret;
 }


function ys_combo($ys_seccion, $ys_default="-", $ys_1echo_2ret = 1, $ys_order = "CFG_AutoNum")
 {
 //--- Devolver options de Combos en base a Tabla "config"
 $ys_rs   = "";
 $ys_echo = "";
 $ys_sel  = "";
 $SQL     = "";
 $SQL .= "SELECT";
 $SQL .= " *";
 $SQL .= " FROM";
 $SQL .= " ".TB_CONFIG;
 $SQL .= " WHERE";
 $SQL .= " CFG_Seccion='".$ys_seccion."'";
 $SQL .= " AND";
 $SQL .= " CFG_Activo";
 $SQL .= " ORDER BY";
 $SQL .= " ".$ys_order." ASC";
 $SQL .= ";";

if($ys_seccion=="")
 {
 echo $SQL;
 echo "<br>";
 }

 $ys_rs=YQuery($SQL,"1");
 if($ys_rs)
  {
  //--- Hay registros
  //--- Recorrer los registros
  $ys_rs= YQuery($SQL);
  $ys_echo = "";
  while ($ys_file = mysqli_fetch_assoc($ys_rs))
   {
   $ys_sel = " ";
   if($ys_file["CFG_Codigo"]==$ys_default) {$ys_sel=" selected ";}
   $ys_echo .= "<option".$ys_sel."value='".$ys_file["CFG_Codigo"]."'>".$ys_file["CFG_ValorC"]."</option>";
   $ys_echo .= "\n";
   }
  }
 else
  {
  $ys_echo .= "<option selected value='-'>N/S</option>";
  $ys_echo .= "\n";
  }
 if($ys_1echo_2ret == 1) {echo $ys_echo;}
 if($ys_1echo_2ret == 2) {return $ys_echo;}
 }

//**************************************************
//          OBTENER DATOS SEGUN EL IDIOMA
//**************************************************
function ys_get_texto($ys_codigo, $ys_ret=FALSE)
 {
 $ys_echo = "";
 $ys_echo = trim(ys_searchdata(TB_IDIOMA, "IDI_Codigo='".$ys_codigo."' AND IDI_Idioma='".$_SESSION['idioma_actual']."'", "IDI_Contenido", FALSE));
 if($ys_echo=="") $ys_echo="&nbsp;";
 if($ys_ret)
  {return $ys_echo;}
 else
  {echo $ys_echo;}
 }


//**************************************************
//     CARGAR EN LAS TABLAS LOS DATOS DE LA API
//**************************************************
function ys_CargarTablas($ys_datos="Y")
 {
 $SQL       = "";
 $tK1       = 0;
 $ys_result = "";
 $ys_total  = "";

 //--- **** LOGICA APLICADA EN ACTIVACION DE ELEMENTOS Cat / Bra / Gam *****
 //--- EN BD | EN API | ACCION
 //---  Si   |   Si   | Activar el indicador de API.
 //---  No   |   Si   | Insertar registro y activar indicador de API.
 //---  Si   |   No   | Dejar el indicador de API apagado.

 //--- Desactivar todos los registros de Existencia en API.
 $SQL  = "UPDATE ".TB_CATEGORIES." SET CAT_EnAPI = 0";
 YQuery($SQL);

 $SQL  = "UPDATE ".TB_BRANDS." SET BRA_EnAPI = 0";
 YQuery($SQL);

 $SQL  = "UPDATE ".TB_GAMES." SET GAM_EnAPI = 0";
 YQuery($SQL);

 //--- Procesar Categories.
 if($ys_datos=="Y" || $ys_datos=="C")
  {
  //--- Leer Categories.
  $ys_result = ys_ListFilters(CLIENT_PHRASE);
  $ys_total = count($ys_result["categories"]);

  //--- Recorrer Categories.
  for($tK1 = 0; $tK1 <= $ys_total-1; $tK1++)
   {
   $ys_id   = addslashes(trim($ys_result["categories"][$tK1]["id"]));
   $ys_name = addslashes(strtoupper(trim($ys_result["categories"][$tK1]["name"])));
   if(ys_searchdata($ys_tabla=TB_CATEGORIES, $ys_condicion="CAT_Codigo='".$ys_id."'", $ys_campo="", $ys_0Campo_1Contar_2AutoNum_3Max=YSEARCH_CONTAR))
    {
    //--- SI existe la Categorie. Activar API.
    $SQL  = "";
    $SQL .= "UPDATE ";
    $SQL .= " ".TB_CATEGORIES;
    $SQL .= " SET";
    $SQL .= " CAT_EnAPI  = 1";
    $SQL .= " WHERE";
    $SQL .= " CAT_Codigo = '".$ys_id."'";
    }
   else
    {
    //--- NO existe la Categorie. Agregar y activar API.
    $SQL  = "";
    $SQL .= "INSERT INTO";
    $SQL .= " ".TB_CATEGORIES;
    $SQL .= " VALUES";
    $SQL .= " (";
    $SQL .= " 0"                                                .",";
    $SQL .= " '".$ys_id                                     ."'".",";
    $SQL .= " '".$ys_name                                   ."'".",";
    $SQL .= " '".$ys_result["categories"][$tK1]["position"] ."'".",";
    $SQL .= " 0"                                                .",";
    $SQL .= " 0"                                                .",";
    $SQL .= " 1"                                                .",";
    $SQL .= " 1";
    $SQL .= " )";
    }
   YQuery($ys_query=$SQL, $ys_rows=0);
   }
  }

 //--- Procesar Brands.
 if($ys_datos=="Y" || $ys_datos=="B")
  {
  //--- Leer Brands.
  $ys_result = ys_ListFilters(CLIENT_PHRASE);
  $ys_total = count($ys_result["brands"]);

  //--- Recorrer Brands.
  for($tK1 = 0; $tK1 <= $ys_total-1; $tK1++)
   {
   $ys_id   = addslashes(trim($ys_result["brands"][$tK1]["id"]));
   $ys_name = addslashes(trim($ys_result["brands"][$tK1]["name"]));
   $ys_cat  = addslashes($ys_result["brands"][$tK1]["category"]);
   $ys_img  = addslashes($ys_result["brands"][$tK1]["img"]);

   if(ys_searchdata($ys_tabla=TB_BRANDS, $ys_condicion="BRA_Codigo='".$ys_id."' AND BRA_CodCat='".$ys_cat."'", $ys_campo="", $ys_0Campo_1Contar_2AutoNum_3Max=YSEARCH_CONTAR))
    {
    //--- SI existe la Brand. Activar API.
    $SQL  = "";
    $SQL .= "UPDATE ";
    $SQL .= " ".TB_BRANDS;
    $SQL .= " SET";
    $SQL .= " BRA_EnAPI  = 1";
    $SQL .= " WHERE";
    $SQL .= " BRA_Codigo = '".$ys_id."'";
    $SQL .= " AND";
    $SQL .= " BRA_CodCat = '".$ys_cat."'";
    }
   else
    {
    //--- NO existe la Brand. Agregar y activar API.
    $SQL  = "";
    $SQL .= "INSERT INTO";
    $SQL .= " ".TB_BRANDS;
    $SQL .= " VALUES";
    $SQL .= " (";
    $SQL .= " 0"              .",";
    $SQL .= " '".$ys_id   ."'".",";
    $SQL .= " '".$ys_name ."'".",";
    $SQL .= " '".$ys_cat  ."'".",";
    $SQL .= " '".$ys_img  ."'".",";
    $SQL .= " 0"              .",";
    $SQL .= " 0"              .",";
    $SQL .= " 1"              .",";
    $SQL .= " 1";
    $SQL .= " )";
    }
   YQuery($ys_query=$SQL, $ys_rows=0);
   }
  }

 //--- Procesar Games.
 if($ys_datos=="Y" || $ys_datos=="G")
  {
  //--- Leer Games.
  $ys_result = ys_ListGames($ys_in_page=1, $ys_in_perPage=1, $ys_st_p=""); //--- Leer para obtener cantidad de juegos.
  $ys_total  = $ys_result["pagination"]["total"]-1;
  $ys_result = ys_ListGames($ys_in_page=1, $ys_in_perPage=($ys_total), $ys_st_p=""); //--- Obtener listado de todos los juegos.

  //--- Recorrer Games.
  for($tK1 = 0; $tK1 <= $ys_total-1; $tK1++)
   {
   $ys_id   = addslashes(trim($ys_result["games"][$tK1]["id"]));
   $ys_name = addslashes(trim($ys_result["games"][$tK1]["g"]));
   $ys_cat  = addslashes($ys_result["games"][$tK1]["c"]);
   $ys_bra  = addslashes($ys_result["games"][$tK1]["b"]);
   $ys_p    = addslashes($ys_result["games"][$tK1]["p"]);
   $ys_m    = $ys_result["games"][$tK1]["m"];
   $ys_img  = addslashes($ys_result["games"][$tK1]["img"]);

   if(ys_searchdata($ys_tabla=TB_GAMES, $ys_condicion="GAM_Codigo='".$ys_id."' AND GAM_Modo='".$ys_m."'", $ys_campo="", $ys_0Campo_1Contar_2AutoNum_3Max=YSEARCH_CONTAR))
    {
    //--- SI existe el Game. Activar API.
    $SQL  = "";
    $SQL .= "UPDATE ";
    $SQL .= " ".TB_GAMES;
    $SQL .= " SET";
    $SQL .= " GAM_EnAPI  = 1";
    $SQL .= " WHERE";
    $SQL .= " GAM_Codigo = '".$ys_id."'";
    }
   else
    {
    //--- NO existe el Game. Agregar y activar API.
    $SQL  = "";
    $SQL .= "INSERT INTO";
    $SQL .= " ".TB_GAMES;
    $SQL .= " VALUES";
    $SQL .= " (";
    $SQL .= " 0"              .","; //--- GAM_AutoNum
    $SQL .= " '".$ys_id   ."'".","; //--- GAM_Codigo
    $SQL .= " '".$ys_name ."'".","; //--- GAM_Nombre
    $SQL .= " '".$ys_cat  ."'".","; //--- GAM_CodCat
    $SQL .= " '".$ys_bra  ."'".","; //--- GAM_CodBra
    $SQL .= " '".$ys_p    ."'".","; //--- GAM_Control
    $SQL .= " '".$ys_m    ."'".","; //--- GAM_Modo
    $SQL .= " '".""       ."'".","; //--- GAM_Tipo
    $SQL .= " '".$ys_img  ."'".","; //--- GAM_Imagen
    $SQL .= " '".""       ."'".","; //--- GAM_Fecha
    $SQL .= " 0".              ","; //--- GAM_Clicks
    $SQL .= " 0".              ","; //--- GAM_Favs
    $SQL .= " 0".              ","; //--- GAM_Nue
    $SQL .= " 0".              ","; //--- GAM_Pop
    $SQL .= " 1".              ","; //--- GAM_EnAPI
    $SQL .= " 1"                  ; //--- GAM_Activo
    $SQL .= " )";
    }
   YQuery($ys_query=$SQL, $ys_rows=0);
   }
  }
 }

//**************************************************
//              MOSTRAR LAS CATEGORIAS
//**************************************************
function ys_ShowCategories()
 {
 $ys_rs    = "";
 $tK1      = 0;
 $ys_echo  = "";
 $ys_act   = "";
 $ys_url     = ""; //--- URL del juego.
 $ys_CodCat = "";   //--- C�digo de la Categor�a.

 //--- Cargar llamada a SQL.
 $SQL  = "";
 $SQL .= "SELECT";
 $SQL .= " *";
 $SQL .= " FROM";
 $SQL .= " ".TB_CATEGORIES;
 $SQL .= " WHERE";
 $SQL .= " CAT_Activo";
 $SQL .= " AND";
 $SQL .= " CAT_EnAPI";
 $SQL .= " ORDER BY";
 $SQL .= " CAT_Posicion ASC";

 $ys_rs = YQuery($ys_query=$SQL, $ys_rows=1);

 //--- Mostrar las Categor�as
 if($ys_rs)
  {
  $ys_rs= YQuery($ys_query=$SQL, $ys_rows=0);
  while ($ys_file = mysqli_fetch_assoc($ys_rs))
   {
   $ys_act = "";
   $ys_CodCat   = $ys_file["CAT_Codigo"];
   if($ys_CodCat=="slot") {$ys_act="active";}

   if(WEB_ISMOBILE)
    {
    $ys_echo .= "<div class='item'>";
    $ys_echo .= "\n";
    $ys_echo .= " <div class='bar-top-border ".$ys_act."' OnClick='ys_js_GetGames(\"C\", \"".$ys_CodCat."\", \"\");'>";
    $ys_echo .= "\n";
    $ys_echo .= "  <img class='img-fluid img-bar-top' src='images/iconos/".$ys_CodCat.".png' alt=''>";
    $ys_echo .= "\n";
    $ys_echo .= "  <p class='bar-top-p'>".$ys_file["CAT_Movil"]."</p>";
    $ys_echo .= "\n";
    $ys_echo .= " </div>";
    $ys_echo .= "\n";
    $ys_echo .= "</div>";
    $ys_echo .= "\n";
    }
   else
    {
    $tK1++;
    //--- S�lo para juegos CASINO EN VIVO.
    if($ys_CodCat=="slot_live")
     {
     $ys_url   = UNIVERSAL_API."/api/launch?gameid=alg-roulette&p=ms&b=Absolute Live Gaming&m=wb&sessionid=";
     $ys_echo .= " <a role='button' href='#' class='btn header-btn Fsub-header".($tK1)." ".$ys_act."' OnClick='juego_simple(\"".$ys_url."\");'>".$ys_file["CAT_Nombre"]."</a>";
     $ys_echo .= "\n";
     }
    //--- S�lo para JUEGOS VIRTUALES.
    if($ys_CodCat=="virtual")
     {
     $ys_echo .= "<div class='header-btn'>";
     $ys_echo .= "\n";
     $ys_echo .= " <a id='juegosvi' role='button' href='#' class='btn nav-color ' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>JUEGOS VIRTUALES</a>";
     $ys_echo .= "\n";
     $ys_echo .= " <div class='dropdown-menu drop-color scroll-provee' aria-labelledby='juegosvi'>";
     $ys_echo .= "\n";
     $ys_url   = UNIVERSAL_API."/api/launch?gameid=10100&p=gr&b=GoldenRace&m=wb&sessionid=";
     $ys_echo .= "  <a class='dropdown-item' href='#' OnClick='juego_simple(\"".$ys_url."\");'>GoldenRace</a>";
     $ys_echo .= "\n";
     $ys_url   = UNIVERSAL_API."/api/launch?gameid=kiron-horses&p=ms&b=Kiron&m=wb&sessionid=";
     $ys_echo .= "  <a class='dropdown-item' href='#' OnClick='juego_simple(\"".$ys_url."\");'>Kiron</a>";
     $ys_echo .= "\n";
     $ys_url   = UNIVERSAL_API."/api/launch?gameid=sportrace&p=vg&b=SportRace&m=wb&sessionid=";
     $ys_echo .= "  <a class='dropdown-item' href='#' OnClick='juego_simple(\"".$ys_url."\");'>SportRace</a>";
     $ys_echo .= "\n";
     $ys_echo .= "  <a class='dropdown-item' href='#' OnClick='ys_js_GetGames(ys_sel=\"B\", ys_cat=\"".$ys_CodCat."\", ys_brand=\"Nsoft\", ys_tipos=\"\");'>NSoft</a>";
     $ys_echo .= "\n";
     $ys_echo .= " </div>";
     $ys_echo .= "\n";
     $ys_echo .= "</div>";
     $ys_echo .= "\n";
     }

    //--- S�lo para juegos LOTERIA.
    if($ys_CodCat=="lottery")
     {
     $ys_url   = UNIVERSAL_API."/api/launch?gameid=liw-Keno1080&p=ms&b=LiW&m=wb&sessionid=";
     $ys_echo .= " <a role='button' href='#' class='btn header-btn Fsub-header".($tK1)." ".$ys_act."' OnClick='juego_simple(\"".$ys_url."\");'>".$ys_file["CAT_Nombre"]."</a>";
     $ys_echo .= "\n";
     }

    //--- S�lo para JUEGOS TV.
    if($ys_CodCat=="tv")
     {
     $ys_echo .= "<div class='header-btn'>";
     $ys_echo .= "\n";
     $ys_echo .= "<a id='juegostvv' role='button' href='#' class='btn nav-color ' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>".$ys_file["CAT_Nombre"]."</a>";
     $ys_echo .= "\n";
     $ys_echo .= "<div class='dropdown-menu drop-color scroll-provee'>";
     $ys_echo .= "\n";
     $ys_url   = UNIVERSAL_API."/api/launch?gameid=100181&p=gr&b=HollywoodTv&m=wb&sessionid=";
     $ys_echo .= "<a class='dropdown-item' href='javascript:void(0)' OnClick='juego_simple(\"".$ys_url."\")'>Hollywood TV</a>";
     $ys_echo .= "\n";
     $ys_url   = UNIVERSAL_API."/api/launch?gameid=tvbet&p=tv&b=tvBet&m=wb&sessionid=";
     $ys_echo .= "<a class='dropdown-item' href='javascript:void(0)' OnClick='juego_simple(\"".$ys_url."\")'>TV BET</a>";
     $ys_echo .= "\n";
     $ys_echo .= "</div>";
     $ys_echo .= "\n";
     $ys_echo .= "</div>";
     $ys_echo .= "\n";
     }

    //--- S�lo para POKER.
    if($ys_CodCat=="poker")
     {
     $ys_url   = UNIVERSAL_API."/api/launch?gameid=evenbet&p=eb&b=UniversalPoker&m=wb&sessionid=";
     $ys_echo .= " <a role='button' href='#' class='btn header-btn Fsub-header".($tK1)." ".$ys_act."' OnClick='juego_simple(\"".$ys_url."\");'>".$ys_file["CAT_Nombre"]."</a>";
     }

    //--- Juegos diferentes a CASINO EN VIVO y JUEGOS DE TV.
    if($ys_CodCat!="slot_live" &&
       $ys_CodCat!="lottery"   &&
       $ys_CodCat!="tv"        &&
       $ys_CodCat!="virtual"   &&
       $ys_CodCat!="poker")
     {
     $ys_echo .= " <a role='button' href='#' class='btn header-btn Fsub-header".($tK1)." ".$ys_act."' OnClick='ys_js_GetGames(\"C\", \"".$ys_CodCat."\", \"\");juegos_tipos(\"".$ys_CodCat."\")'>".$ys_file["CAT_Nombre"]."</a>";
     }
    $ys_echo .= "\n";
     
    } //--- if WEB_MOBILE - ELSE

   } //--- while

  } //--- if ys_rs
 return $ys_echo;
 }

//**************************************************
//        ASIGNAR INDICADOR DE JUEGOS NUEVOS
//**************************************************
function ys_AsignarNue()
 {
 //--- $ys_porc: El porcentaje para determinar Juegos Nuevos y Juegos Populares
 $ys_dias = 0;

 //--- Limpiar asignaci�n de Juegos Nuevos.
 $SQL  = "";
 $SQL .= "UPDATE ";
 $SQL .= " ".TB_GAMES;
 $SQL .= " SET";
 $SQL .= " GAM_Nue = 0";
 YQuery($SQL);

 //--- Asignar los juegos Nuevos en base a fecha de entrada.
 $ys_dias = ys_getconfig($ys_seccion="juego", $ys_codigo="diasvence", $ys_tipo="N");

 $SQL  = "";
 $SQL .= "UPDATE ";
 $SQL .= " ".TB_GAMES;
 $SQL .= " SET";
 $SQL .= " GAM_Nue = 1";
 $SQL .= " WHERE";
 $SQL .= " DATEDIFF(CURDATE(), DATE_FORMAT(GAM_Fecha, '%Y-%m-%d')) <= ".$ys_dias;
 $SQL .= " AND";
 $SQL .= " GAM_Activo";
 YQuery($SQL);
 }

//**************************************************
//       ASIGNAR INDICADOR DE JUEGOS POPULARES
//**************************************************
function ys_AsignarPop()
 {
 //--- $ys_porc: El porcentaje para determinar Juegos Nuevos y Juegos Populares
 $ys_max      = 0; //--- Cantidad m�xima de Clicks registrados entre todos los juegos.
 $ys_limite   = 0; //--- L�mite m�nimo de Clicks para considerar a un juego como "Popular".
 $ys_porcen   = 0; //--- Porcentaje asignado en CMS para determinar Juegos Populares.
 $ys_calcporc = 0; //--- Cantidad neta de Clicks que entran en el porcentaje de Juegos Populares.

 //--- Calcula porcentaje l�mite de Clicks.
 $ys_porcen   = ys_getconfig($ys_seccion="juego", $ys_codigo="porcpop", $ys_tipo="N");
 $ys_max      = ys_searchdata($ys_tabla=TB_GAMES, $ys_condicion="GAM_AutoNum<>0", $ys_campo="GAM_Clicks", $ys_0Campo_1Contar_2AutoNum_3Max=YSEARCH_MAX);
 $ys_calcporc = intval(($ys_max * $ys_porcen)/100);
 $ys_pop      = $ys_max - $ys_calcporc;

 //--- Limpiar asignaci�n de Juegos Populares.
 $SQL    = "";
 $SQL .= "UPDATE ";
 $SQL .= " ".TB_GAMES;
 $SQL .= " SET";
 $SQL .= " GAM_Pop = 0";
 YQuery($SQL);

 //--- Asignar los juegos Populares.
 $SQL    = "";
 $SQL .= "UPDATE ";
 $SQL .= " ".TB_GAMES;
 $SQL .= " SET";
 $SQL .= " GAM_Pop = 1";
 $SQL .= " WHERE";
 $SQL .= " GAM_Clicks >= ".$ys_pop;
 YQuery($SQL);
 }


//**************************************************
//       MOSTRAR LAS CAPAS PARA LAS SECCIONES
//**************************************************
function ys_ShowSection($ys_sec)
 {
 $ys_echo  = "";
 $ys_desc = ys_searchdata($ys_tabla=TB_GTYPES, $ys_condicion="GTY_Codigo='".$ys_sec."' AND GTY_Activo", $ys_campo="GTY_Desc", $ys_0Campo_1Contar_2AutoNum_3Max=YSEARCH_CAMPO);
 
 if(WEB_ISMOBILE)
  {
  $ys_echo .= "<!--- SECCION DE ".$ys_desc." -->";
  $ys_echo .= "\n";
  $ys_echo .= "<section id='ys_sec".$ys_sec."' class='home-bg pl-0 pt-0 pr-0' style='display: block;'>";
  $ys_echo .= "\n";
  $ys_echo .= " <p class='title-games'>".$ys_desc."</p>";
  $ys_echo .= "\n";
  $ys_echo .= " <div id='ys_show".$ys_sec."' class='row no-gutters'>";
  $ys_echo .= "\n";
  $ys_echo .= " </div>";
  $ys_echo .= "\n";
  $ys_echo .= "</section>";
  $ys_echo .= "\n";
  $ys_echo .= "<!--- FIN DE SECCION DE ".$ys_desc." -->";
  }
 else
  {
  //--- Muestra el HTM de las diferentes para desplegar juegos.
  $ys_echo .= "<!--- SECCION DE ".$ys_desc." -->";
  $ys_echo .= "\n";
  $ys_echo .= "<section id='ys_sec".$ys_sec."' class='body-casino'>";
  $ys_echo .= "\n";
  $ys_echo .= " <!--- TITULO DE SECCION DE ".$ys_desc." -->";
  $ys_echo .= "\n";
  $ys_echo .= " <div class='row no-gutters'>";
  $ys_echo .= "\n";
  $ys_echo .= "  <div class='col-12'>";
  $ys_echo .= "\n";
  $ys_echo .= "   <p class='title-casino'>".$ys_desc."</p>";
  $ys_echo .= "\n";
  $ys_echo .= "  </div>";
  $ys_echo .= "\n";
  $ys_echo .= " </div>";
  $ys_echo .= "\n";
  $ys_echo .= " <!--- FIN DE TITULO DE SECCION DE ".$ys_desc." -->";
  $ys_echo .= "\n";
  $ys_echo .= " <!--- LISTA DE ".$ys_desc." -->";
  $ys_echo .= "\n";
  $ys_echo .= " <div id='ys_show".$ys_sec."' class='game-list'>";
  $ys_echo .= "\n";
  $ys_echo .= " <div class='mostrar-div'>";
  $ys_echo .= "\n";
  $ys_echo .= "  <span id='vermas".$ys_sec."' style='display:none;' class='btn-mostrar' onclick=''>Ver M�s <i style='color:red' class='fas fa-angle-double-down'></i></span>";
  $ys_echo .= "\n";
  $ys_echo .= "  <span id='vermenos".$ys_sec."' style='display:none;' class='btn-mostrar'>Ver Menos <i style='color:red' class='fas fa-angle-double-up'></i></span>";
  $ys_echo .= "\n";            
  $ys_echo .= " </div>";
  $ys_echo .= "\n";
  $ys_echo .= " </div>";
  $ys_echo .= "\n";
  $ys_echo .= " <!--- FIN DE LISTA DE ".$ys_desc." -->";
  $ys_echo .= "\n";
  $ys_echo .= "</section>";
  $ys_echo .= "\n";
  $ys_echo .= "<!--- FIN DE SECCION DE ".$ys_desc." -->";
  $ys_echo .= "\n";
  }
 echo $ys_echo;
 }


//**************************************************
//            GENERADORES DE ID Y CLAVES
//**************************************************
function YGUID($ys_llaves=false, $ys_guion=false)
 {
 //--- Generar GUID de 32 bytes.
 $ys_guion = ($ys_guion ? "-" : "");
 if (function_exists('com_create_guid'))
  {return com_create_guid();}
 else
  {
  usleep(1309);
  mt_srand((double)microtime()*10000);
  $ys_string = strtoupper(md5(uniqid(rand(), true)));
  $ys_guid = ($ys_llaves ? "{" : "")
             .substr($ys_string,  0,  8).$ys_guion
             .substr($ys_string,  8,  4).$ys_guion
             .substr($ys_string, 12,  4).$ys_guion
             .substr($ys_string, 16,  4).$ys_guion
             .substr($ys_string, 20, 12)
             .($ys_llaves ? "}" : "");
  return $ys_guid;
  }
 }

function YID()
 {
 //--- Regresa un ID ALFANUMERICO �nico de 13 bytes.
 usleep(1309);
 return str_replace(array("/","."), "Y", strtoupper(crypt(uniqid(), bin2hex(random_bytes(64)))));
 }

function YSerial()
 {
 //--- Regresa un SERIAL NUMERICO �nico de 13 bytes.
 usleep(1309);
 return round(microtime(true)*1000, 0);
 }

function YToken()
 {
 //--- Regresa un TOKEN ALFANUMERICO �nico de 128 bytes.
 usleep(1309);
 return strtoupper(bin2hex(random_bytes(64)));
 }
?>
