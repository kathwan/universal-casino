<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Universal Race</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/x-icon" href="../images/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800,900&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="../css/animate.css">
    
    <link rel="stylesheet" href="../css/owl.carousel.min.css">
    <link rel="stylesheet" href="../css/owl.theme.default.min.css">
    <link rel="stylesheet" href="../css/magnific-popup.css">

    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="../css/jquery.timepicker.css">

    <link rel="stylesheet" href="../css/flaticon.css">
    <link rel="stylesheet" href="../css/style-movil.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.css" id="theme-styles">
    <script language="Javascript" type="text/javascript" src="js/ys_js_ajax.js"></script>
    <script language="Javascript" type="text/javascript" src="include_meta_js.php"></script>
  </head>
  <body class="body-bg1">

    <div class="header">
        <a href="home.php" style="width: 145px;"><img class="img-fluid" src="../images/logo.png" alt="logo"></img></a>
        <!-- <a class="whatsapp-icon" target="blank" href="https://wa.me/51987592623"><i class="fab fa-whatsapp"></i></a> -->
        <div class="user-div" data-toggle="modal" data-target="#user-perfil">
            <i class="far fa-user"></i>
            <span class="money-user">10.63 PEN</span>
        </div>
    </div>

    <div class="bar-top">
      <div class="row no-gutters">
        <div class="col-12">
          <div class="owl-carousel carousel1">
            <div class="item">
              <div class="bar-top-border">
                <a class="no-decoration" href="home.php">
                    <img class="img-fluid img-bar-top" src="../images/iconos/caballo.png" alt="">
                    <p class="bar-top-p">HIPICA</p>
                </a>
              </div>
            </div>
            <div class="item">
              <div class="bar-top-border">
                <a class="no-decoration" href="casino.php">
                    <img class="img-fluid img-bar-top" src="../images/iconos/slot.png" alt="">
                    <p class="bar-top-p">CASINO</p>
                </a>
              </div>
            </div>
            <div class="item">
              <div class="bar-top-border">
                <a class="no-decoration" href="casino-live.php">
                    <img class="img-fluid img-bar-top" src="../images/iconos/casino.png" alt="">
                    <p class="bar-top-p">LIVE CASINO</p>
                </a>
              </div>
            </div>
            <div class="item">
              <div class="bar-top-border">
                <img class="img-fluid img-bar-top" src="../images/iconos/virtuales.png" alt="">
                <p class="bar-top-p">VIRTUALES</p>
              </div>
            </div>
            <div class="item">
              <div class="bar-top-border">
                <img class="img-fluid img-bar-top" src="../images/iconos/award.png" alt="">
                <p class="bar-top-p">DEPORTES VR</p>
              </div>
            </div>
            <div class="item">
              <div class="bar-top-border">
                <img class="img-fluid img-bar-top" src="../images/iconos/bingo.png" alt="">
                <p class="bar-top-p">LOTERIA</p>
              </div>
            </div>
            <div class="item">
              <div class="bar-top-border">
                <img class="img-fluid img-bar-top" src="../images/iconos/poker.png" alt="">
                <p class="bar-top-p">POKER</p>
              </div>
            </div>
            <div class="item">
              <div class="bar-top-border">
                <img class="img-fluid img-bar-top" src="../images/iconos/bingo2.png" alt="">
                <p class="bar-top-p">V BINGO</p>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>

    <div class="promo-bg">
      <div class="owl-carousel carousel2">
        <div class="item">
          <img class="img-fluid" src="../images/casino/slider2.png" atl="promo">
        </div>
        <div class="item">
          <img class="img-fluid" src="../images/casino/slider1.png" atl="promo">
        </div>
      </div>
    </div>

    <div class="jackpot-bg">
        

        <div class="owl-carousel carousel3">
            <div class="item">
                <div class="casino-tabs">
                    <p>All Games</p>
                </div> 
            </div>
            <div class="item">
                <div class="casino-tabs">
                    <p>Top Games</p>
                </div>
            </div>
            <div class="item">
                <div class="casino-tabs">
                    <p>NEW</p>
                </div>
            </div>
            <div class="item">
                <div class="casino-tabs">
                    <p>Booming</p>
                </div>
            </div>
            <div class="item">
                <div class="casino-tabs">
                    <p>Spinmatic</p>
                </div>
            </div>
            <div class="item">
                <div class="casino-tabs">
                    <p>WORLDMATCH</p>
                </div>
            </div>
            <div class="item">
                <div class="casino-tabs">
                    <p>ORYS</p>
                </div>
            </div>
            <div class="item">
                <div class="casino-tabs">
                    <p>SPINOMENAL</p>
                </div>
            </div>
            <div class="item">
                <div class="casino-tabs">
                    <p>FELIX GAMING</p>
                </div>
            </div>
            <div class="item">
                <div class="casino-tabs">
                    <p>1X2GAMING</p>
                </div>
            </div>
        </div>

        <div class="row no-gutters">
            <div class="col-6">
                <div class="favorito-btn">
                    <i class="far fa-heart icon-heart" aria-hidden="true"></i>
                    <span>Mostrar Favoritos</span>
                </div>
            </div>
            <div class="col-6">
                <div class="jackpot-list-2 dropdown-toggle">
                    <a role="button" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proveedores</a>
                    <div class="dropdown-menu drop-casino" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Todos Los Juegos (753)</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Spinmatic (44)</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Booming (90)</a>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div id="game-list">

    </div>

    <!-- <div class="footer">
        <div class="footer-div">
            <div class="footer-links active">
                <a href="home.php">
                  <i class="fas fa-home"></i>
                  <p class="btn-footer-icon">Inicio</p>
                </a>
            </div>
            <div class="footer-links">
                <a href="#" data-toggle="modal" data-target="#programas">
                  <i class="fas fa-book"></i>
                  <p class="btn-footer-icon">Programas</p>
                </a>
            </div>
            <div class="footer-links">
                <a href="promotions.php">
                  <i class="fas fa-tags"></i>
                  <p class="btn-footer-icon">Promociónes</p>
                </a>
            </div>
            <div class="footer-links">
                <a href="#" data-toggle="modal" data-target="#user-resultado">
                  <i class="fas fa-clipboard-list"></i>
                  <p class="btn-footer-icon">Resultado</p>
                </a>
            </div>
            <div class="footer-links n-border-r">
                <a href="#" data-toggle="modal" data-target="#user-apuesta">
                  <i class="fas fa-clipboard-check"></i>
                  <p class="btn-footer-icon">Ticket de apuesta</p>
                </a>
            </div>
        </div>
    </div> -->
    
    <!-- Modal -->
    <div class="modal fade" id="user-perfil" tabindex="-1" role="dialog" aria-labelledby="userperfil" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row">
                      <div class="col-6">
                        <p class="moda-p">Artigel # 1000074</p>
                      </div>
                      <div class="col-6">
                        <a class="modal-salir" href="">Cerrar Sesión <i class="fas fa-sign-out-alt"></i></a>
                      </div>
                    </div>
                    <hr class="mt-0">
                    <div class="row">
                      <div class="col-6">
                        <p class="moda-p">Saldo :</p>
                      </div>
                      <div class="col-6">
                        <div class="modal-money">
                          <p class="moda-p mb-3" >10520 PEN</p>
                          <div class="mb-3"><i class="fas fa-sync-alt"></i></div>
                        </div>
                      </div>
                    </div>
                    <hr class="mt-0">
                    <div class="row">
                      <div class="col-6">
                        <p class="moda-p">Saldo Bono :</p>
                      </div>
                      <div class="col-6">
                        <div class="modal-money">
                          <p class="moda-p mb-3" >10520 PEN</p>
                          <div class="mb-3"><i class="fas fa-sync-alt"></i></div>
                        </div>
                      </div>
                    </div>
                    <hr class="mt-0 mb-0">
                    <div class="row">
                      <div class="col-12">
                        <a href="deposit.php" role="button" class="btn btn-success btn-block mt-3 mb-3 ">Depositar</a>
                      </div>
                    </div>
                    <hr class="mt-0">

                    <div class="row">
                      
                      <div class="col-6">
                        <a class="modal-text moda-p" href="#" data-dismiss="modal" data-toggle="modal" data-target="#programas">
                          <i class="fas fa-book mb-3 mr-3"></i>
                          <p class="mb-3">Programas</p>
                        </a>
                      </div>
                      <div class="col-6">
                        <a class="modal-text moda-p" href="#">
                          <i class="fas fa-horse-head mb-3 mr-3"></i>
                          <p class="mb-3">Retirados</p>
                        </a>
                      </div>
                      <div class="col-6">
                        <a class="modal-text moda-p" href="#" data-dismiss="modal" data-toggle="modal" data-target="#user-resultado">
                          <i class="fas fa-clipboard-list mb-3 mr-3"></i>
                          <p class="mb-3">Resultados</p>
                        </a>
                      </div>
                      <div class="col-6">
                        <a class="modal-text moda-p" href="mybets.php">
                          <i class="fas fa-clipboard-check mb-3 mr-3"></i>
                          <p class="mb-3">Mis Apuestas</p>
                        </a>
                      </div>
                      <div class="col-6">
                        <a class="modal-text moda-p" href="payment-history.php">
                          <i class="fas fa-search-dollar mb-3 mr-3"></i>
                          <p class="mb-3">Movimientos</p>
                        </a>
                      </div>
                      <div class="col-6">
                        <a class="modal-text moda-p" href="retirement.php">
                          <i class="fas fa-university mb-3 mr-3"></i>
                          <p class="mb-3">Retiros</p>
                        </a>
                      </div>
                      <div class="col-6">
                        <a class="modal-text moda-p" href="#" data-dismiss="modal" data-toggle="modal" data-target="#user-clave">
                          <i class="fas fa-unlock-alt mb-3 mr-3"></i>
                          <p class="mb-3">Cambiar Clave</p>
                        </div>
                        </a>
                      <div class="col-6">
                        <a class="modal-text moda-p" href="profile.php">
                          <i class="fas fa-user-shield mb-3 mr-3"></i>
                          <p class="mb-3">Mi Cuenta</p>
                        </a>
                      </div>
                      <div class="col-6">
                        
                      </div>

                    </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen">
    <div class="espere-logo"><img class="img-fluid" src="../images/espere.gif" alt=""></div>
    <!-- <svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg> -->
  </div>
  
  <script src="https://kit.fontawesome.com/a076d05399.js"></script>
  <script src="../js/jquery.min.js"></script>
  <script src="../js/jquery-migrate-3.0.1.min.js"></script>
  <script src="../js/popper.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <script src="../js/jquery.easing.1.3.js"></script>
  <script src="../js/jquery.waypoints.min.js"></script>
  <script src="../js/jquery.stellar.min.js"></script>
  <script src="../js/jquery.animateNumber.min.js"></script>
  <script src="../js/bootstrap-datepicker.js"></script>
  <script src="../js/jquery.timepicker.min.js"></script>
  <script src="../js/owl.carousel.min.js"></script>
  <script src="../js/jquery.magnific-popup.min.js"></script>
  <script src="../js/scrollax.min.js"></script>
  <script src="../js/main.js"></script>
  <script src="../js/mystyles_movil.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  </body>
</html>