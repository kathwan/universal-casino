<?php
include_once("include_sessions.php");
include_once("ys_sql_funciones.php");
include_once("ys_leer_api.php");

//--- Obtener el Token actualizado para el usuario registrado.
$ys_retval = "";
$ys_return = "";

//--- Obtener Token desde la API.
ys_reload_vars();
$ys_result = ys_StartSession($_SESSION["ULOG"], $_SESSION["UBAL"]);

if($ys_result["status"])
 {
 //--- El usuario es v�lido.
 //--- 1) Obtener el Token.
 //--- 2) Grabar datos de Sesi�n de usuario.
 $ys_retval = $ys_result["val"]["token"];

 //--- Grabar SessionID de usuario.
 $SQL = "";
 $SQL .= "INSERT INTO";
 $SQL .= " ".TB_SESIONES;
 $SQL .= " VALUES";
 $SQL .= " (";
 $SQL .= " 0"                       .",";   //--- Autonum�rico
 $SQL .= " '".$_SESSION["ULOG"] ."'".",";   //--- Login de Usuario actual.
 $SQL .= " '" .$ys_retval       ."'".",";   //--- Token generado por ys_StartSession() desde la API.
 $SQL .= " '".YGetDate()        ."'".",";   //--- Fecha (AAAAMMDD).
 $SQL .= " '".YGetTime()        ."'".",";   //--- Hora (HH:MM:SS 24h).
 $SQL .= " 1"                           ;   //--- Registro Activo.
 $SQL .= " )";
 YQuery($SQL);
 }
else
 {$ys_retval = "ERROR: ".$ys_result["err"];}

echo $ys_retval;
?>
