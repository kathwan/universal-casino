<!-- ENCABEZADO -->
<div class="header">

 <!-- LOGOTIPO -->
 <a class="user-logo-casino" href="home.php"><img class="img-fluid" src="images/logo.png" alt="logo"></img></a>
 <!-- FIN DE LOGOTIPO -->

 <!-- PROMOCIONES -->
 <div class="d-flex-end user-icon-w">  
  <a href="promotions.php" class="user-icons active">
   <svg class="img-svg pulse-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M32 271.692v192c0 17.664 14.368 32 32 32h160v-224H32zM480 143.692H378.752c7.264-4.96 13.504-9.888 17.856-14.304 25.824-25.952 25.824-68.192 0-94.144-25.088-25.28-68.8-25.216-93.856 0-13.888 13.92-50.688 70.592-45.6 108.448h-2.304c5.056-37.856-31.744-94.528-45.6-108.448-25.088-25.216-68.8-25.216-93.856 0-25.792 25.952-25.792 68.192-.032 94.144 4.384 4.416 10.624 9.344 17.888 14.304H32c-17.632 0-32 14.368-32 32v48c0 8.832 7.168 16 16 16h208v-64h64v64h208c8.832 0 16-7.168 16-16v-48c0-17.632-14.336-32-32-32zm-257.888-1.056s-1.344 1.056-5.92 1.056c-22.112 0-64.32-22.976-78.112-36.864-13.408-13.504-13.408-35.52 0-49.024 6.496-6.528 15.104-10.112 24.256-10.112 9.12 0 17.728 3.584 24.224 10.112 21.568 21.696 43.008 77.12 35.552 84.832zm73.664 1.056c-4.544 0-5.888-1.024-5.888-1.056-7.456-7.712 13.984-63.136 35.552-84.832 12.896-13.024 35.456-13.088 48.48 0 13.44 13.504 13.44 35.52 0 49.024-13.792 13.888-56 36.864-78.144 36.864zM288 271.692v224h160c17.664 0 32-14.336 32-32v-192H288z"/></svg>
   <span class="Fheader1">Promociones</span>
  </a>
 </div>
 <!-- FIN DE PROMOCIONES -->

 <!-- DATOS DE USUARIO -->
 <div class="user-data">

  <!-- DATOS ADMINISTRATIVOS -->
  <div class="d-flex-center w-100">

   <!-- SALUDO -->
   <div class="user-wi1">
    <span class="Fheader5">Hola,</span>
    <br>
    <span class="Fheader5">
     <span id="usu_nombres">visitante.</span>
    </span>
    <span>
    </span>
   </div>
   <!-- FIN DE SALUDO -->

   <!-- SALDO -->
   <div class="input-group user-wi2">
    <div class="input-group-prepend w-100">
     <span class="input-group-text user-saldo Fheader6" id="inputsaldo">Saldo:</span>
     <span class="input-group-text user-saldo user-saldo-cash" id="inputsaldo">
     <span id="usu_moneda"></span>
     &nbsp;
     <span id="usu_balance">0.00</span>
     </span>
    </div>
   </div>
   <!-- FIN DE SALDO -->

   <!-- SALDO BONO -->
   <div class="input-group user-wi3">
    <div class="input-group-prepend w-100">
     <span class="input-group-text user-saldo Fheader7" id="inputsaldo">Saldo Bono:</span>
     <span class="input-group-text user-saldo user-saldo-cash" id="inputsaldo">USD 0.00</span>
    </div>
   </div>
   <!-- FIN DE SALDO BONO -->

   <!-- BOTON DE DEPOSITO -->
   <div class="ma-5 user-wi4">
    <button class="btn user-btn-de Fheader8" data-toggle="modal" data-target="#user-deposit">Dep�sito</button>
   </div>
   <!-- FIN DE BOTON DE DEPOSITO -->
   
   <!-- BOTON DE RECARGAR -->
   <div class="ma-5 user-wi5">
    <button class="btn user-btn" OnClick="ys_js_GetUserData(ys_d=3);"><i class="fas fa-redo-alt"></i></button>
   </div>
   <!-- FIN DE BOTON DE RECARGAR -->

   <!-- BOTON DE SALIR -->
   <div class="ma-5 user-wi6">
    <button class="btn user-btn Fheader9">Salir <i class="fas fa-sign-out-alt"></i></button>
   </div>
   <!-- FIN DE BOTON DE SALIR -->
  </div>
  <!-- FIN DE DATOS ADMINISTRATIVOS -->
 
  <!-- NAVEGACION DE USUARIO -->
  <div class="user-nav">
   <a class="nav-a-user Fheader10" href="" data-toggle="modal" data-target="#user-apuestas">Apuestas realizadas</a>
   <a class="nav-a-user Fheader11" href="" data-toggle="modal" data-target="#user-movimiento">Movimientos</a>
   <a class="nav-a-user Fheader12" href="" data-toggle="modal" data-target="#user-retiro">Retiros</a>
   <a class="nav-a-user Fheader13" href="" data-toggle="modal" data-target="#user-clave">Cambio de clave</a>
   <a class="nav-a-user Fheader14" href="" data-toggle="modal" data-target="#user-perfil">Mi cuenta</a>
  </div>
  <!-- FIN DE NAVEGACION DE USUARIO -->
 </div> 
  <!-- FIN DE DATOS DE USUARIO -->
</div>
<!-- FIN DE ENCABEZADO -->
