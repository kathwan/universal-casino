REM ***** VIEWCASINO *****

RD /S /Q C:\XAMPP\htdocs
MD C:\XAMPP\htdocs

COPY *.css C:\XAMPP\htdocs
COPY *.csv C:\XAMPP\htdocs
COPY *.doc C:\XAMPP\htdocs
COPY *.ico C:\XAMPP\htdocs
COPY *.inc C:\XAMPP\htdocs
COPY *.js  C:\XAMPP\htdocs
COPY *.mp3 C:\XAMPP\htdocs
COPY *.pdf C:\XAMPP\htdocs
COPY *.php C:\XAMPP\htdocs
COPY *.png C:\XAMPP\htdocs
COPY *.pps C:\XAMPP\htdocs
COPY *.wav C:\XAMPP\htdocs
COPY *.xls C:\XAMPP\htdocs
COPY .htaccess C:\XAMPP\htdocs

MD C:\XAMPP\htdocs\css
XCOPY css C:\XAMPP\htdocs\css /E /C /Y /H /R

MD C:\XAMPP\htdocs\fonts
XCOPY fonts C:\XAMPP\htdocs\fonts /E /C /Y /H /R

MD C:\XAMPP\htdocs\images
XCOPY images C:\XAMPP\htdocs\images /E /C /Y /H /R

MD C:\XAMPP\htdocs\images_paises
XCOPY images_paises C:\XAMPP\htdocs\images_paises /E /C /Y /H /R

MD C:\XAMPP\htdocs\js
XCOPY js C:\XAMPP\htdocs\js /E /C /Y /H /R

MD C:\XAMPP\htdocs\sessions
XCOPY sessions C:\XAMPP\htdocs\sessions /E /C /Y /H /R

MD C:\XAMPP\htdocs\ys_phpmailer
XCOPY ys_phpmailer C:\XAMPP\htdocs\ys_phpmailer /E /C /Y /H /R
