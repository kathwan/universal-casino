<?php
include_once("include_is_mobile.php");
include_once("ys_sql_funciones.php");
$ys_ismobile = new Mobile_Detect;
$ys_name = "lobbyuni";
header('Content-Type:text/html; charset=windows-1252');
mb_internal_encoding("Windows-1252");

//--- Constantes Generales
if(!defined("WEB_VERSION"))  {define("WEB_VERSION"  , "1.00.000");}                                //--- Versi�n de la Web.
if(!defined("WEB_ISOPAIS"))  {define("WEB_ISOPAIS"  , "PE");}                                      //--- C�digo ISO de Pa�s sede de la Web.
if(!defined("WEB_TELPAIS"))  {define("WEB_TELPAIS"  , "+51");}                                     //--- C�digo telef�nico de Pa�s sede de la Web.
if(!defined("WEB_ISMOBILE")) {define("WEB_ISMOBILE" , ($ys_ismobile->isMobile() ? 1 : 0));}        //--- Dispositivo utilizado: wb: Web (PC) / mb: M�vil
if(!defined("DEFAULT_NOM"))  {define("DEFAULT_NOM"  , "LOBBY UNIVERSALSOFT");}                     //--- Web por defecto.
if(!defined("DEFAULT_WEB"))  {define("DEFAULT_WEB"  , "http://www.lobbyuniversalsoft.com");}       //--- URL por defecto.

//--- HTML Language
if(!defined("WEB_LANG")) {define("WEB_LANG", "es-PE");}

//--- Constantes para ys_leer_api.php
if(!defined("UNIVERSAL_API"))    {define("UNIVERSAL_API", "https://test.apiuniversalsoft.com");}   //--- API de Universal.
if(!defined("DEMO_API"))         {define("DEMO_API", "https://demo.apiuniversalsoft.com");}        //--- API de Universal.
if(!defined("CLIENT_PHRASE"))    {define("CLIENT_PHRASE", "C00243766056TestLocal");}
// if(!defined("CLIENT_PHRASE")) {define("CLIENT_PHRASE", "C00172198641DemoLocal");}

//--- Constantes DB
if(!defined("YDB_HOST"))      {define("YDB_HOST", "localhost");}
if(!defined("YDB_LOCAL"))     {define("YDB_LOCAL", "127.0.0.1");}
if(!defined("YDB_HOST_DEMO")) {define("YDB_HOST_DEMO", "demo.apiuniversalsoft.com");}
if(!defined("YDB_BASE"))      {define("YDB_BASE", "lobbyuni_casino");}
if(!defined("YDB_USER"))      {define("YDB_USER", "lobbyuni_LobbyUS");}
if(!defined("YDB_PASS"))      {define("YDB_PASS", "!2020LobbyUS$$");}

//--- Constantes para ys_searchdata()
if(!defined("YSEARCH_CAMPO"))   {define("YSEARCH_CAMPO", 0);}   //--- Devuelve el valor del campo
if(!defined("YSEARCH_CONTAR"))  {define("YSEARCH_CONTAR", 1);}  //--- Devuelve cantidad de registros
if(!defined("YSEARCH_AUTONUM")) {define("YSEARCH_AUTONUM", 2);} //--- Devuelve AutoNum
if(!defined("YSEARCH_MAX"))     {define("YSEARCH_MAX", 3);}     //--- Devuelve el valor m�ximo para el campo dado.

//--- Constantes para YFecha()
if(!defined("YFECHA_NUM"))   {define("YFECHA_NUM" , "0");}      //--- Funci�n YFecha() - Fecha num�rica: 99/99/9999
if(!defined("YFECHA_COR"))   {define("YFECHA_COR" , "1");}      //--- Funci�n YFecha() - Fecha corta: 99/Sep/9999
if(!defined("YFECHA_LAR"))   {define("YFECHA_LAR" , "2");}      //--- Funci�n YFecha() - Fecha larga: 99/Septiembre/9999

//--- Constantes para ys_combo()
if(!defined("YCOMBO_ECHO")) {define("YCOMBO_ECHO", 1);}    //--- Muestra directamente los Options
if(!defined("YCOMBO_RET"))  {define("YCOMBO_RET", 2);}     //--- Devuelve la cadena con los Options

//--- Constantes para Nombres de Tablas
if(!defined("TB_BRANDS"))        {define("TB_BRANDS",        "y_brands");}
if(!defined("TB_CATEGORIES"))    {define("TB_CATEGORIES",    "y_categories");}
if(!defined("TB_CONFIG"))        {define("TB_CONFIG",        "y_config");}
if(!defined("TB_GAMES"))         {define("TB_GAMES",         "y_games");}
if(!defined("TB_GTYPES"))        {define("TB_GTYPES",        "y_gtypes");}
if(!defined("TB_IDIOMAS"))       {define("TB_IDIOMAS",       "y_idiomas");}
if(!defined("TB_LOCATION"))      {define("TB_LOCATION",      "y_location");}
if(!defined("TB_SESIONES"))      {define("TB_SESIONES",      "y_sesiones");}
if(!defined("TB_TRANSACT"))      {define("TB_TRANSACT",      "y_transact");}
if(!defined("TB_USUARIOS"))      {define("TB_USUARIOS",      "y_usuarios");}
if(!defined("TB_USUCF"))         {define("TB_USUCF",         "y_usucf");}
if(!defined("TB_VISITAS"))       {define("TB_VISITAS",       "y_visitas");}
if(!defined("TB_VISITAS_LOGIN")) {define("TB_VISITAS_LOGIN", "y_visitas_login");}

if($_SERVER['SERVER_NAME']==YDB_LOCAL || $_SERVER['SERVER_NAME']==YDB_HOST)
 {
 //--- Web Local
 if(!defined("WEB_IP")) {define("WEB_IP", "209.45.60.42");}
 }
else
 {
 //--- Web Host
 if(!defined("WEB_IP")) {define("WEB_IP", "179.61.12.108");}
 date_default_timezone_set("America/Lima");
 }

//--- Iniciar Sesi�n de navegador.
ini_set('session_save_path', $_SERVER['DOCUMENT_ROOT'].'/sessions');
session_save_path($_SERVER['DOCUMENT_ROOT'].'/sessions');
if(session_name()!=$ys_name)
 {
 session_name($ys_name);
 session_start();
// if(@session_start() == false) {session_destroy(); session_start();}
 }
set_time_limit(0);

if(!isset($_SESSION["EMP_COD"])) {$_SESSION["EMP_COD"] = ys_getconfig("datosemp", "EMP_Codigo",      "C");}
if(!isset($_SESSION["EMP_NOM"])) {$_SESSION["EMP_NOM"] = ys_getconfig("datosemp", "EMP_Nombre",      "C");}
if(!isset($_SESSION["EMP_RAZ"])) {$_SESSION["EMP_RAZ"] = ys_getconfig("datosemp", "EMP_RazonSocial", "C");}
if(!isset($_SESSION["EMP_DOM"])) {$_SESSION["EMP_DOM"] = ys_getconfig("datosemp", "EMP_Dominio",     "C");}
if(!isset($_SESSION["EMP_URL"])) {$_SESSION["EMP_URL"] = ys_getconfig("datosemp", "EMP_URL",         "C");}

if(!isset($_SESSION["PAISCOD"])) {$_SESSION["PAISCOD"] = ys_getpais(2);}  //--- ACTUAL: Codigo ISO del pa�s.
if(!isset($_SESSION["PAISNOM"])) {$_SESSION["PAISNOM"] = ys_getpais(4);}  //--- ACTUAL: Nombre del pa�s en MAYUSCULAS.
if(!isset($_SESSION["PAISTEL"])) {$_SESSION["PAISTEL"] = ys_getpais(5);}  //--- ACTUAL: C�digo telef�nico del pa�s. Si no existe se devuelve "00".
if(!isset($_SESSION['PAISIDI'])) {$_SESSION['PAISIDI'] = "ES";}           //--- ACTUAL: C�digo ISO de idioma del pa�s.

//---- Datos de Usuario activo
if(!isset($_SESSION['UNUM'])) {$_SESSION['UNUM'] = 0;}          //--- Autonum�rico del registro del usuario.
if(!isset($_SESSION['ULOG'])) {$_SESSION['ULOG'] = "demo01";}   //--- ACTUAL: Login del usuario.
if(!isset($_SESSION['UBAL'])) {$_SESSION['UBAL'] = 0;}          //--- Balance del usuario.
ys_reload_vars(); //--- Actualiza variables de sesi�n para Usuario activo.

function ys_titulo()
 {
 $ys_file  = "";
 $ys_echo  = "";
 $ys_file  = ys_get_script();
 $ys_echo .= "<title>".$_SESSION["EMP_NOM"]." v".WEB_VERSION;
 $ys_echo .= "</title>";
 $ys_echo .= "\n";
 echo $ys_echo;
 }
?>
