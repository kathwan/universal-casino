<?php
include_once("include_sessions.php");
include_once("ys_sql_funciones.php");

//--- --------------------------------------------------
//--- Determinar la solicitud que envi� el cliente.
//--- --------------------------------------------------
$ys_url = "";
$ys_url = explode("?", $_SERVER["REQUEST_URI"], 139);
$ys_url = basename($ys_url[0]);
if(strcmp($ys_url, "getUserBalance")!=0 &&
   strcmp($ys_url, "insertBet")!=0      &&
   strcmp($ys_url, "betResult")!=0      &&
   strcmp($ys_url, "gamePager")!=0      &&
   strcmp($ys_url, "gameTypes")!=0
  )
 {
 //--- La solicitud es incorrecta.
 $ys_retarr = [
              'status'  => 0,
              'balance' => 0,
              'code'    => 'INTERNAL_ERROR',
              'message' => 'EndPoint not found. Check case sensitive.'
              ];
 ys_EP_Response($ys_retarr);
 }

//--- --------------------------------------------------
//--- Definir que sea obligatoriamente m�todo POST.
//--- --------------------------------------------------
if($_SERVER["REQUEST_METHOD"]!="POST")
 {
 $ys_retarr = [
              'status'  => 0,
              'balance' => 0,
              'code'    => 'INTERNAL_ERROR',
              'message' => 'Method not allowed.'
              ];
 ys_EP_Response($ys_retarr);
 }

//--- --------------------------------------------------
//---      GETBALANCE - Obtener balance de usuario.
//--- --------------------------------------------------
if(strcmp($ys_url, "getUserBalance")==0)
 {
 $ys_bo_validar = true; //--- Control de validaci�n de par�metros.
 $ys_SES_SessionID = "";
 unset($ys_retarr);

 //--- Recibir par�metros en JSON y decodificar en forma de Array.
 $ys_post = json_decode(file_get_contents("php://input"), true);

 //--- Grabar Log.
 ys_EP_Log($ys_evento="-", $ys_ep=$ys_url, $ys_contenido="");
 ys_EP_Log($ys_evento="", $ys_ep=$ys_url, $ys_contenido="Recibi� ys_post.");
 ys_EP_Log($ys_evento="ep", $ys_ep=$ys_url, $ys_contenido=$ys_post);

 //--- SessionID
 $ys_bo_validar = $ys_bo_validar && (count($ys_post) == 1);                     //--- Se recibi� s�lo UN par�metro.
 $ys_bo_validar = $ys_bo_validar && array_key_exists("sessionid", $ys_post);   //--- Se recibi� el par�metro esperado.
 $ys_bo_validar = $ys_bo_validar && is_string($ys_post["sessionid"]);          //--- El par�metro es tipo String.
 $ys_bo_validar = $ys_bo_validar && (strlen($ys_post["sessionid"]) == 128);    //--- La cadena es de 128 bytes de longitud.

 //--- Grabar Log.
 ys_EP_Log($ys_evento="", $ys_ep=$ys_url, $ys_contenido="Pas� validaci�n de recepci�n de par�metros.");

 //--- Si hubo error en las validaciones devolver error.
 if(!$ys_bo_validar)
  {
  $ys_retarr = [
               'status'  => 0,
               'balance' => 0,
               'code'    => 'INTERNAL_ERROR',
               'message' => 'Bad parameters.'
               ];
  ys_EP_Response($ys_retarr);
  }

 //--- Si llega hasta aqu� es porque la solicitud es correcta. Se toma el par�metro recibido.
 $ys_SES_SessionID = $ys_post["sessionid"];

 //--- Verificar que exista el SessionID.
 //--- Si no existe, la funci�n muestra el error y termina la ejecuci�n del script.
 ys_EP_SessionExist($ys_SES_SessionID);

 //--- Grabar Log.
 ys_EP_Log($ys_evento="", $ys_ep=$ys_url, $ys_contenido="Pas� validaci�n de existencia de SessionID");

 //--- Se ha encontrado la SessionID. Cargar Login del usuario.
 $ys_USU_Login   = ys_searchdata($ys_tabla=TB_SESIONES, $ys_condicion="SES_SessionID='".$ys_SES_SessionID."'", $ys_campo="SES_UserLogin", $ys_0Campo_1Contar_2AutoNum_3Max = YSEARCH_CAMPO);

 //--- Verificar que el usuario exista y que est� activo.
 //--- Si no existe, la funci�n muestra el error y termina la ejecuci�n del script.
 ys_EP_UserExist($ys_USU_Login);

 //--- Grabar Log.
 ys_EP_Log($ys_evento="", $ys_ep=$ys_url, $ys_contenido="Pas� validaci�n de existencia de usuario.");

 //--- El usuario existe. Devolver respuesta con balance y salir.
 $ys_USU_Balance = ys_searchdata($ys_tabla=TB_USUARIOS, $ys_condicion="USU_Login='".$ys_USU_Login."'", $ys_campo="USU_Balance", $ys_0Campo_1Contar_2AutoNum_3Max = YSEARCH_CAMPO);
 $ys_USU_Balance = round($ys_USU_Balance, 2);    //--- Obliga a devolver dos cifras.

 //--- Grabar Log.
 ys_EP_Log($ys_evento="", $ys_ep=$ys_url, $ys_contenido="Pas� consulta de balance para devolver respuesta.");

 //--- Devolver respuesta satisfactoria y salir.
 $ys_retarr = [
              'status'  => 1,
              'balance' => $ys_USU_Balance,
              'code'    => '',
              'message' => 'No error.'
              ];
 ys_EP_Response($ys_retarr);
 }

//--- --------------------------------------------------
//--- INSERTBET - Agregar apuesta.
//--- --------------------------------------------------
if(strcmp($ys_url, "insertBet")==0)
 {
 $ys_bo_validar = true; //--- Control de validaci�n de par�metros.
 unset($ys_retarr);

 //--- Variables de campos.
 $ys_TRA_Codigo       = "";  //--- ENDPOINT trxid     : string
 $ys_TRA_Movim        = "";  //--- ENDPOINT movement  : string (Siempre es BET)
 $ys_TRA_Monto        = 0 ;  //--- ENDPOINT amount    : float (Siempre debe ser negativo)
 $ys_SES_SessionID    = "";  //--- ENDPOINT sessionid : string
 $ys_TRA_CodGame      = "";  //--- ENDPOINT gameid    : string
 $ys_TRA_NomGame      = "";  //--- ENDPOINT game_name : string
 $ys_TRA_Custom       = "";  //--- ENDPOINT custom    : string
 $ys_TRA_UserLogin    = "";  //--- Login del usuario.
 $ys_USU_Balance      = 0 ;  //--- Balance del usuario.

 //--- Recibir par�metros en JSON y decodificar en forma de Array.
 $ys_post = json_decode(file_get_contents("php://input"), true);

 //--- Grabar Log.
 ys_EP_Log($ys_evento="-", $ys_ep=$ys_url, $ys_contenido="");
 ys_EP_Log($ys_evento="", $ys_ep=$ys_url, $ys_contenido="Recibi� ys_post.");
 ys_EP_Log($ys_evento="ep", $ys_ep=$ys_url, $ys_contenido=$ys_post);

 //--- Verificar cantidad de par�metros.
 $ys_bo_validar = $ys_bo_validar && (count($ys_post) == 6 || count($ys_post) == 7); //--- Se recibieron seis (06) � siete (07) par�metros.

 //--- Verificar que existan todos los par�metros con sus nombres correctos.
 $ys_bo_validar = $ys_bo_validar && array_key_exists("trxid"        , $ys_post);    //--- Se recibi� el par�metro esperado.
 $ys_bo_validar = $ys_bo_validar && array_key_exists("movement"     , $ys_post);    //--- Se recibi� el par�metro esperado.
 $ys_bo_validar = $ys_bo_validar && array_key_exists("amount"       , $ys_post);    //--- Se recibi� el par�metro esperado.
 $ys_bo_validar = $ys_bo_validar && array_key_exists("sessionid"    , $ys_post);    //--- Se recibi� el par�metro esperado.
 $ys_bo_validar = $ys_bo_validar && array_key_exists("gameid"       , $ys_post);    //--- Se recibi� el par�metro esperado.
 $ys_bo_validar = $ys_bo_validar && array_key_exists("game_name"    , $ys_post);    //--- Se recibi� el par�metro esperado.
 if(count($ys_post)==7)
  {
  //--- Si se recibieron siete par�metros, el s�ptimo debe ser "custom".
  $ys_bo_validar = $ys_bo_validar && array_key_exists("custom"       , $ys_post);   //--- Se recibi� el par�metro esperado.
  }

 //--- Verificar los tipos de par�metro.
 $ys_bo_validar = $ys_bo_validar && is_string($ys_post["trxid"]);         //--- El par�metro es tipo String.
 $ys_bo_validar = $ys_bo_validar && is_string($ys_post["movement"]);      //--- El par�metro es tipo String.
 $ys_bo_validar = $ys_bo_validar && ($ys_post["movement"]=="BET");        //--- El par�metro "movement" debe ser "BET".
 $ys_bo_validar = $ys_bo_validar && is_numeric($ys_post["amount"]);       //--- El par�metro es tipo Num�rico.
 $ys_bo_validar = $ys_bo_validar && is_string($ys_post["sessionid"]);     //--- El par�metro es tipo String.
 $ys_bo_validar = $ys_bo_validar && is_string($ys_post["gameid"]);        //--- El par�metro es tipo String.
 $ys_bo_validar = $ys_bo_validar && is_string($ys_post["game_name"]);     //--- El par�metro es tipo String.
 if(count($ys_post)==7)
  {
  //--- Si se recibieron siete par�metros, "custom" debe ser tipo String.
  $ys_bo_validar = $ys_bo_validar && is_string($ys_post["custom"]);       //--- El par�metro es tipo String.
  }

 //--- Verificar el tama�o de los par�metros medibles.
 $ys_bo_validar = $ys_bo_validar && (strlen($ys_post["sessionid"]) == 128);    //--- La cadena es de 128 bytes de longitud.

 //--- Grabar Log.
 ys_EP_Log($ys_evento="", $ys_ep=$ys_url, $ys_contenido="Pas� validaci�n de recepci�n de par�metros.");

 //--- Si hubo error en las validaciones devolver error.
 if(!$ys_bo_validar)
  {
  $ys_retarr = [
               'status'  => 0,
               'balance' => 0,
               'code'    => 'INTERNAL_ERROR',
               'message' => 'Bad parameters.'
               ];
  ys_EP_Response($ys_retarr);
  }

 //--- Verificar que exista el SessionID.
 //--- Si no existe, la funci�n muestra el error y termina la ejecuci�n del script.
 ys_EP_SessionExist($ys_post["sessionid"]);

 //--- Grabar Log.
 ys_EP_Log($ys_evento="", $ys_ep=$ys_url, $ys_contenido="Pas� verificaci�n de existencia de SessionID.");

 //--- Verificar que el trxid no est� duplicado.
 //--- Si el TRXID ya existe, devolver error y salir.
 if(ys_searchdata($ys_tabla=TB_TRANSACT, $ys_condicion="TRA_Codigo='".$ys_post["trxid"]."'", $ys_campo="", $ys_0Campo_1Contar_2AutoNum_3Max=YSEARCH_CONTAR))
  {
  $ys_retarr = [
               'status'  => 0,
               'balance' => 0,
               'code'    => 'TRX_DUPLICATED',
               'message' => 'Duplicated TRXID.'
               ];
  ys_EP_Response($ys_retarr);
  }

 //--- Grabar Log.
 ys_EP_Log($ys_evento="", $ys_ep=$ys_url, $ys_contenido="Pas� verificaci�n de TRXID duplicado.");

 //--- Si llega hasta aqu� es porque la solicitud es correcta. Se toman los par�metros recibidos.
 $ys_TRA_Codigo       = $ys_post["trxid"];            //--- trxid     : string
 $ys_TRA_Movim        = $ys_post["movement"];         //--- movement  : string (Siempre es BET)
 $ys_TRA_Monto        = $ys_post["amount"];           //--- amount    : float (Debe ser negativo. Si no lo es, se transforma en negativo.)
 $ys_TRA_Monto        = (abs($ys_TRA_Monto)*(-1));    //--- Convertir el monto en negativo.
 $ys_TRA_Monto        = round($ys_TRA_Monto, 2);      //--- Debe tener obligatoriamente dos decimales.
 $ys_SES_SessionID    = $ys_post["sessionid"];        //--- sessionid : string
 $ys_TRA_CodGame      = $ys_post["gameid"];           //--- gameid    : string
 $ys_TRA_NomGame      = $ys_post["game_name"];        //--- game_name : string
 if(count($ys_post)==7)
  {
  //--- Si se recibieron siete par�metros, tomar el valor de "custom".
  $ys_TRA_Custom       = $ys_post["custom"];          //--- custom    : string
  }

 //--- Buscar Login del usuario.
 $ys_TRA_UserLogin   = ys_searchdata($ys_tabla=TB_SESIONES, $ys_condicion="SES_SessionID='".$ys_SES_SessionID."'", $ys_campo="SES_UserLogin", $ys_0Campo_1Contar_2AutoNum_3Max = YSEARCH_CAMPO);

 //--- Grabar Log.
 ys_EP_Log($ys_evento="", $ys_ep=$ys_url, $ys_contenido="Pas� b�squeda de Login de usuario.");

 //--- Verificar que el usuario exista y que est� activo.
 //--- Si no existe, la funci�n muestra el error y termina la ejecuci�n del script.
 ys_EP_UserExist($ys_TRA_UserLogin);

 //--- Grabar Log.
 ys_EP_Log($ys_evento="", $ys_ep=$ys_url, $ys_contenido="Pas� verificaci�n de existencia de usuario.");

 //--- Lee el balance actual del usuario para limitar la apuesta.
 $ys_USU_Balance = ys_searchdata($ys_tabla=TB_USUARIOS, $ys_condicion="USU_Login='".$ys_TRA_UserLogin."'", $ys_campo="USU_Balance", $ys_0Campo_1Contar_2AutoNum_3Max = YSEARCH_CAMPO);
 if(($ys_USU_Balance + $ys_TRA_Monto) < 0)
  {
  //--- No hay saldo suficiente para hacer esta apuesta.
  //--- Mostrar mensaje de error y salir.
  $ys_retarr = [
               'status'  => 0,
               'balance' => 0,
               'code'    => 'INSUFFICIENT_FUNDS',
               'message' => 'Not enough balance for BET.'
               ];
  ys_EP_Response($ys_retarr);
  }

 //--- Grabar Log.
 ys_EP_Log($ys_evento="", $ys_ep=$ys_url, $ys_contenido="Pas� la verificaci�n de que apuesta es menor que balance.");

 //--- Actualizar balance del usuario.
 $SQL  = "";
 $SQL .= "UPDATE ";
 $SQL .= " ".TB_USUARIOS;
 $SQL .= " SET";
 $SQL .= " USU_Balance = (USU_Balance + ".$ys_TRA_Monto.")";
 $SQL .= " WHERE";
 $SQL .= " USU_Login = '".$ys_TRA_UserLogin."'";
 YQuery($SQL);

 //--- Grabar Log.
 ys_EP_Log($ys_evento="", $ys_ep=$ys_url, $ys_contenido="Actualiz� balance de usuario.");

 //--- Grabar transacci�n
 $SQL  = "";
 $SQL .= "INSERT INTO";
 $SQL .= " ".TB_TRANSACT;
 $SQL .= " VALUES";
 $SQL .= " (";
 $SQL .= "0"                        .",";
 $SQL .= " '".$ys_TRA_Codigo    ."'".",";
 $SQL .= " '".$ys_TRA_Movim     ."'".",";
 $SQL .= " " .$ys_TRA_Monto         .",";
 $SQL .= " '".$ys_TRA_CodGame   ."'".",";
 $SQL .= " '".$ys_TRA_NomGame   ."'".",";
 $SQL .= " '".$ys_TRA_Custom    ."'".",";
 $SQL .= " '".""                ."'".",";
 $SQL .= " '".YGetDate()        ."'".",";
 $SQL .= " '".YGetTime()        ."'".",";
 $SQL .= " '".$ys_TRA_UserLogin ."'"    ;
 $SQL .= ")";
 YQuery($SQL);

 //--- Grabar Log.
 ys_EP_Log($ys_evento="", $ys_ep=$ys_url, $ys_contenido="Grab� transacci�n.");

 //--- Obtener balance actual del usuario.
 $ys_USU_Balance = ys_searchdata($ys_tabla=TB_USUARIOS, $ys_condicion="USU_Login='".$ys_TRA_UserLogin."'", $ys_campo="USU_Balance", $ys_0Campo_1Contar_2AutoNum_3Max = YSEARCH_CAMPO);
 $ys_USU_Balance = round($ys_USU_Balance, 2);    //--- Debe tener obligatoriamente dos decimales.

 //--- Grabar Log.
 ys_EP_Log($ys_evento="", $ys_ep=$ys_url, $ys_contenido="Consult� balance de usuario para devolver respuesta.");

 //--- Devolver respuesta satisfactoria y salir.
 $ys_retarr = [
              'status'  => 1,
              'balance' => $ys_USU_Balance,
              'code'    => '',
              'message' => 'No error.'
              ];
 ys_EP_Response($ys_retarr);
 }

//--- --------------------------------------------------
//--- BETRESULT - Resultado de la apuesta. S�lo se recibe cuando hay REFUND o WIN.
//--- --------------------------------------------------
if(strcmp($ys_url, "betResult")==0)
 {
 $ys_bo_validar = true; //--- Control de validaci�n de par�metros.
 $ys_refund_BW  = "";   //--- Movimiento del REFUND. Puede ser BET o WIN.
 unset($ys_retarr);

 //--- Variables de campos.
 $ys_TRA_Codigo       = "";  //--- ENDPOINT trxid        : string
 $ys_TRA_Movim        = "";  //--- ENDPOINT movement     : string (REFUND / WIN)
 $ys_TRA_Monto        = 0 ;  //--- ENDPOINT amount       : float (Siempre debe ser negativo)
 $ys_SES_SessionID    = "";  //--- ENDPOINT sessionid    : string
 $ys_TRA_CodGame      = "";  //--- ENDPOINT gameid       : string
 $ys_TRA_NomGame      = "";  //--- ENDPOINT game_name    : string
 $ys_TRA_Custom       = "";  //--- ENDPOINT custom       : string
 $ys_TRA_ReferenceBet = "";  //--- ENDPOINT referenceBet : string
 $ys_USU_Balance      = 0 ;  //--- Balance del usuario.

 //--- Recibir par�metros en JSON y decodificar en forma de Array.
 $ys_post = json_decode(file_get_contents("php://input"), true);

 //--- Grabar Log.
 ys_EP_Log($ys_evento="-", $ys_ep=$ys_url, $ys_contenido="");
 ys_EP_Log($ys_evento="", $ys_ep=$ys_url, $ys_contenido="Recibi� ys_post.");
 ys_EP_Log($ys_evento="ep", $ys_ep=$ys_url, $ys_contenido=$ys_post);

 //--- Verificar cantidad de par�metros.
 $ys_bo_validar = $ys_bo_validar && (count($ys_post) == 7 || count($ys_post) == 8); //--- Se recibieron siete (07) u ocho (08) par�metros.

 //--- Verificar que existan todos los par�metros con sus nombres correctos.
 $ys_bo_validar = $ys_bo_validar && array_key_exists("trxid"        , $ys_post);    //--- Se recibi� el par�metro esperado.
 $ys_bo_validar = $ys_bo_validar && array_key_exists("movement"     , $ys_post);    //--- Se recibi� el par�metro esperado.
 $ys_bo_validar = $ys_bo_validar && array_key_exists("amount"       , $ys_post);    //--- Se recibi� el par�metro esperado.
 $ys_bo_validar = $ys_bo_validar && array_key_exists("sessionid"    , $ys_post);    //--- Se recibi� el par�metro esperado.
 $ys_bo_validar = $ys_bo_validar && array_key_exists("gameid"       , $ys_post);    //--- Se recibi� el par�metro esperado.
 $ys_bo_validar = $ys_bo_validar && array_key_exists("game_name"    , $ys_post);    //--- Se recibi� el par�metro esperado.
 if(count($ys_post)==8)
  {
  //--- Si se recibieron ocho (08) par�metros, debe existir "custom".
  $ys_bo_validar = $ys_bo_validar && array_key_exists("custom"       , $ys_post);   //--- Se recibi� el par�metro esperado.
  }
 $ys_bo_validar = $ys_bo_validar && array_key_exists("referenceBet" , $ys_post);    //--- Se recibi� el par�metro esperado.

 //--- Verificar los tipos de par�metro.
 $ys_bo_validar = $ys_bo_validar && is_string($ys_post["trxid"]);         //--- El par�metro es tipo String.
 $ys_bo_validar = $ys_bo_validar && is_string($ys_post["movement"]);      //--- El par�metro es tipo String.
 $ys_bo_validar = $ys_bo_validar && ($ys_post["movement"]=="REFUND" ||
                                     $ys_post["movement"]=="WIN");        //--- El par�metro "movement" debe ser "REFUND" o "WIN".
 $ys_bo_validar = $ys_bo_validar && is_numeric($ys_post["amount"]);       //--- El par�metro es tipo Num�rico.
 $ys_bo_validar = $ys_bo_validar && is_string($ys_post["sessionid"]);     //--- El par�metro es tipo String.
 $ys_bo_validar = $ys_bo_validar && is_string($ys_post["gameid"]);        //--- El par�metro es tipo String.
 $ys_bo_validar = $ys_bo_validar && is_string($ys_post["game_name"]);     //--- El par�metro es tipo String.
 if(count($ys_post)==8)
  {
  //--- Si se recibieron ocho (08) par�metros, "custom" debe ser tipo String.
  $ys_bo_validar = $ys_bo_validar && is_string($ys_post["custom"]);       //--- El par�metro es tipo String.
  }
 $ys_bo_validar = $ys_bo_validar && is_string($ys_post["referenceBet"]);  //--- El par�metro es tipo String.

 //--- Verificar el tama�o de los par�metros medibles.
 $ys_bo_validar = $ys_bo_validar && (strlen($ys_post["sessionid"]) == 128);    //--- La cadena es de 128 bytes de longitud.

 //--- Grabar Log.
 ys_EP_Log($ys_evento="", $ys_ep=$ys_url, $ys_contenido="Pas� validaci�n de recepci�n de par�metros.");

 //--- Si hubo error en las validaciones devolver error.
 if(!$ys_bo_validar)
  {
  $ys_retarr = [
               'status'  => 0,
               'balance' => 0,
               'code'    => 'INTERNAL_ERROR',
               'message' => 'Bad parameters.'
               ];
  ys_EP_Response($ys_retarr);
  }

 //--- Verificar que exista el SessionID.
 //--- Si no existe, la funci�n muestra el error y termina la ejecuci�n del script.
 ys_EP_SessionExist($ys_post["sessionid"]);

 //--- Grabar Log.
 ys_EP_Log($ys_evento="", $ys_ep=$ys_url, $ys_contenido="Pas� verificaci�n de existencia de SessionID.");

 //--- Verificar que el trxid no est� duplicado.
 //--- Si el TRXID ya existe, devolver error y salir.
 if(ys_searchdata($ys_tabla=TB_TRANSACT, $ys_condicion="TRA_Codigo='".$ys_post["trxid"]."'", $ys_campo="", $ys_0Campo_1Contar_2AutoNum_3Max=YSEARCH_CONTAR))
  {
  $ys_retarr = [
               'status'  => 0,
               'balance' => 0,
               'code'    => 'TRX_DUPLICATED',
               'message' => 'Duplicated TRXID.'
               ];
  ys_EP_Response($ys_retarr);
  }

 //--- Grabar Log.
 ys_EP_Log($ys_evento="", $ys_ep=$ys_url, $ys_contenido="Pas� verificaci�n de TRXID duplicado.");

 //--- Si llega hasta aqu� es porque la solicitud es correcta. Se toman los par�metros recibidos.
 $ys_TRA_Codigo       = $ys_post["trxid"];            //--- trxid        : string
 $ys_TRA_Movim        = $ys_post["movement"];         //--- movement     : string (REFUND / WIN)
 $ys_TRA_Monto        = $ys_post["amount"];           //--- amount       : float

 $ys_TRA_Monto        = round($ys_TRA_Monto, 2);      //--- Debe tener obligatoriamente dos decimales.
 $ys_SES_SessionID    = $ys_post["sessionid"];        //--- sessionid    : string
 $ys_TRA_CodGame      = $ys_post["gameid"];           //--- gameid       : string
 $ys_TRA_NomGame      = $ys_post["game_name"];        //--- game_name    : string
 if(count($ys_post)==8)
  {
  //--- Si se recibieron ocho (08) par�metros, tomar el valor de "custom".
  $ys_TRA_Custom       = $ys_post["custom"];          //--- custom       : string
  }
 $ys_TRA_ReferenceBet = $ys_post["referenceBet"];     //--- referenceBet : string

 //--- Buscar Login del usuario.
 $ys_TRA_UserLogin   = ys_searchdata($ys_tabla=TB_SESIONES, $ys_condicion="SES_SessionID='".$ys_SES_SessionID."'", $ys_campo="SES_UserLogin", $ys_0Campo_1Contar_2AutoNum_3Max = YSEARCH_CAMPO);

 //--- Grabar Log.
 ys_EP_Log($ys_evento="", $ys_ep=$ys_url, $ys_contenido="Pas� b�squeda de Login de usuario.");

 //--- Verificar que el usuario exista y que est� activo.
 //--- Si no existe, la funci�n muestra el error y termina la ejecuci�n del script.
 ys_EP_UserExist($ys_TRA_UserLogin);

 //--- Grabar Log.
 ys_EP_Log($ys_evento="", $ys_ep=$ys_url, $ys_contenido="Pas� buscar verificaci�n de existencia de usuario.");

 //--- Verificar que exista el ReferenceBet recibido.
 if(!ys_searchdata($ys_tabla=TB_TRANSACT, $ys_condicion="TRA_Codigo='".$ys_TRA_ReferenceBet."'", $ys_campo="", $ys_0Campo_1Contar_2AutoNum_3Max = YSEARCH_CONTAR))
  {
  //--- No se encontr� el ReferenceBet previo. Devolver error y salir.
  $ys_retarr = [
               'status'  => 0,
               'balance' => 0,
               'code'    => 'BETREFERENCE_NOT_FOUND',
               'message' => 'BET Reference not found.'
               ];
  ys_EP_Response($ys_retarr);
  }

 //--- Grabar Log.
 ys_EP_Log($ys_evento="", $ys_ep=$ys_url, $ys_contenido="Verific� que exista el Reference Bet.");

 //--- Procesar movimientos
 if($ys_TRA_Movim=="WIN")
  {
  //--- Si es WIN, asegurarse de que el monto sea POSITIVO.
  $ys_TRA_Monto = abs($ys_TRA_Monto);

  //--- Grabar Log.
  ys_EP_Log($ys_evento="", $ys_ep=$ys_url, $ys_contenido="Proces� WIN.");
  }

 //--- Si el movimiento es REFUND, buscar el TRA_Movim del ReferenceBet recibido.
 if($ys_TRA_Movim=="REFUND")
  {
  $ys_refund_BW = ys_searchdata($ys_tabla=TB_TRANSACT, $ys_condicion="TRA_Codigo='".$ys_refund."'", $ys_campo="TRA_Movim", $ys_0Campo_1Contar_2AutoNum_3Max = YSEARCH_CAMPO);

  //--- Si el movimiento es BET, se hace REEMBOLSO al usuario.
  //--- El monto debe ser POSITIVO.
  if($ys_refund_BW=="BET")
   {$ys_TRA_Monto = abs($ys_TRA_Monto);}

  //--- Si el movimiento es WIN, se debe descontar el monto del usuario.
  //--- El monto debe ser NEGATIVO.
  if($ys_refund_BW=="WIN")
   {
   $ys_TRA_Monto = abs($ys_TRA_Monto);
   $ys_TRA_Monto = (abs($ys_TRA_Monto)*(-1));    //--- Convertir el monto en negativo.
   }

  $ys_TRA_Monto = round($ys_TRA_Monto, 2);       //--- Debe tener obligatoriamente dos decimales.

  //--- Grabar Log.
  ys_EP_Log($ys_evento="", $ys_ep=$ys_url, $ys_contenido="Proces� REFUND.");
  }

 //--- Actualizar monto para el usuario de acuerdo al movimiento previo.
 $SQL  = "";
 $SQL .= "UPDATE ";
 $SQL .= " ".TB_USUARIOS;
 $SQL .= " SET";
 $SQL .= " USU_Balance = (USU_Balance + ".$ys_TRA_Monto.")";
 $SQL .= " WHERE";
 $SQL .= " USU_Login = '".$ys_TRA_UserLogin."'";
 YQuery($SQL);

 //--- Grabar Log.
 ys_EP_Log($ys_evento="", $ys_ep=$ys_url, $ys_contenido="Actualiz� datos del usuario.");

 //--- Grabar transacci�n
 $SQL  = "";
 $SQL .= "INSERT INTO";
 $SQL .= " ".TB_TRANSACT;
 $SQL .= " VALUES";
 $SQL .= " (";
 $SQL .= "0"                          .",";
 $SQL .= " '".$ys_TRA_Codigo       ."'".",";
 $SQL .= " '".$ys_TRA_Movim        ."'".",";
 $SQL .= " " .$ys_TRA_Monto            .",";
 $SQL .= " '".$ys_TRA_CodGame      ."'".",";
 $SQL .= " '".$ys_TRA_NomGame      ."'".",";
 $SQL .= " '".$ys_TRA_Custom       ."'".",";
 $SQL .= " '".$ys_TRA_ReferenceBet ."'".",";
 $SQL .= " '".YGetDate()           ."'".",";
 $SQL .= " '".YGetTime()           ."'".",";
 $SQL .= " '".$ys_TRA_UserLogin    ."'"    ;
 $SQL .= ")";
 YQuery($SQL);

 //--- Grabar Log.
 ys_EP_Log($ys_evento="", $ys_ep=$ys_url, $ys_contenido="Grab� transacci�n.");

 //--- Obtener balance actual del usuario.
 $ys_USU_Balance = ys_searchdata($ys_tabla=TB_USUARIOS, $ys_condicion="USU_Login='".$ys_TRA_UserLogin."'", $ys_campo="USU_Balance", $ys_0Campo_1Contar_2AutoNum_3Max = YSEARCH_CAMPO);
 $ys_USU_Balance = round($ys_USU_Balance, 2);    //--- Debe tener obligatoriamente dos decimales.

 //--- Grabar Log.
 ys_EP_Log($ys_evento="", $ys_ep=$ys_url, $ys_contenido="Consult� balance de usuario para devolver respuesta.");

 //--- Devolver respuesta satisfactoria y salir.
 $ys_retarr = [
              'status'  => 1,
              'balance' => $ys_USU_Balance,
              'code'    => '',
              'message' => 'No error.'
              ];
 ys_EP_Response($ys_retarr);
 }

//--- --------------------------------------------------
//---        VERIFICACION DE EXISTENCIA DE SESION
//--- --------------------------------------------------
function ys_EP_SessionExist($ys_sessionid)
 {
 //--- Funci�n de EndPoint.
 //--- Verificar que el SessionID exista y que est� activo.
 if(!ys_searchdata($ys_tabla=TB_SESIONES,
                   $ys_condicion="SES_SessionID='".$ys_sessionid."'",
                   $ys_campo="",
                   $ys_0Campo_1Contar_2AutoNum_3Max = YSEARCH_CONTAR))
  {
  //--- Si no existe la SessionID, devolver error y salir.
  $ys_retarr = [
               'status'  => 0,
               'balance' => 0,
               'code'    => 'SESSION_NOT_FOUND',
               'message' => 'SessionID value not found.'
               ];
  ys_EP_Response($ys_retarr);
  }
 }

//--- --------------------------------------------------
//---        VERIFICACION DE EXISTENCIA DE USUARIO
//--- --------------------------------------------------
function ys_EP_UserExist($ys_usu_login)
 {
 //--- Funci�n de EndPoint.
 //--- Verificar que el usuario exista y que est� activo.
 if(!ys_searchdata($ys_tabla=TB_USUARIOS,
                   $ys_condicion="USU_Login='".$ys_usu_login."' AND USU_Activo",
                   $ys_campo="",
                   $ys_0Campo_1Contar_2AutoNum_3Max = YSEARCH_CONTAR))
  {
  //--- Si no existe el usuario, devolver error y salir.
  $ys_retarr = [
               'status'  => 0,
               'balance' => 0,
               'code'    => 'USER_NOT_FOUND',
               'message' => 'User not found.'
               ];
  ys_EP_Response($ys_retarr);
  }
 }

//--- --------------------------------------------------
//--- LOG DE LLAMADAS A LOS END POINTS
//--- --------------------------------------------------
function ys_EP_Log($ys_evento="ep", $ys_ep="", $ys_contenido="")
 {
 $ys_string = "";

 $ys_string .= "[".YGetDate()."] - ";
 $ys_string .= "[".YGetTime()."] - ";
 $ys_string .= "[".$ys_ep."] - [";

 if($ys_evento=="ep")
  {
  //--- El Log es para un EndPoint.
  if($ys_ep=="getUserBalance")
   {
   $ys_string .= "SessionID: ".substr($ys_contenido["sessionid"], 0, 13)  ."...]";
   }

  if($ys_ep=="insertBet")
   {
   $ys_string .= $ys_contenido["trxid"]                     ."] - [";
   $ys_string .= $ys_contenido["movement"]                  ."] - [";
   $ys_string .= $ys_contenido["amount"]                    ."] - [";
   $ys_string .= substr($ys_contenido["sessionid"], 0, 13)  ."] - [";
   $ys_string .= $ys_contenido["gameid"]                    ."] - [";
   $ys_string .= $ys_contenido["game_name"]                 ."]";
   }

  if($ys_ep=="betResult")
   {
   $ys_string .= $ys_contenido["trxid"]                     ."] - [";
   $ys_string .= $ys_contenido["movement"]                  ."] - [";
   $ys_string .= $ys_contenido["amount"]                    ."] - [";
   $ys_string .= substr($ys_contenido["sessionid"], 0, 13)  ."] - [";
   $ys_string .= $ys_contenido["gameid"]                    ."] - [";
   $ys_string .= $ys_contenido["game_name"]                 ."] - [";
   $ys_string .= $ys_contenido["custom"]                    ."] - [";
   $ys_string .= $ys_contenido["referenceBet"]              ."]";
   }

  if($ys_ep=="gamePager")
   {
   //$ys_string .= $ys_contenido["cat"]                       ."] - [";
   //$ys_string .= $ys_contenido["type"]                      ."] - [";
   //$ys_string .= $ys_contenido["device"]                    ."]";
   }
  }

 if($ys_evento=="")
  {
  //--- El Log es para un evento de los procedimientos.
  $ys_string .= $ys_contenido."]";
  }

 if($ys_evento=="-")
  {
  //--- L�nea de divisi�n para identificar Logs.
  $ys_string .= $ys_contenido."--------------------------------------------------]";
  }

 $ys_file = fopen("__log.y", "a");
 fwrite($ys_file, $ys_string.PHP_EOL);
 fclose($ys_file);
 }

//--- --------------------------------------------------
//--- RESPUESTA Y SALIDA DE LOS ENDPOINTS
//--- --------------------------------------------------
function ys_EP_Response(&$ys_retarr, $ys_exit=true)
 {
 //--- La solicitud es correcta.
 header('Content-Type: application/json; charset=windows-1252;');
 header('Cache-Control: no-cache, must-revalidate');
 http_response_code(200); //--- 200 Ok.
 echo json_encode($ys_retarr, true);
 if($ys_exit) {exit();}
 }


//--- --------------------------------------------------
//--- gamePager - Obtener datos de juegos para paginaci�n.
//--- --------------------------------------------------
if(strcmp($ys_url, "gamePager")==0)
 {
 $ys_bo_validar = true;
 $tK1 = 0;
 unset($ys_retarr);
 
  //--- Variables de campos de respuesta.
 //--- >> {
 //        estado
 //        limite
 //        registros
 //        paginas
 //        juegos
 //--- >>   [999999
 //--- >>    {
 //           id       : (ST) Identificador del juego.
 //           name     : (ST) Nombre del juego.
 //           category : (ST) C�digo de la Categor�a a la que pertenece el juego.
 //           brand    : (ST) Marca (Brand)
 //           device   : (ST) mb: M�vil / wb: Web
 //           control  : (ST) ?????
 //           image    : (ST) Ruta de la imagen.
 //           favtot   : (IN) Total de Favs recibidos (Ser� negativo cuando no se sumen valores a los Favs).
 //           favest   : (ST) Estado actual del Fav: fas: Activo / far: Inactivo.
 //--- >>    }
 //--- >>   ]

 $ys_GAM_Codigo  = "";  //--- ENDPOINT C�digo de juego     : (ST) 100
 $ys_GAM_Nombre  = "";  //--- ENDPOINT Nombre del juego    : (ST) 100
 $ys_GAM_CodCat  = "";  //--- ENDPOINT C�digo de Categor�a : (ST) 100
 $ys_GAM_CodBra  = "";  //--- ENDPOINT C�digo de Brand     : (ST) 100
 $ys_GAM_Modo    = "";  //--- ENDPOINT Dispositivo         : (ST) 100
 $ys_GAM_Control = "";  //--- ENDPOINT game_name    : string
 $ys_GAM_Imagen  = "";  //--- ENDPOINT game_name    : string

 //--- Recibir par�metros en JSON y decodificar en forma de Array.
 $ys_Cat    = "";
 $ys_Type   = "";
 $ys_Device = "";
 if(isset($_POST["cat"]))    {$ys_Cat = $_POST["cat"];}
 if(isset($_POST["type"]))   {$ys_Type = $_POST["type"];}
 if(isset($_POST["device"])) {$ys_Device = $_POST["device"];}

 //--- Grabar Log.
 ys_EP_Log($ys_evento="-", $ys_ep=$ys_url, $ys_contenido="");
 ys_EP_Log($ys_evento="", $ys_ep=$ys_url, $ys_contenido="Recibi� ys_post.");

 //--- Grabar Log.
 ys_EP_Log($ys_evento="", $ys_ep=$ys_url, $ys_contenido="Pas� validaci�n de recepci�n de par�metros.");

 //--- Si hubo error en las validaciones devolver error.
 if(!$ys_bo_validar)
  {
  $ys_retarr = [
               'estado'     => 0,
               'limite'     => 0,
               'registros'  => 0,
               'paginas'    => 0,
               'juegos'     => "Error de validaci�n."
               ];
  ys_EP_Response($ys_retarr);
  }

 //--- Si llega hasta aqu� es porque la solicitud es correcta. Se toma el par�metro recibido.

 //--- Grabar Log.
 ys_EP_Log($ys_evento="", $ys_ep=$ys_url, $ys_contenido="Pas� a devolver respuesta.");

 //--- Cargar llamada a SQL.
 $SQL  = "";
 $SQL .= "SELECT";
 $SQL .= " GAM_Codigo, GAM_Nombre, GAM_CodCat, GAM_CodBra, GAM_Control, GAM_Modo, GAM_Imagen, GAM_Favs";
 $SQL .= " FROM";
 $SQL .= " ".TB_GAMES;
 $SQL .= " WHERE";
 $SQL .= " GAM_CodCat='".$ys_Cat."'";
 if($ys_Type!="")
  {
  $SQL .= " AND";
  if($ys_Type=="POPU")
   {$SQL .= " GAM_Pop";}
  elseif($ys_Type=="NUEV")
   {$SQL .= " GAM_Nue";}
  else
   {$SQL .= " GAM_Tipo='".$ys_Type."'";}
  }
 $SQL .= " AND";
 $SQL .= " GAM_Modo='".$ys_Device."'";
 $SQL .= " AND";
 $SQL .= " GAM_Activo";
 $SQL .= " AND";
 $SQL .= " GAM_EnAPI";
 $SQL .= " ORDER BY";
 $SQL .= " GAM_Nombre ASC";
 $ys_rs = YQuery($ys_query=$SQL, $ys_rows=1);

 $tK1 = $ys_rs; //--- Toma temporalmente la cantidad de registros afectados para enviarlos en ys_retarr.

 //--- Leer los registros.
 if($ys_rs)
  {
  $ys_rs= YQuery($SQL, 0);
  //--- Cargar informaci�n de control.
  $ys_retarr["estado"]    = 1;
  $ys_retarr["limite"]    = intval(ys_getconfig($ys_seccion="juegos", $ys_codigo="paginado", $ys_tipo="N"));
  $ys_retarr["registros"] = intval($tK1);
  $ys_retarr["paginas"]   = ceil($ys_retarr["registros"] / $ys_retarr["limite"]);
  while ($ys_file = mysqli_fetch_assoc($ys_rs))
   {
   //--- Determina si el Fav est� activo para este usuario.
   //--- 0: No existe el registro. Desactivado. (far)
   //--- 1: El usuario marc� Fav. Activado. (fas)
   //--- 2: El usuario desmarc� Fav. Desactivado. (far)
   //--- ys_favest: Estado actual del indicador de Favs.
   
   $ys_favest = ys_searchdata($ys_tabla=TB_USUCF, $ys_condicion="UCF_UsuLogin='".$_SESSION['ULOG']."' AND UCF_TipoCBG='G' AND UCF_CodCBG='".addslashes(trim($ys_file["GAM_Codigo"]))."'", $ys_campo="UCF_Favs", $ys_0Campo_1Contar_2AutoNum_3Max=YSEARCH_CAMPO);

   if($ys_favest=="")
    {
    //--- El usuario NO HA pulsado nunca Fav a este juego.
    $ys_favest=0;
    }

   $ys_favtot = intval($ys_file["GAM_Favs"]);
   if($ys_favest==1 || $ys_favest==2)
    {
    //--- El usuario SI HA pulsado Fav a este juego antes.
    //--- Convertir la cantidad de Favs en n�mero negativo para enviar una se�al a ys_js_Contadores().
    $ys_favtot = ($ys_favtot * -1);
    }

   //--- Establecer el valor del indicador de Favs.
   //--- Se enciende (fas) �nicamente cuando el valor del campo sea 1.
   if($ys_favest==0 || $ys_favest==2)
    {$ys_favest = "far";}
   else
    {$ys_favest = "fas";}

   $ys_retarr["juegos"][]= array('id' => $ys_file["GAM_Codigo"],
                                 'name' => $ys_file["GAM_Nombre"],
                                 'category' => $ys_file["GAM_CodCat"],
                                 'brand' => $ys_file["GAM_CodBra"],
                                 'device' => $ys_file["GAM_Modo"],
                                 'control' => $ys_file["GAM_Control"],
                                 'image' => $ys_file["GAM_Imagen"],
                                 'favtot' => $ys_favtot,
                                 'favest' => $ys_favest
                                 );
   }
  }
 else
  {
  //--- Si hubo error en las validaciones devolver error.
  $ys_retarr = [
               'estado'     => 0,
               'limite'     => 0,
               'registros'  => 0,
               'paginas'    => 0,
               'juegos'     => "Error de lectura de datos. - ".$SQL
               ];
  }
 //--- Devolver respuesta satisfactoria y salir.
 ys_EP_Response($ys_retarr);
 }


//--- --------------------------------------------------
//--- gameTypes - Devuelve los tipos de juego.
//--- --------------------------------------------------
if(strcmp($ys_url, "gameTypes")==0)
 {
 $tK1 = 0;
 unset($ys_retarr);

 //--- Variables de campos de respuesta.
 //--- >> {
 //        estado
 //        limite
 //        registros
 //        tipos
 //--- >>   [999999
 //--- >>    {
 //           ind      : (IN) Autonum�rico del registro (Indice).
 //           cod      : (ST) C�digo del Tipo.
 //           nom      : (ST) Nombre del Tipo.
 //           tot      : (IN) Total de juegos que tienen ese Tipo de juego.
 //--- >>    }
 //--- >>   ]

 $ys_GTY_AutoNum = "";  //--- ENDPOINT Indice del Tipo: (IN) 7
 $ys_GTY_Codigo  = "";  //--- ENDPOINT C�digo del Tipo: (ST) 5
 $ys_GTY_Desc    = "";  //--- ENDPOINT Nombre del Tipo: (ST) 30

 //--- Grabar Log.
 ys_EP_Log($ys_evento="-", $ys_ep=$ys_url, $ys_contenido="");
 ys_EP_Log($ys_evento="", $ys_ep=$ys_url, $ys_contenido="Recibi� ys_post.");

 //--- Grabar Log.
 ys_EP_Log($ys_evento="", $ys_ep=$ys_url, $ys_contenido="Pas� a devolver respuesta.");

 //--- Cargar llamada a SQL.
 $SQL  = "";
 $SQL .= "SELECT";
 $SQL .= " GTY_AutoNum, GTY_Codigo, GTY_Desc";
 $SQL .= " FROM";
 $SQL .= " ".TB_GTYPES;
 $SQL .= " WHERE";
 $SQL .= " GTY_Activo";
 $SQL .= " ORDER BY";
 $SQL .= " GTY_Pos ASC";
 $ys_rs = YQuery($SQL, 1);
 $tK1 = $ys_rs; //--- Toma temporalmente la cantidad de registros afectados para enviarlos en ys_retarr.

 //--- Leer los registros.
 if($ys_rs)
  {
  $ys_rs = YQuery($SQL, 0);
  //--- Cargar informaci�n de control.
  $ys_retarr["estado"]    = 1;
  $ys_retarr["registros"] = intval($tK1);
  while ($ys_file = mysqli_fetch_assoc($ys_rs))
   {
   $ys_GTY_AutoNum = $ys_file["GTY_AutoNum"];
   $ys_GTY_Codigo  = $ys_file["GTY_Codigo"];
   $ys_GTY_Desc    = $ys_file["GTY_Desc"];
   $tK1 = ys_searchdata(TB_GAMES, "GAM_Tipo='".$ys_GTY_Codigo."'", $ys_campo="", YSEARCH_CONTAR);
   $ys_retarr["tipos"][]= array('ind' => $ys_GTY_AutoNum,
                                'cod' => $ys_GTY_Codigo,
                                'nom' => $ys_GTY_Desc,
                                'tot' => $tK1
                                );
   }
  }
 else
  {
  //--- Si hubo error en las validaciones devolver error.
  $ys_retarr = [
               'estado'     => 0,
               'registros'  => 0,
               'tipos'      => ""
               ];
  }
 //--- Devolver respuesta satisfactoria y salir.
 ys_EP_Response($ys_retarr);
 }
?>
