<!--- <?php echo DEFAULT_NOM; ?> -->
<!--- HTML5 ENCODING STYLE META [CHARSET] -->
<meta                                      charset = "windows-1252">
<!--- HTML5 VIEWPORT META -->
<meta name       = "viewport"              content = "width=device-width, initial-scale=1.0">
<!--- HTTP EQUIVALENT METAS -->
<meta http-equiv = "Content-Type"          content = "text/html">
<meta http-equiv = "Content-Script-Type"   content = "text/html">
<meta http-equiv = "Content-Style-Type"    content = "text/css">
<meta http-equiv = "Cache-Control"         content = "no-cache, must-revalidate">
<meta http-equiv = "Pragma"                content = "no-cache">
<meta http-equiv = "imagetoolbar"          content = "no">
<meta http-equiv = "Window-Target"         content = "_top">
<!--- TOP METAS -->
<meta name       = "title"                 content = "<?php echo $_SESSION["EMP_COD"]; ?> - <?php echo $_SESSION["EMP_RAZ"]; ?>">
<meta http-equiv = "description"           content = "<?php echo $_SESSION["EMP_COD"]; ?> - <?php echo $_SESSION["EMP_RAZ"]; ?>">
<meta name       = "keywords"              content = "keywords">
<meta name       = "robots"                content = "all">
<meta name       = "robots"                content = "noydir">
<meta name       = "revisit-after"         content = "139 days">
<!--- ADDITIONAL METAS -->
<meta name       = "expires"               content = "tue, 13 Sep 2050">
<meta name       = "author"                content = "<?php echo DEFAULT_NOM; ?>">
<meta name       = "abstract"              content = "<?php echo $_SESSION["EMP_COD"]; ?> - <?php echo $_SESSION["EMP_RAZ"]; ?>">
<meta name       = "resource-type"         content = "document">
<meta name       = "generator"             content = "Microsoft FrontPage 3.0">
<meta name       = "ProgId"                content = "FrontPage.Editor.Document">
<meta name       = "contact"               content = "mailto: <?php echo ys_getconfig("datosemp", "EMP_Email", "C"); ?>">
<meta name       = "copyright"             content = "<?php echo $_SESSION["EMP_COD"]; ?> - <?php echo $_SESSION["EMP_RAZ"]; ?>">
<meta name       = "distribution"          content = "global">
<meta name       = "google"                content = "notranslate">
<meta name       = "googlebot"             content = "code">
<meta name       = "googlebot"             content = "noodp">
<meta name       = "googlebot"             content = "noarchive">
<meta name       = "googlebot"             content = "all">
<meta name       = "google"                content = "notranslate">
<meta name       = "language"              content = "Spanish">
<meta name       = "MSThemeCompatible"     content = "yes">
<meta name       = "keywords"              content = "keywords">
<meta name       = "no-email-collection"   content = "lu_nospam_tyc.php">
<meta name       = "rating"                content = "general">
<meta name       = "reply-to"              content = "mailto: <?php echo ys_getconfig("datosemp", "EMP_Email", "C"); ?>">
<meta name       = "slurp"                 content = "noydir">
<meta name       = "web_author"            content = "<?php echo DEFAULT_NOM; ?>">
<!--- MISCELLANEOUS METAS -->
<meta name       = "contactName"           content = "<?php echo ys_getconfig("datosemp", "EMP_Nombre", "C"); ?>">
<meta name       = "contactOrganization"   content = "<?php echo ys_getconfig("datosemp", "EMP_Nombre", "C"); ?>">
<meta name       = "contactStreetAddress1" content = "<?php echo ys_getconfig("datosemp", "EMP_Direccion", "C"); ?>">
<meta name       = "contactZipcode"        content = "<?php echo ys_getconfig("datosemp", "EMP_Zip", "C"); ?>">
<meta name       = "contactCity"           content = "<?php echo ys_getconfig("datosemp", "EMP_Ciud", "C"); ?>">
<meta name       = "contactCountry"        content = "<?php echo ys_getconfig("datosemp", "EMP_Pais", "C"); ?>">
<meta name       = "contactPhoneNumber"    content = "<?php echo ys_getconfig("datosemp", "EMP_Cel", "C"); ?>">
<meta name       = "contactFaxNumber"      content = "<?php echo ys_getconfig("datosemp", "EMP_Cel", "C"); ?>">
<meta name       = "contactNetworkAddress" content = "<?php echo ys_getconfig("datosemp", "EMP_Email", "C"); ?>">
<meta name       = "copyright"             content = "<?php echo ys_getconfig("datosemp", "EMP_RazonSocial", "C"); ?>">
<meta name       = "creation_date"         content = "fri, 13 Feb 1998">
<meta name       = "host"                  content = "<?php echo ys_getconfig("datosemp", "EMP_URL", "C"); ?>">
<meta name       = "host-admin"            content = "<?php echo DEFAULT_NOM; ?>">
<meta name       = "id"                    content = "Microsoft FrontPage 3.0">
<meta name       = "identifier-url"        content = "<?php echo ys_getconfig("datosemp", "EMP_URL", "C"); ?>">
<meta name       = "language"              content = "<?php echo WEB_LANG; ?>">
<meta name       = "operator"              content = "<?php echo DEFAULT_NOM; ?>">
<meta name       = "page-topic"            content = "<?php echo $_SESSION["EMP_COD"]; ?> - <?php echo $_SESSION["EMP_RAZ"]; ?>">
<meta name       = "page-type"             content = "<?php echo $_SESSION["EMP_COD"]; ?> - <?php echo $_SESSION["EMP_RAZ"]; ?>">
<meta name       = "presdate"              content = "sat, 20 Feb 2020">
<meta name       = "subject"               content = "<?php echo $_SESSION["EMP_COD"]; ?> - <?php echo $_SESSION["EMP_RAZ"]; ?>">
<meta name       = "version"               content = "<?php echo WEB_VERSION; ?>">
<!-- TWITTER CARD METAS -->
<meta name       = "twitter:card"          content = "summary">
<meta name       = "twitter:site"          content = "@<?php echo ys_getconfig("datosemp", "EMP_TW", "C"); ?>">
<meta name       = "twitter:creator"       content = "@<?php echo ys_getconfig("datosemp", "EMP_TW", "C"); ?>">
<meta name       = "twitter:title"         content = "<?php echo $_SESSION["EMP_COD"]; ?> - <?php echo $_SESSION["EMP_RAZ"]; ?>">
<meta name       = "twitter:description"   content = "<?php echo $_SESSION["EMP_COD"]; ?> - <?php echo $_SESSION["EMP_RAZ"]; ?>">
<meta name       = "twitter:image"         content = "images/_PNG_LogoUNIVERSAL_Logo.png">
<!--- DUBLIN CORE METADATA INITIATIVE -->
<meta name       = "DC.title"              content = "<?php echo $_SESSION["EMP_RAZ"]; ?>">
<meta name       = "DC.creator"            content = "<?php echo DEFAULT_NOM; ?>">
<meta name       = "DC.description"        content = "<?php echo $_SESSION["EMP_COD"]; ?> - <?php echo $_SESSION["EMP_RAZ"]; ?>">
<meta name       = "DC.subject"            content = "<?php echo $_SESSION["EMP_COD"]; ?>">
<meta name       = "DC.publisher"          content = "<?php echo DEFAULT_WEB; ?>">
<meta name       = "DC.contributors"       content = "<?php echo DEFAULT_NOM; ?>">
<meta name       = "DC.date"               scheme  = "W3CDTF"   content = "2050-09-13">
<meta name       = "DC.type"               scheme  = "DCMIType" content = "document">
<meta name       = "DC.format"             scheme  = "IMT"      content = "text/html">
<meta name       = "DC.language"           scheme  = "RFC3066"  content = "<?php echo WEB_LANG; ?>">
<meta name       = "DC.coverage"                                content = "World">
<meta name       = "DC.rights"                                  content = "lu_tyc_uso.php">
<!--- VANCOUVER SCHEMA METAS -->
<meta name       = "VW96.ObjectType"       content = "Catalog">
<meta name       = "VW96.ObjectType"       keywords = "Software Computing">
<!--- OPEN GRAPH METAS -->
<meta property   = "og:title"              content = "<?php echo $_SESSION["EMP_COD"]; ?> - <?php echo $_SESSION["EMP_RAZ"]; ?>">
<meta property   = "og:type"               content = "website">
<meta property   = "og:description"        content = "<?php echo $_SESSION["EMP_COD"]; ?> - <?php echo $_SESSION["EMP_RAZ"]; ?>">
<meta property   = "og:image"              content = "images/_PNG_LogoUNIVERSAL_Logo.png">
<meta property   = "og:image:type"         content = "image/png">
<meta property   = "og:image:width"        content = "200">
<meta property   = "og:image:height"       content = "88">
<meta property   = "og:image:alt"          content = "<?php echo $_SESSION["EMP_COD"]; ?> - <?php echo $_SESSION["EMP_RAZ"]; ?>">
<meta property   = "og:url"                content = "<?php echo ys_getconfig("datosemp", "EMP_URL", "C"); ?>">
<meta property   = "og:locale"             content = "<?php echo WEB_LANG; ?>">
<meta property   = "og:locale:alternate"   content = "es_LA">
<meta property   = "og:site_name"          content = "<?php echo $_SESSION["EMP_COD"]; ?> - <?php echo $_SESSION["EMP_RAZ"]; ?>">
<!--- MICROSOFT WINDOWS PHONE APP TILE METAS (HTML ORIENTED) -->
<meta name="msapplication-square70x70logo"   content = "images/ms_metadata_70x70.png">
<meta name="msapplication-square150x150logo" content = "images/ms_metadata_150x150.png">
<meta name="msapplication-wide310x150logo"   content = "images/ms_metadata_310x150.png">
<meta name="msapplication-square310x310logo" content = "images/ms_metadata_310x310.png">
<meta name="msapplication-TileColor"         content = "#FFFFFF">
<!--- MICROSOFT WINDOWS PHONE APP TILE METAS (XML ORIENTED) -->
<meta name="msapplication-config"            content ="xml_metadata_tile.xml" />
<!--- MICROSOFT WINDOWS PHONE APP TILE METAS (APP NAME) -->
<meta name="application-name"                content ="<?php echo $_SESSION["EMP_COD"]; ?>">
<!--- LINK REVERSE [NOT SUPPORTED IN HTML5] -->
<link rev        = "made"                  href      = "mailto: <?php echo ys_getconfig("datosemp", "EMP_Email", "C"); ?>">
<!--- LINK RELATIONS -->
<link rel        = "canonical"             href      = "<?php echo ys_getconfig("datosemp", "EMP_URL", "C"); ?>">
<link rel        = "apple-touch-icon"                              sizes = "180x180" href = "images/apple-touch-icon.png">
<link rel        = "shortcut icon"         href      = "favicon.ico">
<link rel        = "icon"                  type      = "image/png" sizes = "32x32"   href = "favicon-32x32.png">
<link rel        = "icon"                  type      = "image/png" sizes = "16x16"   href = "favicon-16x16.png">
<link rel        = "manifest"                                                        href = "site.webmanifest">
<script language ="Javascript"             type      ="text/javascript"              src  = "js/ys_js_ajax.js"></script>
<!--// <?php echo $_SESSION["EMP_COD"]; ?> - <?php echo $_SESSION["EMP_RAZ"]; ?> //-->
<?php ys_titulo(); ?>
