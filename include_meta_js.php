<?php include_once("include_sessions.php"); ?>
<script language = "Javascript" type = "text/javascript">
function ys_js_GetGames(ys_sel='', ys_cat='', ys_brand='', ys_tipos='')
 {
<?php
if(!$_SESSION["ISMOVIL"])
 {
 echo " juegos_slot(); //--- Crea el ambiente para despliegue de im�genes de juegos.";
 echo "\n";
 }
?>
 if(ys_sel=='C')
  {
  ys_PopulateCombos(ys_sel=ys_sel, ys_cat=ys_cat, ys_brand='', ys_tipos='');
<?php
if($_SESSION["ISMOVIL"])
 {
 echo "  setTimeout(show_owl, 500);";
 echo "\n";
 }
?>
  }
 }

//--- Obtener los IDs de las Categor�as (Salas) para determinar Combo de Provedores (Brands) y de Tipos.
function ys_PopulateCombos(ys_sel='', ys_cat='', ys_brand='', ys_tipos='')
 {
 GetAjax('ys_ajax_brands.php?c='+ys_cat, 'ys_showprov', 'GET');
 GetAjax('ys_ajax_types.php?c='+ys_cat, 'ys_showtipos', 'GET');
 }

function ys_js_ShowGames(ys_sel='', ys_cat='', ys_brand='', ys_bname='', ys_tipos='')
 {
 //--- Desplegar grupos de juegos.
 $('.body-casino').hide();
 GetAjax('ys_ajax_showgames.php?s='+ys_sel+'&c='+ys_cat+'&b='+ys_brand+'&t='+ys_tipos, 'ys_showPROV', 'GET');
 $('#ys_secPROV .title-casino').html("Proveedor: "+ys_bname);
 $('#ys_secPROV').show();
 console.log(ys_sel);
 if(ys_sel=='C')
  {
  $('.body-casino').show();
  $('#ys_secPROV').hide();
  }
 
 }

function ys_js_ShowFavs()
 {
 GetAjax('ys_ajax_showgames.php?s=&c=&b=&t=FAVO&p=0', 'ys_showFAVO', 'GET');
 }

//--- Iniciar el juego y actualizar contadores de Clicks y Favoritos.
function ys_js_LaunchGame(ys_c='', ys_p='', ys_b='', ys_g='', ys_m='')
 {
 // ys_c: CodCat
 // ys_p: Control
 // ys_b: CodBrand
 // ys_g: CodGame
 // ys_m: Modo
 // ys_t: Token
 ys_url = "";

 //--- Actualizar contadores.
 ys_js_Contadores(ys_c, ys_p, ys_b, ys_g, ys_m, ys_cf=1);

 // Se debe generar el Token justo antes de iniciar el juego.
 ys_url = "<?php echo UNIVERSAL_API; ?>/api/launch?gameid="+ys_g+"&p="+ys_p+"&b="+ys_b+"&m="+ys_m+"&sessionid="+ys_js_GetToken();
 ys_js_GetUserData(ys_d=3);
 //--- Actualiza Balance del Usuario e inicia el juego.
 abrir_modal(ys_url);
 }

function ys_js_GetToken()
 {
 var ys_AjaxToken;
 /* Para navegadores distintos a Internet Explorer */
 try {ys_AjaxToken = new ActiveXObject("Msxml2.XMLHTTP");}
 /* Para IE */
 catch (e)
  {
  try {ys_AjaxToken = new ActiveXObject("Microsoft.XMLHTTP");} 
  catch (e) {ys_AjaxToken = "Y";}
  }
 if (ys_AjaxToken=="Y" && typeof XMLHttpRequest!='undefined') {ys_AjaxToken = new XMLHttpRequest();}
 ys_AjaxToken.open("GET", "ys_ajax_gettoken.php", false);
 ys_AjaxToken.send(null);
 return ys_AjaxToken.responseText;
 }

function ys_js_GetUserData(ys_d=3)
 {
 //--- Consulta en sevidor los datos del usuario.
 ys_c = "";
 if(ys_d==1) {ys_c="usu_nombres";}
 if(ys_d==2) {ys_c="usu_moneda";}
 if(ys_d==3) {ys_c="usu_balance";}
 GetAjax('ys_ajax_user_data.php?d='+ys_d, ys_c, 'GET');
 }

function juego_simple(url)
 {
 url += ys_js_GetToken();
 $(".sliders-bg").hide();
 $(".body-casino").hide();
 $(".banner_section").hide();
 $("#games-single").show();
 if($("#games-single").find("iframe").length ) $("#games-single").find("iframe").remove();
 $("#games-single").append(`<iframe src="${url}" class="my-iflame" frameborder="0"></iframe>`);
 ys_js_GetUserData(d=3);
 }

function juegos_slot()
 {
 $(".sliders-bg").show();
 $(".body-casino").css("padding-top","6rem");
 $("#main-jackpot").show();
 $(".banner_section").show();
 $("#games-single").hide();
 if( $("#games-single").find("iframe").length ) $("#games-single").find("iframe").remove();
 ys_js_GetUserData(ys_d=3);
 }
</script>
