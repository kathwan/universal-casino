<?php
include_once("include_sessions.php");
include_once("ys_sql_funciones.php");

//--- Este script es la versi�n que muestra los tipos de juego seg�n la Categor�a.

//--- Variables que se usar�n
$ys_codtipo       = ""; //--- C�digo del Tipo de Juego.
$ys_nomtipo       = ""; //--- Nombre del Tipo de Juego.
$ys_condicion     = ""; //--- Condici�n de filtrado para cantidad de juegos de acuerdo al Tipo de juego.
$ys_CatCod        = ""; //--- C�digo de la Categor�a.
$ys_cantg         = 0;  //--- CANTIDAD DE JUEGOS de cada tipo que se asignar�n.
$ys_tot_gen_games = 0;  //--- Total general de Juegos almacenados seg�n los tipos.
$ys_echo          = ""; //--- Construcci�n del HTML.
$ys_showdiv       = ""; //--- HTML que se devolver�.

//--- Recibir par�metro. Id de la Categor�a. (Valor por defecto: "")
if(isset($_GET["c"])) {$ys_CatCod = $_GET["c"];}

//--- Control de errores
$ys_CatCodError = 0;

//----------------------------------------
//--- Contar los juegos de cada Tipo y desplegarlos en el men� correspondiente.
//--- Se cargan los valores en el DIV.

if(WEB_ISMOBILE)
 {
 }
else
 {
 //--- ***** TORNEOS *****.
 $ys_echo .= " <a class='dropdown-item Fcasino4' href='javascript:void(0)'>TORNEOS (-)</a>";
 $ys_echo .= "\n";
 $ys_echo .= " <div class='dropdown-divider'></div>";
 $ys_echo .= "\n";
 }

//--- Recorrer las capas para despliegue de juegos.
$SQL = "";
$SQL .= "SELECT";
$SQL .= " *";
$SQL .= " FROM";
$SQL .= " ".TB_GTYPES;
$SQL .= " WHERE";
$SQL .= " GTY_Activo";
$SQL .= " ORDER BY";
$SQL .= " GTY_Pos ASC";
$ys_rs=YQuery($SQL,"1");
if($ys_rs)
 {
 $ys_rs= YQuery($SQL);
 //--- Desplegar las opciones del men�.
 while ($ys_file = mysqli_fetch_assoc($ys_rs))
  {
  $ys_codtipo = $ys_file["GTY_Codigo"];
  $ys_condicion = "";
  $ys_condicion .= "GAM_CodCat='".$ys_CatCod."' AND GAM_Modo='".(WEB_ISMOBILE ? "mb" : "wb")."'";
  if($ys_codtipo=="POPU" || $ys_codtipo=="POPU")
   {$ys_condicion .= " AND GAM_Pop";}
  elseif($ys_codtipo=="POPU" || $ys_codtipo=="NUEV")
   {$ys_condicion .= " AND GAM_Nue";}
  else
   {$ys_condicion .= " AND GAM_Tipo='".$ys_codtipo."'";}
  $ys_cantg = ys_searchdata($ys_tabla=TB_GAMES, $ys_condicion, $ys_campo="", $ys_0Campo_1Contar_2AutoNum_3Max=YSEARCH_CONTAR);
  $ys_tot_gen_games += $ys_cantg;

  if($ys_cantg)
   {
   //--- Despliega el Tipo s�lo si existen juegos de este Tipo.
   if(WEB_ISMOBILE)
    {
    $ys_nomtipo = $ys_file["GTY_Movil"];
    $ys_echo .= " <div class='item'>";
    $ys_echo .= "\n";
    $ys_echo .= "  <div class='casino-tabs'>";
    $ys_echo .= "\n";
    $ys_echo .= "   <p>".$ys_nomtipo."</p>";
    $ys_echo .= "\n";
    $ys_echo .= "  </div>";
    $ys_echo .= "\n";
    $ys_echo .= " </div>";
    $ys_echo .= "\n";
    }
   else
    {
    $ys_nomtipo = $ys_file["GTY_Desc"];
    $ys_echo .= " <a class='dropdown-item Fcasino3' href='javascript:void(0)' onclick='ys_name(\"".$ys_codtipo."\")'><span>".$ys_nomtipo."</span><span>(".$ys_cantg.")</span></a>";
    $ys_echo .= "\n";
    $ys_echo .= " <div class='dropdown-divider'></div>";
    $ys_echo .= "\n";
    }
   }
  }
 } //--- if $ys_rs


if(WEB_ISMOBILE)
 {
 $ys_showdiv .= $ys_echo;
 }
else
 {
 //--- ***** DESPLIEGUE WEB *****.
 $ys_showdiv .= "<a class='Fcasino1' role='button' href='#' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Todos los juegos (".$ys_tot_gen_games.")</a>";
 $ys_showdiv .= "\n";
 $ys_showdiv .= "<div class='dropdown-menu drop-casino scroll-provee' aria-labelledby='navbarDropdown' x-placement='bottom-start'>";
 $ys_showdiv .= "\n";
 $ys_showdiv .= " <a class='dropdown-item Fcasino1' href='javascript:void(0)' OnClick='ys_js_ShowGames(ys_sel=\"T\", ys_cat=\"".$ys_CatCod."\", ys_brand=\"\", ys_tipos=\"\")'>Todos los juegos (".$ys_tot_gen_games.")</a>";
 $ys_showdiv .= "\n";
 $ys_showdiv .= " <div class='dropdown-divider'></div>";
 $ys_showdiv .= "\n";
 $ys_showdiv .= $ys_echo;
 $ys_showdiv .= "</div>";
 $ys_showdiv .= "\n";
 }
echo $ys_showdiv;
?>
