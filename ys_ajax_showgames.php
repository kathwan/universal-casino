<?php
include_once("include_sessions.php");
include_once("ys_sql_funciones.php");

//--- Este script es la versi�n que muestra las im�genes y los juegos leyendo bases de datos

//--- Variables auqe se usar�n
$ys_Sel      = ""; //--- AJAX - Indica qu� fue solicitado: C - Category / B - Brand / T - Type
$ys_CatCod   = ""; //--- Cod de Categor�a para AJAX.
$ys_BrandCod = ""; //--- Cod de Brand para AJAX.
$ys_Tipo     = ""; //--- Tipo de Game para SQL.
$ys_showdiv  = ""; //--- HTM que se devolver�.

//--- Recibir par�metro. Id de la Categor�a. (Valor por defecto: "")
if(isset($_GET["s"])) {$ys_Sel      = $_GET["s"];}
if(isset($_GET["c"])) {$ys_CatCod   = $_GET["c"];}
if(isset($_GET["b"])) {$ys_BrandCod = $_GET["b"];}
if(isset($_GET["t"])) {$ys_Tipo     = $_GET["t"];}

//--- Control de errores
$ys_SelError      = 0;
$ys_CatCodError   = 0;
$ys_BrandCodError = 0;
$ys_TiposError    = 0;

//----------------------------------------
//--- Se cargan los valores
$ys_showdiv = "";
$ys_showdiv .= ys_ShowGamesLinks($ys_sel=$ys_Sel, $ys_cat_id=$ys_CatCod, $ys_brand_id=$ys_BrandCod, $ys_tipo=$ys_Tipo);
echo $ys_showdiv;


//**************************************************
//     MOSTRAR IMAGENES Y ENLACES DE LOS JUEGOS
//**************************************************
function ys_ShowGamesLinks($ys_sel="", $ys_cat_id="", $ys_brand_id="", $ys_tipo="")
 {
 //--- $ys_sel: Indica qu� se desplegar�: C - Category / B - Brand / T - Type / F - Favorite
 $ys_rs      = "";
 $ys_echo    = "";
 $tK1        = 0;
 $ys_img     = ""; //--- URL de la imagen.

 if($ys_sel=="B" && $ys_brand_id=="")
  {
  //--- Si se llama desde Brands y no hay Brand seleccionada,
  //--- entonces es el elemento de "Todos los proveedores"
  //--- y se toma como la llamada a la Category.
  $ys_sel="C";
  }

 if($ys_tipo!="FAVO")
  {
  //--- Cargar llamada a SQL para Categories / Brands / Types.
  if($ys_sel=="C")
    {
    //--- 
    $ys_brand_id = "";
    $ys_tipos    = "";
    }
  $SQL  = "";
  $SQL .= "SELECT";
  $SQL .= " *";
  $SQL .= " FROM";
  $SQL .= " ".TB_GAMES;
  $SQL .= " WHERE";
  $SQL .= " GAM_Modo='".(WEB_ISMOBILE ? "mb" : "wb")."'";
  $SQL .= " AND";
  $SQL .= " GAM_CodCat='".$ys_cat_id."'";
  $SQL .= " AND";
  $SQL .= " GAM_Activo";
  $SQL .= " AND";
  $SQL .= " GAM_EnAPI";
  if($ys_sel=="B")
   {$SQL .= " AND GAM_CodBra='".$ys_brand_id."'";}
  if($ys_sel=="C" || $ys_sel=="T")
   {
   //--- Se seleccion� opci�n del Combo de Tipos.
   //--- S�lo compara los Tipos si ys_tipo tiene contenido.
   //--- Cuando ys_tipo es blanco, se seleccionaron "Todos los juegos".
   if($ys_tipo!="")
    {
    if($ys_tipo=="NUEV" || $ys_tipo=="POPU")
     {
     if($ys_tipo=="NUEV") {$SQL .= " AND GAM_Nue";} //--- Se hizo llamada a juegos Nuevos.
     if($ys_tipo=="POPU") {$SQL .= " AND GAM_Pop";} //--- Se hizo llamada a juegos Populares.
     }
    else
     {$SQL .= " AND GAM_Tipo='".$ys_tipo."'";} //--- Selecciona seg�n el Tipo de Juego.
    }
   }
  $SQL .= " ORDER BY";
  $SQL .= " GAM_Codigo ASC";
  }
 else
  {
  //--- Cargar llamada a SQL para bot�n de Favoritos.
  $SQL  = "";
  $SQL .= "SELECT";
  $SQL .= " *";
  $SQL .= " FROM";
  $SQL .= " ".TB_GAMES;
  $SQL .= " WHERE";
  $SQL .= " GAM_Modo='".(WEB_ISMOBILE ? 'mb' : 'wb')."'";
  $SQL .= " AND";
  $SQL .= " GAM_Activo";
  $SQL .= " AND";
  $SQL .= " GAM_EnAPI";
  $SQL .= " AND";
  $SQL .= " GAM_Codigo IN";
  $SQL .= " (SELECT";
  $SQL .= "  UCF_CodCBG";
  $SQL .= "  FROM";
  $SQL .= "  ".TB_USUCF;
  $SQL .= "  WHERE";
  $SQL .= "  UCF_UsuLogin='".$_SESSION['ULOG']."'";
  $SQL .= "  AND";
  $SQL .= "  UCF_TipoCBG='G'";
  $SQL .= "  AND";
  $SQL .= "  UCF_Favs=1)";
  $SQL .= " ORDER BY";
  $SQL .= " GAM_Codigo ASC";
  }

 $ys_rs = YQuery($ys_query=$SQL, $ys_rows=1);

 //--- Mostrar las im�genes de los juegos
 if($ys_rs)
  {
  //--- Existen registros para mostrar.
  $ys_rs= YQuery($ys_query=$SQL, $ys_rows=0);
  //--- Aqu� ya se tiene la lectura completa de los registros que ser�n mostrados.
  while ($ys_file = mysqli_fetch_assoc($ys_rs))
   {
   $ys_echo .= "<!--- ".$ys_file["GAM_Nombre"]." -->";

   //--- Lee cantidad TOTAL de Favs registrados para este juego.
   $ys_favs = ys_searchdata($ys_tabla=TB_GAMES, $ys_condicion="GAM_Codigo='".addslashes(trim($ys_file["GAM_Codigo"]))."'", $ys_campo="GAM_Favs", $ys_0Campo_1Contar_2AutoNum_3Max=YSEARCH_CAMPO);
   //--- Determina si el Fav est� activo para este usuario.
   //--- 0: No existe el registro. Desactivado. (far)
   //--- 1: El usuario marc� Fav. Activado. (fas)
   //--- 2: El usuario desmarc� Fav. Desactivado. (far)
   $ys_far_fas = ys_searchdata($ys_tabla=TB_USUCF, $ys_condicion="UCF_UsuLogin='".$_SESSION['ULOG']."' AND UCF_TipoCBG='G' AND UCF_CodCBG='".addslashes(trim($ys_file["GAM_Codigo"]))."'", $ys_campo="UCF_Favs", $ys_0Campo_1Contar_2AutoNum_3Max=YSEARCH_CAMPO);
   if($ys_far_fas=="")
    {
    //--- El usuario NO HA pulsado nunca Fav a este juego.
    $ys_far_fas=0;
    }
   if($ys_far_fas==1 || $ys_far_fas==2)
    {
    //--- El usuario SI HA pulsado Fav a este juego antes.
    //--- Convertir la cantidad de Favs en n�mero negativo para enviar una se�al a ys_js_Contadores().
    $ys_favs = ($ys_favs * (-1));
    }
   //--- Establecer el valor de ys_far_fas.
   //--- Campo | Descripci�n                                                      | Valor Final
   //---   0   | El usuario no ha pulsado Fav nunca.                              | far (Inactivo)
   //---   1   | El usuario puls� Fav y actualmente SI es su favorito.            | fas (Activo)
   //---   2   | El usuario puls� Fav y luego desmarc� el juego como su favorito. | far (Inactivo)
   if($ys_far_fas==0 || $ys_far_fas==2)
    {$ys_far_fas = "far";}
   else
    {$ys_far_fas = "fas";}

   //--- Imagen del juego y proveedor.
   $ys_img_game = utf8_decode(UNIVERSAL_API."/".$ys_file["GAM_Imagen"]);
   $ys_img_prov = utf8_decode(DEMO_API."/images/casino/providers/".$ys_file["GAM_CodBra"].".png");

   if(WEB_ISMOBILE)
    {
    $ys_echo .= "\n";
    $ys_echo .= "<div class='col-6 effect-padd'>";
    $ys_echo .= "\n";
    $ys_echo .= " <div class='game_inner game-sm' style='background-image:url(".$ys_img_game.")'>";
    $ys_echo .= "\n";
    $ys_echo .= "  <div class='jackpot-div'>";
    $ys_echo .= "\n";
    $ys_echo .= "   <span id='f-".addslashes(trim(str_replace("@", "_", $ys_file["GAM_Codigo"])))."'><i class='far fa-heart' id='spadegaming-5-fortune-sa' aria-hidden='true' OnClick='ys_js_Contadores(ys_c=\"".addslashes(trim($ys_file["GAM_CodCat"]))."\", ys_p=\"".$ys_file["GAM_Control"]."\", ys_b=\"".addslashes(trim($ys_file["GAM_CodBra"]))."\", ys_g=\"".addslashes(trim($ys_file["GAM_Codigo"]))."\", ys_m=\"".$ys_file["GAM_Modo"]."\", ys_cf=0, ys_n=".$ys_favs.");likes(\"".addslashes(trim(str_replace("@", "_", $ys_file["GAM_Codigo"])))."\");'></i>".abs($ys_favs)."</span>";
    $ys_echo .= "\n";
    $ys_echo .= "  </div>";
    $ys_echo .= "\n";
    $ys_echo .= "  <div class='game_overlay'>";
    $ys_echo .= "\n";
    $ys_echo .= "   <div class='play'>";
    $ys_echo .= "\n";
    $ys_echo .= "    <a href='javascript:void(0)' OnClick='ys_js_LaunchGame(ys_c=\"".addslashes(trim($ys_file["GAM_CodCat"]))."\", ys_p=\"".$ys_file["GAM_Control"]."\", ys_b=\"".addslashes(trim($ys_file["GAM_CodBra"]))."\", ys_g=\"".addslashes(trim($ys_file["GAM_Codigo"]))."\", ys_m=\"".$ys_file["GAM_Modo"]."\");');' class='circle-play f-1'>";
    $ys_echo .= "\n";
    $ys_echo .= "     <i class='fas fa-play' aria-hidden='true'></i>";
    $ys_echo .= "\n";
    $ys_echo .= "    </a>";
    $ys_echo .= "\n";
    $ys_echo .= "   </div>";
    $ys_echo .= "\n";
    $ys_echo .= "   <div class='game_title'>";
    $ys_echo .= "\n";
    $ys_echo .= "    <img class='img-fluid img-game' src='".$ys_img_prov."' alt=''>";
    $ys_echo .= "\n";
    $ys_echo .= "   </div>";
    $ys_echo .= "\n";
    $ys_echo .= "  </div>";
    $ys_echo .= "\n";
    $ys_echo .= " </div>";
    $ys_echo .= "\n";
    $ys_echo .= " <div class='new-text-div'>";
    $ys_echo .= "\n";
    $ys_echo .= "  <p class='title-game'>".addslashes(trim($ys_file["GAM_Nombre"]))."</p>";
    $ys_echo .= "\n";
    $ys_echo .= " </div>";
    $ys_echo .= "\n";
    $ys_echo .= "</div>";
    $ys_echo .= "\n";
    }
   else
    {
    $ys_echo .= "\n";
    $ys_echo .= "<div id='p-".addslashes(trim(str_replace("@", "_", $ys_file["GAM_Codigo"])))."' class='col-item sm'>";
    $ys_echo .= "\n";
    $ys_echo .= " <div class='row no-gutters'>";
    $ys_echo .= "\n";
    $ys_echo .= "  <div class='col-12 effect-padd2'>";
    $ys_echo .= "\n";
    $ys_echo .= "<div class='effect-padd'>";
    $ys_echo .= "\n";
    $ys_echo .= "<div class='new-text-div'>";
    $ys_echo .= "\n";
    $ys_echo .= "<p class='title-game'>".addslashes(trim($ys_file["GAM_Nombre"]))."</p>";
    $ys_echo .= "\n";
    $ys_echo .= "</div>";
    $ys_echo .= "\n";
    $ys_echo .= "   <div class='game_inner game-sm my-effect' style='background-image:url(".$ys_img_game.")'>";
    $ys_echo .= "\n";
    $ys_echo .= "    <div class='fav_holder'>";
    $ys_echo .= "\n";
    $ys_echo .= "     <div class='like-game'>";
    $ys_echo .= "\n";
    $ys_echo .= "      <i class='".$ys_far_fas." fa-heart' style='display:block' id='".addslashes(trim(str_replace("@", "_", $ys_file["GAM_Codigo"])))."' aria-hidden='true' OnClick='ys_js_Contadores(ys_c=\"".addslashes(trim($ys_file["GAM_CodCat"]))."\", ys_p=\"".$ys_file["GAM_Control"]."\", ys_b=\"".addslashes(trim($ys_file["GAM_CodBra"]))."\", ys_g=\"".addslashes(trim($ys_file["GAM_Codigo"]))."\", ys_m=\"".$ys_file["GAM_Modo"]."\", ys_cf=0, ys_n=".$ys_favs.");likes(\"".addslashes(trim(str_replace("@", "_", $ys_file["GAM_Codigo"])))."\");'></i>";
    $ys_echo .= "\n";
    $ys_echo .= "      <span id='f-".addslashes(trim(str_replace("@", "_", $ys_file["GAM_Codigo"])))."' class='span-casino'>".abs($ys_favs)."</span>";
    $ys_echo .= "\n";
    $ys_echo .= "     </div>";
    $ys_echo .= "\n";
    $ys_echo .= "    </div>";
    $ys_echo .= "\n";
    $ys_echo .= "    <div class='game_overlay'>";
    $ys_echo .= "\n";
    $ys_echo .= "     <div class='play'>";
    $ys_echo .= "\n";
    $ys_echo .= "      <div class='circle-play'></div>";
    $ys_echo .= "\n";
    $ys_echo .= "       <!-- URL DEL JUEGO -->";
    $ys_echo .= "\n";
    $ys_echo .= "       <a href='javascript:void(0)' OnClick='ys_js_LaunchGame(ys_c=\"".addslashes(trim($ys_file["GAM_CodCat"]))."\", ys_p=\"".$ys_file["GAM_Control"]."\", ys_b=\"".addslashes(trim($ys_file["GAM_CodBra"]))."\", ys_g=\"".addslashes(trim($ys_file["GAM_Codigo"]))."\", ys_m=\"".$ys_file["GAM_Modo"]."\");'><i class='fas fa-play' aria-hidden='true'></i></a>";
    $ys_echo .= "\n";
    $ys_echo .= "       <!-- FIN DE URL DEL JUEGO -->";
    $ys_echo .= "\n";
    $ys_echo .= "      </div>";
    $ys_echo .= "\n";
    $ys_echo .= "      <div class='game_title'><img class='img-fluid' src='".$ys_img_prov."' alt=''><p class='name-casino'>".addslashes(trim($ys_file["GAM_Nombre"]))."</p></div>";
    $ys_echo .= "\n";
    $ys_echo .= "       <p class='name-casino'>".addslashes(trim($ys_file["GAM_Nombre"]))."</p>";
    $ys_echo .= "\n";
    $ys_echo .= "      </div>";
    $ys_echo .= "\n";
    $ys_echo .= "     </div>";
    $ys_echo .= "\n";
    $ys_echo .= "    </div>";
    $ys_echo .= "\n";
    $ys_echo .= "   </div>";
    $ys_echo .= "\n";
    $ys_echo .= "   </div>";
    $ys_echo .= "\n";
    $ys_echo .= "  </div>";
    $ys_echo .= "\n";
    $ys_echo .= "</div>";
    $ys_echo .= "\n";
    }    //--- if WEB_ISMOBILE ... ELSE
   $ys_echo .= "<!--- FIN DE ".addslashes(trim($ys_file["GAM_Nombre"]))." -->";
   $ys_echo .= "\n";
   }    //--- while ys_file = mysqli_fetch_assoc ys_rs
  }    //--- if ys_rs
 return $ys_echo;
 }
?>
