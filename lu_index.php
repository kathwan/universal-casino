<?php
include_once("include_sessions.php");
include_once("ys_sql_funciones.php");
include_once("ys_leer_api.php");
?>
<!DOCTYPE html>
<html lang="<?php echo WEB_LANG; ?>">
<head>
<?php include_once("include_meta.php"); ?>
<?php include_once("include_meta_css.php"); ?>
<link rel="stylesheet" href="css/style-casinov2.css">
<script language = "Javascript" type = "text/javascript" src = "include_script_jsdelivr.js"></script>
<?php include_once("include_meta_js.php"); ?>
<script language="Javascript" type="text/javascript" src="js/ys_js_ajax.js"></script>
</head>

<body class="casino-bg" OnLoad="ys_js_GetGames(ys_sel='C', ys_cat='slot');hover_tv();ys_js_GetUserData(ys_d=1);ys_js_GetUserData(ys_d=2);ys_js_GetUserData(ys_d=3);">

<?php include_once("include_encabezado.php"); ?>

<!-- BARRA DE MENU -->
<div class="sub-header">
 <!-- LISTA DE SALAS DE JUEGO (CATEGORIAS) -->
<?php echo ys_ShowCategories(); ?>
 <!-- FIN DE SALAS DE JUEGO (CATEGORIAS) -->
  
 <!-- SELECCION DE IDIOMA -->
 <div class="flex-c-m">
  <div class="form-flat">
  </div>
   <img id="flat-img" class="img-fluid" style="width: 40px;" src="images_paises/pais_ES.jpg" alt="">
  <select id="inputPais" class="form-control form-idioma" name="inputPais">
   <option selected value="ES">ES</option>
   <option value="US">US</option>
   <option value="FR">FR</option>
   <option value="BR">BR</option>
  </select>
 </div>
 <!-- FIN DE SELECCION DE IDIOMA -->
 
</div>
<!-- FIN DE BARRA DE MENU -->

<!--- SECCION DE SLIDER -->
<div class="sliders-bg">

 <!--- IMAGEN DE SLIDER -->
 <div class="owl-carousel">
 <div class="item">
 <img class="img-fluid" style="height: 375px;" src="images/casino/slider3.png" atl="promo"></img>
 </div>
 </div>
 <!--- FIN DE IMAGEN DE SLIDER -->

 <!--- SUBMENU DE JUEGOS -->
 <div class="jackpot-bg">

  <!--- INDICADORES DE JACKPOT -->
  <div class="jackpot-div">
  <!--- 
  <div class="jackpot-list">
  <span class="j-p-title">JACKPOT : </span>
  <span class="j-p-sub">$6.623.150,25</span>
  </div>
  <div class="jackpot-list">
  <img class="img-fluid" src="images/casino/aog.png" alt="">
  <span class="j-p-sub">$75.256,05</span>
  </div>
  <div class="jackpot-list">
  <img class="img-fluid" src="images/casino/aog.png" alt="">
  <span class="j-p-sub">$1.089.110,10</span>
  </div>
  <div class="jackpot-list">
  <img class="img-fluid" src="images/casino/aog.png" alt="">
  <span class="j-p-sub">$58.251,73</span>
  </div>
  -->
  </div>
  <!--- FIN DE INDICADORES DE JACKPOT -->
  
  <!--- BARRA DE SELECCION DE JUEGOS -->
  <div class="jackpot-div pt-0">

   <!--- FORMULARIO DE BUSQUEDA -->
   <form class="search-div search-casino">
   <input id="searchcasino" class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
   <button class="btn btn-outline-danger btn-search my-2 my-sm-0" type="submit"><i class="fas fa-search" aria-hidden="true"></i></button>
   </form>
   <!--- FIN DE FORMULARIO DE BUSQUEDA -->

   <!--- SELECTOR DE TIPO DE DESPLIEGUE -->
   <div class="jackpot-list-2">
    <!---
    <div class="toggle-button-cover">
     <label class="label-site" for="site">ON SITE</label>
     <div class="button r" id="button-1">
      <input type="checkbox" class="checkbox">
      <div class="knobs"></div>
      <div class="layer"></div>
     </div>
     <label class="label-site" for="popup">POP UP</label>
    </div>
    -->
    <button class="btn btn-primary"><a href="ys_renovar_juegos.php">Renovar juegos</a></button>
   </div>
   <!--- FIN DE SELECTOR DE TIPO DE DESPLIEGUE -->

   <!--- COMBO DE PROVEEDORES -->
   <div class='jackpot-list-2 dropdown-toggle'>
   <span id="ys_showprov">
   </span>
   </div>
   <!--- FIN DE COMBO DE PROVEEDORES -->

   <!--- COMBO DE JUEGOS -->
   <div class="jackpot-list-2 dropdown-toggle dropdown">
   <span id="ys_showtipos">
   </span>
   </div>
   <!--- FIN DE COMBO DE JUEGOS -->

   <!--- LISTA DE FAVORITOS -->
   <div class="jackpot-list-2">
    <a role="button" href="javascript:void(0);" class="Fcasino10" OnClick="ys_js_ShowFavs();">Favoritos</a><i class="far fa-heart icon-heart"></i>
   </div>
   <!--- FIN DE LISTA DE FAVORITOS -->

  </div>
  <!--- FIN DE BARRA DE SELECCION DE JUEGOS -->
  
  </div>
 <!--- FIN DE SUBMENU DE JUEGOS -->

</div>
<!--- FIN DE SECCION DE SLIDER -->

<?php
 //--- Recorrer las capas para despliegue de juegos.
 $SQL = "";
 $SQL .= "SELECT";
 $SQL .= " *";
 $SQL .= " FROM";
 $SQL .= " ".TB_GTYPES;
 $SQL .= " WHERE";
 $SQL .= " GTY_Top";
 $SQL .= " AND";
 $SQL .= " GTY_Activo";
 $SQL .= " ORDER BY";
 $SQL .= " GTY_Pos ASC";
 $ys_rs=YQuery($SQL,"1");
 if($ys_rs)
  {
  $ys_rs= YQuery($SQL);
  //--- Desplegar las capas superiores encontradas.
  while ($ys_file = mysqli_fetch_assoc($ys_rs))
   {ys_ShowSection($ys_file["GTY_Codigo"]);}
  } //--- if $ys_rs
?>

<!--- SECCION DE JUEGOS SIMPLES -->
<section id='games-single' class='body-casino pt-1'>
<iframe class='my-iflame' src='#' frameborder='0'></iframe>
</section>
<!--- FIN DE SECCION DE JUEGOS SIMPLES -->

<!--- SECCION DE BANNERS -->
<?php include_once("include_index_banner.php"); ?>
<!--- FIN DE SECCION DE BANNERS -->

<?php
 //--- Recorrer las capas para despliegue de juegos.
 $SQL = "";
 $SQL .= "SELECT";
 $SQL .= " *";
 $SQL .= " FROM";
 $SQL .= " ".TB_GTYPES;
 $SQL .= " WHERE";
 $SQL .= " NOT GTY_Top";
 $SQL .= " AND";
 $SQL .= " GTY_Activo";
 $SQL .= " ORDER BY";
 $SQL .= " GTY_Pos ASC";
 $ys_rs=YQuery($SQL,"1");
 if($ys_rs)
  {
  $ys_rs= YQuery($SQL);
  //--- Desplegar las capas superiores encontradas.
  while ($ys_file = mysqli_fetch_assoc($ys_rs))
   {ys_ShowSection($ys_file["GTY_Codigo"]);}
  } //--- if $ys_rs
?>

<!-- Modal -->
<div class="modal-slot">
    <div class="body-slot">
        <button class="btn btn-danger btn-programs modal-prog">Cerrar</button>
        <iframe src="javascript:void(0);" id="iframe" frameborder="0" style="flex: 1 1 0%; width: 100%; height: 100%;"></iframe>
    </div>
</div>

<a class="scroll-top" href="#scrolltop" style="display: none;"><i class="fas fa-chevron-up"></i></a>

<?php include_once("include_js_scripts.php"); ?>

</body>
</html>
