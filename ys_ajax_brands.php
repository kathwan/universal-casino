<?php
include_once("include_sessions.php");
include_once("ys_sql_funciones.php");

//--- Este script es la versi�n que muestra los proveedores leyendo bases de datos

//--- Variables que se usar�n
$ys_CatCod        = "";  //--- Cod de Categor�a para AJAX.
$ys_cantg         = 0;   //--- Array de CANTIDAD DE JUEGOS de Brands (Proveedores).
$ys_tot_gen_games = 0;   //--- Total de Games.
$ys_showdiv       = "";  //--- HTML que se devolver�.


//--- Recibir par�metro. Id de la Categor�a. (Valor por defecto: "")
if(isset($_GET["c"])) {$ys_CatCod = $_GET["c"];}

//--- Control de errores
$ys_CatCodError = 0;

//----------------------------------------
//--- Cargar los proveedores (Brands) cuya categor�a sea ys_CatCod
$ys_rs   = "";
$ys_echo = "";
$SQL     = "";
$SQL .= "SELECT";
$SQL .= " *";
$SQL .= " FROM";
$SQL .= " ".TB_BRANDS;
$SQL .= " WHERE";
$SQL .= " BRA_CodCat='".$ys_CatCod."'";
$SQL .= " AND";
$SQL .= " BRA_Activo";
$SQL .= " AND";
$SQL .= " BRA_EnApi";
$SQL .= " ORDER BY";
$SQL .= " BRA_Codigo ASC";
$ys_rs = YQuery($ys_query=$SQL, $ys_rows=1);
if($ys_rs)
 {
 //--- Hay registros
 //--- Recorrer los registros
 $ys_rs= YQuery($SQL);
 $ys_echo = "";
 $ys_tot_gen_games = 0;
 while ($ys_file = mysqli_fetch_assoc($ys_rs))
  {
  $ys_cantg = ys_searchdata($ys_tabla=TB_GAMES, $ys_condicion="GAM_CodCat='".$ys_CatCod."' AND GAM_CodBra='".$ys_file["BRA_Codigo"]."' AND GAM_Modo='".(WEB_ISMOBILE ? "mb" : "wb")."'", $ys_campo="", $ys_0Campo_1Contar_2AutoNum_3Max=YSEARCH_CONTAR);
  $ys_tot_gen_games += $ys_cantg;
  if($ys_cantg)
   {
   //--- Muestra s�lo los proveedores que contengan juegos en esta categor�a.
   if(WEB_ISMOBILE)
    {
    $ys_echo .= "<a class='dropdown-item' href='javascript:void(0)' OnClick='ys_js_ShowGames(ys_sel=\"B\", ys_cat=\"".$ys_CatCod."\", ys_brand=\"".$ys_file["BRA_Codigo"]."\", ys_brand=\"".$ys_file["BRA_Codigo"]."\", ys_bname=\"".$ys_file["BRA_Nombre"]."\")'><span>".$ys_file["BRA_Nombre"]."</span><span>(".$ys_cantg.")</span></a>";
    $ys_echo .= "\n";
    $ys_echo .= "<div class='dropdown-divider'></div>";
    $ys_echo .= "\n";
    }
   else
    {
    $ys_echo .= "<a class='dropdown-item' href='javascript:void(0)' OnClick='ys_js_ShowGames(ys_sel=\"B\", ys_cat=\"".$ys_CatCod."\", ys_brand=\"".$ys_file["BRA_Codigo"]."\", ys_brand=\"".$ys_file["BRA_Codigo"]."\", ys_bname=\"".$ys_file["BRA_Nombre"]."\")'><span>".$ys_file["BRA_Nombre"]."</span><span>(".$ys_cantg.")</span></a>";
    $ys_echo .= "\n";
    } //--- if WEB_ISMOBILE ... else
   } //--- if $ys_cantg
  }
 }
else
 {
 if(WEB_ISMOBILE)
  {
  $ys_echo .= "<a class='dropdown-item' href='#'>[Sin proveedores]</a>";
  $ys_echo .= "\n";
  }
 else
  {
  $ys_echo .= "<a class='dropdown-item Fcasino1' href='#'>[Sin proveedores]</a>";
  $ys_echo .= "\n";
  }
 }

 if(WEB_ISMOBILE)
  {
  $ys_showdiv  = "";
  $ys_showdiv .= $ys_echo;
  }
 else
  {
  //--- Se cargan los valores
  $ys_showdiv  = "";
  $ys_showdiv .= "<a class='Fcasino9' role='button' href='#' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Proveedores</a>";
  $ys_showdiv .= "\n";
  $ys_showdiv .= "<div id='proveedorDown' class='dropdown-menu drop-casino scroll-provee".($ys_CatCod == "slot" ? " drop-casino-slot" : "")."' aria-labelledby='navbarDropdown'>";
  $ys_showdiv .= "\n";
  $ys_showdiv .= "<a class='dropdown-item Fcasino1' href='javascript:void(0)' OnClick='ys_js_ShowGames(ys_sel=\"C\", ys_cat=\"".$ys_CatCod."\", ys_brand=\"\")'>Proveedores (".$ys_tot_gen_games.")</a>";
  $ys_showdiv .= "\n";
  $ys_showdiv .= $ys_echo;
  $ys_showdiv .= "</div>";
  $ys_showdiv .= "\n";
  }
echo $ys_showdiv;
?>
