<?php
header('Content-Type:text/html; charset=windows-1252');

//**************************************************
//**************************************************
//**************************************************
//**************************************************
//**************************************************

/*
//--------------------------------------------------
// ChangeClientIP
//--- IP Local: 209.45.60.42
//--- IP Host : 179.61.12.108
echo "<br><br><br><br><br>ChangeClientIP<br>";
$ys_result = ys_ChangeClientIP("C00243766056TestLocal", "179.61.12.108");
echo "ChangeClientIP: ".$ys_result;
echo "<br>";
echo "<br>";
echo "<br>";
exit();


//--------------------------------------------------
// CreateUser
echo "<br><br><br><br><br>CreateUser<br>";
ys_CreateUser("universal", "Nom", "Ape", "PE", "PEN", 1309, "M");

//--------------------------------------------------
// StartSession
echo "<br><br><br><br><br>StartSession<br>";
$ys_result = ys_StartSession("demo", 1309);
var_dump($ys_result);
echo "<br>";
echo $ys_result["val"]["token"];

//--------------------------------------------------
// ListGames
echo "<br><br><br><br><br>ListGames<br>";
$ys_result = ys_ListGames("demo", $ys_in_page=1, $ys_in_perPage=100);
// var_dump($ys_result);
echo "<br>";
echo "Juegos: ".count($ys_result["games"]);
echo "<br>";

foreach ($ys_result as $array1)
 {
 foreach ($array1 as $valor)
  {
  if(is_string($valor["id"]))
   {
   echo $valor["id"];
   echo "<br>";
   }
  }
 }

//--------------------------------------------------
// ListFilters
echo "<br><br><br><br><br>ListFilters<br>";
$ys_result = ys_ListFilters(CLIENT_PHRASE);
$ys_totb = count($ys_result["brands"]);
$ys_totc = count($ys_result["categories"]);
echo "<br>";
echo "Marcas: ".$ys_totb;
echo "<br>";
echo "Categor�as: ".$ys_totc;
echo "<br>";

// var_dump($ys_result);
echo "<br><br>----------<br><br>";

for ($tK1 = 0; $tK1 <= $ys_totb-1; $tK1++)
 {
 echo "Nombre completo del Proveedor: ".$ys_result["brands"][$tK1]["id"];
 echo "<br>";
 }

echo "<br><br>----------<br><br>";

for ($tK1 = 0; $tK1 <= $ys_totb-1; $tK1++)
 {
 echo "C�digo de categor�as a la que pertenece el Proveedor ".$ys_result["brands"][$tK1]["id"]." en 'brands' : ".$ys_result["brands"][$tK1]["category"];
 echo "<br>";
 }

echo "<br><br>----------<br><br>";

for ($tK1 = 0; $tK1 <= $ys_totc-1; $tK1++)
 {
 echo "C�digos de categor�as en 'categories': ".$ys_result["categories"][$tK1]["id"];
 echo "<br>";
 }
echo "<br>";

echo "<br><br>----------<br><br>";

for ($tK1 = 0; $tK1 <= $ys_totc-1; $tK1++)
 {
 echo "Nombres de categor�as en 'categories': ".$ys_result["categories"][$tK1]["name"];
 echo "<br>";
 }
echo "<br>";

echo "<br><br>----------<br><br>";

//--------------------------------------------------
// LaunchGame
$ys_result = ys_LaunchGame($ys_st_sessionid="1",
                           $ys_st_gameid="spinsane-gameview_netenthtml",
                           $ys_st_p="sl",
                           $ys_st_b="Netent",
                           $ys_st_m="mb"
                           );


//--------------------------------------------------
// TransactionsReport
$ys_result = ys_TransactionsReport($ys_st_prhase=CLIENT_PHRASE,
                                   $ys_st_page="1",
                                   $ys_st_perPage="500",
                                   $ys_st_username="demo"
                                   );

$ys_totd = count($ys_result["data"]);
echo "<br>";
echo "Transacciones total: ".$ys_totd;
echo "<br>";

for ($tK1 = 0; $tK1 <= $ys_totd-1; $tK1++)
 {
 echo "trxid: ".$ys_result["data"][$tK1]["trxid"];
 echo "<br>";
 }
echo "<br>";

*/


//**************************************************
//                 INICIAR SESION
//**************************************************
function ys_StartSession($ys_st_username,
                         $ys_fl_balance
                        )
 {
 //--- POST
 //--- VALORES DEVUELTOS
 //--- >> {
 //        status : (IN) 1/0 Estado de operaci�n correcta/incorrecta.
 //        err    : (ST) Descripci�n del error.
 //        cd     : (IN) ?????
 //        val    : (AR) Validaci�n
 //        {
 //         token : (ST) Token de conexi�n (128 bytes)
 //        }
 //--- >> }

 //--- URL de API
 $ys_url = UNIVERSAL_API."/api/auth";

 //--- Par�metros de solicitud
 $ys_campos = [
              'username' => $ys_st_username,
              'balance'  => $ys_fl_balance
              ];

 //--- Construir URL de los datos del POST
 $ys_query = json_encode($ys_campos, true);

 //--- Abrir conexi�n
 $ys_curl = curl_init();

 //--- Enviar la URL y los datos
 curl_setopt($ys_curl, CURLOPT_URL, $ys_url);
 curl_setopt($ys_curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
 curl_setopt($ys_curl, CURLOPT_CUSTOMREQUEST, "POST");
 curl_setopt($ys_curl, CURLOPT_POSTFIELDS, $ys_query);
 curl_setopt($ys_curl, CURLOPT_RETURNTRANSFER, true); 

 //--- Ejecutar POST
 $ys_retval = curl_exec($ys_curl);

 //--- Cerrar conexi�n
 curl_close($ys_curl);

 //--- Devuelve en formato JSON
 return json_decode($ys_retval, true);
 }


//**************************************************
//                 LISTAR JUEGOS
//**************************************************
function ys_ListGames($ys_in_page=1,
                      $ys_in_perPage=1,
                      $ys_st_p = ""
                     )
 {
 //--- GET
 //--- VALORES DEVUELTOS
 //--- >> {
 //        games
 //--- >>   [999999
 //--- >>    {
 //           id  : (ST) Identificador del juego.
 //           g   : (ST) Nombre del juego.
 //           b   : (ST) Marca (Brand)
 //           p   : (ST) ?????
 //           c   : (ST) C�digo de la Categor�a a la que pertenece el juego.
 //           m   : (ST) mb: M�vil / wb: Web
 //           img : (ST) Ruta de la imagen.
 //--- >>    }
 //--- >>   ]
 //--- >>  pagination
 //--- >>   {
 //          page     : (IN) N�mero de p�gina actual.
 //          total    : (IN) Total general de juegos.
 //          perPage  : (IN) Cantidad de despliegues por p�gina.
 //          previous : (IN) Cantidad de p�ginas previas a la actual.
 //          next     : (IN) Cantidad de p�ginas restantes seg�n la paginaci�n actual.
 //--- >>   }
 //        isTotal     : (BO) Indica si se ha llegado al final del listado.
 //--- >> }

 //--- URL de API
 $ys_url = UNIVERSAL_API."/api/gamelist";

 //--- Par�metros de solicitud
 $ys_campos = [
              'page'     => $ys_in_page,
              'perPage'  => $ys_in_perPage,
              'p'        => $ys_st_p
              ];

 //--- Construir URL de los datos del GET
 $ys_query = "page=".$ys_in_page."&perPage=".$ys_in_perPage;

 //--- Ejecutar la llamada
 $ys_retval = file_get_contents($ys_url."?".$ys_query);

/*
 $ys_curl = curl_init();
 curl_setopt($ys_curl, CURLOPT_URL, $ys_url."?".$ys_query);
 curl_setopt($ys_curl, CURLOPT_CUSTOMREQUEST, "GET");
 curl_setopt($ys_curl, CURLOPT_RETURNTRANSFER, true);
 $ys_retval = curl_exec($ys_curl);
 curl_close($ys_curl);
*/

 //--- Devuelve en formato JSON
 return json_decode($ys_retval, true);
 }

//**************************************************
//                 LISTAR FILTROS
//**************************************************
function ys_ListFilters($ys_st_phrase="")
 {
 //--- GET
 //--- VALORES DEVUELTOS
 //--- >> {
 //        brands (AR)
 //--- >>   [
 //--- >>    {
 //           id       : (ST) Nombre completo identificador de la marca (Proveedor).
 //           name     : (ST) Nombre de la marca (Proveedor).
 //           category : (ST) C�digo de la Categor�a a la que pertenece el proveedor.
 //           img      : (ST) Ruta de la imagen.
 //--- >>    }
 //--- >>   ]
 //--- >>  categories
 //--- >>   [
 //--- >>    {
 //           id       : (ST) C�digo de la Categor�a.
 //           name     : (ST) Nombre de la Categor�a.
 //           position : (IN) Posici�n en la lista.
 //--- >>    }
 //--- >>   ]
 //--- >> }

 //--- URL de API
 $ys_url = UNIVERSAL_API."/api/gamefilter";

 //--- Par�metros de solicitud
 $ys_campos = [
              "http" =>
               [
               "header" => "phrase:".$ys_st_phrase
               ]
              ];

 $ys_context = stream_context_create($ys_campos);

 //--- Ejecutar la llamada con el Header
 $ys_retval = file_get_contents($ys_url, false, $ys_context);
 
 return json_decode($ys_retval, true);
 }


//**************************************************
//                  CREAR USUARIO
//**************************************************
function ys_CreateUser($ys_st_username,
                       $ys_st_firstname,
                       $ys_st_lastname,
                       $ys_st_country,
                       $ys_st_currency,
                       $ys_fl_balance,
                       $ys_st_gender
                       )
 {
 //--- POST - Enviar JSON.
 //--- VALORES DEVUELTOS - Recibir JSON.
 //--- >> {
 //        status : (IN) 1/0 Estado de operaci�n correcta/incorrecta.
 //        err    : (OB) Descripci�n del error.
 //--- >>  val
 //--- >>   {
 //          user    : (ST) Nombre del usuario.
 //          balance : (ST) Balance actual.
 //--- >>    }
 //--- >>   ]
 //--- >> }


 //--- URL de API
 $ys_url = UNIVERSAL_API."/api/createuser";

 //--- Par�metros de solicitud
 $ys_campos = [
              'username'  => $ys_st_username,
              'firstname' => $ys_st_firstname,
              'lastname'  => $ys_st_lastname,
              'country'   => $ys_st_country,
              'currency'  => $ys_st_currency,
              'balance'   => $ys_fl_balance,
              'gender'    => $ys_st_gender
              ];

 //--- Codificar JSON.
 $ys_query = json_encode($ys_campos, true);

 //--- Abrir conexi�n
 $ys_curl = curl_init();

 //--- Enviar la URL y los datos
 curl_setopt($ys_curl, CURLOPT_URL, $ys_url);
 curl_setopt($ys_curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
 curl_setopt($ys_curl, CURLOPT_CUSTOMREQUEST, "POST");
 curl_setopt($ys_curl, CURLOPT_RETURNTRANSFER, true); 
 curl_setopt($ys_curl, CURLOPT_POSTFIELDS, $ys_query);

 //--- CURLOPT_RETURNTRANSFER devuelve el contenido del cURL en vez de desplegarlo
 curl_setopt($ys_curl, CURLOPT_RETURNTRANSFER, true); 

 //--- Ejecutar
 $ys_retval = curl_exec($ys_curl);

 //--- Cerrar conexi�n
 curl_close($ys_curl);

 return json_decode($ys_retval, true);
 }


//**************************************************
//                 CAMBIAR IP DE CLIENTE
//**************************************************
function ys_ChangeClientIP($ys_st_phrase="",
                           $ys_st_ip=""
                          )
 {
 //--- PUT
 //--- VALORES DEVUELTOS
 //--- >> {
 //        status : (IN) 1/0 Estado de operaci�n correcta/incorrecta.
 //        r
 //--- >>   {
 //--- >>   client: (ST) Nombre del Cliente
 //--- >>   }
 //--- >> }

 //--- URL de API
 $ys_url = UNIVERSAL_API."/api/client/security/address";

 //--- Par�metros de solicitud
 $ys_campos = [
              'phrase' => $ys_st_phrase,
              'ip'     => $ys_st_ip
              ];

 //--- Construir URL de los datos del POST
 $ys_query = json_encode($ys_campos, true);

 //--- Abrir conexi�n
 $ys_curl = curl_init();

 //--- Enviar la URL y los datos
 curl_setopt($ys_curl, CURLOPT_URL, $ys_url);
 curl_setopt($ys_curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
 curl_setopt($ys_curl, CURLOPT_CUSTOMREQUEST, "PUT");
 curl_setopt($ys_curl, CURLOPT_RETURNTRANSFER, true); 
 curl_setopt($ys_curl, CURLOPT_POSTFIELDS, $ys_query);

 //--- Ejecutar
 $ys_retval = curl_exec($ys_curl);

 //--- Cerrar conexi�n
 curl_close($ys_curl);

 //--- Devolver Array.
 return $ys_retval;
 }


//**************************************************
// INICIAR JUEGO
//**************************************************
function ys_LaunchGame($ys_st_sessionid="",
                       $ys_st_gameid="",
                       $ys_st_p="",
                       $ys_st_b="",
                       $ys_st_m=""
                       )
 {
 //--- GET
 //--- VALORES DEVUELTOS

 //--- URL de API
 $ys_url = UNIVERSAL_API."/api/launch";

 //--- Par�metros de solicitud
 $ys_campos = [
              'sessionid' => $ys_st_sessionid,
              'gameid'    => $ys_st_gameid,
              'p'         => $ys_st_p,
              'b'         => $ys_st_b,
              'm'         => $ys_st_m
              ];

 $ys_query = http_build_query($ys_campos);

 //--- Ejecutar la llamada con el Header
 $ys_retval = file_get_contents($ys_url."?".$ys_query);

 return $ys_retval;
 }



//**************************************************
// REPORTE DE TRANSACCIONES
//**************************************************
function ys_TransactionsReport($ys_st_phrase="",
                               $ys_st_page="",
                               $ys_st_perPage="",
                               $ys_st_username=""
                               )
 {
 //--- GET
 //--- VALORES DEVUELTOS
 //--- >> {
 //        data
 //--- >>   [
 //--- >>    {
 //           trxid        : (ST) Identificador de la transacci�n.
 //           movement     : (ST) Tipo de movimiento: WIN (+) / REFUND (-) / BET (-) �OTRO? ?????.
 //           referenceBet : (ST) Apuesta de referencia
 //           amount       : (FL) Monto del movimiento. Positivo o Negativo.
 //           date         : (ST) Fecha y hora. UNIX.
 //           gameid       : (ST) Identificador del juego.
 //           user         : (ST) Nombre del usuario..
 //--- >>    }
 //--- >>   ]

 //--- >>  pagination
 //--- >>   {
 //          page     : (IN) N�mero de p�gina actual.
 //          total    : (IN) Total general de transacciones.
 //          perPage  : (IN) Cantidad de despliegues por p�gina.
 //          previous : (IN) Cantidad de p�ginas previas a la actual.
 //          next     : (IN) Cantidad de p�ginas restantes seg�n la paginaci�n actual.
 //--- >>   }

 //        isTotal     : (BO) Indica si se ha llegado al final del listado.
 //--- >> }

 //--- URL de API
 $ys_url = UNIVERSAL_API."/api/client/transactions";

 //--- Par�metros de solicitud
 $ys_header = [
              "http" =>
               [
               "header" => "phrase:".$ys_st_phrase
               ]
              ];
 $ys_campos = [
              'page'     => $ys_st_page,
              'perPage'  => $ys_st_perPage,
              'username' => $ys_st_username
              ];

 $ys_context = stream_context_create($ys_header);
 $ys_query = http_build_query($ys_campos);

 //--- Ejecutar la llamada con el Header
 $ys_retval = file_get_contents($ys_url."?".$ys_query, false, $ys_context);
 
 return json_decode($ys_retval, true);
 }


function file_get_contents_curl($ys_url, $ys_headers="0")
 {
 $ys_curl = curl_init();
 curl_setopt($ys_curl, CURLOPT_HEADER, $ys_headers);
 curl_setopt($ys_curl, CURLOPT_URL, $ys_url);
 curl_setopt($ys_curl, CURLOPT_HTTPGET, true);
 curl_setopt($ys_curl, CURLOPT_PUT, false);
 curl_setopt($ys_curl, CURLOPT_POST, false);
 curl_setopt($ys_curl, CURLOPT_RETURNTRANSFER, true);
 $ys_retval = curl_exec($ch);
 curl_close($ch);
 return json_decode($ys_retval, true);
 }
?>
