id serial NOT NULL,
nombre character varying,
apellido character varying,
telefono character varying,
tipo_documento character varying,
documento character varying,
correo character varying,
fecha_nacimiento date,
usuario character varying,
contrasena character varying,
cod_pais character varying,
estado character varying,
id_banca character varying,
arroba character varying,
id_ca character varying,
server character varying,
estatus boolean DEFAULT true,
fecha_ingreso date DEFAULT ('now'::text)::date,
ultimo_ingreso date DEFAULT ('now'::text)::date,
activo boolean DEFAULT true,
moneda numeric,
token character varying DEFAULT ''::character varying,
plan_pago numeric NOT NULL DEFAULT 1, -- plan de pago
lim_pago numeric NOT NULL DEFAULT 0, -- limite de pago
saldo numeric(23,2) NOT NULL DEFAULT 0, -- saldo del cliente
direccion character varying DEFAULT ''::character varying,
ciudad character varying DEFAULT ''::character varying,
certificados boolean NOT NULL DEFAULT false, -- certificado
sino boolean NOT NULL DEFAULT false, -- sino
mon boolean NOT NULL DEFAULT false, -- monitorear
ultima_juga numeric DEFAULT (date_part('epoch'::text, now()))::integer,
tipo character varying DEFAULT 'W'::character varying
