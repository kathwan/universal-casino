CREATE TABLE IF NOT EXISTS y_usuarios
(
  USU_AutoNum int(7) UNSIGNED NOT NULL AUTO_INCREMENT,
  USU_Nombres char(50) COLLATE latin1_spanish_ci NOT NULL DEFAULT '',
  USU_Apellidos char(50) COLLATE latin1_spanish_ci NOT NULL DEFAULT '',
  USU_UserName char(100) COLLATE latin1_spanish_ci NOT NULL DEFAULT '',
  USU_Balance decimal(12,2) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (USU_AutoNum)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

INSERT INTO y_usuarios VALUES
(0, '', '', 'demo', 177.74),
(0, '', '', 'demo2', 9938),
(0, '', '', 'demo3', 2958),
(0, '', '', 'demo4', 188.1),
(0, '', '', 'demo5', 188.1),
(0, '', '', 'demo6', 60.47),
(0, '', '', 'demo7', 168.2),
(0, '', '', 'demo8', 86),
(0, '', '', 'demo9', 178),
(0, '', '', 'demo10', 2.2),
(0, '', '', 'demo11', 10000),
(0, '', '', 'demo12', 10000),
(0, '', '', 'demo13', 10000);
