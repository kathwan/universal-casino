CREATE TABLE IF NOT EXISTS y_games (
  GAM_AutoNum int(7) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Campo de control.',
  GAM_Codigo char(100) COLLATE latin1_spanish_ci NOT NULL DEFAULT '' COMMENT 'C?digo del juego.',
  GAM_Nombre char(100) COLLATE latin1_spanish_ci NOT NULL DEFAULT '' COMMENT 'Nombre del juego.',
  GAM_CodCat char(15) COLLATE latin1_spanish_ci NOT NULL DEFAULT '' COMMENT 'C?digo de Categor?a',
  GAM_CodBra char(20) COLLATE latin1_spanish_ci NOT NULL DEFAULT '' COMMENT 'C?digo de Brand (Proveedor).',
  GAM_Control char(5) COLLATE latin1_spanish_ci NOT NULL DEFAULT '' COMMENT 'Par?metro de control.',
  GAM_Modo char(5) COLLATE latin1_spanish_ci NOT NULL DEFAULT '' COMMENT 'Modo wb: Web / mb: M?vil',
  GAM_Tipo char(5) COLLATE latin1_spanish_ci NOT NULL DEFAULT '' COMMENT 'Tipo de juego. Tomado de y_games_t',
  GAM_Imagen char(100) COLLATE latin1_spanish_ci NOT NULL DEFAULT '' COMMENT 'Ruta de la imagen de identificaci?n.',
  GAM_Fecha char(8) COLLATE latin1_spanish_ci NOT NULL DEFAULT '' COMMENT 'Fecha de ingreso al listado de juegos.',
  GAM_Clicks int(7) unsigned NOT NULL DEFAULT 0 COMMENT 'Cantidad de Clicks registrados a este Game.',
  GAM_Favs int(7) unsigned NOT NULL DEFAULT 0 COMMENT 'Cantidad de Favs registrados a este Game.',
  GAM_Nue tinyint(1) unsigned NOT NULL DEFAULT 0 COMMENT 'Indica si el juego es Nuevo (1) o No lo es (0).',
  GAM_Pop tinyint(1) unsigned NOT NULL DEFAULT 0 COMMENT 'Indica si el juego es Popular (1) o No lo es (0).',
  GAM_Activo tinyint(1) unsigned NOT NULL DEFAULT 1 COMMENT 'Indica si est? Activo (1) o Inactivo (0).',
  GAM_EnAPI tinyint(1) unsigned NOT NULL DEFAULT 1 COMMENT 'Indica si el Game existe o no en la API.',
  PRIMARY KEY (GAM_AutoNum),
  KEY GAM_CBGM_T (GAM_CodCat,GAM_CodBra,GAM_Codigo,GAM_Modo,GAM_Tipo)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci ROW_FORMAT=FIXED COMMENT='Datos generales de los juegos.';

INSERT INTO y_games VALUES
(1, 'spinsane-gameview_netenthtml', 'Spinsane', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_spinsane.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2, 'sweetyhoneyfruity-gameview_netenthtml', 'Sweety Honey Fruity', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_sweetyhoneyfruity.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3, 'twinhappiness-gameview_netenthtml', 'Twin Happiness', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_twinhappiness.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4, 'victoriousmax-gameview_netenthtml', 'Victorious Max', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_victoriousmax.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(5, 'vikings-gameview_netenthtml', 'Vikings', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_vikings.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(6, 'wingsofriches-gameview_netenthtml', 'Wings of Riches', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_wingsofriches.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(7, 'admiral-gameview_amatic_mobile', 'Admiral Nelson', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_admiral.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(8, 'bellsonfirerombo-gameview_amatic_mobile', 'Bells on Fire Rombo', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_bellsonfirerombo.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(9, 'bigpanda-gameview_amatic_mobile', 'Big Panda', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_bigpanda.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(10, 'billyonaire-gameview_amatic_mobile', 'Billyonaire', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_billyonaire.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(11, 'casinova-gameview_amatic_mobile', 'Casinova', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_casinova.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(12, 'cooldiamondsii-gameview_amatic_mobile', 'Cool Diamonds II', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_cooldiamondsii.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(13, 'dragonskingdom-gameview_amatic_mobile', 'Dragons Kingdom', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_dragonskingdom.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(14, 'eyeofra-gameview_amatic_mobile', 'Eye of Ra', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_eyeofra.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(15, 'goldenbook-gameview_amatic_mobile', 'Golden Book', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_goldenbook.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(16, 'grandtiger-gameview_amatic_mobile', 'Grand Tiger', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_grandtiger.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(17, 'grandx-gameview_amatic_mobile', 'GrandX', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_grandx.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(18, 'hot81-gameview_amatic_mobile', 'Hot 81', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_hot81.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(19, 'hotdiamonds-gameview_amatic_mobile', 'Hot Diamonds', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_hotdiamonds.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(20, 'ladyjoker-gameview_amatic_mobile', 'Lady Joker', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_ladyjoker.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(21, 'redchilli-gameview_amatic_mobile', 'Red Chilli', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_redchilli.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(22, 'rouletteroyal-gameview_amatic_mobile', 'Roulette Royal', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_rouletteroyal.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(23, 'royalunicorn-gameview_amatic_mobile', 'Royal Unicorn', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_royalunicorn.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(24, 'twentyseven-gameview_amatic_mobile', 'Hot 27', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_twentyseven.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(25, 'africansimba-gameview_novomatic', 'African Simba', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_africansimba.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(26, 'barsandsevens-gameview_novomatic', 'Bars & Sevens', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_barsandsevens.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(27, 'captainventure-gameview_novomatic', 'Captain Venture', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_captainventure.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(28, 'firebird-gameview_novomatic', 'Firebird', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_firebird.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(29, 'hypnohippo-gameview_novomatic', 'Hypno Hippo', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_hypnohippo.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(30, 'luckyladyscharmdeluxe6-gameview_novomatic', 'Lucky Ladys Charm Deluxe 6', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_luckyladyscharmdeluxe6.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(31, 'mysticsecrets-gameview_novomatic', 'Mystic Secrets', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_mysticsecrets.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(32, 'oceantale-gameview_novomatic', 'Ocean Tale', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_oceantale.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(33, 'queenofheartsdeluxe-gameview_novomatic', 'Queen of Hearts Deluxe', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_queenofheartsdeluxe.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(34, 'redhot40-gameview_novomatic', 'Red Hot 40', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_redhot40.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(35, 'redhotburning-gameview_novomatic', 'Red Hot Burning', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_redhotburning.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(36, 'redhotfruits-gameview_novomatic', 'Red Hot Fruits', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_redhotfruits.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(37, 'royaldynasty-gameview_novomatic', 'Royal Dynasty', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_royaldynasty.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(38, 'secretelixir-gameview_novomatic', 'Secret Elixir', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_secretelixir.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(39, 'seven-gameview_novomatic', 'Sevens', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_seven.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(40, 'xtrahot-gameview_novomatic', 'Xtra Hot', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_xtrahot.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(41, '529-gameview_egt_mobile', '40 Burning Hot', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_529.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(42, '532-gameview_egt_mobile', '5 Burning Heart', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_532.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(43, '533-gameview_egt_mobile', '10 Burning Heart', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_533.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(44, '536-gameview_egt_mobile', '40 Burning Dice', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_536.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(45, '548-gameview_egt_mobile', '100 Burning Hot', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_548.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(46, 'admiral-gameview_amatic_mobile', 'Admiral Nelson', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_admiral.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(47, 'africansimba-gameview_novomatic', 'African Simba', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_africansimba.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(48, 'barsandsevens-gameview_novomatic', 'Bars & Sevens', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_barsandsevens.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(49, 'bellsonfirerombo-gameview_amatic_mobile', 'Bells on Fire Rombo', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_bellsonfirerombo.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(50, 'bigpanda-gameview_amatic_mobile', 'Big Panda', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_bigpanda.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(51, 'billyonaire-gameview_amatic_mobile', 'Billyonaire', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_billyonaire.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(52, 'captainventure-gameview_novomatic', 'Captain Venture', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_captainventure.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(53, 'cashomatic-gameview_netenthtml', 'Cash-o-Matic', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_cashomatic.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(54, 'casinova-gameview_amatic_mobile', 'Casinova', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_casinova.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(55, 'cooldiamondsii-gameview_amatic_mobile', 'Cool Diamonds II', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_cooldiamondsii.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(56, 'deadoralive2-gameview_netenthtml', 'Dead or Alive 2', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_deadoralive2.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(57, 'dragonskingdom-gameview_amatic_mobile', 'Dragons Kingdom', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_dragonskingdom.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(58, 'eyeofra-gameview_amatic_mobile', 'Eye of Ra', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_eyeofra.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(59, 'firebird-gameview_novomatic', 'Firebird', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_firebird.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(60, 'goldenbook-gameview_amatic_mobile', 'Golden Book', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_goldenbook.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(61, 'grandspinn-gameview_netenthtml', 'Grand Spinn', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_grandspinn.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(62, 'grandtiger-gameview_amatic_mobile', 'Grand Tiger', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_grandtiger.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(63, 'grandx-gameview_amatic_mobile', 'GrandX', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_grandx.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(64, 'hot81-gameview_amatic_mobile', 'Hot 81', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_hot81.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(65, 'hotdiamonds-gameview_amatic_mobile', 'Hot Diamonds', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_hotdiamonds.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(66, 'hypnohippo-gameview_novomatic', 'Hypno Hippo', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_hypnohippo.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(67, 'jackandthebeanstalk_touch-gameview_netenthtml', 'Jack and the Beanstalk Touch', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_jackandthebeanstalk_touch.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(68, 'ladyjoker-gameview_amatic_mobile', 'Lady Joker', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_ladyjoker.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(69, 'luckyladyscharmdeluxe6-gameview_novomatic', 'Lucky Ladys Charm Deluxe 6', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_luckyladyscharmdeluxe6.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(70, 'magicportals_touch-gameview_netenthtml', 'Magic Portals Touch', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_magicportals_touch.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(71, 'mysticsecrets-gameview_novomatic', 'Mystic Secrets', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_mysticsecrets.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(72, 'mythicmaiden_touch-gameview_netenthtml', 'Mythic Maiden Touch', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_mythicmaiden_touch.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(73, 'narcos-gameview_netenthtml', 'Narcos', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_narcos.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(74, 'oceantale-gameview_novomatic', 'Ocean Tale', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_oceantale.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(75, 'queenofheartsdeluxe-gameview_novomatic', 'Queen of Hearts Deluxe', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_queenofheartsdeluxe.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(76, 'redchilli-gameview_amatic_mobile', 'Red Chilli', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_redchilli.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(77, 'redhot40-gameview_novomatic', 'Red Hot 40', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_redhot40.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(78, 'redhotburning-gameview_novomatic', 'Red Hot Burning', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_redhotburning.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(79, 'redhotfruits-gameview_novomatic', 'Red Hot Fruits', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_redhotfruits.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(80, 'rouletteroyal-gameview_amatic_mobile', 'Roulette Royal', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_rouletteroyal.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(81, 'royaldynasty-gameview_novomatic', 'Royal Dynasty', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_royaldynasty.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(82, 'royalunicorn-gameview_amatic_mobile', 'Royal Unicorn', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_royalunicorn.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(83, 'secretelixir-gameview_novomatic', 'Secret Elixir', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_secretelixir.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(84, 'seven-gameview_novomatic', 'Sevens', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_seven.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(85, 'spinsane-gameview_netenthtml', 'Spinsane', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_spinsane.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(86, 'sweetyhoneyfruity-gameview_netenthtml', 'Sweety Honey Fruity', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_sweetyhoneyfruity.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(87, 'twentyseven-gameview_amatic_mobile', 'Hot 27', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_twentyseven.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(88, 'twinhappiness-gameview_netenthtml', 'Twin Happiness', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_twinhappiness.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(89, 'victorious_touch-gameview_netenthtml', 'Victorious Touch', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_victorious_touch.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(90, 'victoriousmax-gameview_netenthtml', 'Victorious Max', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_victoriousmax.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(91, 'vikings-gameview_netenthtml', 'Vikings', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_vikings.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(92, 'wingsofriches-gameview_netenthtml', 'Wings of Riches', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_wingsofriches.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(93, 'xtrahot-gameview_novomatic', 'Xtra Hot', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_xtrahot.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(94, 'cashomatic-gameview_netenthtml', 'Cash-o-Matic', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_cashomatic.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(95, 'deadoralive2-gameview_netenthtml', 'Dead or Alive 2', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_deadoralive2.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(96, 'grandspinn-gameview_netenthtml', 'Grand Spinn', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_grandspinn.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(97, 'narcos-gameview_netenthtml', 'Narcos', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_narcos.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(98, 'platipus-luckydolphin', 'Lucky Dolphin', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-luckydolphin.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(99, 'spadegaming-mr-chu-tycoon', 'Mr Chu Tycoon', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-mr-chu-tycoon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(100, 'wazdan-365', 'Beauty Fruity', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-365.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(101, 'evoplay-football', 'Football', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-football.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(102, 'redrake-mother-of-horus', 'Mother of Horus', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-mother-of-horus.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(103, 'hub88-age-of-ice-dragons', 'Age of Ice Dragons', 'slot', 'Kalamba', 'ms', 'mb', '', 'img/mrslotty/hub88-age-of-ice-dragons.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(104, 'slotexchange-death-of-gods', 'Death of gods', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-death-of-gods.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(105, 'liw-Special2', 'First ball color (Win 5/20)', 'lottery', 'LiW', 'ms', 'mb', '', 'img/mrslotty/liw-Special2.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(106, 'simpleplay-cai-shen-dao', 'Cai Shen Dao', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-cai-shen-dao.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(107, 'platipus-luckydolphin', 'Lucky Dolphin', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-luckydolphin.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(108, 'spadegaming-mr-chu-tycoon', 'Mr Chu Tycoon', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-mr-chu-tycoon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(109, 'wazdan-365', 'Beauty Fruity', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-365.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(110, 'evoplay-football', 'Football', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-football.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(111, 'redrake-mother-of-horus', 'Mother of Horus', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-mother-of-horus.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(112, 'hub88-age-of-ice-dragons', 'Age of Ice Dragons', 'slot', 'Kalamba', 'ms', 'wb', '', 'img/mrslotty/hub88-age-of-ice-dragons.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(113, 'slotexchange-death-of-gods', 'Death of gods', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-death-of-gods.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(114, 'liw-Special2', 'First ball color (Win 5/20)', 'lottery', 'LiW', 'ms', 'wb', '', 'img/mrslotty/liw-Special2.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(115, 'simpleplay-cai-shen-dao', 'Cai Shen Dao', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-cai-shen-dao.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(116, 'platipus-richywitchy', 'Richy Witchy', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-richywitchy.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(117, 'spadegaming-fishing-god', 'Fishing God', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-fishing-god.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(118, 'wazdan-366', 'Space Gem', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-366.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(119, 'evoplay-legend-of-ra', 'Legend of Ra', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-legend-of-ra.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(120, 'redrake-cai-shen-88', 'Cai Shen 88', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-cai-shen-88.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(121, 'hub88-atlantis-thunder', 'Atlantis Thunder', 'slot', 'Kalamba', 'ms', 'mb', '', 'img/mrslotty/hub88-atlantis-thunder.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(122, 'slotexchange-anger-of-zeus', 'Anger of Zeus', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-anger-of-zeus.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(123, 'liw-Lotto520', 'Win 5/20', 'lottery', 'LiW', 'ms', 'mb', '', 'img/mrslotty/liw-Lotto520.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(124, 'simpleplay-the-masked-prince', 'The Masked Prince', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-the-masked-prince.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(125, 'platipus-richywitchy', 'Richy Witchy', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-richywitchy.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(126, 'spadegaming-fishing-god', 'Fishing God', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-fishing-god.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(127, 'wazdan-366', 'Space Gem', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-366.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(128, 'evoplay-legend-of-ra', 'Legend of Ra', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-legend-of-ra.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(129, 'redrake-cai-shen-88', 'Cai Shen 88', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-cai-shen-88.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(130, 'hub88-atlantis-thunder', 'Atlantis Thunder', 'slot', 'Kalamba', 'ms', 'wb', '', 'img/mrslotty/hub88-atlantis-thunder.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(131, 'slotexchange-anger-of-zeus', 'Anger of Zeus', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-anger-of-zeus.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(132, 'liw-Lotto520', 'Win 5/20', 'lottery', 'LiW', 'ms', 'wb', '', 'img/mrslotty/liw-Lotto520.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(133, 'simpleplay-the-masked-prince', 'The Masked Prince', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-the-masked-prince.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(134, 'platipus-magicalwolf', 'Magical Wolf', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-magicalwolf.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(135, 'spadegaming-brothers-kingdom', 'Brothers Kingdom', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-brothers-kingdom.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(136, 'wazdan-367', 'Larry the Leprechaun', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-367.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(137, 'evoplay-the-emperors-tomb', 'The Emperor\'s Tomb', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-the-emperors-tomb.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(138, 'redrake-mata-hari-the-spy', 'Mata Hari: the spy', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-mata-hari-the-spy.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(139, 'hub88-joker-supreme', 'Joker Supreme', 'slot', 'Kalamba', 'ms', 'mb', '', 'img/mrslotty/hub88-joker-supreme.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(140, 'slotexchange-treasure-of-amazons', 'Treasure of Amazons', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-treasure-of-amazons.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(141, 'liw-Lotto1020', 'Win 10/20', 'lottery', 'LiW', 'ms', 'mb', '', 'img/mrslotty/liw-Lotto1020.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(142, 'simpleplay-shanghai-godfather', 'Shanghai Godfather', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-shanghai-godfather.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(143, 'platipus-magicalwolf', 'Magical Wolf', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-magicalwolf.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(144, 'spadegaming-brothers-kingdom', 'Brothers Kingdom', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-brothers-kingdom.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(145, 'wazdan-367', 'Larry the Leprechaun', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-367.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(146, 'evoplay-the-emperors-tomb', 'The Emperor\'s Tomb', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-the-emperors-tomb.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(147, 'redrake-mata-hari-the-spy', 'Mata Hari: the spy', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-mata-hari-the-spy.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(148, 'hub88-joker-supreme', 'Joker Supreme', 'slot', 'Kalamba', 'ms', 'wb', '', 'img/mrslotty/hub88-joker-supreme.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(149, 'slotexchange-treasure-of-amazons', 'Treasure of Amazons', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-treasure-of-amazons.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(150, 'liw-Lotto1020', 'Win 10/20', 'lottery', 'LiW', 'ms', 'wb', '', 'img/mrslotty/liw-Lotto1020.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(151, 'simpleplay-shanghai-godfather', 'Shanghai Godfather', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-shanghai-godfather.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(152, 'platipus-mistressofamazon', 'Mistress of Amazon', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-mistressofamazon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(153, 'spadegaming-golden-fist', 'Golden Fist', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-golden-fist.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(154, 'wazdan-368', 'Magic Stars 6', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-368.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(155, 'evoplay-necromancer', 'Necromancer', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-necromancer.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(156, 'redrake-million-7', 'Million 7', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-million-7.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(157, 'hub88-mermaids-galore', 'Mermaids Galore', 'slot', 'Kalamba', 'ms', 'mb', '', 'img/mrslotty/hub88-mermaids-galore.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(158, 'slotexchange-gold-of-egypt', 'Gold of Egypt', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-gold-of-egypt.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(159, 'liw-Lotto137', 'Drawn number (1/37)', 'lottery', 'LiW', 'ms', 'mb', '', 'img/mrslotty/liw-Lotto137.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(160, 'simpleplay-double-happiness', 'Double Happiness', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-double-happiness.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(161, 'platipus-mistressofamazon', 'Mistress of Amazon', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-mistressofamazon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(162, 'spadegaming-golden-fist', 'Golden Fist', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-golden-fist.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(163, 'wazdan-368', 'Magic Stars 6', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-368.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(164, 'evoplay-necromancer', 'Necromancer', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-necromancer.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(165, 'redrake-million-7', 'Million 7', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-million-7.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(166, 'hub88-mermaids-galore', 'Mermaids Galore', 'slot', 'Kalamba', 'ms', 'wb', '', 'img/mrslotty/hub88-mermaids-galore.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(167, 'slotexchange-gold-of-egypt', 'Gold of Egypt', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-gold-of-egypt.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(168, 'liw-Lotto137', 'Drawn number (1/37)', 'lottery', 'LiW', 'ms', 'wb', '', 'img/mrslotty/liw-Lotto137.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(169, 'simpleplay-double-happiness', 'Double Happiness', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-double-happiness.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(170, 'platipus-arabiantales', 'Arabian Tales', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-arabiantales.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(171, 'spadegaming-candy-pop', 'Candy Pop', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-candy-pop.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(172, 'wazdan-369', 'Lucky Reels', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-369.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(173, 'evoplay-naughty-girls-cabaret', 'Naughty Girls Cabaret', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-naughty-girls-cabaret.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(174, 'redrake-the-adventures-of-ali-baba', 'The adventures of Ali Baba', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-the-adventures-of-ali-baba.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(175, 'hub88-tree-of-gold', 'Tree of Gold', 'slot', 'Kalamba', 'ms', 'mb', '', 'img/mrslotty/hub88-tree-of-gold.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(176, 'slotexchange-magical-age', 'Magical age', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-magical-age.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(177, 'liw-Lotto580', 'Win 5/80', 'lottery', 'LiW', 'ms', 'mb', '', 'img/mrslotty/liw-Lotto580.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(178, 'simpleplay-world-cup-fever', 'World Cup Fever', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-world-cup-fever.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(179, 'platipus-arabiantales', 'Arabian Tales', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-arabiantales.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(180, 'spadegaming-candy-pop', 'Candy Pop', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-candy-pop.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(181, 'wazdan-369', 'Lucky Reels', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-369.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(182, 'evoplay-naughty-girls-cabaret', 'Naughty Girls Cabaret', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-naughty-girls-cabaret.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(183, 'redrake-the-adventures-of-ali-baba', 'The adventures of Ali Baba', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-the-adventures-of-ali-baba.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(184, 'hub88-tree-of-gold', 'Tree of Gold', 'slot', 'Kalamba', 'ms', 'wb', '', 'img/mrslotty/hub88-tree-of-gold.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(185, 'slotexchange-magical-age', 'Magical age', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-magical-age.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(186, 'liw-Lotto580', 'Win 5/80', 'lottery', 'LiW', 'ms', 'wb', '', 'img/mrslotty/liw-Lotto580.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(187, 'simpleplay-world-cup-fever', 'World Cup Fever', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-world-cup-fever.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(188, 'platipus-junglespin', 'Jungle Spin', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-junglespin.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(189, 'spadegaming-gangster-axe', 'Gangster Axe', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-gangster-axe.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(190, 'wazdan-371', 'Dragons Lucky 8', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-371.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(191, 'evoplay-atlantis', 'Atlantis', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-atlantis.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(192, 'redrake-super-12-stars', 'Super 12 Stars', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-super-12-stars.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(193, 'hub88-cutey-cats', 'Cutey Cats', 'slot', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-cutey-cats.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(194, 'slotexchange-viking-queen', 'Viking queen', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-viking-queen.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(195, 'liw-Lotto1080', 'Win 10/80', 'lottery', 'LiW', 'ms', 'mb', '', 'img/mrslotty/liw-Lotto1080.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(196, 'simpleplay-dragon-8', 'Dragon 8', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-dragon-8.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(197, 'platipus-junglespin', 'Jungle Spin', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-junglespin.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(198, 'spadegaming-gangster-axe', 'Gangster Axe', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-gangster-axe.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(199, 'wazdan-371', 'Dragons Lucky 8', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-371.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(200, 'evoplay-atlantis', 'Atlantis', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-atlantis.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(201, 'redrake-super-12-stars', 'Super 12 Stars', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-super-12-stars.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(202, 'hub88-cutey-cats', 'Cutey Cats', 'slot', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-cutey-cats.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(203, 'slotexchange-viking-queen', 'Viking queen', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-viking-queen.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(204, 'liw-Lotto1080', 'Win 10/80', 'lottery', 'LiW', 'ms', 'wb', '', 'img/mrslotty/liw-Lotto1080.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(205, 'simpleplay-dragon-8', 'Dragon 8', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-dragon-8.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(206, 'platipus-fruitysevens', 'Fruity Sevens', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-fruitysevens.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(207, 'spadegaming-fafafa2', 'FaFaFa2', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-fafafa2.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(208, 'wazdan-372', 'Relic Hunters and the Book of Faith', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-372.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(209, 'evoplay-epic-gladiators', 'Epic Gladiators', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-epic-gladiators.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(210, 'redrake-the-travels-of-marco', 'The Travels of Marco', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-the-travels-of-marco.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(211, 'hub88-hallowinner', 'Hallowinner', 'slot', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-hallowinner.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(212, 'slotexchange-drive-wheels', 'Drive wheels', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-drive-wheels.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(213, 'liw-Lotto2080', 'Win 20/80', 'lottery', 'LiW', 'ms', 'mb', '', 'img/mrslotty/liw-Lotto2080.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(214, 'simpleplay-child-of-wealth', 'Child of Wealth', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-child-of-wealth.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(215, 'platipus-fruitysevens', 'Fruity Sevens', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-fruitysevens.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(216, 'spadegaming-fafafa2', 'FaFaFa2', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-fafafa2.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(217, 'wazdan-372', 'Relic Hunters and the Book of Faith', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-372.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(218, 'evoplay-epic-gladiators', 'Epic Gladiators', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-epic-gladiators.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(219, 'redrake-the-travels-of-marco', 'The Travels of Marco', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-the-travels-of-marco.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(220, 'hub88-hallowinner', 'Hallowinner', 'slot', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-hallowinner.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(221, 'slotexchange-drive-wheels', 'Drive wheels', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-drive-wheels.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(222, 'liw-Lotto2080', 'Win 20/80', 'lottery', 'LiW', 'ms', 'wb', '', 'img/mrslotty/liw-Lotto2080.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(223, 'simpleplay-child-of-wealth', 'Child of Wealth', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-child-of-wealth.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(224, 'platipus-juicyspins', 'Juicy Spins', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-juicyspins.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(225, 'spadegaming-princess-wang', 'Princess Wang', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-princess-wang.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(226, 'wazdan-373', 'Neon City', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-373.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(227, 'evoplay-sindbad', 'Sindbad', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-sindbad.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(228, 'redrake-knights', 'Knights', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-knights.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(229, 'hub88-wildcraft', 'Wildcraft', 'slot', 'Kalamba', 'ms', 'mb', '', 'img/mrslotty/hub88-wildcraft.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(230, 'play-pearls-zodiac_signs', 'Zodiac Signs', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-zodiac_signs.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(231, 'liw-Lotto3080', 'Win 30/80', 'lottery', 'LiW', 'ms', 'mb', '', 'img/mrslotty/liw-Lotto3080.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(232, 'simpleplay-fortune-lion', 'Fortune Lion', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-fortune-lion.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(233, 'platipus-juicyspins', 'Juicy Spins', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-juicyspins.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(234, 'spadegaming-princess-wang', 'Princess Wang', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-princess-wang.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(235, 'wazdan-373', 'Neon City', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-373.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(236, 'evoplay-sindbad', 'Sindbad', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-sindbad.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(237, 'redrake-knights', 'Knights', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-knights.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(238, 'hub88-wildcraft', 'Wildcraft', 'slot', 'Kalamba', 'ms', 'wb', '', 'img/mrslotty/hub88-wildcraft.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(239, 'play-pearls-zodiac_signs', 'Zodiac Signs', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-zodiac_signs.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(240, 'liw-Lotto3080', 'Win 30/80', 'lottery', 'LiW', 'ms', 'wb', '', 'img/mrslotty/liw-Lotto3080.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(241, 'simpleplay-fortune-lion', 'Fortune Lion', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-fortune-lion.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(242, 'platipus-crocoman', 'Crocoman', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-crocoman.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(243, 'spadegaming-wow-prosperity', 'Wow Prosperity', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-wow-prosperity.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(244, 'wazdan-378', 'Magic Stars 9', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-378.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(245, 'evoplay-totem-island', 'Totem Island', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-totem-island.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(246, 'redrake-bonnie-clyde', 'Bonnie & Clyde', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-bonnie-clyde.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(247, 'hub88-wild-fruit', 'Wild Fruit', 'slot', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-wild-fruit.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(248, 'play-pearls-wild_west', 'Wild West', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-wild_west.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(249, 'liw-Lotto4080', 'Win 40/80', 'lottery', 'LiW', 'ms', 'mb', '', 'img/mrslotty/liw-Lotto4080.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(250, 'simpleplay-lucky-fa', 'Lucky Fa', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-lucky-fa.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(251, 'platipus-crocoman', 'Crocoman', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-crocoman.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(252, 'spadegaming-wow-prosperity', 'Wow Prosperity', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-wow-prosperity.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(253, 'wazdan-378', 'Magic Stars 9', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-378.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(254, 'evoplay-totem-island', 'Totem Island', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-totem-island.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(255, 'redrake-bonnie-clyde', 'Bonnie & Clyde', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-bonnie-clyde.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(256, 'hub88-wild-fruit', 'Wild Fruit', 'slot', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-wild-fruit.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(257, 'play-pearls-wild_west', 'Wild West', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-wild_west.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(258, 'liw-Lotto4080', 'Win 40/80', 'lottery', 'LiW', 'ms', 'wb', '', 'img/mrslotty/liw-Lotto4080.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(259, 'simpleplay-lucky-fa', 'Lucky Fa', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-lucky-fa.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(260, 'platipus-safariadventures', 'Safari Adventures', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-safariadventures.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(261, 'spadegaming-jungle-king', 'Jungle King', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-jungle-king.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(262, 'wazdan-379', 'Butterfly Lovers', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-379.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(263, 'evoplay-robinson', 'Robinson', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-robinson.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(264, 'redrake-tiger-and-dragon', 'Tiger and Dragon', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-tiger-and-dragon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(265, 'hub88-hong-bao', 'Hong Bao', 'slot', 'Kalamba', 'ms', 'mb', '', 'img/mrslotty/hub88-hong-bao.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(266, 'play-pearls-wild_school', 'Wild School - Hentai', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-wild_school.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(267, 'liw-Lotto5080', 'Win 50/80', 'lottery', 'LiW', 'ms', 'mb', '', 'img/mrslotty/liw-Lotto5080.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(268, 'simpleplay-super-7', 'Super 7', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-super-7.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(269, 'platipus-safariadventures', 'Safari Adventures', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-safariadventures.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(270, 'spadegaming-jungle-king', 'Jungle King', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-jungle-king.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(271, 'wazdan-379', 'Butterfly Lovers', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-379.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(272, 'evoplay-robinson', 'Robinson', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-robinson.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(273, 'redrake-tiger-and-dragon', 'Tiger and Dragon', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-tiger-and-dragon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(274, 'hub88-hong-bao', 'Hong Bao', 'slot', 'Kalamba', 'ms', 'wb', '', 'img/mrslotty/hub88-hong-bao.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(275, 'play-pearls-wild_school', 'Wild School - Hentai', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-wild_school.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(276, 'liw-Lotto5080', 'Win 50/80', 'lottery', 'LiW', 'ms', 'wb', '', 'img/mrslotty/liw-Lotto5080.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(277, 'simpleplay-super-7', 'Super 7', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-super-7.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(278, 'platipus-fairyforest', 'Fairy Forest', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-fairyforest.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(279, 'spadegaming-pan-fairy', 'Pan Fairy', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-pan-fairy.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(280, 'wazdan-513', 'Joker Poker', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-513.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(281, 'evoplay-candy-dreams', 'Candy Dreams', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-candy-dreams.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(282, 'redrake-magic-wilds', 'Magic Wilds', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-magic-wilds.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(283, 'hub88-doctor-electro', 'Doctor Electro', 'slot', 'Kalamba', 'ms', 'mb', '', 'img/mrslotty/hub88-doctor-electro.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(284, 'play-pearls-wild_school_soft', 'Wild School', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-wild_school_soft.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(285, 'liw-Keno580', 'Keno 5/80', 'lottery', 'LiW', 'ms', 'mb', '', 'img/mrslotty/liw-Keno580.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(286, 'simpleplay-fortune-cat', 'Fortune Cat', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-fortune-cat.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(287, 'platipus-fairyforest', 'Fairy Forest', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-fairyforest.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(288, 'spadegaming-pan-fairy', 'Pan Fairy', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-pan-fairy.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(289, 'wazdan-513', 'Joker Poker', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-513.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(290, 'evoplay-candy-dreams', 'Candy Dreams', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-candy-dreams.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(291, 'redrake-magic-wilds', 'Magic Wilds', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-magic-wilds.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(292, 'hub88-doctor-electro', 'Doctor Electro', 'slot', 'Kalamba', 'ms', 'wb', '', 'img/mrslotty/hub88-doctor-electro.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(293, 'play-pearls-wild_school_soft', 'Wild School', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-wild_school_soft.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(294, 'liw-Keno580', 'Keno 5/80', 'lottery', 'LiW', 'ms', 'wb', '', 'img/mrslotty/liw-Keno580.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(295, 'simpleplay-fortune-cat', 'Fortune Cat', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-fortune-cat.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(296, 'platipus-princessofbirds', 'Princess of Birds', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-princessofbirds.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(297, 'spadegaming-shanghai-008', 'ShangHai 008', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-shanghai-008.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(298, 'wazdan-514', 'Turbo Poker', 'slot', 'Wazdan', 'ms', 'mb', 'JUME', 'img/mrslotty/wazdan-514.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(299, 'evoplay-roll-the-dice', 'Roll The Dice', 'slot', 'Evoplay', 'ms', 'mb', 'JUME', 'img/mrslotty/evoplay-roll-the-dice.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(300, 'redrake-maya', 'Maya', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-maya.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(301, 'hub88-midas-treasure', 'Midas Treasure', 'slot', 'Kalamba', 'ms', 'mb', '', 'img/mrslotty/hub88-midas-treasure.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(302, 'play-pearls-wild_leprechaun', 'Wild Leprechaun', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-wild_leprechaun.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(303, 'liw-Keno1080', 'Keno 10/80', 'lottery', 'LiW', 'ms', 'mb', '', 'img/mrslotty/liw-Keno1080.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(304, 'simpleplay-saint-of-mahjong', 'Saint of Mahjong', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-saint-of-mahjong.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(305, 'platipus-princessofbirds', 'Princess of Birds', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-princessofbirds.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(306, 'spadegaming-shanghai-008', 'ShangHai 008', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-shanghai-008.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(307, 'wazdan-514', 'Turbo Poker', 'slot', 'Wazdan', 'ms', 'wb', 'JUME', 'img/mrslotty/wazdan-514.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(308, 'evoplay-roll-the-dice', 'Roll The Dice', 'slot', 'Evoplay', 'ms', 'wb', 'JUME', 'img/mrslotty/evoplay-roll-the-dice.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(309, 'redrake-maya', 'Maya', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-maya.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(310, 'hub88-midas-treasure', 'Midas Treasure', 'slot', 'Kalamba', 'ms', 'wb', '', 'img/mrslotty/hub88-midas-treasure.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(311, 'play-pearls-wild_leprechaun', 'Wild Leprechaun', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-wild_leprechaun.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(312, 'liw-Keno1080', 'Keno 10/80', 'lottery', 'LiW', 'ms', 'wb', '', 'img/mrslotty/liw-Keno1080.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(313, 'simpleplay-saint-of-mahjong', 'Saint of Mahjong', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-saint-of-mahjong.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(314, 'platipus-jewelbang', 'Jewel Bang', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-jewelbang.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(315, 'spadegaming-first-love', 'First Love', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-first-love.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(316, 'wazdan-516', 'American Poker V', 'slot', 'Wazdan', 'ms', 'mb', 'JUME', 'img/mrslotty/wazdan-516.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(317, 'evoplay-heads-tails', 'Heads & Tails', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-heads-tails.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(318, 'redrake-viva-las-vegas', 'Viva Las Vegas', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-viva-las-vegas.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(319, 'hub88-caribbean-anne', 'Caribbean Anne', 'slot', 'Kalamba', 'ms', 'mb', '', 'img/mrslotty/hub88-caribbean-anne.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(320, 'play-pearls-vampires', 'Vampires', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-vampires.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(321, 'liw-Keno2080', 'Keno 20/80', 'lottery', 'LiW', 'ms', 'mb', '', 'img/mrslotty/liw-Keno2080.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(322, 'simpleplay-prosperity-tree', 'Prosperity Tree', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-prosperity-tree.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(323, 'platipus-jewelbang', 'Jewel Bang', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-jewelbang.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(324, 'spadegaming-first-love', 'First Love', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-first-love.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(325, 'wazdan-516', 'American Poker V', 'slot', 'Wazdan', 'ms', 'wb', 'JUME', 'img/mrslotty/wazdan-516.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(326, 'evoplay-heads-tails', 'Heads & Tails', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-heads-tails.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(327, 'redrake-viva-las-vegas', 'Viva Las Vegas', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-viva-las-vegas.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(328, 'hub88-caribbean-anne', 'Caribbean Anne', 'slot', 'Kalamba', 'ms', 'wb', '', 'img/mrslotty/hub88-caribbean-anne.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(329, 'play-pearls-vampires', 'Vampires', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-vampires.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(330, 'liw-Keno2080', 'Keno 20/80', 'lottery', 'LiW', 'ms', 'wb', '', 'img/mrslotty/liw-Keno2080.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(331, 'simpleplay-prosperity-tree', 'Prosperity Tree', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-prosperity-tree.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(332, 'platipus-fieryplanet', 'Fiery Planet', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-fieryplanet.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(333, 'spadegaming-golden-monkey', 'Golden Monkey', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-golden-monkey.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(334, 'wazdan-518', 'Three Cards', 'slot', 'Wazdan', 'ms', 'mb', 'JUME', 'img/mrslotty/wazdan-518.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(335, 'evoplay-more-or-less', 'More or Less', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-more-or-less.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(336, 'redrake-speed-heroes', 'Speed Heroes', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-speed-heroes.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(337, 'hub88-zombie-queen', 'Zombie Queen', 'slot', 'Kalamba', 'ms', 'mb', '', 'img/mrslotty/hub88-zombie-queen.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(338, 'play-pearls-uga_age', 'Uga Age', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-uga_age.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(339, 'simpleplay-red-chamber', 'Red Chamber', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-red-chamber.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(340, 'platipus-fieryplanet', 'Fiery Planet', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-fieryplanet.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(341, 'spadegaming-golden-monkey', 'Golden Monkey', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-golden-monkey.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(342, 'wazdan-518', 'Three Cards', 'slot', 'Wazdan', 'ms', 'wb', 'JUME', 'img/mrslotty/wazdan-518.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(343, 'evoplay-more-or-less', 'More or Less', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-more-or-less.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(344, 'redrake-speed-heroes', 'Speed Heroes', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-speed-heroes.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(345, 'hub88-zombie-queen', 'Zombie Queen', 'slot', 'Kalamba', 'ms', 'wb', '', 'img/mrslotty/hub88-zombie-queen.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(346, 'play-pearls-uga_age', 'Uga Age', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-uga_age.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(347, 'simpleplay-red-chamber', 'Red Chamber', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-red-chamber.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(348, 'platipus-cleosgold', 'Cleo\'s Gold', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-cleosgold.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(349, 'spadegaming-fist-of-gold', 'Fist of Gold', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-fist-of-gold.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(350, 'wazdan-519', 'Magic Poker', 'slot', 'Wazdan', 'ms', 'mb', 'JUME', 'img/mrslotty/wazdan-519.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(351, 'evoplay-blackjack-lucky-sevens', 'BlackJack Lucky Sevens', 'slot', 'Evoplay', 'ms', 'mb', 'JUME', 'img/mrslotty/evoplay-blackjack-lucky-sevens.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(352, 'redrake-mystic-mirror', 'Mystic Mirror', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-mystic-mirror.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(353, 'hub88-mission-diamond-hunt', 'Mission Diamond Hunt', 'slot', 'Kalamba', 'ms', 'mb', '', 'img/mrslotty/hub88-mission-diamond-hunt.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(354, 'play-pearls-true_3d_classic_slotmachine', 'True 3D Classic Slotmachine', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-true_3d_classic_slotmachine.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(355, 'simpleplay-volley-beauties', 'Volley Beauties', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-volley-beauties.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(356, 'platipus-cleosgold', 'Cleo\'s Gold', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-cleosgold.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(357, 'spadegaming-fist-of-gold', 'Fist of Gold', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-fist-of-gold.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(358, 'wazdan-519', 'Magic Poker', 'slot', 'Wazdan', 'ms', 'wb', 'JUME', 'img/mrslotty/wazdan-519.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(359, 'evoplay-blackjack-lucky-sevens', 'BlackJack Lucky Sevens', 'slot', 'Evoplay', 'ms', 'wb', 'JUME', 'img/mrslotty/evoplay-blackjack-lucky-sevens.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(360, 'redrake-mystic-mirror', 'Mystic Mirror', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-mystic-mirror.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(361, 'hub88-mission-diamond-hunt', 'Mission Diamond Hunt', 'slot', 'Kalamba', 'ms', 'wb', '', 'img/mrslotty/hub88-mission-diamond-hunt.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(362, 'play-pearls-true_3d_classic_slotmachine', 'True 3D Classic Slotmachine', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-true_3d_classic_slotmachine.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(363, 'simpleplay-volley-beauties', 'Volley Beauties', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-volley-beauties.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(364, 'platipus-baccarat', 'Baccarat PRO NEW', 'slot', 'Platipus', 'ms', 'mb', 'JUME', 'img/mrslotty/platipus-baccarat.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(365, 'spadegaming-tiger-warrior', 'Tiger Warrior', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-tiger-warrior.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(366, 'wazdan-520', 'American Poker Gold', 'slot', 'Wazdan', 'ms', 'mb', 'JUME', 'img/mrslotty/wazdan-520.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(367, 'evoplay-oasis-poker-classic', 'Oasis Poker Classic', 'slot', 'Evoplay', 'ms', 'mb', 'JUME', 'img/mrslotty/evoplay-oasis-poker-classic.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(368, 'redrake-judges-rule-the-show', 'Judges Rule the Show!', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-judges-rule-the-show.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(369, 'hub88-blood-moon-express', 'Blood Moon Express', 'slot', 'Kalamba', 'ms', 'mb', '', 'img/mrslotty/hub88-blood-moon-express.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(370, 'play-pearls-tropic_paradise', 'Tropic Paradise', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-tropic_paradise.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(371, 'simpleplay-zombie-hunter', 'Zombie Hunter', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-zombie-hunter.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(372, 'platipus-baccarat', 'Baccarat PRO NEW', 'slot', 'Platipus', 'ms', 'wb', 'JUME', 'img/mrslotty/platipus-baccarat.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(373, 'spadegaming-tiger-warrior', 'Tiger Warrior', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-tiger-warrior.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(374, 'wazdan-520', 'American Poker Gold', 'slot', 'Wazdan', 'ms', 'wb', 'JUME', 'img/mrslotty/wazdan-520.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(375, 'evoplay-oasis-poker-classic', 'Oasis Poker Classic', 'slot', 'Evoplay', 'ms', 'wb', 'JUME', 'img/mrslotty/evoplay-oasis-poker-classic.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(376, 'redrake-judges-rule-the-show', 'Judges Rule the Show!', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-judges-rule-the-show.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(377, 'hub88-blood-moon-express', 'Blood Moon Express', 'slot', 'Kalamba', 'ms', 'wb', '', 'img/mrslotty/hub88-blood-moon-express.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(378, 'play-pearls-tropic_paradise', 'Tropic Paradise', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-tropic_paradise.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(379, 'simpleplay-zombie-hunter', 'Zombie Hunter', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-zombie-hunter.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(380, 'platipus-baccaratvip', 'Baccarat VIP', 'slot', 'Platipus', 'ms', 'mb', 'JUME', 'img/mrslotty/platipus-baccaratvip.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(381, 'spadegaming-ho-yeah-monkey', 'Ho Yeah Monkey', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-ho-yeah-monkey.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(382, 'wazdan-769', 'Black Jack', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-769.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(383, 'evoplay-oasis-poker-pro-series', 'Oasis Poker Pro Series', 'slot', 'Evoplay', 'ms', 'mb', 'JUME', 'img/mrslotty/evoplay-oasis-poker-pro-series.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(384, 'redrake-jack-olantern', 'Jack O\'Lantern vs The Headless Horseman', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-jack-olantern.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(385, 'hub88-big-bounty-bill', 'Big Bounty Bill', 'slot', 'Kalamba', 'ms', 'mb', '', 'img/mrslotty/hub88-big-bounty-bill.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(386, 'play-pearls-tiki_beach', 'Tiki Beach', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-tiki_beach.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(387, 'simpleplay-cheung-po-tsai', 'Cheung Po Tsai', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-cheung-po-tsai.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(388, 'platipus-baccaratvip', 'Baccarat VIP', 'slot', 'Platipus', 'ms', 'wb', 'JUME', 'img/mrslotty/platipus-baccaratvip.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(389, 'spadegaming-ho-yeah-monkey', 'Ho Yeah Monkey', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-ho-yeah-monkey.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(390, 'wazdan-769', 'Black Jack', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-769.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(391, 'evoplay-oasis-poker-pro-series', 'Oasis Poker Pro Series', 'slot', 'Evoplay', 'ms', 'wb', 'JUME', 'img/mrslotty/evoplay-oasis-poker-pro-series.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(392, 'redrake-jack-olantern', 'Jack O\'Lantern vs The Headless Horseman', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-jack-olantern.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(393, 'hub88-big-bounty-bill', 'Big Bounty Bill', 'slot', 'Kalamba', 'ms', 'wb', '', 'img/mrslotty/hub88-big-bounty-bill.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(394, 'play-pearls-tiki_beach', 'Tiki Beach', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-tiki_beach.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(395, 'simpleplay-cheung-po-tsai', 'Cheung Po Tsai', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-cheung-po-tsai.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(396, 'platipus-baccaratmini', 'Baccarat Mini', 'slot', 'Platipus', 'ms', 'mb', 'JUME', 'img/mrslotty/platipus-baccaratmini.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(397, 'spadegaming-sea-emperor', 'Sea Emperor', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-sea-emperor.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(398, 'wazdan-771', 'Caribbean Beach Poker', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-771.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(399, 'evoplay-ussr-seventies', 'USSR Seventies', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-ussr-seventies.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(400, 'redrake-the-asp-of-cleopatra', 'The Asp of Cleopatra', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-the-asp-of-cleopatra.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(401, 'hub88-temple-of-heroes', 'Temple of Heroes', 'slot', 'Kalamba', 'ms', 'mb', '', 'img/mrslotty/hub88-temple-of-heroes.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(402, 'play-pearls-the_vikings', 'The Vikings', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-the_vikings.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(403, 'simpleplay-angels-demons', 'Angels & Demons', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-angels-demons.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(404, 'platipus-baccaratmini', 'Baccarat Mini', 'slot', 'Platipus', 'ms', 'wb', 'JUME', 'img/mrslotty/platipus-baccaratmini.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(405, 'spadegaming-sea-emperor', 'Sea Emperor', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-sea-emperor.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(406, 'wazdan-771', 'Caribbean Beach Poker', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-771.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(407, 'evoplay-ussr-seventies', 'USSR Seventies', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-ussr-seventies.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(408, 'redrake-the-asp-of-cleopatra', 'The Asp of Cleopatra', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-the-asp-of-cleopatra.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(409, 'hub88-temple-of-heroes', 'Temple of Heroes', 'slot', 'Kalamba', 'ms', 'wb', '', 'img/mrslotty/hub88-temple-of-heroes.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(410, 'play-pearls-the_vikings', 'The Vikings', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-the_vikings.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(411, 'simpleplay-angels-demons', 'Angels & Demons', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-angels-demons.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(412, 'platipus-blackjack', 'Blackjack', 'slot', 'Platipus', 'ms', 'mb', 'JUME', 'img/mrslotty/platipus-blackjack.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(413, 'spadegaming-zeus', 'ZEUS', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-zeus.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(414, 'wazdan-1029', 'Extra Bingo', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-1029.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(415, 'evoplay-the-slavs', 'The Slavs', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-the-slavs.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(416, 'redrake-super-10-stars', 'Super 10 Stars', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-super-10-stars.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(417, 'hub88-joker-supreme-xmas', 'Joker Supreme Xmas', 'slot', 'Kalamba', 'ms', 'mb', '', 'img/mrslotty/hub88-joker-supreme-xmas.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(418, 'play-pearls-star_trex', 'Star Trex', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-star_trex.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(419, 'simpleplay-golden-chicken', 'Golden Chicken', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-golden-chicken.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(420, 'platipus-blackjack', 'Blackjack', 'slot', 'Platipus', 'ms', 'wb', 'JUME', 'img/mrslotty/platipus-blackjack.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(421, 'spadegaming-zeus', 'ZEUS', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-zeus.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(422, 'wazdan-1029', 'Extra Bingo', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-1029.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(423, 'evoplay-the-slavs', 'The Slavs', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-the-slavs.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(424, 'redrake-super-10-stars', 'Super 10 Stars', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-super-10-stars.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(425, 'hub88-joker-supreme-xmas', 'Joker Supreme Xmas', 'slot', 'Kalamba', 'ms', 'wb', '', 'img/mrslotty/hub88-joker-supreme-xmas.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(426, 'play-pearls-star_trex', 'Star Trex', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-star_trex.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(427, 'simpleplay-golden-chicken', 'Golden Chicken', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-golden-chicken.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(428, 'platipus-blackjackvip', 'Blackjack Vip', 'slot', 'Platipus', 'ms', 'mb', 'JUME', 'img/mrslotty/platipus-blackjackvip.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(429, 'spadegaming-golden-lotus-se', 'Golden Lotus SE', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-golden-lotus-se.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(430, 'wazdan-1284', 'Casino Roulette', 'slot', 'Wazdan', 'ms', 'mb', 'RULF', 'img/mrslotty/wazdan-1284.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(431, 'evoplay-four-aces', 'Four Aces', 'slot', 'Evoplay', 'ms', 'mb', 'JUME', 'img/mrslotty/evoplay-four-aces.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(432, 'redrake-eastern-goddesses', 'Eastern Goddesses', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-eastern-goddesses.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(433, 'hub88-pyro-pixie', 'Pyro Pixie', 'slot', 'Kalamba', 'ms', 'mb', '', 'img/mrslotty/hub88-pyro-pixie.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(434, 'play-pearls-spartan_gold', 'Spartan Gold', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-spartan_gold.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(435, 'simpleplay-ji-gong', 'Ji Gong', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-ji-gong.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(436, 'platipus-blackjackvip', 'Blackjack Vip', 'slot', 'Platipus', 'ms', 'wb', 'JUME', 'img/mrslotty/platipus-blackjackvip.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(437, 'spadegaming-golden-lotus-se', 'Golden Lotus SE', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-golden-lotus-se.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(438, 'wazdan-1284', 'Casino Roulette', 'slot', 'Wazdan', 'ms', 'wb', 'RULF', 'img/mrslotty/wazdan-1284.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(439, 'evoplay-four-aces', 'Four Aces', 'slot', 'Evoplay', 'ms', 'wb', 'JUME', 'img/mrslotty/evoplay-four-aces.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(440, 'redrake-eastern-goddesses', 'Eastern Goddesses', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-eastern-goddesses.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(441, 'hub88-pyro-pixie', 'Pyro Pixie', 'slot', 'Kalamba', 'ms', 'wb', '', 'img/mrslotty/hub88-pyro-pixie.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(442, 'play-pearls-spartan_gold', 'Spartan Gold', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-spartan_gold.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(443, 'simpleplay-ji-gong', 'Ji Gong', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-ji-gong.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(444, 'betsolutions-high-low', 'High low', 'iq', 'Betsolutions', 'ms', 'mb', '', 'img/mrslotty/betsolutions-high-low.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(445, 'spadegaming-golden-chicken', 'Golden Chicken', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-golden-chicken.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(446, 'wazdan-1286', 'Gold Roulette', 'slot', 'Wazdan', 'ms', 'mb', 'RULF', 'img/mrslotty/wazdan-1286.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(447, 'evoplay-red-queen', 'Red Queen', 'slot', 'Evoplay', 'ms', 'mb', 'JUME', 'img/mrslotty/evoplay-red-queen.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(448, 'redrake-ryan-obryan', 'Ryan O\'Bryan and the Celtic Fairies', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-ryan-obryan.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(449, 'hub88-crystal-cavern', 'Crystal Cavern', 'slot', 'Kalamba', 'ms', 'mb', '', 'img/mrslotty/hub88-crystal-cavern.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(450, 'play-pearls-soldiers_fortune', 'Soldiers Fortune', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-soldiers_fortune.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(451, 'simpleplay-the-guard', 'The Guard', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-the-guard.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(452, 'betsolutions-high-low', 'High low', 'iq', 'Betsolutions', 'ms', 'wb', '', 'img/mrslotty/betsolutions-high-low.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(453, 'spadegaming-golden-chicken', 'Golden Chicken', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-golden-chicken.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(454, 'wazdan-1286', 'Gold Roulette', 'slot', 'Wazdan', 'ms', 'wb', 'RULF', 'img/mrslotty/wazdan-1286.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(455, 'evoplay-red-queen', 'Red Queen', 'slot', 'Evoplay', 'ms', 'wb', 'JUME', 'img/mrslotty/evoplay-red-queen.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(456, 'redrake-ryan-obryan', 'Ryan O\'Bryan and the Celtic Fairies', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-ryan-obryan.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(457, 'hub88-crystal-cavern', 'Crystal Cavern', 'slot', 'Kalamba', 'ms', 'wb', '', 'img/mrslotty/hub88-crystal-cavern.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(458, 'play-pearls-soldiers_fortune', 'Soldiers Fortune', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-soldiers_fortune.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(459, 'simpleplay-the-guard', 'The Guard', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-the-guard.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(460, 'betsolutions-zeppelin', 'Zeppelin', 'iq', 'Betsolutions', 'ms', 'mb', '', 'img/mrslotty/betsolutions-zeppelin.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(461, 'spadegaming-5-fortune-dragons', '5 Fortune Dragons', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-5-fortune-dragons.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(462, 'wazdan-65868', 'Jumping Fruits', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-65868.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(463, 'evoplay-thimbles', 'Thimbles', 'slot', 'Evoplay', 'ms', 'mb', 'JUME', 'img/mrslotty/evoplay-thimbles.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(464, 'redrake-wildcano', 'Wildcano', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-wildcano.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(465, 'hub88-joker-max', 'Joker Max', 'slot', 'Kalamba', 'ms', 'mb', '', 'img/mrslotty/hub88-joker-max.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(466, 'play-pearls-roll_n_ride', 'Roll & Ride', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-roll_n_ride.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(467, 'nsoft-lucky-six', 'Lucky Six', 'lottery', 'Nsoft', 'ms', 'mb', '', 'img/mrslotty/nsoft-lucky-six.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(468, 'simpleplay-fruit-poppers', 'Fruit Poppers', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-fruit-poppers.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(469, 'betsolutions-zeppelin', 'Zeppelin', 'iq', 'Betsolutions', 'ms', 'wb', '', 'img/mrslotty/betsolutions-zeppelin.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(470, 'spadegaming-5-fortune-dragons', '5 Fortune Dragons', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-5-fortune-dragons.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(471, 'wazdan-65868', 'Jumping Fruits', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-65868.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(472, 'evoplay-thimbles', 'Thimbles', 'slot', 'Evoplay', 'ms', 'wb', 'JUME', 'img/mrslotty/evoplay-thimbles.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(473, 'redrake-wildcano', 'Wildcano', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-wildcano.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(474, 'hub88-joker-max', 'Joker Max', 'slot', 'Kalamba', 'ms', 'wb', '', 'img/mrslotty/hub88-joker-max.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(475, 'play-pearls-roll_n_ride', 'Roll & Ride', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-roll_n_ride.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(476, 'nsoft-lucky-six', 'Lucky Six', 'lottery', 'Nsoft', 'ms', 'wb', '', 'img/mrslotty/nsoft-lucky-six.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(477, 'simpleplay-fruit-poppers', 'Fruit Poppers', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-fruit-poppers.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(478, 'betsolutions-dice', 'Dice', 'iq', 'Betsolutions', 'ms', 'mb', '', 'img/mrslotty/betsolutions-dice.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(479, 'spadegaming-baby-cai-shen', 'Baby Cai Shen', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-baby-cai-shen.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(480, 'wazdan-65869', 'Los Muertos', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-65869.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(481, 'evoplay-aeronauts', 'Aeronauts', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-aeronauts.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(482, 'redrake-electric-sevens', 'Electric Sevens', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-electric-sevens.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(483, 'hub88-hammer-of-fortune', 'Hammer of Fortune', 'arcade', 'Green Jade', 'ms', 'mb', '', 'img/mrslotty/hub88-hammer-of-fortune.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(484, 'play-pearls-red_nights', 'Red Nights', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-red_nights.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(485, 'nsoft-lucky-x', 'Lucky X', 'lottery', 'Nsoft', 'ms', 'mb', '', 'img/mrslotty/nsoft-lucky-x.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(486, 'simpleplay-diamond-crush', 'Diamond Crush', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-diamond-crush.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(487, 'betsolutions-dice', 'Dice', 'iq', 'Betsolutions', 'ms', 'wb', '', 'img/mrslotty/betsolutions-dice.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(488, 'spadegaming-baby-cai-shen', 'Baby Cai Shen', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-baby-cai-shen.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(489, 'wazdan-65869', 'Los Muertos', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-65869.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(490, 'evoplay-aeronauts', 'Aeronauts', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-aeronauts.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(491, 'redrake-electric-sevens', 'Electric Sevens', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-electric-sevens.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(492, 'hub88-hammer-of-fortune', 'Hammer of Fortune', 'arcade', 'Green Jade', 'ms', 'wb', '', 'img/mrslotty/hub88-hammer-of-fortune.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(493, 'play-pearls-red_nights', 'Red Nights', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-red_nights.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(494, 'nsoft-lucky-x', 'Lucky X', 'lottery', 'Nsoft', 'ms', 'wb', '', 'img/mrslotty/nsoft-lucky-x.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(495, 'simpleplay-diamond-crush', 'Diamond Crush', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-diamond-crush.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(496, 'betsolutions-mines', 'Mines', 'iq', 'Betsolutions', 'ms', 'mb', '', 'img/mrslotty/betsolutions-mines.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(497, 'spadegaming-cai-shen-888', 'Cai Shen 888', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-cai-shen-888.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(498, 'wazdan-65877', 'Magic Fruits', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-65877.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(499, 'evoplay-prohibition', 'Prohibition', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-prohibition.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(500, 'redrake-myrtle-the-witch', 'Myrtle the Witch', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-myrtle-the-witch.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(501, 'hub88-spin-bet-station', 'Spin Bet Station', 'arcade', 'Green Jade', 'ms', 'mb', '', 'img/mrslotty/hub88-spin-bet-station.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(502, 'play-pearls-pharaohs_treasure', 'Pharaohs Treasure', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-pharaohs_treasure.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(503, 'nsoft-next-six', 'Next Six', 'lottery', 'Nsoft', 'ms', 'mb', '', 'img/mrslotty/nsoft-next-six.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(504, 'simpleplay-tropical-treasure', 'Tropical Treasure', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-tropical-treasure.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(505, 'betsolutions-mines', 'Mines', 'iq', 'Betsolutions', 'ms', 'wb', '', 'img/mrslotty/betsolutions-mines.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(506, 'spadegaming-cai-shen-888', 'Cai Shen 888', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-cai-shen-888.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(507, 'wazdan-65877', 'Magic Fruits', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-65877.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(508, 'evoplay-prohibition', 'Prohibition', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-prohibition.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(509, 'redrake-myrtle-the-witch', 'Myrtle the Witch', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-myrtle-the-witch.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(510, 'hub88-spin-bet-station', 'Spin Bet Station', 'arcade', 'Green Jade', 'ms', 'wb', '', 'img/mrslotty/hub88-spin-bet-station.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(511, 'play-pearls-pharaohs_treasure', 'Pharaohs Treasure', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-pharaohs_treasure.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(512, 'nsoft-next-six', 'Next Six', 'lottery', 'Nsoft', 'ms', 'wb', '', 'img/mrslotty/nsoft-next-six.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(513, 'simpleplay-tropical-treasure', 'Tropical Treasure', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-tropical-treasure.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(514, 'truelab-startup-valley', 'StartUp Valley', 'slot', 'TrueLab', 'ms', 'mb', '', 'img/mrslotty/truelab-startup-valley.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(515, 'spadegaming-mermaid', 'Mermaid', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-mermaid.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(516, 'wazdan-65878', 'Fruits Go Bananas', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-65878.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(517, 'evoplay-european-roulette', 'European Roulette', 'slot', 'Evoplay', 'ms', 'mb', 'RULF', 'img/mrslotty/evoplay-european-roulette.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(518, 'redrake-the-secret-of-the-opera', 'The Secret of the Opera', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-the-secret-of-the-opera.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(519, 'hub88-coin-flip-deluxe', 'Coin Flip Deluxe', 'arcade', 'Green Jade', 'ms', 'mb', '', 'img/mrslotty/hub88-coin-flip-deluxe.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(520, 'play-pearls-pharaoh', 'Pharaoh', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-pharaoh.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(521, 'netgame-african-king', 'African King', 'slot', 'Netgame', 'ms', 'mb', '', 'img/mrslotty/netgame-african-king.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(522, 'simpleplay-north-south-lions', 'North South Lions', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-north-south-lions.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(523, 'truelab-startup-valley', 'StartUp Valley', 'slot', 'TrueLab', 'ms', 'wb', '', 'img/mrslotty/truelab-startup-valley.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(524, 'spadegaming-mermaid', 'Mermaid', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-mermaid.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(525, 'wazdan-65878', 'Fruits Go Bananas', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-65878.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(526, 'evoplay-european-roulette', 'European Roulette', 'slot', 'Evoplay', 'ms', 'wb', 'RULF', 'img/mrslotty/evoplay-european-roulette.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(527, 'redrake-the-secret-of-the-opera', 'The Secret of the Opera', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-the-secret-of-the-opera.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(528, 'hub88-coin-flip-deluxe', 'Coin Flip Deluxe', 'arcade', 'Green Jade', 'ms', 'wb', '', 'img/mrslotty/hub88-coin-flip-deluxe.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(529, 'play-pearls-pharaoh', 'Pharaoh', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-pharaoh.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(530, 'netgame-african-king', 'African King', 'slot', 'Netgame', 'ms', 'wb', '', 'img/mrslotty/netgame-african-king.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(531, 'simpleplay-north-south-lions', 'North South Lions', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-north-south-lions.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(532, 'eagaming-2', 'Bagua', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-2.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(533, 'spadegaming-golden-whale', 'Golden Whale', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-golden-whale.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(534, 'wazdan-65879', 'Haunted Hospital', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-65879.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(535, 'evoplay-monster-lab', 'Monster Lab', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-monster-lab.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(536, 'redrake-3-butterflies', '3 Butterflies', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-3-butterflies.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(537, 'hub88-village-brewery', 'Village Brewery', 'slot', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-village-brewery.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(538, 'play-pearls-penguin_vacation', 'Penguin Vacation', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-penguin_vacation.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(539, 'netgame-bananas10', 'Bananas10', 'slot', 'Netgame', 'ms', 'mb', '', 'img/mrslotty/netgame-bananas10.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(540, 'simpleplay-red-dragon', 'Red Dragon', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-red-dragon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(541, 'eagaming-2', 'Bagua', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-2.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(542, 'spadegaming-golden-whale', 'Golden Whale', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-golden-whale.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(543, 'wazdan-65879', 'Haunted Hospital', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-65879.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(544, 'evoplay-monster-lab', 'Monster Lab', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-monster-lab.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(545, 'redrake-3-butterflies', '3 Butterflies', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-3-butterflies.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(546, 'hub88-village-brewery', 'Village Brewery', 'slot', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-village-brewery.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(547, 'play-pearls-penguin_vacation', 'Penguin Vacation', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-penguin_vacation.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(548, 'netgame-bananas10', 'Bananas10', 'slot', 'Netgame', 'ms', 'wb', '', 'img/mrslotty/netgame-bananas10.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(549, 'simpleplay-red-dragon', 'Red Dragon', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-red-dragon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(550, 'eagaming-3', 'Neptune Treasure', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-3.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(551, 'spadegaming-king-pharaoh', 'King Pharaoh', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-king-pharaoh.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(552, 'wazdan-65880', 'In The Forest', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-65880.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(553, 'evoplay-the-great-conflict', 'The Great Conflict', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-the-great-conflict.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(554, 'redrake-red-dragon-vs-blue-dragon', 'Red Dragon vs Blue Dragon', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-red-dragon-vs-blue-dragon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(555, 'hub88-hurricash', 'Hurricash', 'slot', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-hurricash.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(556, 'play-pearls-ocean_riches', 'Ocean Riches', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-ocean_riches.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(557, 'netgame-cleo-s-heart', 'Cleo\'s Heart', 'slot', 'Netgame', 'ms', 'mb', '', 'img/mrslotty/netgame-cleo-s-heart.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(558, 'simpleplay-funny-farm', 'Funny Farm', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-funny-farm.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(559, 'eagaming-3', 'Neptune Treasure', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-3.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(560, 'spadegaming-king-pharaoh', 'King Pharaoh', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-king-pharaoh.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(561, 'wazdan-65880', 'In The Forest', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-65880.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(562, 'evoplay-the-great-conflict', 'The Great Conflict', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-the-great-conflict.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(563, 'redrake-red-dragon-vs-blue-dragon', 'Red Dragon vs Blue Dragon', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-red-dragon-vs-blue-dragon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(564, 'hub88-hurricash', 'Hurricash', 'slot', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-hurricash.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(565, 'play-pearls-ocean_riches', 'Ocean Riches', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-ocean_riches.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(566, 'netgame-cleo-s-heart', 'Cleo\'s Heart', 'slot', 'Netgame', 'ms', 'wb', '', 'img/mrslotty/netgame-cleo-s-heart.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(567, 'simpleplay-funny-farm', 'Funny Farm', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-funny-farm.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(568, 'eagaming-4', 'Crypto Mania', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-4.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(569, 'spadegaming-wong-po', 'Wong Po', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-wong-po.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(570, 'wazdan-65881', 'Double Tigers', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-65881.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(571, 'evoplay-battle-tanks', 'Battle Tanks', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-battle-tanks.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(572, 'redrake-respins-diamonds', 'Respins & Diamonds', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-respins-diamonds.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(573, 'hub88-vegas-baby', 'Vegas Baby', 'slot', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-vegas-baby.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(574, 'play-pearls-native_indians', 'Native Indians', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-native_indians.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(575, 'netgame-cloverstones', 'Cloverstones', 'slot', 'Netgame', 'ms', 'mb', '', 'img/mrslotty/netgame-cloverstones.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(576, 'simpleplay-innocent-classmates', 'Innocent Classmates', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-innocent-classmates.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(577, 'eagaming-4', 'Crypto Mania', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-4.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(578, 'spadegaming-wong-po', 'Wong Po', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-wong-po.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(579, 'wazdan-65881', 'Double Tigers', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-65881.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(580, 'evoplay-battle-tanks', 'Battle Tanks', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-battle-tanks.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(581, 'redrake-respins-diamonds', 'Respins & Diamonds', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-respins-diamonds.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(582, 'hub88-vegas-baby', 'Vegas Baby', 'slot', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-vegas-baby.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(583, 'play-pearls-native_indians', 'Native Indians', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-native_indians.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(584, 'netgame-cloverstones', 'Cloverstones', 'slot', 'Netgame', 'ms', 'wb', '', 'img/mrslotty/netgame-cloverstones.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(585, 'simpleplay-innocent-classmates', 'Innocent Classmates', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-innocent-classmates.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(586, 'eagaming-5', 'Dynamite Reels', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-5.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(587, 'spadegaming-big-prosperity-sa', 'Big Prosperity SA', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-big-prosperity-sa.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(588, 'wazdan-65882', 'Fenix Play', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-65882.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(589, 'evoplay-dolphins-treasure', 'Dolphins Treasure', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-dolphins-treasure.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(590, 'redrake-super-7-stars', 'Super 7 Stars', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-super-7-stars.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(591, 'tpg-tpg-777', 'TPG 777', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-tpg-777.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(592, 'hub88-banana-bingo', 'Banana Bingo', 'video_bingo', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-banana-bingo.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(593, 'play-pearls-music_stage', 'Music Stage', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-music_stage.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(594, 'netgame-crazy-scientist-2-js', 'Crazy Scientist 2 JS', 'slot', 'Netgame', 'ms', 'mb', '', 'img/mrslotty/netgame-crazy-scientist-2-js.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(595, 'simpleplay-dragon-tiger', 'Dragon & Tiger', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-dragon-tiger.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(596, 'eagaming-5', 'Dynamite Reels', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-5.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(597, 'spadegaming-big-prosperity-sa', 'Big Prosperity SA', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-big-prosperity-sa.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(598, 'wazdan-65882', 'Fenix Play', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-65882.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(599, 'evoplay-dolphins-treasure', 'Dolphins Treasure', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-dolphins-treasure.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(600, 'redrake-super-7-stars', 'Super 7 Stars', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-super-7-stars.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(601, 'tpg-tpg-777', 'TPG 777', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-tpg-777.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(602, 'hub88-banana-bingo', 'Banana Bingo', 'video_bingo', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-banana-bingo.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(603, 'play-pearls-music_stage', 'Music Stage', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-music_stage.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(604, 'netgame-crazy-scientist-2-js', 'Crazy Scientist 2 JS', 'slot', 'Netgame', 'ms', 'wb', '', 'img/mrslotty/netgame-crazy-scientist-2-js.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(605, 'simpleplay-dragon-tiger', 'Dragon & Tiger', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-dragon-tiger.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(606, 'eagaming-6', 'Lightning God', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-6.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(607, 'spadegaming-lucky-koi', 'Lucky Koi', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-lucky-koi.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(608, 'wazdan-65883', 'Fenix Play 27', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-65883.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(609, 'evoplay-ussr-grocery', 'USSR Grocery', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-ussr-grocery.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(610, 'redrake-heidi-at-oktoberfest', 'Heidi at Oktoberfest', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-heidi-at-oktoberfest.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(611, 'tpg-space-galaxy', 'Space Galaxy', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-space-galaxy.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(612, 'hub88-bingo-bruxaria', 'Bingo Bruxaria', 'video_bingo', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-bingo-bruxaria.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(613, 'play-pearls-mayan_gold', 'Mayan Gold', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-mayan_gold.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(614, 'netgame-diamond-shoot', 'Diamond Shoot', 'slot', 'Netgame', 'ms', 'mb', '', 'img/mrslotty/netgame-diamond-shoot.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(615, 'simpleplay-fantasy-goddess', 'Fantasy Goddess', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-fantasy-goddess.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(616, 'eagaming-6', 'Lightning God', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-6.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(617, 'spadegaming-lucky-koi', 'Lucky Koi', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-lucky-koi.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(618, 'wazdan-65883', 'Fenix Play 27', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-65883.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(619, 'evoplay-ussr-grocery', 'USSR Grocery', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-ussr-grocery.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(620, 'redrake-heidi-at-oktoberfest', 'Heidi at Oktoberfest', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-heidi-at-oktoberfest.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(621, 'tpg-space-galaxy', 'Space Galaxy', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-space-galaxy.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(622, 'hub88-bingo-bruxaria', 'Bingo Bruxaria', 'video_bingo', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-bingo-bruxaria.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(623, 'play-pearls-mayan_gold', 'Mayan Gold', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-mayan_gold.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(624, 'netgame-diamond-shoot', 'Diamond Shoot', 'slot', 'Netgame', 'ms', 'wb', '', 'img/mrslotty/netgame-diamond-shoot.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(625, 'simpleplay-fantasy-goddess', 'Fantasy Goddess', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-fantasy-goddess.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(626, 'eagaming-7', 'Nugget Hunter', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-7.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(627, 'spadegaming-iceland-sa', 'Iceland SA', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-iceland-sa.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(628, 'wazdan-65884', 'Magic Hot 4 Deluxe', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-65884.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(629, 'evoplay-baccarat-777', 'Baccarat 777', 'slot', 'Evoplay', 'ms', 'mb', 'JUME', 'img/mrslotty/evoplay-baccarat-777.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(630, 'redrake-mega-stellar', 'Mega Stellar', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-mega-stellar.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(631, 'tpg-vampires-feast', 'Vampires Feast', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-vampires-feast.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(632, 'hub88-bingo-trevo-da-sorte', 'Bingo Trevo da Sorte', 'video_bingo', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-bingo-trevo-da-sorte.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(633, 'play-pearls-mafia_story', 'Mafia Story', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-mafia_story.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(634, 'netgame-disco-fruit', 'Disco Fruits', 'slot', 'Netgame', 'ms', 'mb', '', 'img/mrslotty/netgame-disco-fruit.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(635, 'simpleplay-bikini-chaser', 'Bikini Chaser', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-bikini-chaser.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(636, 'eagaming-7', 'Nugget Hunter', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-7.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(637, 'spadegaming-iceland-sa', 'Iceland SA', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-iceland-sa.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(638, 'wazdan-65884', 'Magic Hot 4 Deluxe', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-65884.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(639, 'evoplay-baccarat-777', 'Baccarat 777', 'slot', 'Evoplay', 'ms', 'wb', 'JUME', 'img/mrslotty/evoplay-baccarat-777.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(640, 'redrake-mega-stellar', 'Mega Stellar', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-mega-stellar.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(641, 'tpg-vampires-feast', 'Vampires Feast', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-vampires-feast.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(642, 'hub88-bingo-trevo-da-sorte', 'Bingo Trevo da Sorte', 'video_bingo', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-bingo-trevo-da-sorte.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(643, 'play-pearls-mafia_story', 'Mafia Story', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-mafia_story.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(644, 'netgame-disco-fruit', 'Disco Fruits', 'slot', 'Netgame', 'ms', 'wb', '', 'img/mrslotty/netgame-disco-fruit.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(645, 'simpleplay-bikini-chaser', 'Bikini Chaser', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-bikini-chaser.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(646, 'eagaming-8', 'Witchs Brew', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-8.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(647, 'spadegaming-festive-lion', 'Festive Lion', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-festive-lion.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(648, 'wazdan-65886', 'Magic of the Ring Deluxe', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-65886.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(649, 'evoplay-mystery-planet', 'Mystery Planet', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-mystery-planet.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(650, 'redrake-super-5-stars', 'Super 5 Stars', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-super-5-stars.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(651, 'tpg-xian-xia', 'Xian Xia', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-xian-xia.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(652, 'hub88-bingo-hortinha', 'Bingo Hortinha', 'video_bingo', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-bingo-hortinha.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(653, 'play-pearls-luckys_pizza', 'Lucky\'s Pizza', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-luckys_pizza.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(654, 'netgame-dragon-sevens', 'Dragon Sevens', 'slot', 'Netgame', 'ms', 'mb', '', 'img/mrslotty/netgame-dragon-sevens.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(655, 'simpleplay-creepy-cuddlers', 'Creepy Cuddlers', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-creepy-cuddlers.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(656, 'eagaming-8', 'Witchs Brew', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-8.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(657, 'spadegaming-festive-lion', 'Festive Lion', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-festive-lion.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(658, 'wazdan-65886', 'Magic of the Ring Deluxe', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-65886.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(659, 'evoplay-mystery-planet', 'Mystery Planet', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-mystery-planet.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(660, 'redrake-super-5-stars', 'Super 5 Stars', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-super-5-stars.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(661, 'tpg-xian-xia', 'Xian Xia', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-xian-xia.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(662, 'hub88-bingo-hortinha', 'Bingo Hortinha', 'video_bingo', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-bingo-hortinha.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(663, 'play-pearls-luckys_pizza', 'Lucky\'s Pizza', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-luckys_pizza.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(664, 'netgame-dragon-sevens', 'Dragon Sevens', 'slot', 'Netgame', 'ms', 'wb', '', 'img/mrslotty/netgame-dragon-sevens.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(665, 'simpleplay-creepy-cuddlers', 'Creepy Cuddlers', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-creepy-cuddlers.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(666, 'eagaming-9', 'Bushido Blade', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-9.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(667, 'spadegaming-cai-yuan-guang-jin', 'Cai Yuan Guang Jin', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-cai-yuan-guang-jin.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(668, 'wazdan-65887', 'Highway to Hell Deluxe', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-65887.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(669, 'evoplay-fruits-land', 'Fruits Land', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-fruits-land.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(670, 'redrake-the-legendary-red-dragon', 'The Legendary Red Dragon', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-the-legendary-red-dragon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(671, 'tpg-mischievous-pirates', 'Mischievous Pirates', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-mischievous-pirates.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(672, 'hub88-miami-bonus-wheel', 'Miami Bonus Wheel', 'slot', 'Kalamba', 'ms', 'mb', '', 'img/mrslotty/hub88-miami-bonus-wheel.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(673, 'play-pearls-lizard_disco', 'Lizard Disco', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-lizard_disco.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(674, 'netgame-frosty-fruits', 'Frosty Fruits', 'slot', 'Netgame', 'ms', 'mb', '', 'img/mrslotty/netgame-frosty-fruits.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(675, 'simpleplay-three-star-god', 'Three Star God', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-three-star-god.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(676, 'eagaming-9', 'Bushido Blade', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-9.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(677, 'spadegaming-cai-yuan-guang-jin', 'Cai Yuan Guang Jin', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-cai-yuan-guang-jin.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(678, 'wazdan-65887', 'Highway to Hell Deluxe', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-65887.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(679, 'evoplay-fruits-land', 'Fruits Land', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-fruits-land.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(680, 'redrake-the-legendary-red-dragon', 'The Legendary Red Dragon', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-the-legendary-red-dragon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(681, 'tpg-mischievous-pirates', 'Mischievous Pirates', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-mischievous-pirates.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(682, 'hub88-miami-bonus-wheel', 'Miami Bonus Wheel', 'slot', 'Kalamba', 'ms', 'wb', '', 'img/mrslotty/hub88-miami-bonus-wheel.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(683, 'play-pearls-lizard_disco', 'Lizard Disco', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-lizard_disco.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(684, 'netgame-frosty-fruits', 'Frosty Fruits', 'slot', 'Netgame', 'ms', 'wb', '', 'img/mrslotty/netgame-frosty-fruits.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(685, 'simpleplay-three-star-god', 'Three Star God', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-three-star-god.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(686, 'eagaming-10', 'Four Tigers', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-10.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(687, 'spadegaming-fafafa', 'FaFaFa', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-fafafa.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(688, 'wazdan-65892', 'Slot Jam', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-65892.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(689, 'evoplay-charming-queens', 'Charming Queens', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-charming-queens.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(690, 'redrake-queens-and-diamonds', 'Queens and Diamonds', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-queens-and-diamonds.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(691, 'tpg-dj-rock', 'DJ Rock', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-dj-rock.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(692, 'hub88-griffinsquest', 'griffinsquest', 'slot', 'Kalamba', 'ms', 'mb', '', 'img/mrslotty/hub88-griffinsquest.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(693, 'play-pearls-las_vegas', 'Las Vegas', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-las_vegas.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(694, 'netgame-fruits-fury', 'Fruits Fury', 'slot', 'Netgame', 'ms', 'mb', '', 'img/mrslotty/netgame-fruits-fury.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(695, 'simpleplay-new-year-rich', 'New Year Rich', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-new-year-rich.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(696, 'eagaming-10', 'Four Tigers', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-10.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(697, 'spadegaming-fafafa', 'FaFaFa', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-fafafa.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(698, 'wazdan-65892', 'Slot Jam', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-65892.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(699, 'evoplay-charming-queens', 'Charming Queens', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-charming-queens.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(700, 'redrake-queens-and-diamonds', 'Queens and Diamonds', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-queens-and-diamonds.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(701, 'tpg-dj-rock', 'DJ Rock', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-dj-rock.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(702, 'hub88-griffinsquest', 'griffinsquest', 'slot', 'Kalamba', 'ms', 'wb', '', 'img/mrslotty/hub88-griffinsquest.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(703, 'play-pearls-las_vegas', 'Las Vegas', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-las_vegas.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(704, 'netgame-fruits-fury', 'Fruits Fury', 'slot', 'Netgame', 'ms', 'wb', '', 'img/mrslotty/netgame-fruits-fury.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(705, 'simpleplay-new-year-rich', 'New Year Rich', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-new-year-rich.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(706, 'eagaming-11', 'Horus Eye', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-11.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(707, 'spadegaming-lucky-cai-shen', 'Lucky Cai Shen', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-lucky-cai-shen.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(708, 'wazdan-65895', 'Valhalla', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-65895.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(709, 'evoplay-lucky-girls', 'Lucky Girls', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-lucky-girls.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(710, 'redrake-ragin-buffalo', 'Ragin\' Buffalo', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-ragin-buffalo.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(711, 'tpg-blessing-mouse', 'Blessing Mouse', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-blessing-mouse.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(712, 'hub88-blazingbull', 'blazingbull', 'slot', 'Kalamba', 'ms', 'mb', '', 'img/mrslotty/hub88-blazingbull.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(713, 'play-pearls-juice_box', 'Juice Box', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-juice_box.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(714, 'netgame-golden-fruit', 'Golden Fruits', 'slot', 'Netgame', 'ms', 'mb', '', 'img/mrslotty/netgame-golden-fruit.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(715, 'evoplay-french-roulette-classic', 'French Roulette Classic', 'slot', 'Evoplay', 'ms', 'mb', 'RULF', 'img/mrslotty/evoplay-french-roulette-classic.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(716, 'eagaming-11', 'Horus Eye', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-11.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(717, 'spadegaming-lucky-cai-shen', 'Lucky Cai Shen', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-lucky-cai-shen.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(718, 'wazdan-65895', 'Valhalla', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-65895.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(719, 'evoplay-lucky-girls', 'Lucky Girls', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-lucky-girls.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(720, 'redrake-ragin-buffalo', 'Ragin\' Buffalo', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-ragin-buffalo.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(721, 'tpg-blessing-mouse', 'Blessing Mouse', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-blessing-mouse.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(722, 'hub88-blazingbull', 'blazingbull', 'slot', 'Kalamba', 'ms', 'wb', '', 'img/mrslotty/hub88-blazingbull.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(723, 'play-pearls-juice_box', 'Juice Box', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-juice_box.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(724, 'netgame-golden-fruit', 'Golden Fruits', 'slot', 'Netgame', 'ms', 'wb', '', 'img/mrslotty/netgame-golden-fruit.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(725, 'evoplay-french-roulette-classic', 'French Roulette Classic', 'slot', 'Evoplay', 'ms', 'wb', 'RULF', 'img/mrslotty/evoplay-french-roulette-classic.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(726, 'eagaming-12', 'Octagon Gem', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-12.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(727, 'spadegaming-double-fortune', 'Double Fortune', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-double-fortune.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(728, 'wazdan-65896', 'Sizzling 777 Deluxe', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-65896.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(729, 'evoplay-rock-paper-scissors', 'Rock Paper Scissors', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-rock-paper-scissors.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(730, 'redrake-siberian-wolf', 'Siberian Wolf', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-siberian-wolf.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(731, 'tpg-ying-cai-shen', 'Ying Cai Shen', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-ying-cai-shen.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(732, 'hub88-griffins-glory', 'Griffin\'s Glory', 'slot', 'Kalamba', 'ms', 'mb', '', 'img/mrslotty/hub88-griffins-glory.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(733, 'play-pearls-big_game_hunter', 'John Hunter: Big Game Hunter', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-big_game_hunter.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(734, 'netgame-golden-skulls', 'Golden Skulls', 'slot', 'Netgame', 'ms', 'mb', '', 'img/mrslotty/netgame-golden-skulls.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(735, 'eagaming-12', 'Octagon Gem', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-12.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(736, 'spadegaming-double-fortune', 'Double Fortune', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-double-fortune.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(737, 'wazdan-65896', 'Sizzling 777 Deluxe', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-65896.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(738, 'evoplay-rock-paper-scissors', 'Rock Paper Scissors', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-rock-paper-scissors.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(739, 'redrake-siberian-wolf', 'Siberian Wolf', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-siberian-wolf.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(740, 'tpg-ying-cai-shen', 'Ying Cai Shen', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-ying-cai-shen.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(741, 'hub88-griffins-glory', 'Griffin\'s Glory', 'slot', 'Kalamba', 'ms', 'wb', '', 'img/mrslotty/hub88-griffins-glory.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(742, 'play-pearls-big_game_hunter', 'John Hunter: Big Game Hunter', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-big_game_hunter.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(743, 'netgame-golden-skulls', 'Golden Skulls', 'slot', 'Netgame', 'ms', 'wb', '', 'img/mrslotty/netgame-golden-skulls.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(744, 'eagaming-13', 'Dragon Powerflame', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-13.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(745, 'spadegaming-alibaba', 'Alibaba', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-alibaba.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(746, 'wazdan-65904', 'Juicy Reels', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-65904.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(747, 'evoplay-russian-poker', 'Russian Poker', 'slot', 'Evoplay', 'ms', 'mb', 'JUME', 'img/mrslotty/evoplay-russian-poker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(748, 'redrake-mysteries-of-egypt', 'Mysteries of Egypt', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-mysteries-of-egypt.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(749, 'tpg-great-ocean', 'Great Ocean', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-great-ocean.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(750, 'hub88-bingolicia', 'Bingolícia', 'video_bingo', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-bingolicia.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(751, 'play-pearls-jacks_or_better_saloon', 'Jacks or Better Saloon', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-jacks_or_better_saloon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(752, 'netgame-hit-in-vegas', 'Hit in Vegas', 'slot', 'Netgame', 'ms', 'mb', '', 'img/mrslotty/netgame-hit-in-vegas.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(753, 'eagaming-13', 'Dragon Powerflame', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-13.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(754, 'spadegaming-alibaba', 'Alibaba', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-alibaba.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(755, 'wazdan-65904', 'Juicy Reels', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-65904.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(756, 'evoplay-russian-poker', 'Russian Poker', 'slot', 'Evoplay', 'ms', 'wb', 'JUME', 'img/mrslotty/evoplay-russian-poker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(757, 'redrake-mysteries-of-egypt', 'Mysteries of Egypt', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-mysteries-of-egypt.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(758, 'tpg-great-ocean', 'Great Ocean', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-great-ocean.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(759, 'hub88-bingolicia', 'Bingolícia', 'video_bingo', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-bingolicia.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(760, 'play-pearls-jacks_or_better_saloon', 'Jacks or Better Saloon', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-jacks_or_better_saloon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(761, 'netgame-hit-in-vegas', 'Hit in Vegas', 'slot', 'Netgame', 'ms', 'wb', '', 'img/mrslotty/netgame-hit-in-vegas.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(762, 'eagaming-15', 'Enter KTV', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-15.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(763, 'spadegaming-gods-kitchen', 'God\'s Kitchen', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-gods-kitchen.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(764, 'wazdan-131416', 'Burning Reels', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-131416.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(765, 'evoplay-robots-energy-conflict', 'Robots: Energy Conflict', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-robots-energy-conflict.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(766, 'redrake-world-football', 'World Football', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-world-football.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(767, 'tpg-5-dragons', '5 Dragons', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-5-dragons.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(768, 'hub88-circus-bingo', 'Circus Bingo', 'video_bingo', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-circus-bingo.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(769, 'play-pearls-scratch_cards_iron_sky', 'Iron Sky Vrilya Scratch Card', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-scratch_cards_iron_sky.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(770, 'netgame-jungle-2', 'Jungle 2', 'slot', 'Netgame', 'ms', 'mb', '', 'img/mrslotty/netgame-jungle-2.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(771, 'eagaming-15', 'Enter KTV', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-15.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(772, 'spadegaming-gods-kitchen', 'God\'s Kitchen', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-gods-kitchen.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(773, 'wazdan-131416', 'Burning Reels', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-131416.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(774, 'evoplay-robots-energy-conflict', 'Robots: Energy Conflict', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-robots-energy-conflict.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(775, 'redrake-world-football', 'World Football', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-world-football.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(776, 'tpg-5-dragons', '5 Dragons', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-5-dragons.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(777, 'hub88-circus-bingo', 'Circus Bingo', 'video_bingo', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-circus-bingo.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(778, 'play-pearls-scratch_cards_iron_sky', 'Iron Sky Vrilya Scratch Card', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-scratch_cards_iron_sky.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(779, 'netgame-jungle-2', 'Jungle 2', 'slot', 'Netgame', 'ms', 'wb', '', 'img/mrslotty/netgame-jungle-2.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(780, 'eagaming-16', 'Money Vault', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-16.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(781, 'spadegaming-master-chef', 'Master Chef', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-master-chef.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(782, 'wazdan-131419', 'Wild Jack', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-131419.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(783, 'evoplay-syndicate', 'Syndicate', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-syndicate.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(784, 'redrake-disco-nights', 'Disco Nights', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-disco-nights.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(785, 'tpg-king-of-seven', 'King of Seven', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-king-of-seven.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(786, 'hub88-bingo-genio', 'Bingo Gênio', 'video_bingo', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-bingo-genio.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(787, 'play-pearls-iron_sky', 'Iron Sky', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-iron_sky.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(788, 'netgame-magic-dragons', 'Magic Dragons', 'slot', 'Netgame', 'ms', 'mb', '', 'img/mrslotty/netgame-magic-dragons.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(789, 'eagaming-16', 'Money Vault', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-16.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(790, 'spadegaming-master-chef', 'Master Chef', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-master-chef.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(791, 'wazdan-131419', 'Wild Jack', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-131419.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(792, 'evoplay-syndicate', 'Syndicate', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-syndicate.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(793, 'redrake-disco-nights', 'Disco Nights', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-disco-nights.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(794, 'tpg-king-of-seven', 'King of Seven', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-king-of-seven.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(795, 'hub88-bingo-genio', 'Bingo Gênio', 'video_bingo', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-bingo-genio.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(796, 'play-pearls-iron_sky', 'Iron Sky', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-iron_sky.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(797, 'netgame-magic-dragons', 'Magic Dragons', 'slot', 'Netgame', 'ms', 'wb', '', 'img/mrslotty/netgame-magic-dragons.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(798, 'eagaming-17', 'Burning Pearl', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-17.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(799, 'spadegaming-daddys-vacation', 'Daddys Vacation', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-daddys-vacation.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(800, 'wazdan-131420', 'Magic Fruits 4', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-131420.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(801, 'evoplay-trip-to-the-future', 'Trip to the Future', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-trip-to-the-future.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(802, 'redrake-muertitos', 'Muertitos', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-muertitos.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(803, 'tpg-five-blessings', 'Five Blessings', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-five-blessings.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(804, 'hub88-bingo-iglu', 'Bingo Iglu', 'video_bingo', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-bingo-iglu.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(805, 'play-pearls-hot_cash_chest', 'Hot Cash Chest', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-hot_cash_chest.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(806, 'netgame-magic-tree', 'Magic Tree', 'slot', 'Netgame', 'ms', 'mb', '', 'img/mrslotty/netgame-magic-tree.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(807, 'eagaming-17', 'Burning Pearl', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-17.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(808, 'spadegaming-daddys-vacation', 'Daddys Vacation', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-daddys-vacation.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(809, 'wazdan-131420', 'Magic Fruits 4', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-131420.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(810, 'evoplay-trip-to-the-future', 'Trip to the Future', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-trip-to-the-future.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(811, 'redrake-muertitos', 'Muertitos', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-muertitos.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(812, 'tpg-five-blessings', 'Five Blessings', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-five-blessings.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(813, 'hub88-bingo-iglu', 'Bingo Iglu', 'video_bingo', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-bingo-iglu.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(814, 'play-pearls-hot_cash_chest', 'Hot Cash Chest', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-hot_cash_chest.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(815, 'netgame-magic-tree', 'Magic Tree', 'slot', 'Netgame', 'ms', 'wb', '', 'img/mrslotty/netgame-magic-tree.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(816, 'eagaming-19', 'Lucky Rooster', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-19.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(817, 'spadegaming-da-fu-xiao-fu', 'Da Fu Xiao Fu', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-da-fu-xiao-fu.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(818, 'wazdan-131422', 'Hungry Shark', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-131422.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(819, 'evoplay-et-lost-socks', 'E.T. Lost Socks', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-et-lost-socks.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(820, 'redrake-megamoney', 'Megamoney', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-megamoney.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(821, 'tpg-mermaid-treasure', 'Mermaid Treasure', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-mermaid-treasure.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(822, 'hub88-bingo-cientista-doidao', 'Bingo Cientista Doidão', 'video_bingo', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-bingo-cientista-doidao.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(823, 'play-pearls-hot_7_wheel', 'Hot 7 Wheel', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-hot_7_wheel.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(824, 'netgame-red-hot-chili-7-s', 'Red Hot Chili 7\'s', 'slot', 'Netgame', 'ms', 'mb', '', 'img/mrslotty/netgame-red-hot-chili-7-s.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(825, 'eagaming-19', 'Lucky Rooster', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-19.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(826, 'spadegaming-da-fu-xiao-fu', 'Da Fu Xiao Fu', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-da-fu-xiao-fu.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(827, 'wazdan-131422', 'Hungry Shark', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-131422.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(828, 'evoplay-et-lost-socks', 'E.T. Lost Socks', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-et-lost-socks.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(829, 'redrake-megamoney', 'Megamoney', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-megamoney.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(830, 'tpg-mermaid-treasure', 'Mermaid Treasure', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-mermaid-treasure.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(831, 'hub88-bingo-cientista-doidao', 'Bingo Cientista Doidão', 'video_bingo', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-bingo-cientista-doidao.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(832, 'play-pearls-hot_7_wheel', 'Hot 7 Wheel', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-hot_7_wheel.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(833, 'netgame-red-hot-chili-7-s', 'Red Hot Chili 7\'s', 'slot', 'Netgame', 'ms', 'wb', '', 'img/mrslotty/netgame-red-hot-chili-7-s.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(834, 'eagaming-20', 'Yggdrasil', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-20.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(835, 'spadegaming-great-stars-sa', 'Great Stars SA', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-great-stars-sa.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(836, 'wazdan-131424', 'Football Mania Deluxe', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-131424.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(837, 'evoplay-american-roulette-3d-classic', 'American Roulette 3D Classic', 'slot', 'Evoplay', 'ms', 'mb', 'RULA', 'img/mrslotty/evoplay-american-roulette-3d-classic.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(838, 'redrake-the-gold-of-poseidon', 'The gold of Poseidon', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-the-gold-of-poseidon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(839, 'tpg-the-lost-city-of-gold', 'The Lost City of Gold', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-the-lost-city-of-gold.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(840, 'hub88-bingo-pirata', 'Bingo Pirata', 'video_bingo', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-bingo-pirata.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(841, 'play-pearls-hippie_roll', 'Hippie Roll', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-hippie_roll.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(842, 'netgame-royal-fruits-20', 'Royal Fruits 20', 'slot', 'Netgame', 'ms', 'mb', '', 'img/mrslotty/netgame-royal-fruits-20.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(843, 'eagaming-20', 'Yggdrasil', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-20.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(844, 'spadegaming-great-stars-sa', 'Great Stars SA', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-great-stars-sa.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(845, 'wazdan-131424', 'Football Mania Deluxe', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-131424.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(846, 'evoplay-american-roulette-3d-classic', 'American Roulette 3D Classic', 'slot', 'Evoplay', 'ms', 'wb', 'RULA', 'img/mrslotty/evoplay-american-roulette-3d-classic.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(847, 'redrake-the-gold-of-poseidon', 'The gold of Poseidon', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-the-gold-of-poseidon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(848, 'tpg-the-lost-city-of-gold', 'The Lost City of Gold', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-the-lost-city-of-gold.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(849, 'hub88-bingo-pirata', 'Bingo Pirata', 'video_bingo', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-bingo-pirata.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(850, 'play-pearls-hippie_roll', 'Hippie Roll', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-hippie_roll.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(851, 'netgame-royal-fruits-20', 'Royal Fruits 20', 'slot', 'Netgame', 'ms', 'wb', '', 'img/mrslotty/netgame-royal-fruits-20.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(852, 'eagaming-21', 'Hot Fruits', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-21.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(853, 'spadegaming-dragon-gold-sa', 'Dragon Gold SA', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-dragon-gold-sa.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(854, 'wazdan-131428', 'Jackpot Builders', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-131428.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(855, 'evoplay-et-races', 'E.T. Races', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-et-races.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(856, 'redrake-planet-67', 'Planet 67', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-planet-67.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(857, 'tpg-jin-ping-mei', 'Jin Ping Mei', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-jin-ping-mei.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(858, 'hub88-jade-puzzle', 'Jade Puzzle', 'arcade', 'Green Jade', 'ms', 'mb', '', 'img/mrslotty/hub88-jade-puzzle.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(859, 'play-pearls-heavy_metal_princess', 'Heavy Metal Princess', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-heavy_metal_princess.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(860, 'netgame-royal-fruits-40', 'Royal Fruits 40', 'slot', 'Netgame', 'ms', 'mb', '', 'img/mrslotty/netgame-royal-fruits-40.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(861, 'eagaming-21', 'Hot Fruits', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-21.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(862, 'spadegaming-dragon-gold-sa', 'Dragon Gold SA', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-dragon-gold-sa.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(863, 'wazdan-131428', 'Jackpot Builders', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-131428.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(864, 'evoplay-et-races', 'E.T. Races', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-et-races.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(865, 'redrake-planet-67', 'Planet 67', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-planet-67.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(866, 'tpg-jin-ping-mei', 'Jin Ping Mei', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-jin-ping-mei.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(867, 'hub88-jade-puzzle', 'Jade Puzzle', 'arcade', 'Green Jade', 'ms', 'wb', '', 'img/mrslotty/hub88-jade-puzzle.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(868, 'play-pearls-heavy_metal_princess', 'Heavy Metal Princess', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-heavy_metal_princess.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(869, 'netgame-royal-fruits-40', 'Royal Fruits 40', 'slot', 'Netgame', 'ms', 'wb', '', 'img/mrslotty/netgame-royal-fruits-40.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(870, 'eagaming-22', 'Caishen Riches', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-22.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(871, 'spadegaming-wong-choy-sa', 'Wong Choy SA', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-wong-choy-sa.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(872, 'wazdan-131431', 'Black Hawk', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-131431.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(873, 'evoplay-high-striker', 'High Striker', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-high-striker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(874, 'redrake-triple-bonus-poker', 'TRIPLE BONUS POKER', 'slot', 'Red Rake', 'ms', 'mb', 'JUME', 'img/mrslotty/redrake-triple-bonus-poker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(875, 'tpg-lucky-gems', 'Lucky Gems', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-lucky-gems.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(876, 'slotexchange-smithers', 'Smithers', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-smithers.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(877, 'play-pearls-hallo_wins_day', 'Hallo Wins Day', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-hallo_wins_day.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(878, 'netgame-shining-lady', 'Shining Princess', 'slot', 'Netgame', 'ms', 'mb', '', 'img/mrslotty/netgame-shining-lady.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(879, 'eagaming-22', 'Caishen Riches', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-22.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(880, 'spadegaming-wong-choy-sa', 'Wong Choy SA', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-wong-choy-sa.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(881, 'wazdan-131431', 'Black Hawk', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-131431.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(882, 'evoplay-high-striker', 'High Striker', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-high-striker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(883, 'redrake-triple-bonus-poker', 'TRIPLE BONUS POKER', 'slot', 'Red Rake', 'ms', 'wb', 'JUME', 'img/mrslotty/redrake-triple-bonus-poker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(884, 'tpg-lucky-gems', 'Lucky Gems', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-lucky-gems.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(885, 'slotexchange-smithers', 'Smithers', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-smithers.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(886, 'play-pearls-hallo_wins_day', 'Hallo Wins Day', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-hallo_wins_day.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(887, 'netgame-shining-lady', 'Shining Princess', 'slot', 'Netgame', 'ms', 'wb', '', 'img/mrslotty/netgame-shining-lady.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(888, 'eagaming-23', 'Crypto Mania Bingo', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-23.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(889, 'spadegaming-5-fortune-sa', '5 Fortune SA', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-5-fortune-sa.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(890, 'wazdan-131432', 'Sizzling 777', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-131432.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(891, 'evoplay-poker-teen-patti', 'Poker Teen Patti', 'slot', 'Evoplay', 'ms', 'mb', 'JUME', 'img/mrslotty/evoplay-poker-teen-patti.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(892, 'redrake-sequential-royal', 'SEQUENTIAL ROYAL', 'slot', 'Red Rake', 'ms', 'mb', 'JUME', 'img/mrslotty/redrake-sequential-royal.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(893, 'tpg-book-of-dragons', 'Book of Dragons', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-book-of-dragons.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(894, 'slotexchange-atlas', 'Atlas', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-atlas.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(895, 'play-pearls-golden_stars', 'Golden Stars', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-golden_stars.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(896, 'netgame-space-rocks-2', 'Space Rocks 2', 'slot', 'Netgame', 'ms', 'mb', '', 'img/mrslotty/netgame-space-rocks-2.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(897, 'eagaming-23', 'Crypto Mania Bingo', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-23.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(898, 'spadegaming-5-fortune-sa', '5 Fortune SA', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-5-fortune-sa.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(899, 'wazdan-131432', 'Sizzling 777', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-131432.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(900, 'evoplay-poker-teen-patti', 'Poker Teen Patti', 'slot', 'Evoplay', 'ms', 'wb', 'JUME', 'img/mrslotty/evoplay-poker-teen-patti.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(901, 'redrake-sequential-royal', 'SEQUENTIAL ROYAL', 'slot', 'Red Rake', 'ms', 'wb', 'JUME', 'img/mrslotty/redrake-sequential-royal.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(902, 'tpg-book-of-dragons', 'Book of Dragons', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-book-of-dragons.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(903, 'slotexchange-atlas', 'Atlas', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-atlas.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(904, 'play-pearls-golden_stars', 'Golden Stars', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-golden_stars.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(905, 'netgame-space-rocks-2', 'Space Rocks 2', 'slot', 'Netgame', 'ms', 'wb', '', 'img/mrslotty/netgame-space-rocks-2.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(906, 'eagaming-24', 'Neptune Treasure Bingo', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-24.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(907, 'spadegaming-santa-gifts', 'Santa Gifts', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-santa-gifts.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(908, 'wazdan-196952', 'Magic Target', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-196952.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(909, 'evoplay-nuke-world', 'Nuke World', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-nuke-world.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(910, 'redrake-royal-court', 'ROYAL COURT', 'slot', 'Red Rake', 'ms', 'mb', 'JUME', 'img/mrslotty/redrake-royal-court.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(911, 'tpg-goldy-piggy', 'Goldy Piggy', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-goldy-piggy.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(912, 'slotexchange-bora-bora', 'Bora-Bora', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-bora-bora.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(913, 'play-pearls-golden_potion', 'Golden Potion', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-golden_potion.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(914, 'netgame-thunder-strike', 'Thunder Strike', 'slot', 'Netgame', 'ms', 'mb', '', 'img/mrslotty/netgame-thunder-strike.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(915, 'evoplay-exploding-fruits', 'Exploding Fruits', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-exploding-fruits.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(916, 'eagaming-24', 'Neptune Treasure Bingo', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-24.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(917, 'spadegaming-santa-gifts', 'Santa Gifts', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-santa-gifts.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(918, 'wazdan-196952', 'Magic Target', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-196952.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(919, 'evoplay-nuke-world', 'Nuke World', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-nuke-world.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(920, 'redrake-royal-court', 'ROYAL COURT', 'slot', 'Red Rake', 'ms', 'wb', 'JUME', 'img/mrslotty/redrake-royal-court.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(921, 'tpg-goldy-piggy', 'Goldy Piggy', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-goldy-piggy.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(922, 'slotexchange-bora-bora', 'Bora-Bora', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-bora-bora.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(923, 'play-pearls-golden_potion', 'Golden Potion', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-golden_potion.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(924, 'netgame-thunder-strike', 'Thunder Strike', 'slot', 'Netgame', 'ms', 'wb', '', 'img/mrslotty/netgame-thunder-strike.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(925, 'evoplay-exploding-fruits', 'Exploding Fruits', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-exploding-fruits.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(926, 'shehebeach', 'She/He_beach', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/shehebeach.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(927, 'eagaming-25', 'Bagua2', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-25.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(928, 'spadegaming-lucky-meow', 'Lucky Meow', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-lucky-meow.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(929, 'wazdan-196956', 'Magic Hot 4', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-196956.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(930, 'evoplay-reign-of-dragons', 'Reign of Dragons', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-reign-of-dragons.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(931, 'redrake-five-aces', 'FIVE ACES', 'slot', 'Red Rake', 'ms', 'mb', 'JUME', 'img/mrslotty/redrake-five-aces.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(932, 'tpg-777-slot', '777 Slot', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-777-slot.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(933, 'slotexchange-candies', 'Candies', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-candies.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(934, 'play-pearls-golden_egg_keno', 'Golden Egg Keno', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-golden_egg_keno.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(935, 'netgame-volcano-fruits', 'Volcano Fruits', 'slot', 'Netgame', 'ms', 'mb', '', 'img/mrslotty/netgame-volcano-fruits.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(936, 'tpg-king-of-fruits', 'King of Fruits', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-king-of-fruits.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(937, 'shehebeach', 'She/He_beach', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/shehebeach.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(938, 'eagaming-25', 'Bagua2', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-25.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(939, 'spadegaming-lucky-meow', 'Lucky Meow', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-lucky-meow.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(940, 'wazdan-196956', 'Magic Hot 4', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-196956.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(941, 'evoplay-reign-of-dragons', 'Reign of Dragons', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-reign-of-dragons.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(942, 'redrake-five-aces', 'FIVE ACES', 'slot', 'Red Rake', 'ms', 'wb', 'JUME', 'img/mrslotty/redrake-five-aces.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(943, 'tpg-777-slot', '777 Slot', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-777-slot.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(944, 'slotexchange-candies', 'Candies', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-candies.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(945, 'play-pearls-golden_egg_keno', 'Golden Egg Keno', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-golden_egg_keno.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(946, 'netgame-volcano-fruits', 'Volcano Fruits', 'slot', 'Netgame', 'ms', 'wb', '', 'img/mrslotty/netgame-volcano-fruits.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(947, 'tpg-king-of-fruits', 'King of Fruits', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-king-of-fruits.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(948, 'marsdinner', 'MarsDinner', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/marsdinner.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(949, 'eagaming-26', 'Octagon Gem 2', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-26.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(950, 'spadegaming-honey-hunter', 'Honey Hunter', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-honey-hunter.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(951, 'wazdan-196958', 'Back to the 70\'s', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-196958.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(952, 'evoplay-dungeon', 'Dungeon', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-dungeon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(953, 'redrake-faces-and-deuces', 'FACES AND DEUCES', 'slot', 'Red Rake', 'ms', 'mb', 'JUME', 'img/mrslotty/redrake-faces-and-deuces.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(954, 'tpg-tarot-deck', 'Tarot Deck', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-tarot-deck.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(955, 'slotexchange-embers', 'Embers', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-embers.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(956, 'play-pearls-golden_dragon', 'Golden Dragon', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-golden_dragon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(957, 'netgame-wild-buffalo', 'Wild Buffalo', 'slot', 'Netgame', 'ms', 'mb', '', 'img/mrslotty/netgame-wild-buffalo.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(958, 'tpg-fortune-hot-pot', 'Fortune Hot Pot', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-fortune-hot-pot.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(959, 'marsdinner', 'MarsDinner', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/marsdinner.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(960, 'eagaming-26', 'Octagon Gem 2', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-26.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(961, 'spadegaming-honey-hunter', 'Honey Hunter', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-honey-hunter.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(962, 'wazdan-196958', 'Back to the 70\'s', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-196958.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(963, 'evoplay-dungeon', 'Dungeon', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-dungeon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(964, 'redrake-faces-and-deuces', 'FACES AND DEUCES', 'slot', 'Red Rake', 'ms', 'wb', 'JUME', 'img/mrslotty/redrake-faces-and-deuces.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(965, 'tpg-tarot-deck', 'Tarot Deck', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-tarot-deck.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(966, 'slotexchange-embers', 'Embers', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-embers.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(967, 'play-pearls-golden_dragon', 'Golden Dragon', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-golden_dragon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(968, 'netgame-wild-buffalo', 'Wild Buffalo', 'slot', 'Netgame', 'ms', 'wb', '', 'img/mrslotty/netgame-wild-buffalo.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(969, 'tpg-fortune-hot-pot', 'Fortune Hot Pot', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-fortune-hot-pot.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(970, 'wild7fruits', 'Wild7Fruits', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/wild7fruits.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(971, 'eagaming-27', 'Dragon Of The Eastern Sea', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-27.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(972, 'spadegaming-drunken-jungle', 'Drunken Jungle', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-drunken-jungle.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(973, 'wazdan-196960', 'Fruit Mania', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-196960.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(974, 'evoplay-sprinkle', 'Sprinkle', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-sprinkle.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(975, 'redrake-double-aces-and-faces', 'DOUBLE ACES AND FACES', 'slot', 'Red Rake', 'ms', 'mb', 'JUME', 'img/mrslotty/redrake-double-aces-and-faces.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(976, 'tpg-fruity-fruit-farm', 'Fruity Fruit Farm', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-fruity-fruit-farm.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(977, 'slotexchange-fairies', 'Fairies', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-fairies.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(978, 'play-pearls-gangster_city', 'Gangster City', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-gangster_city.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(979, 'netgame-wolf-reels', 'Wolf Reels', 'slot', 'Netgame', 'ms', 'mb', '', 'img/mrslotty/netgame-wolf-reels.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(980, 'spadegaming-mega-7', 'Mega 7', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-mega-7.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(981, 'wild7fruits', 'Wild7Fruits', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/wild7fruits.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(982, 'eagaming-27', 'Dragon Of The Eastern Sea', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-27.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(983, 'spadegaming-drunken-jungle', 'Drunken Jungle', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-drunken-jungle.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(984, 'wazdan-196960', 'Fruit Mania', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-196960.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(985, 'evoplay-sprinkle', 'Sprinkle', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-sprinkle.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(986, 'redrake-double-aces-and-faces', 'DOUBLE ACES AND FACES', 'slot', 'Red Rake', 'ms', 'wb', 'JUME', 'img/mrslotty/redrake-double-aces-and-faces.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(987, 'tpg-fruity-fruit-farm', 'Fruity Fruit Farm', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-fruity-fruit-farm.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(988, 'slotexchange-fairies', 'Fairies', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-fairies.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(989, 'play-pearls-gangster_city', 'Gangster City', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-gangster_city.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(990, 'netgame-wolf-reels', 'Wolf Reels', 'slot', 'Netgame', 'ms', 'wb', '', 'img/mrslotty/netgame-wolf-reels.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(991, 'spadegaming-mega-7', 'Mega 7', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-mega-7.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(992, 'trendyskulls', 'Trendy Skulls', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/trendyskulls.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(993, 'eagaming-28', 'Phoenix 888', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-28.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(994, 'spadegaming-spin-stone', 'Spin Stone', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-spin-stone.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(995, 'wazdan-196968', 'Hot Party', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-196968.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(996, 'evoplay-brutal-santa', 'Brutal Santa', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-brutal-santa.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(997, 'redrake-bonus-deuces-wild', 'BONUS DEUCES WILD', 'slot', 'Red Rake', 'ms', 'mb', 'JUME', 'img/mrslotty/redrake-bonus-deuces-wild.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(998, 'tpg-mahjong-house', 'Mahjong House', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-mahjong-house.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(999, 'slotexchange-war-worlds', 'War worlds', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-war-worlds.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1000, 'play-pearls-fun_farm', 'Fun Farm', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-fun_farm.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1001, 'tomhorn-joker-reelz', 'Joker Reelz', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-joker-reelz.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1002, 'platipus-dajidali', 'Da Ji Da Li', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-dajidali.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1003, 'trendyskulls', 'Trendy Skulls', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/trendyskulls.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1004, 'eagaming-28', 'Phoenix 888', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-28.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1005, 'spadegaming-spin-stone', 'Spin Stone', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-spin-stone.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1006, 'wazdan-196968', 'Hot Party', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-196968.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1007, 'evoplay-brutal-santa', 'Brutal Santa', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-brutal-santa.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1008, 'redrake-bonus-deuces-wild', 'BONUS DEUCES WILD', 'slot', 'Red Rake', 'ms', 'wb', 'JUME', 'img/mrslotty/redrake-bonus-deuces-wild.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1009, 'tpg-mahjong-house', 'Mahjong House', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-mahjong-house.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1010, 'slotexchange-war-worlds', 'War worlds', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-war-worlds.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1011, 'play-pearls-fun_farm', 'Fun Farm', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-fun_farm.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1012, 'tomhorn-joker-reelz', 'Joker Reelz', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-joker-reelz.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1013, 'platipus-dajidali', 'Da Ji Da Li', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-dajidali.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1014, 'treasuresofegypt', 'Treasures of Egypt', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/treasuresofegypt.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1015, 'eagaming-29', 'Lady Hawk', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-29.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1016, 'spadegaming-emperor-gate-sa', 'Emperor Gate SA', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-emperor-gate-sa.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1017, 'wazdan-262492', 'Colin The Cat', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-262492.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1018, 'evoplay-maze', 'Maze', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-maze.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1019, 'redrake-aces-eights', 'ACES & EIGHTS', 'slot', 'Red Rake', 'ms', 'mb', 'JUME', 'img/mrslotty/redrake-aces-eights.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1020, 'tpg-sea-world', 'Sea World', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-sea-world.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1021, 'slotexchange-shadows', 'Shadows', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-shadows.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1022, 'play-pearls-fruit_loops', 'Fruit Loops', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-fruit_loops.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1023, 'tomhorn-spinball', 'Spinball', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-spinball.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1024, 'platipus-piratesmap', 'Pirates Map', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-piratesmap.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1025, 'treasuresofegypt', 'Treasures of Egypt', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/treasuresofegypt.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1026, 'eagaming-29', 'Lady Hawk', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-29.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1027, 'spadegaming-emperor-gate-sa', 'Emperor Gate SA', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-emperor-gate-sa.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1028, 'wazdan-262492', 'Colin The Cat', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-262492.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1029, 'evoplay-maze', 'Maze', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-maze.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1030, 'redrake-aces-eights', 'ACES & EIGHTS', 'slot', 'Red Rake', 'ms', 'wb', 'JUME', 'img/mrslotty/redrake-aces-eights.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1031, 'tpg-sea-world', 'Sea World', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-sea-world.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1032, 'slotexchange-shadows', 'Shadows', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-shadows.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1033, 'play-pearls-fruit_loops', 'Fruit Loops', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-fruit_loops.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1034, 'tomhorn-spinball', 'Spinball', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-spinball.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1035, 'platipus-piratesmap', 'Pirates Map', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-piratesmap.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1036, 'trollfaces', 'Troll Faces', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/trollfaces.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1037, 'eagaming-30', 'Chilli Hunter', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-30.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1038, 'spadegaming-lucky-feng-shui', 'Lucky Feng Shui', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-lucky-feng-shui.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1039, 'wazdan-262494', 'Great Book of Magic', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-262494.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1040, 'evoplay-legend-of-kaan', 'Legend of Kaan', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-legend-of-kaan.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1041, 'redrake-aces-deuces-bonus-poker', 'ACES & DEUCES BONUS POKER', 'slot', 'Red Rake', 'ms', 'mb', 'JUME', 'img/mrslotty/redrake-aces-deuces-bonus-poker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1042, 'tpg-christmas-joy', 'Christmas Joy', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-christmas-joy.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1043, 'slotexchange-vikings-war', 'Vikings war', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-vikings-war.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1044, 'play-pearls-fruit_basket', 'Fruit Basket', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-fruit_basket.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1045, 'tomhorn-diamond-hill', 'Diamond Hill', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-diamond-hill.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1046, 'platipus-caishensgifts', 'Caishen\'s Gifts', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-caishensgifts.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1047, 'trollfaces', 'Troll Faces', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/trollfaces.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1048, 'eagaming-30', 'Chilli Hunter', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-30.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1049, 'spadegaming-lucky-feng-shui', 'Lucky Feng Shui', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-lucky-feng-shui.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1050, 'wazdan-262494', 'Great Book of Magic', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-262494.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1051, 'evoplay-legend-of-kaan', 'Legend of Kaan', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-legend-of-kaan.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1052, 'redrake-aces-deuces-bonus-poker', 'ACES & DEUCES BONUS POKER', 'slot', 'Red Rake', 'ms', 'wb', 'JUME', 'img/mrslotty/redrake-aces-deuces-bonus-poker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1053, 'tpg-christmas-joy', 'Christmas Joy', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-christmas-joy.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1054, 'slotexchange-vikings-war', 'Vikings war', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-vikings-war.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1055, 'play-pearls-fruit_basket', 'Fruit Basket', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-fruit_basket.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1056, 'tomhorn-diamond-hill', 'Diamond Hill', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-diamond-hill.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1057, 'platipus-caishensgifts', 'Caishen\'s Gifts', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-caishensgifts.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1058, 'hotfruits', 'HOT Fruits', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/hotfruits.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1059, 'eagaming-31', 'Wild Fairies', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-31.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1060, 'spadegaming-magical-lamp', 'Magical Lamp', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-magical-lamp.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1061, 'wazdan-262496', 'Football Mania', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-262496.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1062, 'evoplay-hungry-night', 'Hungry Night', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-hungry-night.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1063, 'redrake-tens-or-better', 'TENS OR BETTER', 'slot', 'Red Rake', 'ms', 'mb', 'JUME', 'img/mrslotty/redrake-tens-or-better.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1064, 'tpg-full-moon-festival', 'Full Moon Festival', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-full-moon-festival.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1065, 'slotexchange-sirenes', 'Sirenes', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-sirenes.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1066, 'play-pearls-freya', 'Freya', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-freya.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1067, 'tomhorn-hotnfruity', 'Hot\'n\'Fruity', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-hotnfruity.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1068, 'platipus-dragonselement', 'Dragon\'s Element', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-dragonselement.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1069, 'hotfruits', 'HOT Fruits', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/hotfruits.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1070, 'eagaming-31', 'Wild Fairies', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-31.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1071, 'spadegaming-magical-lamp', 'Magical Lamp', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-magical-lamp.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1072, 'wazdan-262496', 'Football Mania', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-262496.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1073, 'evoplay-hungry-night', 'Hungry Night', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-hungry-night.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1074, 'redrake-tens-or-better', 'TENS OR BETTER', 'slot', 'Red Rake', 'ms', 'wb', 'JUME', 'img/mrslotty/redrake-tens-or-better.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1075, 'tpg-full-moon-festival', 'Full Moon Festival', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-full-moon-festival.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1076, 'slotexchange-sirenes', 'Sirenes', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-sirenes.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1077, 'play-pearls-freya', 'Freya', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-freya.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1078, 'tomhorn-hotnfruity', 'Hot\'n\'Fruity', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-hotnfruity.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1079, 'platipus-dragonselement', 'Dragon\'s Element', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-dragonselement.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1080, 'golden7fruits', 'Golden7Fruits', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/golden7fruits.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1081, 'eagaming-33', 'Burning Pearl Bingo', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-33.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1082, 'spadegaming-prosperity-gods', 'Prosperity Gods', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-prosperity-gods.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1083, 'wazdan-262504', 'Magic Stars 5', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-262504.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1084, 'evoplay-courier-sweeper', 'Courier Sweeper', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-courier-sweeper.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1085, 'redrake-deuces-jokers', 'DEUCES & JOKERS', 'slot', 'Red Rake', 'ms', 'mb', 'JUME', 'img/mrslotty/redrake-deuces-jokers.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1086, 'tpg-pac-man', 'Pac-man', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-pac-man.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1087, 'slotexchange-arcanum', 'Arcanum', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-arcanum.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1088, 'play-pearls-enchanted_lot', 'Enchanted Lot', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-enchanted_lot.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1089, 'tomhorn-incass-treasure', 'Incas\'s Treasure', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-incass-treasure.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1090, 'platipus-theancientfour', 'The Ancient Four', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-theancientfour.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1091, 'golden7fruits', 'Golden7Fruits', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/golden7fruits.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1092, 'eagaming-33', 'Burning Pearl Bingo', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-33.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1093, 'spadegaming-prosperity-gods', 'Prosperity Gods', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-prosperity-gods.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1094, 'wazdan-262504', 'Magic Stars 5', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-262504.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1095, 'evoplay-courier-sweeper', 'Courier Sweeper', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-courier-sweeper.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1096, 'redrake-deuces-jokers', 'DEUCES & JOKERS', 'slot', 'Red Rake', 'ms', 'wb', 'JUME', 'img/mrslotty/redrake-deuces-jokers.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1097, 'tpg-pac-man', 'Pac-man', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-pac-man.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1098, 'slotexchange-arcanum', 'Arcanum', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-arcanum.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1099, 'play-pearls-enchanted_lot', 'Enchanted Lot', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-enchanted_lot.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1100, 'tomhorn-incass-treasure', 'Incas\'s Treasure', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-incass-treasure.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1101, 'platipus-theancientfour', 'The Ancient Four', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-theancientfour.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1102, '777diamonds', '777 Diamonds', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/777diamonds.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1103, 'eagaming-34', 'China', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-34.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1104, 'spadegaming-sweet-bakery', 'Sweet Bakery', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-sweet-bakery.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1105, 'wazdan-328030', 'Magic Of The Ring', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-328030.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1106, 'evoplay-scratch-match', 'Scratch Match', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-scratch-match.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1107, 'redrake-double-double-bonus', 'DOUBLE DOUBLE BONUS', 'slot', 'Red Rake', 'ms', 'mb', 'JUME', 'img/mrslotty/redrake-double-double-bonus.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1108, 'tpg-journey-frog', 'Journey Frog', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-journey-frog.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1109, 'slotexchange-royal-flush', 'Royal flush', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-royal-flush.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1110, 'play-pearls-dragons_gold', 'Dragons Gold', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-dragons_gold.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1111, 'tomhorn-kongo-bongo', 'Kongo Bongo', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-kongo-bongo.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1112, 'evoplay-sea-of-spins', 'Sea of Spins', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-sea-of-spins.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1113, '777diamonds', '777 Diamonds', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/777diamonds.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1114, 'eagaming-34', 'China', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-34.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1115, 'spadegaming-sweet-bakery', 'Sweet Bakery', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-sweet-bakery.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1116, 'wazdan-328030', 'Magic Of The Ring', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-328030.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1117, 'evoplay-scratch-match', 'Scratch Match', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-scratch-match.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1118, 'redrake-double-double-bonus', 'DOUBLE DOUBLE BONUS', 'slot', 'Red Rake', 'ms', 'wb', 'JUME', 'img/mrslotty/redrake-double-double-bonus.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1119, 'tpg-journey-frog', 'Journey Frog', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-journey-frog.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1120, 'slotexchange-royal-flush', 'Royal flush', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-royal-flush.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1121, 'play-pearls-dragons_gold', 'Dragons Gold', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-dragons_gold.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1122, 'tomhorn-kongo-bongo', 'Kongo Bongo', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-kongo-bongo.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1123, 'evoplay-sea-of-spins', 'Sea of Spins', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-sea-of-spins.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1124, 'memefaces', 'Meme Faces', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/memefaces.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1125, 'eagaming-35', 'Wild Giant Panda', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-35.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1126, 'spadegaming-pirate-king', 'Pirate King', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-pirate-king.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1127, 'gamshy-bad-monsters', 'Bad Monsters', 'slot', 'Gamshy', 'ms', 'mb', '', 'img/mrslotty/gamshy-bad-monsters.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1128, 'evoplay-book-of-rest', 'Book of Rest', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-book-of-rest.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1129, 'redrake-double-bonus', 'DOUBLE BONUS', 'slot', 'Red Rake', 'ms', 'mb', 'JUME', 'img/mrslotty/redrake-double-bonus.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1130, 'tpg-god-of-fortune', 'God of Fortune', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-god-of-fortune.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1131, 'slotexchange-sexy-pirates', 'Sexy pirates', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-sexy-pirates.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1132, 'play-pearls-dragons_cave', 'Dragons Cave', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-dragons_cave.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1133, 'tomhorn-sherlock', 'Sherlock', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-sherlock.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1134, 'evoplay-surf-zone', 'Surf Zone', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-surf-zone.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1135, 'memefaces', 'Meme Faces', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/memefaces.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1136, 'eagaming-35', 'Wild Giant Panda', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-35.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1137, 'spadegaming-pirate-king', 'Pirate King', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-pirate-king.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1138, 'gamshy-bad-monsters', 'Bad Monsters', 'slot', 'Gamshy', 'ms', 'wb', '', 'img/mrslotty/gamshy-bad-monsters.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1139, 'evoplay-book-of-rest', 'Book of Rest', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-book-of-rest.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1140, 'redrake-double-bonus', 'DOUBLE BONUS', 'slot', 'Red Rake', 'ms', 'wb', 'JUME', 'img/mrslotty/redrake-double-bonus.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1141, 'tpg-god-of-fortune', 'God of Fortune', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-god-of-fortune.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1142, 'slotexchange-sexy-pirates', 'Sexy pirates', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-sexy-pirates.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1143, 'play-pearls-dragons_cave', 'Dragons Cave', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-dragons_cave.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1144, 'tomhorn-sherlock', 'Sherlock', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-sherlock.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1145, 'evoplay-surf-zone', 'Surf Zone', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-surf-zone.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1146, 'cleopatra18', 'Cleopatra 18+', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/cleopatra18.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1147, 'eagaming-36', 'Four Dragons', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-36.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1148, 'spadegaming-triple-panda', 'Triple Panda', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-triple-panda.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1149, 'gamshy-kraken-island', 'Kraken Island', 'slot', 'Gamshy', 'ms', 'mb', '', 'img/mrslotty/gamshy-kraken-island.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1150, 'evoplay-hot-triple-sevens', 'Hot Triple Sevens', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-hot-triple-sevens.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1151, 'redrake-aces-faces', 'ACES & FACES', 'slot', 'Red Rake', 'ms', 'mb', 'JUME', 'img/mrslotty/redrake-aces-faces.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1152, 'tpg-fortune-cat', 'Fortune Cat', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-fortune-cat.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1153, 'slotexchange-goal', 'Goal', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-goal.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1154, 'play-pearls-deep_sea', 'Deep Sea', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-deep_sea.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1155, 'tomhorn-frozen-queen', 'Frozen Queen', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-frozen-queen.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1156, 'evoplay-texas-holdem-bonus', 'Texas Holdem Bonus', 'slot', 'Evoplay', 'ms', 'mb', 'JUME', 'img/mrslotty/evoplay-texas-holdem-bonus.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1157, 'cleopatra18', 'Cleopatra 18+', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/cleopatra18.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1158, 'eagaming-36', 'Four Dragons', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-36.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1159, 'spadegaming-triple-panda', 'Triple Panda', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-triple-panda.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1160, 'gamshy-kraken-island', 'Kraken Island', 'slot', 'Gamshy', 'ms', 'wb', '', 'img/mrslotty/gamshy-kraken-island.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1161, 'evoplay-hot-triple-sevens', 'Hot Triple Sevens', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-hot-triple-sevens.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1162, 'redrake-aces-faces', 'ACES & FACES', 'slot', 'Red Rake', 'ms', 'wb', 'JUME', 'img/mrslotty/redrake-aces-faces.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1163, 'tpg-fortune-cat', 'Fortune Cat', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-fortune-cat.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1164, 'slotexchange-goal', 'Goal', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-goal.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1165, 'play-pearls-deep_sea', 'Deep Sea', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-deep_sea.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1166, 'tomhorn-frozen-queen', 'Frozen Queen', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-frozen-queen.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1167, 'evoplay-texas-holdem-bonus', 'Texas Holdem Bonus', 'slot', 'Evoplay', 'ms', 'wb', 'JUME', 'img/mrslotty/evoplay-texas-holdem-bonus.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1168, 'monsterinos', 'Monsterinos', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/monsterinos.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1169, 'eagaming-37', 'Lions Dance', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-37.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1170, 'spadegaming-dancing-fever', 'Dancing Fever', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-dancing-fever.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1171, 'gamshy-tubolarium', 'Tubolarium', 'slot', 'Gamshy', 'ms', 'mb', '', 'img/mrslotty/gamshy-tubolarium.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1172, 'evoplay-magic-wheel', 'Magic Wheel', 'slot', 'Evoplay', 'ms', 'mb', 'JUME', 'img/mrslotty/evoplay-magic-wheel.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1173, 'redrake-number-bonus', 'NUMBER BONUS', 'slot', 'Red Rake', 'ms', 'mb', 'JUME', 'img/mrslotty/redrake-number-bonus.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1174, 'tpg-soccer-all-star', 'Soccer All Star', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-soccer-all-star.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1175, 'slotexchange-aztecs', 'Aztecs', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-aztecs.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1176, 'play-pearls-dazzling_gems', 'Dazzling Gems', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-dazzling_gems.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1177, 'tomhorn-blackbeards-quest-mini', 'Blackbeard\'s Quest Mini', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-blackbeards-quest-mini.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1178, 'wearecasino-belly-dance', 'Belly Dance', 'slot', 'Wearecasino', 'ms', 'mb', '', 'img/mrslotty/wearecasino-belly-dance.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1179, 'monsterinos', 'Monsterinos', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/monsterinos.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1180, 'eagaming-37', 'Lions Dance', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-37.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1181, 'spadegaming-dancing-fever', 'Dancing Fever', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-dancing-fever.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1182, 'gamshy-tubolarium', 'Tubolarium', 'slot', 'Gamshy', 'ms', 'wb', '', 'img/mrslotty/gamshy-tubolarium.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1183, 'evoplay-magic-wheel', 'Magic Wheel', 'slot', 'Evoplay', 'ms', 'wb', 'JUME', 'img/mrslotty/evoplay-magic-wheel.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1184, 'redrake-number-bonus', 'NUMBER BONUS', 'slot', 'Red Rake', 'ms', 'wb', 'JUME', 'img/mrslotty/redrake-number-bonus.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1185, 'tpg-soccer-all-star', 'Soccer All Star', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-soccer-all-star.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1186, 'slotexchange-aztecs', 'Aztecs', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-aztecs.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1187, 'play-pearls-dazzling_gems', 'Dazzling Gems', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-dazzling_gems.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1188, 'tomhorn-blackbeards-quest-mini', 'Blackbeard\'s Quest Mini', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-blackbeards-quest-mini.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1189, 'wearecasino-belly-dance', 'Belly Dance', 'slot', 'Wearecasino', 'ms', 'wb', '', 'img/mrslotty/wearecasino-belly-dance.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1190, 'superdragonsfire', 'Super Dragons Fire', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/superdragonsfire.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1191, 'eagaming-38', 'Ancient Artifacts', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-38.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1192, 'spadegaming-fishing-war', 'Fishing War', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-fishing-war.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1193, 'gamshy-sweet-maniacs', 'Sweet Maniacs', 'slot', 'Gamshy', 'ms', 'mb', '', 'img/mrslotty/gamshy-sweet-maniacs.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1194, 'evoplay-bomb-squad', 'Bomb Squad', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-bomb-squad.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1195, 'redrake-all-american', 'ALL AMERICAN', 'slot', 'Red Rake', 'ms', 'mb', 'JUME', 'img/mrslotty/redrake-all-american.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1196, 'tpg-gentlemen', 'Gentlemen', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-gentlemen.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1197, 'slotexchange-elven-archer', 'Elven archer', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-elven-archer.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1198, 'play-pearls-dangerous_billy', 'Dangerous Billy', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-dangerous_billy.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1199, 'tomhorn-dragon-riches', 'Dragon Riches', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-dragon-riches.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1200, 'wearecasino-new-classic-cherries', 'New Classic Cherries', 'slot', 'Wearecasino', 'ms', 'mb', '', 'img/mrslotty/wearecasino-new-classic-cherries.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1201, 'superdragonsfire', 'Super Dragons Fire', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/superdragonsfire.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1202, 'eagaming-38', 'Ancient Artifacts', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-38.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1203, 'spadegaming-fishing-war', 'Fishing War', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-fishing-war.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1204, 'gamshy-sweet-maniacs', 'Sweet Maniacs', 'slot', 'Gamshy', 'ms', 'wb', '', 'img/mrslotty/gamshy-sweet-maniacs.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1205, 'evoplay-bomb-squad', 'Bomb Squad', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-bomb-squad.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1206, 'redrake-all-american', 'ALL AMERICAN', 'slot', 'Red Rake', 'ms', 'wb', 'JUME', 'img/mrslotty/redrake-all-american.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1207, 'tpg-gentlemen', 'Gentlemen', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-gentlemen.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1208, 'slotexchange-elven-archer', 'Elven archer', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-elven-archer.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1209, 'play-pearls-dangerous_billy', 'Dangerous Billy', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-dangerous_billy.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1210, 'tomhorn-dragon-riches', 'Dragon Riches', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-dragon-riches.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1211, 'wearecasino-new-classic-cherries', 'New Classic Cherries', 'slot', 'Wearecasino', 'ms', 'wb', '', 'img/mrslotty/wearecasino-new-classic-cherries.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1212, 'goldenjokerdice', 'Golden Joker Dice', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/goldenjokerdice.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1213, 'eagaming-39', 'Taishang Laojun', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-39.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1214, 'spadegaming-highway-fortune', 'Highway Fortune', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-highway-fortune.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1215, 'gamshy-toon-cops', 'Toon Cops', 'slot', 'Gamshy', 'ms', 'mb', '', 'img/mrslotty/gamshy-toon-cops.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1216, 'evoplay-season-sisters', 'Season sisters', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-season-sisters.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1217, 'redrake-joker-poker', 'JOKER POKER', 'slot', 'Red Rake', 'ms', 'mb', 'JUME', 'img/mrslotty/redrake-joker-poker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1218, 'tpg-only-side-by-side-with-you', 'Only side by side with you', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-only-side-by-side-with-you.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1219, 'slotexchange-lost-ship', 'Lost ship', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-lost-ship.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1220, 'play-pearls-colt_pi', 'Colt PI', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-colt_pi.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1221, 'tomhorn-wild-weather', 'Wild Weather', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-wild-weather.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1222, 'redrake-super-15-stars', 'Super 15 Stars', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-super-15-stars.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1223, 'goldenjokerdice', 'Golden Joker Dice', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/goldenjokerdice.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1224, 'eagaming-39', 'Taishang Laojun', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-39.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1225, 'spadegaming-highway-fortune', 'Highway Fortune', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-highway-fortune.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1226, 'gamshy-toon-cops', 'Toon Cops', 'slot', 'Gamshy', 'ms', 'wb', '', 'img/mrslotty/gamshy-toon-cops.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1227, 'evoplay-season-sisters', 'Season sisters', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-season-sisters.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1228, 'redrake-joker-poker', 'JOKER POKER', 'slot', 'Red Rake', 'ms', 'wb', 'JUME', 'img/mrslotty/redrake-joker-poker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1229, 'tpg-only-side-by-side-with-you', 'Only side by side with you', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-only-side-by-side-with-you.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1230, 'slotexchange-lost-ship', 'Lost ship', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-lost-ship.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1231, 'play-pearls-colt_pi', 'Colt PI', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-colt_pi.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1232, 'tomhorn-wild-weather', 'Wild Weather', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-wild-weather.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1233, 'redrake-super-15-stars', 'Super 15 Stars', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-super-15-stars.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1234, 'zeusthethunderer2', 'Zeus the Thunderer II', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/zeusthethunderer2.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1235, 'eagaming-40', 'Peach Banquet', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-40.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1236, 'spadegaming-golf-champions', 'Golf Champions', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-golf-champions.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1237, 'gamshy-jumping-sushi', 'Jumping Sushi', 'slot', 'Gamshy', 'ms', 'mb', '', 'img/mrslotty/gamshy-jumping-sushi.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1238, 'evoplay-mafia-syndicate', 'Mafia Syndicate', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-mafia-syndicate.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1239, 'redrake-bonus-poker', 'BONUS POKER', 'slot', 'Red Rake', 'ms', 'mb', 'JUME', 'img/mrslotty/redrake-bonus-poker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1240, 'tpg-dessert-mario', 'Dessert Mario', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-dessert-mario.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1241, 'slotexchange-boxing-ring', 'Boxing ring', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-boxing-ring.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1242, 'play-pearls-chuggers_pot', 'Chuggers Pot', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-chuggers_pot.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1243, 'tomhorn-red-lights', 'Red Lights', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-red-lights.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1244, 'redrake-1st-of-the-irish', '1st of the Irish', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-1st-of-the-irish.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1245, 'zeusthethunderer2', 'Zeus the Thunderer II', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/zeusthethunderer2.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1246, 'eagaming-40', 'Peach Banquet', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-40.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1247, 'spadegaming-golf-champions', 'Golf Champions', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-golf-champions.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1248, 'gamshy-jumping-sushi', 'Jumping Sushi', 'slot', 'Gamshy', 'ms', 'wb', '', 'img/mrslotty/gamshy-jumping-sushi.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1249, 'evoplay-mafia-syndicate', 'Mafia Syndicate', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-mafia-syndicate.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1250, 'redrake-bonus-poker', 'BONUS POKER', 'slot', 'Red Rake', 'ms', 'wb', 'JUME', 'img/mrslotty/redrake-bonus-poker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1251, 'tpg-dessert-mario', 'Dessert Mario', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-dessert-mario.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1252, 'slotexchange-boxing-ring', 'Boxing ring', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-boxing-ring.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1253, 'play-pearls-chuggers_pot', 'Chuggers Pot', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-chuggers_pot.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1254, 'tomhorn-red-lights', 'Red Lights', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-red-lights.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1255, 'redrake-1st-of-the-irish', '1st of the Irish', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-1st-of-the-irish.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1256, 'monsterbirds', 'Monster Birds', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/monsterbirds.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1257, 'eagaming-41', 'Nezha', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-41.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1258, 'wazdan-65888', '9 Lions', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-65888.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1259, 'gamshy-western-barn', 'Western Barn', 'slot', 'Gamshy', 'ms', 'mb', '', 'img/mrslotty/gamshy-western-barn.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1260, 'evoplay-rich-reels', 'Rich Reels', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-rich-reels.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1261, 'redrake-deuces-wild', 'DEUCES WILD', 'slot', 'Red Rake', 'ms', 'mb', 'JUME', 'img/mrslotty/redrake-deuces-wild.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1262, 'tpg-xi-you-mario', 'Xi You Mario', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-xi-you-mario.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1263, 'slotexchange-indian-legends', 'Indian legends', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-indian-legends.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1264, 'play-pearls-chubby_princess', 'Chubby Princess', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-chubby_princess.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1265, 'tomhorn-monster-madness', 'Monster Madness', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-monster-madness.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1266, 'tomhorn-the-secret-of-ba', 'The Secret of Ba', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-the-secret-of-ba.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1267, 'monsterbirds', 'Monster Birds', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/monsterbirds.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1268, 'eagaming-41', 'Nezha', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-41.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1269, 'wazdan-65888', '9 Lions', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-65888.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1270, 'gamshy-western-barn', 'Western Barn', 'slot', 'Gamshy', 'ms', 'wb', '', 'img/mrslotty/gamshy-western-barn.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1271, 'evoplay-rich-reels', 'Rich Reels', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-rich-reels.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1272, 'redrake-deuces-wild', 'DEUCES WILD', 'slot', 'Red Rake', 'ms', 'wb', 'JUME', 'img/mrslotty/redrake-deuces-wild.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1273, 'tpg-xi-you-mario', 'Xi You Mario', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-xi-you-mario.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1274, 'slotexchange-indian-legends', 'Indian legends', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-indian-legends.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1275, 'play-pearls-chubby_princess', 'Chubby Princess', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-chubby_princess.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1276, 'tomhorn-monster-madness', 'Monster Madness', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-monster-madness.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1277, 'tomhorn-the-secret-of-ba', 'The Secret of Ba', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-the-secret-of-ba.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1278, 'aztecpyramids', 'Aztec Pyramids', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/aztecpyramids.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1279, 'eagaming-43', 'Chinese Boss', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-43.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1280, 'wazdan-258', 'Turbo Play', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-258.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1281, 'gamshy-inferno', 'Inferno', 'slot', 'Gamshy', 'ms', 'mb', '', 'img/mrslotty/gamshy-inferno.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1282, 'evoplay-irish-reels', 'Irish Reels', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-irish-reels.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1283, 'redrake-jacks-or-better', 'JACKS OR BETTER', 'slot', 'Red Rake', 'ms', 'mb', 'JUME', 'img/mrslotty/redrake-jacks-or-better.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1284, 'tpg-dj-mario', 'DJ Mario', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-dj-mario.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1285, 'slotexchange-girls-squad', 'Girls squad', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-girls-squad.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1286, 'play-pearls-cash_spell', 'Cash Spell', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-cash_spell.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1287, 'tomhorn-243-crystal-fruits', '243 Crystal Fruits', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-243-crystal-fruits.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1288, 'evoplay-football-manager', 'Football Manager', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-football-manager.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1289, 'aztecpyramids', 'Aztec Pyramids', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/aztecpyramids.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1290, 'eagaming-43', 'Chinese Boss', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-43.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1291, 'wazdan-258', 'Turbo Play', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-258.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1292, 'gamshy-inferno', 'Inferno', 'slot', 'Gamshy', 'ms', 'wb', '', 'img/mrslotty/gamshy-inferno.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1293, 'evoplay-irish-reels', 'Irish Reels', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-irish-reels.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1294, 'redrake-jacks-or-better', 'JACKS OR BETTER', 'slot', 'Red Rake', 'ms', 'wb', 'JUME', 'img/mrslotty/redrake-jacks-or-better.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1295, 'tpg-dj-mario', 'DJ Mario', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-dj-mario.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1296, 'slotexchange-girls-squad', 'Girls squad', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-girls-squad.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1297, 'play-pearls-cash_spell', 'Cash Spell', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-cash_spell.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1298, 'tomhorn-243-crystal-fruits', '243 Crystal Fruits', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-243-crystal-fruits.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1299, 'evoplay-football-manager', 'Football Manager', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-football-manager.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1300, 'zeusthethunderer', 'Zeus the Thunderer', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/zeusthethunderer.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1301, 'eagaming-44', 'Empress Regnant', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-44.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1302, 'wazdan-259', 'Arcade', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-259.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1303, 'gamshy-fruit-tribe', 'Fruit Tribe', 'slot', 'Gamshy', 'ms', 'mb', '', 'img/mrslotty/gamshy-fruit-tribe.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1304, 'evoplay-texas-holdem-poker', 'Texas Holdem Poker', 'slot', 'Evoplay', 'ms', 'mb', 'JUME', 'img/mrslotty/evoplay-texas-holdem-poker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1305, 'kiron-horses', 'Horses (Dashing Derby)', 'virtual', 'Kiron', 'ms', 'mb', '', 'img/mrslotty/kiron-horses.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1306, 'tpg-keno-neon', 'Keno Neon', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-keno-neon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1307, 'slotexchange-great-sparta', 'Great Sparta', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-great-sparta.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1308, 'play-pearls-bloody_marys_booty', 'Bloody Mary\'s Booty', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-bloody_marys_booty.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1309, 'tomhorn-pandas-run', 'Panda\'s Run', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-pandas-run.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1310, 'hub88-griffins-quest', 'Griffins Quest', 'slot', 'Kalamba', 'ms', 'mb', '', 'img/mrslotty/hub88-griffins-quest.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1311, 'zeusthethunderer', 'Zeus the Thunderer', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/zeusthethunderer.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1312, 'eagaming-44', 'Empress Regnant', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-44.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1313, 'wazdan-259', 'Arcade', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-259.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1314, 'gamshy-fruit-tribe', 'Fruit Tribe', 'slot', 'Gamshy', 'ms', 'wb', '', 'img/mrslotty/gamshy-fruit-tribe.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1315, 'evoplay-texas-holdem-poker', 'Texas Holdem Poker', 'slot', 'Evoplay', 'ms', 'wb', 'JUME', 'img/mrslotty/evoplay-texas-holdem-poker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1316, 'kiron-horses', 'Horses (Dashing Derby)', 'virtual', 'Kiron', 'ms', 'wb', '', 'img/mrslotty/kiron-horses.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1317, 'tpg-keno-neon', 'Keno Neon', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-keno-neon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1318, 'slotexchange-great-sparta', 'Great Sparta', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-great-sparta.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1319, 'play-pearls-bloody_marys_booty', 'Bloody Mary\'s Booty', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-bloody_marys_booty.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1320, 'tomhorn-pandas-run', 'Panda\'s Run', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-pandas-run.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1321, 'hub88-griffins-quest', 'Griffins Quest', 'slot', 'Kalamba', 'ms', 'wb', '', 'img/mrslotty/hub88-griffins-quest.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1322, 'sheheclub', 'She/He_club', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/sheheclub.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1323, 'eagaming-45', 'Fish Hunter Haiba', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-45.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1324, 'wazdan-260', 'Vegas Reels II', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-260.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1325, 'apollo-atlantis', 'Atlantis', 'slot', 'Apollo', 'ms', 'mb', '', 'img/mrslotty/apollo-atlantis.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1326, 'evoplay-rise-of-horus', 'Rise Of Horus', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-rise-of-horus.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1327, 'kiron-dogs', 'Dogs (Platinum Hounds)', 'virtual', 'Kiron', 'ms', 'mb', '', 'img/mrslotty/kiron-dogs.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1328, 'tpg-dragon/tiger', 'Dragon/Tiger', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-Dragon_Tiger.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1329, 'slotexchange-war-of-tanks', 'War of tanks', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-war-of-tanks.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1330, 'play-pearls-barn_kings', 'Barn Kings', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-barn_kings.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1331, 'tomhorn-fire-n-hot', 'Fire \'n\' Hot', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-fire-n-hot.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1332, 'hub88-bingolaco', 'Bingolaço', 'video_bingo', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-bingolaco.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1333, 'sheheclub', 'She/He_club', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/sheheclub.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1334, 'eagaming-45', 'Fish Hunter Haiba', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-45.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1335, 'wazdan-260', 'Vegas Reels II', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-260.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1336, 'apollo-atlantis', 'Atlantis', 'slot', 'Apollo', 'ms', 'wb', '', 'img/mrslotty/apollo-atlantis.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1337, 'evoplay-rise-of-horus', 'Rise Of Horus', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-rise-of-horus.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1338, 'kiron-dogs', 'Dogs (Platinum Hounds)', 'virtual', 'Kiron', 'ms', 'wb', '', 'img/mrslotty/kiron-dogs.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1339, 'tpg-dragon/tiger', 'Dragon/Tiger', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-Dragon_Tiger.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1340, 'slotexchange-war-of-tanks', 'War of tanks', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-war-of-tanks.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1341, 'play-pearls-barn_kings', 'Barn Kings', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-barn_kings.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1342, 'tomhorn-fire-n-hot', 'Fire \'n\' Hot', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-fire-n-hot.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1343, 'hub88-bingolaco', 'Bingolaço', 'video_bingo', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-bingolaco.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1344, 'lionthelord', 'Lion The Lord', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/lionthelord.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1345, 'eagaming-46', 'Fish Hunter Haiba JP', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-46.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1346, 'wazdan-272', 'Magic Hot', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-272.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1347, 'apollo-midnightfruits', 'Midnight Fruits', 'slot', 'Apollo', 'ms', 'mb', '', 'img/mrslotty/apollo-midnightfruits.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1348, 'evoplay-mine-field', 'Mine Field', 'slot', 'Evoplay', 'ms', 'mb', 'JUME', 'img/mrslotty/evoplay-mine-field.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1349, 'kiron-motor-racing', 'Motor racing (Max Car)', 'virtual', 'Kiron', 'ms', 'mb', '', 'img/mrslotty/kiron-motor-racing.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1350, 'tpg-sic-bo', 'Sic Bo', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-sic-bo.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1351, 'slotexchange-sunset-riders', 'Sunset Riders', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-sunset-riders.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1352, 'play-pearls-banana_king', 'Banana King', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-banana_king.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1353, 'tomhorn-shaolin-tiger', 'Shaolin Tiger', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-shaolin-tiger.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1354, 'hub88-rct-samba', 'RCT - Samba', 'slot', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-rct-samba.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1355, 'lionthelord', 'Lion The Lord', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/lionthelord.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1356, 'eagaming-46', 'Fish Hunter Haiba JP', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-46.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1357, 'wazdan-272', 'Magic Hot', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-272.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1358, 'apollo-midnightfruits', 'Midnight Fruits', 'slot', 'Apollo', 'ms', 'wb', '', 'img/mrslotty/apollo-midnightfruits.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1359, 'evoplay-mine-field', 'Mine Field', 'slot', 'Evoplay', 'ms', 'wb', 'JUME', 'img/mrslotty/evoplay-mine-field.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1360, 'kiron-motor-racing', 'Motor racing (Max Car)', 'virtual', 'Kiron', 'ms', 'wb', '', 'img/mrslotty/kiron-motor-racing.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1361, 'tpg-sic-bo', 'Sic Bo', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-sic-bo.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1362, 'slotexchange-sunset-riders', 'Sunset Riders', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-sunset-riders.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1363, 'play-pearls-banana_king', 'Banana King', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-banana_king.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1364, 'tomhorn-shaolin-tiger', 'Shaolin Tiger', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-shaolin-tiger.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1365, 'hub88-rct-samba', 'RCT - Samba', 'slot', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-rct-samba.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1366, 'hothoney22vip', 'HotHoney 22 VIP', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/hothoney22vip.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1367, 'bgaming-AmericanRoulette', 'American Roulette', 'slot', 'BGaming', 'ms', 'mb', 'JUME', 'img/mrslotty/bgaming-AmericanRoulette.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1368, 'wazdan-273', 'Vegas Hot', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-273.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1369, 'apollo-blood', 'Blood', 'slot', 'Apollo', 'ms', 'mb', '', 'img/mrslotty/apollo-blood.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1370, 'evoplay-crown-and-anchor', 'Crown and Anchor', 'slot', 'Evoplay', 'ms', 'mb', 'JUME', 'img/mrslotty/evoplay-crown-and-anchor.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1371, 'kiron-table-tennis', 'Table tennis', 'virtual', 'Kiron', 'ms', 'mb', '', 'img/mrslotty/kiron-table-tennis.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1372, 'tpg-san-gong', 'San Gong', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-san-gong.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1373, 'slotexchange-stewardess', 'Stewardess', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-stewardess.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1374, 'play-pearls-aztec_treasure', 'Aztec Treasure', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-aztec_treasure.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1375, 'tomhorn-geishas-fan', 'Geisha\'s Fan', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-geishas-fan.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1376, 'hub88-rct-halloween', 'RCT - Halloween', 'slot', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-rct-halloween.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1377, 'hothoney22vip', 'HotHoney 22 VIP', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/hothoney22vip.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1378, 'bgaming-AmericanRoulette', 'American Roulette', 'slot', 'BGaming', 'ms', 'wb', 'JUME', 'img/mrslotty/bgaming-AmericanRoulette.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1379, 'wazdan-273', 'Vegas Hot', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-273.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1380, 'apollo-blood', 'Blood', 'slot', 'Apollo', 'ms', 'wb', '', 'img/mrslotty/apollo-blood.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1381, 'evoplay-crown-and-anchor', 'Crown and Anchor', 'slot', 'Evoplay', 'ms', 'wb', 'JUME', 'img/mrslotty/evoplay-crown-and-anchor.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1382, 'kiron-table-tennis', 'Table tennis', 'virtual', 'Kiron', 'ms', 'wb', '', 'img/mrslotty/kiron-table-tennis.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1383, 'tpg-san-gong', 'San Gong', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-san-gong.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1384, 'slotexchange-stewardess', 'Stewardess', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-stewardess.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1385, 'play-pearls-aztec_treasure', 'Aztec Treasure', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-aztec_treasure.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1386, 'tomhorn-geishas-fan', 'Geisha\'s Fan', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-geishas-fan.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1387, 'hub88-rct-halloween', 'RCT - Halloween', 'slot', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-rct-halloween.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1388, 'goldminers', 'Gold Miners', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/goldminers.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1389, 'bgaming-AztecMagic', 'Aztec Magic', 'slot', 'BGaming', 'ms', 'mb', '', 'img/mrslotty/bgaming-AztecMagic.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1390, 'wazdan-274', 'Black Horse', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-274.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1391, 'apollo-mysteryapolloii', 'Mystery Joker II', 'slot', 'Apollo', 'ms', 'mb', '', 'img/mrslotty/apollo-mysteryapolloii.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1392, 'evoplay-western-reels', 'Western Reels', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-western-reels.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1393, 'kiron-keno', 'Keno (Smart Play Keno)', 'virtual', 'Kiron', 'ms', 'mb', '', 'img/mrslotty/kiron-keno.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1394, 'tpg-super-six', 'Super Six', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-super-six.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1395, 'slotexchange-champion-title', 'Champion title', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-champion-title.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1396, 'play-pearls-awesome_5', 'Awesome 5', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-awesome_5.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1397, 'tomhorn-thrones-of-persia', 'Thrones Of Persia', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-thrones-of-persia.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1398, 'hub88-feel-the-music', 'Feel The Music', 'slot', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-feel-the-music.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1399, 'goldminers', 'Gold Miners', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/goldminers.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1400, 'bgaming-AztecMagic', 'Aztec Magic', 'slot', 'BGaming', 'ms', 'wb', '', 'img/mrslotty/bgaming-AztecMagic.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1401, 'wazdan-274', 'Black Horse', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-274.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1402, 'apollo-mysteryapolloii', 'Mystery Joker II', 'slot', 'Apollo', 'ms', 'wb', '', 'img/mrslotty/apollo-mysteryapolloii.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1403, 'evoplay-western-reels', 'Western Reels', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-western-reels.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1404, 'kiron-keno', 'Keno (Smart Play Keno)', 'virtual', 'Kiron', 'ms', 'wb', '', 'img/mrslotty/kiron-keno.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1405, 'tpg-super-six', 'Super Six', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-super-six.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1406, 'slotexchange-champion-title', 'Champion title', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-champion-title.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1407, 'play-pearls-awesome_5', 'Awesome 5', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-awesome_5.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1408, 'tomhorn-thrones-of-persia', 'Thrones Of Persia', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-thrones-of-persia.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1409, 'hub88-feel-the-music', 'Feel The Music', 'slot', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-feel-the-music.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1410, 'mermaidgold', 'Mermaid Gold', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/mermaidgold.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1411, 'bgaming-AztecMagicDeluxe', 'Aztec Magic Deluxe', 'slot', 'BGaming', 'ms', 'mb', '', 'img/mrslotty/bgaming-AztecMagicDeluxe.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1412, 'wazdan-276', 'Fire Bird', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-276.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1413, 'apollo-bonusjokerii', 'Bonus Joker II', 'slot', 'Apollo', 'ms', 'mb', '', 'img/mrslotty/apollo-bonusjokerii.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1414, 'betsolutions-plinko', 'Plinko', 'iq', 'Betsolutions', 'ms', 'mb', '', 'img/mrslotty/betsolutions-plinko.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1415, 'kiron-footbal', 'Football (English Fast League Football Single Match)', 'virtual', 'Kiron', 'ms', 'mb', '', 'img/mrslotty/kiron-footbal.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1416, 'tpg-super-baccarat', 'Super Baccarat', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-super-baccarat.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1417, 'slotexchange-hockey-legend', 'Hockey legend', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-hockey-legend.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1418, 'play-pearls-asgards_gold', 'Asgards Gold', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-asgards_gold.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1419, 'tomhorn-don-juans-peppers', 'Don Juan\'s Peppers', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-don-juans-peppers.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1420, 'hub88-dressing-room', 'Dressing Room', 'slot', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-dressing-room.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1421, 'mermaidgold', 'Mermaid Gold', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/mermaidgold.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1422, 'bgaming-AztecMagicDeluxe', 'Aztec Magic Deluxe', 'slot', 'BGaming', 'ms', 'wb', '', 'img/mrslotty/bgaming-AztecMagicDeluxe.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1423, 'wazdan-276', 'Fire Bird', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-276.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1424, 'apollo-bonusjokerii', 'Bonus Joker II', 'slot', 'Apollo', 'ms', 'wb', '', 'img/mrslotty/apollo-bonusjokerii.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1425, 'betsolutions-plinko', 'Plinko', 'iq', 'Betsolutions', 'ms', 'wb', '', 'img/mrslotty/betsolutions-plinko.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1426, 'kiron-footbal', 'Football (English Fast League Football Single Match)', 'virtual', 'Kiron', 'ms', 'wb', '', 'img/mrslotty/kiron-footbal.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1427, 'tpg-super-baccarat', 'Super Baccarat', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-super-baccarat.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1428, 'slotexchange-hockey-legend', 'Hockey legend', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-hockey-legend.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1429, 'play-pearls-asgards_gold', 'Asgards Gold', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-asgards_gold.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1430, 'tomhorn-don-juans-peppers', 'Don Juan\'s Peppers', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-don-juans-peppers.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1431, 'hub88-dressing-room', 'Dressing Room', 'slot', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-dressing-room.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1432, 'enchanted7s', 'Enchanted 7s', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/enchanted7s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1433, 'bgaming-Baccarat', 'Baccarat', 'slot', 'BGaming', 'ms', 'mb', 'JUME', 'img/mrslotty/bgaming-Baccarat.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1434, 'wazdan-278', 'Captain Shark', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-278.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1435, 'apollo-ninefruits', 'Magic Lady', 'slot', 'Apollo', 'ms', 'mb', '', 'img/mrslotty/apollo-ninefruits.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1436, 'fazi-neonhot5', '5 NeonHot', 'slot', 'Fazi', 'ms', 'mb', '', 'img/mrslotty/fazi-neonhot5.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1437, 'kiron-football-league-round', 'Football League Round (English Fast League Football Round)', 'virtual', 'Kiron', 'ms', 'mb', '', 'img/mrslotty/kiron-football-league-round.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1438, 'tpg-go-gold-fishing-360°', 'Go Gold Fishing 360°', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-Go_Gold_Fishing_360°.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1439, 'slotexchange-dinosaur-era', 'Dinosaur era', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-dinosaur-era.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1440, 'play-pearls-777_hot_ice', '777 Hot Ice', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-777_hot_ice.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1441, 'tomhorn-monkey-27', 'Monkey 27', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-monkey-27.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1442, 'hub88-maui-millions', 'Maui Millions', 'slot', 'Kalamba', 'ms', 'mb', '', 'img/mrslotty/hub88-maui-millions.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1443, 'enchanted7s', 'Enchanted 7s', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/enchanted7s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1444, 'bgaming-Baccarat', 'Baccarat', 'slot', 'BGaming', 'ms', 'wb', 'JUME', 'img/mrslotty/bgaming-Baccarat.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1445, 'wazdan-278', 'Captain Shark', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-278.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1446, 'apollo-ninefruits', 'Magic Lady', 'slot', 'Apollo', 'ms', 'wb', '', 'img/mrslotty/apollo-ninefruits.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1447, 'fazi-neonhot5', '5 NeonHot', 'slot', 'Fazi', 'ms', 'wb', '', 'img/mrslotty/fazi-neonhot5.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1448, 'kiron-football-league-round', 'Football League Round (English Fast League Football Round)', 'virtual', 'Kiron', 'ms', 'wb', '', 'img/mrslotty/kiron-football-league-round.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1449, 'tpg-go-gold-fishing-360°', 'Go Gold Fishing 360°', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-Go_Gold_Fishing_360°.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1450, 'slotexchange-dinosaur-era', 'Dinosaur era', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-dinosaur-era.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1451, 'play-pearls-777_hot_ice', '777 Hot Ice', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-777_hot_ice.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1452, 'tomhorn-monkey-27', 'Monkey 27', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-monkey-27.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1453, 'hub88-maui-millions', 'Maui Millions', 'slot', 'Kalamba', 'ms', 'wb', '', 'img/mrslotty/hub88-maui-millions.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1454, 'vegasafterparty', 'Vegas AfterParty', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/vegasafterparty.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1455, 'bgaming-BlackjackPro', 'Multihand Blackjack Pro', 'slot', 'BGaming', 'ms', 'mb', 'JUME', 'img/mrslotty/bgaming-BlackjackPro.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1456, 'wazdan-280', 'Lucky Queen', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-280.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1457, 'apollo-burningdice', 'Burning Dice', 'slot', 'Apollo', 'ms', 'mb', '', 'img/mrslotty/apollo-burningdice.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1458, 'fazi-alohacharm', 'Aloha Charm', 'slot', 'Fazi', 'ms', 'mb', '', 'img/mrslotty/fazi-alohacharm.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1459, 'kiron-football-hub', 'Football Hub', 'virtual', 'Kiron', 'ms', 'mb', '', 'img/mrslotty/kiron-football-hub.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1460, 'tpg-fishing-fortune-360°', 'Fishing Fortune 360°', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-Fishing_Fortune_360°.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1461, 'slotexchange-lord-of-hell', 'Lord of hell', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-lord-of-hell.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1462, 'play-pearls-28_spins_later', '28 Spins Later', 'slot', 'Play Pearls', 'ms', 'mb', '', 'img/mrslotty/play-pearls-28_spins_later.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1463, 'tomhorn-book-of-spells', 'Book Of Spells', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-book-of-spells.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1464, 'netgame-fortune-cash', 'Fortune Cash', 'slot', 'Netgame', 'ms', 'mb', '', 'img/mrslotty/netgame-fortune-cash.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1465, 'vegasafterparty', 'Vegas AfterParty', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/vegasafterparty.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1466, 'bgaming-BlackjackPro', 'Multihand Blackjack Pro', 'slot', 'BGaming', 'ms', 'wb', 'JUME', 'img/mrslotty/bgaming-BlackjackPro.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1467, 'wazdan-280', 'Lucky Queen', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-280.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1468, 'apollo-burningdice', 'Burning Dice', 'slot', 'Apollo', 'ms', 'wb', '', 'img/mrslotty/apollo-burningdice.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1469, 'fazi-alohacharm', 'Aloha Charm', 'slot', 'Fazi', 'ms', 'wb', '', 'img/mrslotty/fazi-alohacharm.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1470, 'kiron-football-hub', 'Football Hub', 'virtual', 'Kiron', 'ms', 'wb', '', 'img/mrslotty/kiron-football-hub.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1471, 'tpg-fishing-fortune-360°', 'Fishing Fortune 360°', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-Fishing_Fortune_360°.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1472, 'slotexchange-lord-of-hell', 'Lord of hell', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-lord-of-hell.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1473, 'play-pearls-28_spins_later', '28 Spins Later', 'slot', 'Play Pearls', 'ms', 'wb', '', 'img/mrslotty/play-pearls-28_spins_later.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1474, 'tomhorn-book-of-spells', 'Book Of Spells', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-book-of-spells.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1475, 'netgame-fortune-cash', 'Fortune Cash', 'slot', 'Netgame', 'ms', 'wb', '', 'img/mrslotty/netgame-fortune-cash.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1476, 'unicorngems', 'Unicorn Gems', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/unicorngems.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1477, 'bgaming-BlackjackSurrender', 'Blackjack Surrender', 'slot', 'BGaming', 'ms', 'mb', 'JUME', 'img/mrslotty/bgaming-BlackjackSurrender.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1478, 'wazdan-282', 'Hot 777', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-282.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1479, 'apollo-pandora', 'Pandora', 'slot', 'Apollo', 'ms', 'mb', '', 'img/mrslotty/apollo-pandora.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1480, 'fazi-bookofbruno', 'Book of Bruno', 'slot', 'Fazi', 'ms', 'mb', '', 'img/mrslotty/fazi-bookofbruno.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1481, 'kiron-golf', 'Golf', 'virtual', 'Kiron', 'ms', 'mb', '', 'img/mrslotty/kiron-golf.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1482, 'tpg-fishing-fortune', 'Fishing Fortune', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-fishing-fortune.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1483, 'slotexchange-weekend-in-mexico', 'Weekend in Mexico', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-weekend-in-mexico.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1484, 'wearecasino-waikiki', 'Waikiki Heroes', 'slot', 'Wearecasino', 'ms', 'mb', '', 'img/mrslotty/wearecasino-waikiki.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1485, 'tomhorn-blackbeards-quest', 'Blackbeard\'s Quest', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-blackbeards-quest.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1486, 'netgame-fruit-burst', 'Fruit Burst', 'slot', 'Netgame', 'ms', 'mb', '', 'img/mrslotty/netgame-fruit-burst.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1487, 'unicorngems', 'Unicorn Gems', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/unicorngems.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1488, 'bgaming-BlackjackSurrender', 'Blackjack Surrender', 'slot', 'BGaming', 'ms', 'wb', 'JUME', 'img/mrslotty/bgaming-BlackjackSurrender.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1489, 'wazdan-282', 'Hot 777', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-282.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1490, 'apollo-pandora', 'Pandora', 'slot', 'Apollo', 'ms', 'wb', '', 'img/mrslotty/apollo-pandora.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1491, 'fazi-bookofbruno', 'Book of Bruno', 'slot', 'Fazi', 'ms', 'wb', '', 'img/mrslotty/fazi-bookofbruno.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1492, 'kiron-golf', 'Golf', 'virtual', 'Kiron', 'ms', 'wb', '', 'img/mrslotty/kiron-golf.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1493, 'tpg-fishing-fortune', 'Fishing Fortune', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-fishing-fortune.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1494, 'slotexchange-weekend-in-mexico', 'Weekend in Mexico', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-weekend-in-mexico.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1495, 'wearecasino-waikiki', 'Waikiki Heroes', 'slot', 'Wearecasino', 'ms', 'wb', '', 'img/mrslotty/wearecasino-waikiki.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1496, 'tomhorn-blackbeards-quest', 'Blackbeard\'s Quest', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-blackbeards-quest.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1497, 'netgame-fruit-burst', 'Fruit Burst', 'slot', 'Netgame', 'ms', 'wb', '', 'img/mrslotty/netgame-fruit-burst.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1498, 'crazyhalloween', 'Crazy Halloween', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/crazyhalloween.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1499, 'bgaming-BobsCoffeeShop', 'Bob\'s Coffee Shop', 'slot', 'BGaming', 'ms', 'mb', '', 'img/mrslotty/bgaming-BobsCoffeeShop.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1500, 'wazdan-283', 'Magic Fruits 27', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-283.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1501, 'apollo-dice81', 'Dice 81', 'slot', 'Apollo', 'ms', 'mb', '', 'img/mrslotty/apollo-dice81.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1502, 'fazi-bookofspells', 'Book of Spells', 'slot', 'Fazi', 'ms', 'mb', '', 'img/mrslotty/fazi-bookofspells.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1503, 'kiron-spanish-footbal', 'Spanish FastLeague Football Single', 'virtual', 'Kiron', 'ms', 'mb', '', 'img/mrslotty/kiron-spanish-footbal.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1504, 'tpg-hawaii-tiki', 'Hawaii Tiki', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-hawaii-tiki.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1505, 'slotexchange-circus-show', 'Circus show', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-circus-show.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1506, 'wearecasino-fortune-valentine', 'Valentine’s Gift', 'slot', 'Wearecasino', 'ms', 'mb', '', 'img/mrslotty/wearecasino-fortune-valentine.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1507, 'tomhorn-hot-blizzard', 'Hot Blizzard', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-hot-blizzard.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1508, 'netgame-mma-legends', 'MMA LEGENDS', 'slot', 'Netgame', 'ms', 'mb', '', 'img/mrslotty/netgame-mma-legends.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1509, 'crazyhalloween', 'Crazy Halloween', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/crazyhalloween.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1510, 'bgaming-BobsCoffeeShop', 'Bob\'s Coffee Shop', 'slot', 'BGaming', 'ms', 'wb', '', 'img/mrslotty/bgaming-BobsCoffeeShop.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1511, 'wazdan-283', 'Magic Fruits 27', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-283.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1512, 'apollo-dice81', 'Dice 81', 'slot', 'Apollo', 'ms', 'wb', '', 'img/mrslotty/apollo-dice81.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1513, 'fazi-bookofspells', 'Book of Spells', 'slot', 'Fazi', 'ms', 'wb', '', 'img/mrslotty/fazi-bookofspells.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1514, 'kiron-spanish-footbal', 'Spanish FastLeague Football Single', 'virtual', 'Kiron', 'ms', 'wb', '', 'img/mrslotty/kiron-spanish-footbal.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1515, 'tpg-hawaii-tiki', 'Hawaii Tiki', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-hawaii-tiki.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1516, 'slotexchange-circus-show', 'Circus show', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-circus-show.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1517, 'wearecasino-fortune-valentine', 'Valentine’s Gift', 'slot', 'Wearecasino', 'ms', 'wb', '', 'img/mrslotty/wearecasino-fortune-valentine.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1518, 'tomhorn-hot-blizzard', 'Hot Blizzard', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-hot-blizzard.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1519, 'netgame-mma-legends', 'MMA LEGENDS', 'slot', 'Netgame', 'ms', 'wb', '', 'img/mrslotty/netgame-mma-legends.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1520, 'electric7fruits', 'Electric7Fruits', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/electric7fruits.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1521, 'bgaming-BookOfPyramids', 'Book of Pyramids', 'slot', 'BGaming', 'ms', 'mb', '', 'img/mrslotty/bgaming-BookOfPyramids.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1522, 'wazdan-286', 'Wild Jack 81', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-286.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1523, 'apollo-richfish', 'Rich Fish', 'slot', 'Apollo', 'ms', 'mb', '', 'img/mrslotty/apollo-richfish.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1524, 'fazi-bookofspellsdeluxe', 'Book of Spells Deluxe', 'slot', 'Fazi', 'ms', 'mb', '', 'img/mrslotty/fazi-bookofspellsdeluxe.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1525, 'kiron-italian-football', 'Italian FastLeague Football Single', 'virtual', 'Kiron', 'ms', 'mb', '', 'img/mrslotty/kiron-italian-football.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1526, 'tpg-win-the-world', 'Win The World', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-win-the-world.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1527, 'slotexchange-guards-of-woods', 'Guards of woods', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-guards-of-woods.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1528, 'wearecasino-rooaar', 'Rooaar', 'slot', 'Wearecasino', 'ms', 'mb', '', 'img/mrslotty/wearecasino-rooaar.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1529, 'tomhorn-sizable-win', 'Sizable Win', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-sizable-win.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1530, 'bgaming-BookOfCats', 'Book of Cats', 'slot', 'BGaming', 'ms', 'mb', '', 'img/mrslotty/bgaming-BookOfCats.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1531, 'electric7fruits', 'Electric7Fruits', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/electric7fruits.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1532, 'bgaming-BookOfPyramids', 'Book of Pyramids', 'slot', 'BGaming', 'ms', 'wb', '', 'img/mrslotty/bgaming-BookOfPyramids.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1533, 'wazdan-286', 'Wild Jack 81', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-286.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1534, 'apollo-richfish', 'Rich Fish', 'slot', 'Apollo', 'ms', 'wb', '', 'img/mrslotty/apollo-richfish.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1535, 'fazi-bookofspellsdeluxe', 'Book of Spells Deluxe', 'slot', 'Fazi', 'ms', 'wb', '', 'img/mrslotty/fazi-bookofspellsdeluxe.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1536, 'kiron-italian-football', 'Italian FastLeague Football Single', 'virtual', 'Kiron', 'ms', 'wb', '', 'img/mrslotty/kiron-italian-football.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1537, 'tpg-win-the-world', 'Win The World', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-win-the-world.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1538, 'slotexchange-guards-of-woods', 'Guards of woods', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-guards-of-woods.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1539, 'wearecasino-rooaar', 'Rooaar', 'slot', 'Wearecasino', 'ms', 'wb', '', 'img/mrslotty/wearecasino-rooaar.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1540, 'tomhorn-sizable-win', 'Sizable Win', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-sizable-win.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1541, 'bgaming-BookOfCats', 'Book of Cats', 'slot', 'BGaming', 'ms', 'wb', '', 'img/mrslotty/bgaming-BookOfCats.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1542, 'royal7fruits', 'Royal7Fruits', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/royal7fruits.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1543, 'bgaming-BraveViking', 'Brave Viking', 'slot', 'BGaming', 'ms', 'mb', '', 'img/mrslotty/bgaming-BraveViking.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1544, 'wazdan-290', 'Mystery Jack', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-290.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1545, 'apollo-eldorado', 'Eldorado', 'slot', 'Apollo', 'ms', 'mb', '', 'img/mrslotty/apollo-eldorado.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1546, 'fazi-burningice', 'Burning Ice', 'slot', 'Fazi', 'ms', 'mb', '', 'img/mrslotty/fazi-burningice.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1547, 'kiron-horse-racing', 'Horse Racing Roulette V2', 'virtual', 'Kiron', 'ms', 'mb', '', 'img/mrslotty/kiron-horse-racing.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1548, 'tpg-ruyi\'s-royal-love-in-the-palace', 'Ruyi\'s Royal Love in the Palace', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-ruyi_s-royal-love-in-the-palace.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1549, 'slotexchange-cowboy-story', 'Cowboy story', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-cowboy-story.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1550, 'wearecasino-veggie-toons', 'Veggie toons', 'slot', 'Wearecasino', 'ms', 'mb', '', 'img/mrslotty/wearecasino-veggie-toons.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1551, 'tomhorn-black-mummy', 'Black Mummy', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-black-mummy.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1552, 'royal7fruits', 'Royal7Fruits', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/royal7fruits.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1553, 'bgaming-BraveViking', 'Brave Viking', 'slot', 'BGaming', 'ms', 'wb', '', 'img/mrslotty/bgaming-BraveViking.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1554, 'wazdan-290', 'Mystery Jack', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-290.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1555, 'apollo-eldorado', 'Eldorado', 'slot', 'Apollo', 'ms', 'wb', '', 'img/mrslotty/apollo-eldorado.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1556, 'fazi-burningice', 'Burning Ice', 'slot', 'Fazi', 'ms', 'wb', '', 'img/mrslotty/fazi-burningice.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1557, 'kiron-horse-racing', 'Horse Racing Roulette V2', 'virtual', 'Kiron', 'ms', 'wb', '', 'img/mrslotty/kiron-horse-racing.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1558, 'tpg-ruyi\'s-royal-love-in-the-palace', 'Ruyi\'s Royal Love in the Palace', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-ruyi_s-royal-love-in-the-palace.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1559, 'slotexchange-cowboy-story', 'Cowboy story', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-cowboy-story.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1560, 'wearecasino-veggie-toons', 'Veggie toons', 'slot', 'Wearecasino', 'ms', 'wb', '', 'img/mrslotty/wearecasino-veggie-toons.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1561, 'tomhorn-black-mummy', 'Black Mummy', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-black-mummy.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1562, 'classic7fruits', 'Classic7Fruits', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/classic7fruits.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1563, 'bgaming-CaribbeanPoker', 'Caribbean Poker', 'slot', 'BGaming', 'ms', 'mb', 'JUME', 'img/mrslotty/bgaming-CaribbeanPoker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1564, 'wazdan-292', 'Magic Fruits 81', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-292.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1565, 'apollo-rur', 'RUR', 'slot', 'Apollo', 'ms', 'mb', '', 'img/mrslotty/apollo-rur.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1566, 'fazi-burningicedeluxe', 'Burning Ice Deluxe', 'slot', 'Fazi', 'ms', 'mb', '', 'img/mrslotty/fazi-burningicedeluxe.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1567, 'velagaming-horse-racing-02', 'PonyHorseRacing', 'arcade', 'Velagaming', 'ms', 'mb', '', 'img/mrslotty/velagaming-horse-racing-02.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1568, 'tpg-the-advisors-alliance', 'The Advisors Alliance', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-the-advisors-alliance.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1569, 'slotexchange-radioactive-zone', 'Radioactive zone', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-radioactive-zone.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1570, 'wearecasino-vox', 'Vox Slot', 'slot', 'Wearecasino', 'ms', 'mb', '', 'img/mrslotty/wearecasino-vox.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1571, 'tomhorn-triple-joker', 'Triple Joker', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-triple-joker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1572, 'classic7fruits', 'Classic7Fruits', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/classic7fruits.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1573, 'bgaming-CaribbeanPoker', 'Caribbean Poker', 'slot', 'BGaming', 'ms', 'wb', 'JUME', 'img/mrslotty/bgaming-CaribbeanPoker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1574, 'wazdan-292', 'Magic Fruits 81', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-292.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1575, 'apollo-rur', 'RUR', 'slot', 'Apollo', 'ms', 'wb', '', 'img/mrslotty/apollo-rur.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1576, 'fazi-burningicedeluxe', 'Burning Ice Deluxe', 'slot', 'Fazi', 'ms', 'wb', '', 'img/mrslotty/fazi-burningicedeluxe.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1577, 'velagaming-horse-racing-02', 'PonyHorseRacing', 'arcade', 'Velagaming', 'ms', 'wb', '', 'img/mrslotty/velagaming-horse-racing-02.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1578, 'tpg-the-advisors-alliance', 'The Advisors Alliance', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-the-advisors-alliance.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1579, 'slotexchange-radioactive-zone', 'Radioactive zone', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-radioactive-zone.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1580, 'wearecasino-vox', 'Vox Slot', 'slot', 'Wearecasino', 'ms', 'wb', '', 'img/mrslotty/wearecasino-vox.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1581, 'tomhorn-triple-joker', 'Triple Joker', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-triple-joker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1582, 'tropical7fruits', 'Tropical7Fruits', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/tropical7fruits.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1583, 'bgaming-CasinoHoldem', 'Casino Holdem', 'slot', 'BGaming', 'ms', 'mb', 'JUME', 'img/mrslotty/bgaming-CasinoHoldem.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1584, 'wazdan-295', 'Cube Mania', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-295.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1585, 'apollo-fourfruitsii', 'Four Fruits II', 'slot', 'Apollo', 'ms', 'mb', '', 'img/mrslotty/apollo-fourfruitsii.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1586, 'fazi-crystalhot40', 'Crystal Hot 40', 'slot', 'Fazi', 'ms', 'mb', '', 'img/mrslotty/fazi-crystalhot40.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1587, 'velagaming-monkey-racing', 'MonkeyRace', 'arcade', 'Velagaming', 'ms', 'mb', '', 'img/mrslotty/velagaming-monkey-racing.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1588, 'tpg-halloween', 'Halloween', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-halloween.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1589, 'slotexchange-rob-a-bank', 'Rob a bank', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-rob-a-bank.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1590, 'wearecasino-happy-dinner', 'Happy Diner', 'slot', 'Wearecasino', 'ms', 'mb', '', 'img/mrslotty/wearecasino-happy-dinner.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1591, 'tomhorn-flaming-fruit', 'Flaming Fruit', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-flaming-fruit.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1592, 'tropical7fruits', 'Tropical7Fruits', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/tropical7fruits.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1593, 'bgaming-CasinoHoldem', 'Casino Holdem', 'slot', 'BGaming', 'ms', 'wb', 'JUME', 'img/mrslotty/bgaming-CasinoHoldem.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1594, 'wazdan-295', 'Cube Mania', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-295.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1595, 'apollo-fourfruitsii', 'Four Fruits II', 'slot', 'Apollo', 'ms', 'wb', '', 'img/mrslotty/apollo-fourfruitsii.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1596, 'fazi-crystalhot40', 'Crystal Hot 40', 'slot', 'Fazi', 'ms', 'wb', '', 'img/mrslotty/fazi-crystalhot40.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1597, 'velagaming-monkey-racing', 'MonkeyRace', 'arcade', 'Velagaming', 'ms', 'wb', '', 'img/mrslotty/velagaming-monkey-racing.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1598, 'tpg-halloween', 'Halloween', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-halloween.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1599, 'slotexchange-rob-a-bank', 'Rob a bank', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-rob-a-bank.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1600, 'wearecasino-happy-dinner', 'Happy Diner', 'slot', 'Wearecasino', 'ms', 'wb', '', 'img/mrslotty/wearecasino-happy-dinner.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1601, 'tomhorn-flaming-fruit', 'Flaming Fruit', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-flaming-fruit.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1602, 'nomorefruits', 'No More Fruits', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/nomorefruits.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1603, 'bgaming-CherryFiesta', 'Cherry Fiesta', 'slot', 'BGaming', 'ms', 'mb', '', 'img/mrslotty/bgaming-CherryFiesta.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1604, 'wazdan-296', 'Criss Cross 81', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-296.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1605, 'apollo-slotbirds', 'Slot Birds', 'slot', 'Apollo', 'ms', 'mb', '', 'img/mrslotty/apollo-slotbirds.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1606, 'fazi-crystalsofmagic', 'Crystals of Magic', 'slot', 'Fazi', 'ms', 'mb', '', 'img/mrslotty/fazi-crystalsofmagic.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1607, 'velagaming-horse-racing-01', 'HorseRacingDeluxe', 'arcade', 'Velagaming', 'ms', 'mb', '', 'img/mrslotty/velagaming-horse-racing-01.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1608, 'tpg-japanese-fortune', 'Japanese Fortune', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-japanese-fortune.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1609, 'slotexchange-gold-circus', 'Gold circus', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-gold-circus.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1610, 'wearecasino-winning-wine', 'Winning Wine', 'slot', 'Wearecasino', 'ms', 'mb', '', 'img/mrslotty/wearecasino-winning-wine.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1611, 'tomhorn-the-cup', 'The Cup', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-the-cup.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1612, 'nomorefruits', 'No More Fruits', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/nomorefruits.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1613, 'bgaming-CherryFiesta', 'Cherry Fiesta', 'slot', 'BGaming', 'ms', 'wb', '', 'img/mrslotty/bgaming-CherryFiesta.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1614, 'wazdan-296', 'Criss Cross 81', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-296.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1615, 'apollo-slotbirds', 'Slot Birds', 'slot', 'Apollo', 'ms', 'wb', '', 'img/mrslotty/apollo-slotbirds.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1616, 'fazi-crystalsofmagic', 'Crystals of Magic', 'slot', 'Fazi', 'ms', 'wb', '', 'img/mrslotty/fazi-crystalsofmagic.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1617, 'velagaming-horse-racing-01', 'HorseRacingDeluxe', 'arcade', 'Velagaming', 'ms', 'wb', '', 'img/mrslotty/velagaming-horse-racing-01.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1618, 'tpg-japanese-fortune', 'Japanese Fortune', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-japanese-fortune.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1619, 'slotexchange-gold-circus', 'Gold circus', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-gold-circus.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1620, 'wearecasino-winning-wine', 'Winning Wine', 'slot', 'Wearecasino', 'ms', 'wb', '', 'img/mrslotty/wearecasino-winning-wine.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1621, 'tomhorn-the-cup', 'The Cup', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-the-cup.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1622, 'emojislot', 'Emoji Slot', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/emojislot.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1623, 'bgaming-CrazyStarter', 'Crazy Starter', 'slot', 'BGaming', 'ms', 'mb', '', 'img/mrslotty/bgaming-CrazyStarter.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1624, 'wazdan-297', 'Highway To Hell', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-297.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1625, 'apollo-gangsterworld', 'Gangster World', 'slot', 'Apollo', 'ms', 'mb', '', 'img/mrslotty/apollo-gangsterworld.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1626, 'fazi-deepjungle', 'Deep Jungle', 'slot', 'Fazi', 'ms', 'mb', '', 'img/mrslotty/fazi-deepjungle.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1627, 'velagaming-runlight', 'MonkeyStory', 'arcade', 'Velagaming', 'ms', 'mb', '', 'img/mrslotty/velagaming-runlight.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1628, 'tpg-monster-killer', 'Monster Killer', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-monster-killer.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1629, 'slotexchange-voodoo-shop', 'Voodoo shop', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-voodoo-shop.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1630, 'wearecasino-ice-cream-van', 'Ice Cream Van', 'slot', 'Wearecasino', 'ms', 'mb', '', 'img/mrslotty/wearecasino-ice-cream-van.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1631, 'tomhorn-sky-barons', 'Sky Barons', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-sky-barons.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1632, 'emojislot', 'Emoji Slot', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/emojislot.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1633, 'bgaming-CrazyStarter', 'Crazy Starter', 'slot', 'BGaming', 'ms', 'wb', '', 'img/mrslotty/bgaming-CrazyStarter.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1634, 'wazdan-297', 'Highway To Hell', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-297.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1635, 'apollo-gangsterworld', 'Gangster World', 'slot', 'Apollo', 'ms', 'wb', '', 'img/mrslotty/apollo-gangsterworld.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1636, 'fazi-deepjungle', 'Deep Jungle', 'slot', 'Fazi', 'ms', 'wb', '', 'img/mrslotty/fazi-deepjungle.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1637, 'velagaming-runlight', 'MonkeyStory', 'arcade', 'Velagaming', 'ms', 'wb', '', 'img/mrslotty/velagaming-runlight.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1638, 'tpg-monster-killer', 'Monster Killer', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-monster-killer.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1639, 'slotexchange-voodoo-shop', 'Voodoo shop', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-voodoo-shop.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1640, 'wearecasino-ice-cream-van', 'Ice Cream Van', 'slot', 'Wearecasino', 'ms', 'wb', '', 'img/mrslotty/wearecasino-ice-cream-van.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1641, 'tomhorn-sky-barons', 'Sky Barons', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-sky-barons.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1642, 'jokercards', 'Joker Cards', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/jokercards.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1643, 'bgaming-DeepSea', 'Deep Sea', 'slot', 'BGaming', 'ms', 'mb', '', 'img/mrslotty/bgaming-DeepSea.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1644, 'wazdan-298', 'Corrida Romance', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-298.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1645, 'apollo-smilingjokerii', 'Smiling Joker II', 'slot', 'Apollo', 'ms', 'mb', '', 'img/mrslotty/apollo-smilingjokerii.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1646, 'fazi-diamonds', 'Diamonds', 'slot', 'Fazi', 'ms', 'mb', '', 'img/mrslotty/fazi-diamonds.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1647, 'velagaming-pokemon', 'PokemonRun', 'arcade', 'Velagaming', 'ms', 'mb', '', 'img/mrslotty/velagaming-pokemon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1648, 'tpg-eternal-love', 'Eternal Love', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-eternal-love.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1649, 'slotexchange-vampire-lair', 'Vampire lair', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-vampire-lair.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1650, 'wearecasino-indiana-cabbage', 'Indiana Cabagge', 'slot', 'Wearecasino', 'ms', 'mb', '', 'img/mrslotty/wearecasino-indiana-cabbage.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1651, 'tomhorn-savannah-king', 'Savannah King', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-savannah-king.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1652, 'jokercards', 'Joker Cards', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/jokercards.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1653, 'bgaming-DeepSea', 'Deep Sea', 'slot', 'BGaming', 'ms', 'wb', '', 'img/mrslotty/bgaming-DeepSea.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1654, 'wazdan-298', 'Corrida Romance', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-298.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1655, 'apollo-smilingjokerii', 'Smiling Joker II', 'slot', 'Apollo', 'ms', 'wb', '', 'img/mrslotty/apollo-smilingjokerii.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1656, 'fazi-diamonds', 'Diamonds', 'slot', 'Fazi', 'ms', 'wb', '', 'img/mrslotty/fazi-diamonds.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1657, 'velagaming-pokemon', 'PokemonRun', 'arcade', 'Velagaming', 'ms', 'wb', '', 'img/mrslotty/velagaming-pokemon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1658, 'tpg-eternal-love', 'Eternal Love', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-eternal-love.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1659, 'slotexchange-vampire-lair', 'Vampire lair', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-vampire-lair.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1660, 'wearecasino-indiana-cabbage', 'Indiana Cabagge', 'slot', 'Wearecasino', 'ms', 'wb', '', 'img/mrslotty/wearecasino-indiana-cabbage.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1661, 'tomhorn-savannah-king', 'Savannah King', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-savannah-king.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1662, 'fruitcocktail7', 'FruitCocktail7', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/fruitcocktail7.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1663, 'bgaming-DesertTreasure', 'Desert Treasure', 'slot', 'BGaming', 'ms', 'mb', '', 'img/mrslotty/bgaming-DesertTreasure.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1664, 'wazdan-299', 'Wild Girls', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-299.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1665, 'apollo-hell', 'Hell', 'slot', 'Apollo', 'ms', 'mb', '', 'img/mrslotty/apollo-hell.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1666, 'fazi-forestfruits', 'Forest Fruits', 'slot', 'Fazi', 'ms', 'mb', '', 'img/mrslotty/fazi-forestfruits.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1667, 'velagaming-runlight-msp', 'MonkeyStoryPlus', 'arcade', 'Velagaming', 'ms', 'mb', '', 'img/mrslotty/velagaming-runlight-msp.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1668, 'tpg-kawaii-pets', 'Kawaii Pets', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-kawaii-pets.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1669, 'slotexchange-tam-tam', 'Tam-tam', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-tam-tam.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1670, 'wearecasino-chocolate', 'Chocolate Slot', 'slot', 'Wearecasino', 'ms', 'mb', '', 'img/mrslotty/wearecasino-chocolate.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1671, 'tomhorn-feng-fu', 'Feng Fu', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-feng-fu.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1672, 'fruitcocktail7', 'FruitCocktail7', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/fruitcocktail7.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1673, 'bgaming-DesertTreasure', 'Desert Treasure', 'slot', 'BGaming', 'ms', 'wb', '', 'img/mrslotty/bgaming-DesertTreasure.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1674, 'wazdan-299', 'Wild Girls', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-299.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1675, 'apollo-hell', 'Hell', 'slot', 'Apollo', 'ms', 'wb', '', 'img/mrslotty/apollo-hell.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1676, 'fazi-forestfruits', 'Forest Fruits', 'slot', 'Fazi', 'ms', 'wb', '', 'img/mrslotty/fazi-forestfruits.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1677, 'velagaming-runlight-msp', 'MonkeyStoryPlus', 'arcade', 'Velagaming', 'ms', 'wb', '', 'img/mrslotty/velagaming-runlight-msp.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1678, 'tpg-kawaii-pets', 'Kawaii Pets', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-kawaii-pets.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1679, 'slotexchange-tam-tam', 'Tam-tam', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-tam-tam.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1680, 'wearecasino-chocolate', 'Chocolate Slot', 'slot', 'Wearecasino', 'ms', 'wb', '', 'img/mrslotty/wearecasino-chocolate.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1681, 'tomhorn-feng-fu', 'Feng Fu', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-feng-fu.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1682, 'jokerdice', 'Joker Dice', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/jokerdice.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1683, 'bgaming-Domnitors', 'Domnitors', 'slot', 'BGaming', 'ms', 'mb', '', 'img/mrslotty/bgaming-Domnitors.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1684, 'wazdan-300', 'Burning Stars', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-300.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1685, 'apollo-titans', 'Titans', 'slot', 'Apollo', 'ms', 'mb', '', 'img/mrslotty/apollo-titans.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1686, 'fazi-fruitsandstars', 'Fruits and Stars', 'slot', 'Fazi', 'ms', 'mb', '', 'img/mrslotty/fazi-fruitsandstars.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1687, 'velagaming-dingdong', 'DingDong', 'arcade', 'Velagaming', 'ms', 'mb', '', 'img/mrslotty/velagaming-dingdong.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1688, 'tpg-lucky-boy', 'Lucky Boy', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-lucky-boy.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1689, 'slotexchange-golden-luck', 'Golden luck', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-golden-luck.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1690, 'wearecasino-golden-egg', 'Golden Egg', 'slot', 'Wearecasino', 'ms', 'mb', '', 'img/mrslotty/wearecasino-golden-egg.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1691, 'tomhorn-dragon-egg', 'Dragon Egg', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-dragon-egg.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1692, 'jokerdice', 'Joker Dice', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/jokerdice.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1693, 'bgaming-Domnitors', 'Domnitors', 'slot', 'BGaming', 'ms', 'wb', '', 'img/mrslotty/bgaming-Domnitors.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1694, 'wazdan-300', 'Burning Stars', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-300.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1695, 'apollo-titans', 'Titans', 'slot', 'Apollo', 'ms', 'wb', '', 'img/mrslotty/apollo-titans.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1696, 'fazi-fruitsandstars', 'Fruits and Stars', 'slot', 'Fazi', 'ms', 'wb', '', 'img/mrslotty/fazi-fruitsandstars.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1697, 'velagaming-dingdong', 'DingDong', 'arcade', 'Velagaming', 'ms', 'wb', '', 'img/mrslotty/velagaming-dingdong.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1698, 'tpg-lucky-boy', 'Lucky Boy', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-lucky-boy.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1699, 'slotexchange-golden-luck', 'Golden luck', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-golden-luck.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1700, 'wearecasino-golden-egg', 'Golden Egg', 'slot', 'Wearecasino', 'ms', 'wb', '', 'img/mrslotty/wearecasino-golden-egg.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1701, 'tomhorn-dragon-egg', 'Dragon Egg', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-dragon-egg.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1702, 'pandameme', 'PandaMEME', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/pandameme.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1703, 'bgaming-DomnitorsDeluxe', 'Domnitors Deluxe', 'slot', 'BGaming', 'ms', 'mb', '', 'img/mrslotty/bgaming-DomnitorsDeluxe.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1704, 'wazdan-301', 'Joker Explosion', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-301.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1705, 'apollo-horrorjoker', 'Horror Joker', 'slot', 'Apollo', 'ms', 'mb', '', 'img/mrslotty/apollo-horrorjoker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1706, 'fazi-fruitsandstars40', 'Fruits and Stars 40', 'slot', 'Fazi', 'ms', 'mb', '', 'img/mrslotty/fazi-fruitsandstars40.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1707, 'velagaming-fish', 'BlueOcean', 'arcade', 'Velagaming', 'ms', 'mb', '', 'img/mrslotty/velagaming-fish.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1708, 'tpg-korean-bbq', 'Korean BBQ', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-korean-bbq.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1709, 'slotexchange-outbreak', 'Outbreak', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-outbreak.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1710, 'wearecasino-pizza-invaders-slot', 'Pizza Invaders Slot', 'slot', 'Wearecasino', 'ms', 'mb', '', 'img/mrslotty/wearecasino-pizza-invaders-slot.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1711, 'tomhorn-wild-pearl', 'Wild Pearl', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-wild-pearl.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1712, 'pandameme', 'PandaMEME', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/pandameme.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1713, 'bgaming-DomnitorsDeluxe', 'Domnitors Deluxe', 'slot', 'BGaming', 'ms', 'wb', '', 'img/mrslotty/bgaming-DomnitorsDeluxe.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1714, 'wazdan-301', 'Joker Explosion', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-301.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1715, 'apollo-horrorjoker', 'Horror Joker', 'slot', 'Apollo', 'ms', 'wb', '', 'img/mrslotty/apollo-horrorjoker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1716, 'fazi-fruitsandstars40', 'Fruits and Stars 40', 'slot', 'Fazi', 'ms', 'wb', '', 'img/mrslotty/fazi-fruitsandstars40.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1717, 'velagaming-fish', 'BlueOcean', 'arcade', 'Velagaming', 'ms', 'wb', '', 'img/mrslotty/velagaming-fish.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1718, 'tpg-korean-bbq', 'Korean BBQ', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-korean-bbq.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1719, 'slotexchange-outbreak', 'Outbreak', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-outbreak.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1720, 'wearecasino-pizza-invaders-slot', 'Pizza Invaders Slot', 'slot', 'Wearecasino', 'ms', 'wb', '', 'img/mrslotty/wearecasino-pizza-invaders-slot.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1721, 'tomhorn-wild-pearl', 'Wild Pearl', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-wild-pearl.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1722, 'hothoney22', 'HotHoney 22', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/hothoney22.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1723, 'bgaming-DoubleExposure', 'Double Exposure', 'slot', 'BGaming', 'ms', 'mb', 'JUME', 'img/mrslotty/bgaming-DoubleExposure.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1724, 'wazdan-302', 'Super Hot', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-302.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1725, 'apollo-turboslots', 'Turbo Slots', 'slot', 'Apollo', 'ms', 'mb', '', 'img/mrslotty/apollo-turboslots.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1726, 'fazi-hotstars', 'Hot Stars', 'slot', 'Fazi', 'ms', 'mb', '', 'img/mrslotty/fazi-hotstars.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1727, 'velagaming-jungle', 'SafariHunter', 'arcade', 'Velagaming', 'ms', 'mb', '', 'img/mrslotty/velagaming-jungle.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1728, 'tpg-shaolin', 'Shaolin', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-shaolin.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1729, 'slotexchange-safari-theme-park', 'Safari theme park', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-safari-theme-park.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1730, 'wearecasino-woodslot', 'Woodslot', 'slot', 'Wearecasino', 'ms', 'mb', '', 'img/mrslotty/wearecasino-woodslot.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1731, 'tomhorn-loch-ness-monster', 'Loch Ness Monster', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-loch-ness-monster.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1732, 'hothoney22', 'HotHoney 22', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/hothoney22.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1733, 'bgaming-DoubleExposure', 'Double Exposure', 'slot', 'BGaming', 'ms', 'wb', 'JUME', 'img/mrslotty/bgaming-DoubleExposure.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1734, 'wazdan-302', 'Super Hot', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-302.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1735, 'apollo-turboslots', 'Turbo Slots', 'slot', 'Apollo', 'ms', 'wb', '', 'img/mrslotty/apollo-turboslots.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1736, 'fazi-hotstars', 'Hot Stars', 'slot', 'Fazi', 'ms', 'wb', '', 'img/mrslotty/fazi-hotstars.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1737, 'velagaming-jungle', 'SafariHunter', 'arcade', 'Velagaming', 'ms', 'wb', '', 'img/mrslotty/velagaming-jungle.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1738, 'tpg-shaolin', 'Shaolin', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-shaolin.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1739, 'slotexchange-safari-theme-park', 'Safari theme park', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-safari-theme-park.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1740, 'wearecasino-woodslot', 'Woodslot', 'slot', 'Wearecasino', 'ms', 'wb', '', 'img/mrslotty/wearecasino-woodslot.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1741, 'tomhorn-loch-ness-monster', 'Loch Ness Monster', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-loch-ness-monster.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1742, 'dolphinsgold', 'Dolphins Gold', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/dolphinsgold.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1743, 'bgaming-EuropeanRoulette', 'European Roulette', 'slot', 'BGaming', 'ms', 'mb', 'RULF', 'img/mrslotty/bgaming-EuropeanRoulette.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1744, 'wazdan-303', 'Magic Stars', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-303.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1745, 'apollo-horuseye', 'Horus Eye', 'slot', 'Apollo', 'ms', 'mb', '', 'img/mrslotty/apollo-horuseye.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1746, 'fazi-jazzyfruits', 'Jazzy Fruits', 'slot', 'Fazi', 'ms', 'mb', '', 'img/mrslotty/fazi-jazzyfruits.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1747, 'velagaming-bird', 'BirdsOfParadise', 'arcade', 'Velagaming', 'ms', 'mb', '', 'img/mrslotty/velagaming-bird.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1748, 'tpg-three-kingdoms', 'Three Kingdoms', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-three-kingdoms.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1749, 'slotexchange-city-girls', 'City girls', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-city-girls.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1750, 'wearecasino-lucky-memes', 'Lucky Memes', 'slot', 'Wearecasino', 'ms', 'mb', '', 'img/mrslotty/wearecasino-lucky-memes.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1751, 'tomhorn-quick-bingo', 'Quick Bingo', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-quick-bingo.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1752, 'dolphinsgold', 'Dolphins Gold', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/dolphinsgold.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1753, 'bgaming-EuropeanRoulette', 'European Roulette', 'slot', 'BGaming', 'ms', 'wb', 'RULF', 'img/mrslotty/bgaming-EuropeanRoulette.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1754, 'wazdan-303', 'Magic Stars', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-303.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1755, 'apollo-horuseye', 'Horus Eye', 'slot', 'Apollo', 'ms', 'wb', '', 'img/mrslotty/apollo-horuseye.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1756, 'fazi-jazzyfruits', 'Jazzy Fruits', 'slot', 'Fazi', 'ms', 'wb', '', 'img/mrslotty/fazi-jazzyfruits.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1757, 'velagaming-bird', 'BirdsOfParadise', 'arcade', 'Velagaming', 'ms', 'wb', '', 'img/mrslotty/velagaming-bird.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1758, 'tpg-three-kingdoms', 'Three Kingdoms', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-three-kingdoms.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1759, 'slotexchange-city-girls', 'City girls', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-city-girls.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1760, 'wearecasino-lucky-memes', 'Lucky Memes', 'slot', 'Wearecasino', 'ms', 'wb', '', 'img/mrslotty/wearecasino-lucky-memes.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1761, 'tomhorn-quick-bingo', 'Quick Bingo', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-quick-bingo.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1762, 'insects18', 'Insects 18+', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/insects18.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1763, 'bgaming-FantasyPark', 'Fantasy Park', 'slot', 'BGaming', 'ms', 'mb', '', 'img/mrslotty/bgaming-FantasyPark.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1764, 'wazdan-305', 'Lost Treasure', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-305.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1765, 'apollo-wildfruits', 'Wild Fruits', 'slot', 'Apollo', 'ms', 'mb', '', 'img/mrslotty/apollo-wildfruits.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1766, 'fazi-jollypoker', 'Jolly Poker', 'slot', 'Fazi', 'ms', 'mb', 'JUME', 'img/mrslotty/fazi-jollypoker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1767, 'velagaming-slot-fortune', 'FortuneWheel', 'arcade', 'Velagaming', 'ms', 'mb', '', 'img/mrslotty/velagaming-slot-fortune.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1768, 'tpg-golf-club', 'Golf Club', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-golf-club.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1769, 'slotexchange-spartan-warrior', 'Spartan warrior', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-spartan-warrior.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1770, 'wearecasino-blazing-city-night', 'Blazing City Night', 'slot', 'Wearecasino', 'ms', 'mb', '', 'img/mrslotty/wearecasino-blazing-city-night.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1771, 'tomhorn-leprechauns-treasure', 'Leprechaun\'s Treasure', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-leprechauns-treasure.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1772, 'insects18', 'Insects 18+', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/insects18.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1773, 'bgaming-FantasyPark', 'Fantasy Park', 'slot', 'BGaming', 'ms', 'wb', '', 'img/mrslotty/bgaming-FantasyPark.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1774, 'wazdan-305', 'Lost Treasure', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-305.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1775, 'apollo-wildfruits', 'Wild Fruits', 'slot', 'Apollo', 'ms', 'wb', '', 'img/mrslotty/apollo-wildfruits.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1776, 'fazi-jollypoker', 'Jolly Poker', 'slot', 'Fazi', 'ms', 'wb', 'JUME', 'img/mrslotty/fazi-jollypoker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1777, 'velagaming-slot-fortune', 'FortuneWheel', 'arcade', 'Velagaming', 'ms', 'wb', '', 'img/mrslotty/velagaming-slot-fortune.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1778, 'tpg-golf-club', 'Golf Club', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-golf-club.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1779, 'slotexchange-spartan-warrior', 'Spartan warrior', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-spartan-warrior.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1780, 'wearecasino-blazing-city-night', 'Blazing City Night', 'slot', 'Wearecasino', 'ms', 'wb', '', 'img/mrslotty/wearecasino-blazing-city-night.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1781, 'tomhorn-leprechauns-treasure', 'Leprechaun\'s Treasure', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-leprechauns-treasure.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1782, 'xmasparty', 'Xmas Party', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/xmasparty.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1783, 'bgaming-FireLightning', 'Fire Lightning', 'slot', 'BGaming', 'ms', 'mb', '', 'img/mrslotty/bgaming-FireLightning.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1784, 'wazdan-306', 'Beach Party', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-306.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1785, 'apollo-jewelfruits', 'Jewel Fruits', 'slot', 'Apollo', 'ms', 'mb', '', 'img/mrslotty/apollo-jewelfruits.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1786, 'fazi-juicyhot', 'Juicy Hot', 'slot', 'Fazi', 'ms', 'mb', '', 'img/mrslotty/fazi-juicyhot.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1787, 'velagaming-slot-fortune-dragon', 'FortuneDragon', 'arcade', 'Velagaming', 'ms', 'mb', '', 'img/mrslotty/velagaming-slot-fortune-dragon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1788, 'tpg-pokamon', 'Pokamon', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-pokamon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1789, 'slotexchange-mafia-group', 'Mafia group', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-mafia-group.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1790, 'wearecasino-battle-of-cards', 'Battle Of Cards', 'slot', 'Wearecasino', 'ms', 'mb', '', 'img/mrslotty/wearecasino-battle-of-cards.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1791, 'tomhorn-la-playa', 'La Playa', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-la-playa.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1792, 'xmasparty', 'Xmas Party', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/xmasparty.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1793, 'bgaming-FireLightning', 'Fire Lightning', 'slot', 'BGaming', 'ms', 'wb', '', 'img/mrslotty/bgaming-FireLightning.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1794, 'wazdan-306', 'Beach Party', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-306.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1795, 'apollo-jewelfruits', 'Jewel Fruits', 'slot', 'Apollo', 'ms', 'wb', '', 'img/mrslotty/apollo-jewelfruits.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1796, 'fazi-juicyhot', 'Juicy Hot', 'slot', 'Fazi', 'ms', 'wb', '', 'img/mrslotty/fazi-juicyhot.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1797, 'velagaming-slot-fortune-dragon', 'FortuneDragon', 'arcade', 'Velagaming', 'ms', 'wb', '', 'img/mrslotty/velagaming-slot-fortune-dragon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1798, 'tpg-pokamon', 'Pokamon', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-pokamon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1799, 'slotexchange-mafia-group', 'Mafia group', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-mafia-group.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1800, 'wearecasino-battle-of-cards', 'Battle Of Cards', 'slot', 'Wearecasino', 'ms', 'wb', '', 'img/mrslotty/wearecasino-battle-of-cards.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1801, 'tomhorn-la-playa', 'La Playa', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-la-playa.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1802, 'jungletreasure', 'JungleTreasure', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/jungletreasure.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1803, 'bgaming-FrenchRoulette', 'French Roulette', 'slot', 'BGaming', 'ms', 'mb', 'RULF', 'img/mrslotty/bgaming-FrenchRoulette.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1804, 'wazdan-307', 'Miami Beach', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-307.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1805, 'apollo-woodenfruits', 'Wooden Fruits', 'slot', 'Apollo', 'ms', 'mb', '', 'img/mrslotty/apollo-woodenfruits.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1806, 'fazi-katanasoftime', 'Katanas of Time', 'slot', 'Fazi', 'ms', 'mb', '', 'img/mrslotty/fazi-katanasoftime.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1807, 'velagaming-slot-wealth', 'WealthPalace', 'arcade', 'Velagaming', 'ms', 'mb', '', 'img/mrslotty/velagaming-slot-wealth.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1808, 'tpg-the-white-snake', 'The White Snake', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-the-white-snake.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1809, 'slotexchange-cherry-blossoms', 'Cherry blossoms', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-cherry-blossoms.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1810, 'wearecasino-fruity-outlaws', 'Fruity Outlaws', 'slot', 'Wearecasino', 'ms', 'mb', '', 'img/mrslotty/wearecasino-fruity-outlaws.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1811, 'tomhorn-gangland', 'Gangland', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-gangland.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1812, 'jungletreasure', 'JungleTreasure', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/jungletreasure.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1813, 'bgaming-FrenchRoulette', 'French Roulette', 'slot', 'BGaming', 'ms', 'wb', 'RULF', 'img/mrslotty/bgaming-FrenchRoulette.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1814, 'wazdan-307', 'Miami Beach', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-307.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1815, 'apollo-woodenfruits', 'Wooden Fruits', 'slot', 'Apollo', 'ms', 'wb', '', 'img/mrslotty/apollo-woodenfruits.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1816, 'fazi-katanasoftime', 'Katanas of Time', 'slot', 'Fazi', 'ms', 'wb', '', 'img/mrslotty/fazi-katanasoftime.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1817, 'velagaming-slot-wealth', 'WealthPalace', 'arcade', 'Velagaming', 'ms', 'wb', '', 'img/mrslotty/velagaming-slot-wealth.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1818, 'tpg-the-white-snake', 'The White Snake', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-the-white-snake.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1819, 'slotexchange-cherry-blossoms', 'Cherry blossoms', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-cherry-blossoms.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1820, 'wearecasino-fruity-outlaws', 'Fruity Outlaws', 'slot', 'Wearecasino', 'ms', 'wb', '', 'img/mrslotty/wearecasino-fruity-outlaws.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1821, 'tomhorn-gangland', 'Gangland', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-gangland.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1822, 'fruitsanddiamonds', 'Fruits&Diamonds', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/fruitsanddiamonds.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1823, 'bgaming-HawaiiCocktails', 'Hawaii Cocktails', 'slot', 'BGaming', 'ms', 'mb', '', 'img/mrslotty/bgaming-HawaiiCocktails.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1824, 'wazdan-310', 'Lucky Fortune', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-310.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1825, 'apollo-lucky81', 'Lucky 81', 'slot', 'Apollo', 'ms', 'mb', '', 'img/mrslotty/apollo-lucky81.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1826, 'fazi-liveeuropeanroulette', 'Live European Roulette', 'slot', 'Fazi', 'ms', 'mb', 'RULF', 'img/mrslotty/fazi-liveeuropeanroulette.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1827, 'velagaming-slot-kakiemon', 'Kakiemon', 'arcade', 'Velagaming', 'ms', 'mb', '', 'img/mrslotty/velagaming-slot-kakiemon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1828, 'tpg-bikini-beach', 'Bikini Beach', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-bikini-beach.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1829, 'slotexchange-gunslinger', 'Gunslinger', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-gunslinger.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1830, 'wearecasino-racing-fever', 'Racing Fever', 'slot', 'Wearecasino', 'ms', 'mb', '', 'img/mrslotty/wearecasino-racing-fever.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1831, 'tomhorn-wild-sierra', 'Wild Sierra', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-wild-sierra.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1832, 'fruitsanddiamonds', 'Fruits&Diamonds', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/fruitsanddiamonds.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1833, 'bgaming-HawaiiCocktails', 'Hawaii Cocktails', 'slot', 'BGaming', 'ms', 'wb', '', 'img/mrslotty/bgaming-HawaiiCocktails.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1834, 'wazdan-310', 'Lucky Fortune', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-310.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1835, 'apollo-lucky81', 'Lucky 81', 'slot', 'Apollo', 'ms', 'wb', '', 'img/mrslotty/apollo-lucky81.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1836, 'fazi-liveeuropeanroulette', 'Live European Roulette', 'slot', 'Fazi', 'ms', 'wb', 'RULF', 'img/mrslotty/fazi-liveeuropeanroulette.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1837, 'velagaming-slot-kakiemon', 'Kakiemon', 'arcade', 'Velagaming', 'ms', 'wb', '', 'img/mrslotty/velagaming-slot-kakiemon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1838, 'tpg-bikini-beach', 'Bikini Beach', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-bikini-beach.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1839, 'slotexchange-gunslinger', 'Gunslinger', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-gunslinger.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1840, 'wearecasino-racing-fever', 'Racing Fever', 'slot', 'Wearecasino', 'ms', 'wb', '', 'img/mrslotty/wearecasino-racing-fever.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1841, 'tomhorn-wild-sierra', 'Wild Sierra', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-wild-sierra.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1842, '50linesofwar', '50 Lines Of War', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/50linesofwar.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1843, 'bgaming-HeadsTails', 'Heads and Tails', 'slot', 'BGaming', 'ms', 'mb', '', 'img/mrslotty/bgaming-HeadsTails.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1844, 'wazdan-311', 'Golden Sphinx', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-311.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1845, 'apollo-madmechanicdeluxe', 'Mad Mechanic Deluxe', 'slot', 'Apollo', 'ms', 'mb', '', 'img/mrslotty/apollo-madmechanicdeluxe.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1846, 'fazi-luxroulette', 'Lux Roulette', 'slot', 'Fazi', 'ms', 'mb', 'RULF', 'img/mrslotty/fazi-luxroulette.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1847, 'velagaming-slot-gold', 'CityOfGold', 'arcade', 'Velagaming', 'ms', 'mb', '', 'img/mrslotty/velagaming-slot-gold.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1848, 'tpg-ancient-egypt', 'Ancient Egypt', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-ancient-egypt.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1849, 'slotexchange-retro-diner', 'Retro diner', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-retro-diner.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1850, 'allwayspin-AWS_1', 'DJ MONKEY KING', 'slot', 'AllwaySpin', 'ms', 'mb', '', 'img/mrslotty/allwayspin-AWS_1.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1851, 'tomhorn-aces-and-faces-mega-poker', 'Aces And Faces Mega Poker', 'slot', 'Tomhorn', 'ms', 'mb', 'JUME', 'img/mrslotty/tomhorn-aces-and-faces-mega-poker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1852, '50linesofwar', '50 Lines Of War', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/50linesofwar.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1853, 'bgaming-HeadsTails', 'Heads and Tails', 'slot', 'BGaming', 'ms', 'wb', '', 'img/mrslotty/bgaming-HeadsTails.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1854, 'wazdan-311', 'Golden Sphinx', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-311.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1855, 'apollo-madmechanicdeluxe', 'Mad Mechanic Deluxe', 'slot', 'Apollo', 'ms', 'wb', '', 'img/mrslotty/apollo-madmechanicdeluxe.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1856, 'fazi-luxroulette', 'Lux Roulette', 'slot', 'Fazi', 'ms', 'wb', 'RULF', 'img/mrslotty/fazi-luxroulette.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1857, 'velagaming-slot-gold', 'CityOfGold', 'arcade', 'Velagaming', 'ms', 'wb', '', 'img/mrslotty/velagaming-slot-gold.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1858, 'tpg-ancient-egypt', 'Ancient Egypt', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-ancient-egypt.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1859, 'slotexchange-retro-diner', 'Retro diner', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-retro-diner.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1860, 'allwayspin-AWS_1', 'DJ MONKEY KING', 'slot', 'AllwaySpin', 'ms', 'wb', '', 'img/mrslotty/allwayspin-AWS_1.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1861, 'tomhorn-aces-and-faces-mega-poker', 'Aces And Faces Mega Poker', 'slot', 'Tomhorn', 'ms', 'wb', 'JUME', 'img/mrslotty/tomhorn-aces-and-faces-mega-poker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1862, 'agentxmission', 'Agent X MIssion', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/agentxmission.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1863, 'bgaming-HelloEaster', 'Hello Easter', 'slot', 'BGaming', 'ms', 'mb', '', 'img/mrslotty/bgaming-HelloEaster.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1864, 'wazdan-313', 'Triple Star', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-313.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1865, 'mascot-anksunamun_tqoe', 'Anksunamun: the Queen of Egypt', 'slot', 'Mascot', 'ms', 'mb', '', 'img/mrslotty/mascot-anksunamun_tqoe.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1866, 'fazi-megahot', 'Mega Hot', 'slot', 'Fazi', 'ms', 'mb', '', 'img/mrslotty/fazi-megahot.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1867, 'velagaming-slot-belt-road', 'TheBeltAndRoad', 'arcade', 'Velagaming', 'ms', 'mb', '', 'img/mrslotty/velagaming-slot-belt-road.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1868, 'tpg-special-forces', 'Special Forces', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-special-forces.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1869, 'slotexchange-ancient-idols', 'Ancient idols', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-ancient-idols.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1870, 'allwayspin-AWS_2', 'MOUSE OF FORTUNE', 'slot', 'AllwaySpin', 'ms', 'mb', '', 'img/mrslotty/allwayspin-AWS_2.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1871, 'tomhorn-drunken-vikings', 'Drunken Vikings', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-drunken-vikings.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1872, 'agentxmission', 'Agent X MIssion', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/agentxmission.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1873, 'bgaming-HelloEaster', 'Hello Easter', 'slot', 'BGaming', 'ms', 'wb', '', 'img/mrslotty/bgaming-HelloEaster.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1874, 'wazdan-313', 'Triple Star', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-313.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1875, 'mascot-anksunamun_tqoe', 'Anksunamun: the Queen of Egypt', 'slot', 'Mascot', 'ms', 'wb', '', 'img/mrslotty/mascot-anksunamun_tqoe.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1876, 'fazi-megahot', 'Mega Hot', 'slot', 'Fazi', 'ms', 'wb', '', 'img/mrslotty/fazi-megahot.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1877, 'velagaming-slot-belt-road', 'TheBeltAndRoad', 'arcade', 'Velagaming', 'ms', 'wb', '', 'img/mrslotty/velagaming-slot-belt-road.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1878, 'tpg-special-forces', 'Special Forces', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-special-forces.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1879, 'slotexchange-ancient-idols', 'Ancient idols', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-ancient-idols.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1880, 'allwayspin-AWS_2', 'MOUSE OF FORTUNE', 'slot', 'AllwaySpin', 'ms', 'wb', '', 'img/mrslotty/allwayspin-AWS_2.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1881, 'tomhorn-drunken-vikings', 'Drunken Vikings', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-drunken-vikings.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1882, 'wildvegas', 'WildVegas', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/wildvegas.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1883, 'bgaming-HiLoSwitch', 'Hi-Lo Switch', 'slot', 'BGaming', 'ms', 'mb', 'JUME', 'img/mrslotty/bgaming-HiLoSwitch.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1884, 'wazdan-314', 'Good Luck 40', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-314.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1885, 'mascot-lions_pride', 'Lions Pride', 'slot', 'Mascot', 'ms', 'mb', '', 'img/mrslotty/mascot-lions_pride.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1886, 'fazi-monsters', 'Monsters', 'slot', 'Fazi', 'ms', 'mb', '', 'img/mrslotty/fazi-monsters.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1887, 'velagaming-slot-museum', 'TheMuseum', 'arcade', 'Velagaming', 'ms', 'mb', '', 'img/mrslotty/velagaming-slot-museum.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1888, 'tpg-tricky-brains', 'Tricky Brains', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-tricky-brains.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1889, 'slotexchange-musketeers', 'Musketeers', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-musketeers.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1890, 'allwayspin-AWS_3', 'ROCK N OWL', 'slot', 'AllwaySpin', 'ms', 'mb', '', 'img/mrslotty/allwayspin-AWS_3.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1891, 'tomhorn-double-poker', 'Double Poker', 'slot', 'Tomhorn', 'ms', 'mb', 'JUME', 'img/mrslotty/tomhorn-double-poker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1892, 'wildvegas', 'WildVegas', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/wildvegas.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1893, 'bgaming-HiLoSwitch', 'Hi-Lo Switch', 'slot', 'BGaming', 'ms', 'wb', 'JUME', 'img/mrslotty/bgaming-HiLoSwitch.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1894, 'wazdan-314', 'Good Luck 40', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-314.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1895, 'mascot-lions_pride', 'Lions Pride', 'slot', 'Mascot', 'ms', 'wb', '', 'img/mrslotty/mascot-lions_pride.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1896, 'fazi-monsters', 'Monsters', 'slot', 'Fazi', 'ms', 'wb', '', 'img/mrslotty/fazi-monsters.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1897, 'velagaming-slot-museum', 'TheMuseum', 'arcade', 'Velagaming', 'ms', 'wb', '', 'img/mrslotty/velagaming-slot-museum.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1898, 'tpg-tricky-brains', 'Tricky Brains', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-tricky-brains.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1899, 'slotexchange-musketeers', 'Musketeers', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-musketeers.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1900, 'allwayspin-AWS_3', 'ROCK N OWL', 'slot', 'AllwaySpin', 'ms', 'wb', '', 'img/mrslotty/allwayspin-AWS_3.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1901, 'tomhorn-double-poker', 'Double Poker', 'slot', 'Tomhorn', 'ms', 'wb', 'JUME', 'img/mrslotty/tomhorn-double-poker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1902, 'cryptomatrix', 'CryptoMatrix', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/cryptomatrix.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1903, 'bgaming-JacksOrBetter', 'Jacks or Better', 'slot', 'BGaming', 'ms', 'mb', 'JUME', 'img/mrslotty/bgaming-JacksOrBetter.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1904, 'wazdan-315', 'Win & Replay', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-315.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1905, 'mascot-re_kill', 'ReKill', 'slot', 'Mascot', 'ms', 'mb', '', 'img/mrslotty/mascot-re_kill.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1906, 'fazi-pirates', 'Pirates', 'slot', 'Fazi', 'ms', 'mb', '', 'img/mrslotty/fazi-pirates.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1907, 'velagaming-slot-pirate', 'PirateQueen', 'arcade', 'Velagaming', 'ms', 'mb', '', 'img/mrslotty/velagaming-slot-pirate.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1908, 'tpg-f1-racing', 'F1 Racing', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-f1-racing.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1909, 'slotexchange-north-pole', 'North pole', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-north-pole.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1910, 'allwayspin-AWS_4', 'BOOK OF TRICKS', 'slot', 'AllwaySpin', 'ms', 'mb', '', 'img/mrslotty/allwayspin-AWS_4.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1911, 'tomhorn-extra-keno', 'Extra Keno', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-extra-keno.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1912, 'cryptomatrix', 'CryptoMatrix', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/cryptomatrix.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1913, 'bgaming-JacksOrBetter', 'Jacks or Better', 'slot', 'BGaming', 'ms', 'wb', 'JUME', 'img/mrslotty/bgaming-JacksOrBetter.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1914, 'wazdan-315', 'Win & Replay', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-315.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1915, 'mascot-re_kill', 'ReKill', 'slot', 'Mascot', 'ms', 'wb', '', 'img/mrslotty/mascot-re_kill.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1916, 'fazi-pirates', 'Pirates', 'slot', 'Fazi', 'ms', 'wb', '', 'img/mrslotty/fazi-pirates.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1917, 'velagaming-slot-pirate', 'PirateQueen', 'arcade', 'Velagaming', 'ms', 'wb', '', 'img/mrslotty/velagaming-slot-pirate.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1918, 'tpg-f1-racing', 'F1 Racing', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-f1-racing.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1919, 'slotexchange-north-pole', 'North pole', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-north-pole.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1920, 'allwayspin-AWS_4', 'BOOK OF TRICKS', 'slot', 'AllwaySpin', 'ms', 'wb', '', 'img/mrslotty/allwayspin-AWS_4.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1921, 'tomhorn-extra-keno', 'Extra Keno', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-extra-keno.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1922, 'gemstower', 'Gems Tower', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/gemstower.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1923, 'bgaming-JogoDoBicho', 'Jogo do Bicho', 'slot', 'BGaming', 'ms', 'mb', '', 'img/mrslotty/bgaming-JogoDoBicho.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1924, 'wazdan-316', 'KickOff', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-316.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1925, 'mascot-northern_heat', 'Northern Heat', 'slot', 'Mascot', 'ms', 'mb', '', 'img/mrslotty/mascot-northern_heat.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1926, 'fazi-postman', 'Postman', 'slot', 'Fazi', 'ms', 'mb', '', 'img/mrslotty/fazi-postman.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1927, 'velagaming-slot-witch', 'TheWitchMustBeCrazy', 'arcade', 'Velagaming', 'ms', 'mb', '', 'img/mrslotty/velagaming-slot-witch.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1928, 'tpg-baby-pet', 'Baby Pet', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-baby-pet.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1929, 'slotexchange-age-of-jazz', 'Age of jazz', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-age-of-jazz.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1930, 'allwayspin-AWS_5', 'ALICE IN WINTERLAND', 'slot', 'AllwaySpin', 'ms', 'mb', '', 'img/mrslotty/allwayspin-AWS_5.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1931, 'tomhorn-super-keno', 'Super Keno', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-super-keno.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1932, 'gemstower', 'Gems Tower', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/gemstower.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1933, 'bgaming-JogoDoBicho', 'Jogo do Bicho', 'slot', 'BGaming', 'ms', 'wb', '', 'img/mrslotty/bgaming-JogoDoBicho.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1934, 'wazdan-316', 'KickOff', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-316.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1935, 'mascot-northern_heat', 'Northern Heat', 'slot', 'Mascot', 'ms', 'wb', '', 'img/mrslotty/mascot-northern_heat.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1936, 'fazi-postman', 'Postman', 'slot', 'Fazi', 'ms', 'wb', '', 'img/mrslotty/fazi-postman.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1937, 'velagaming-slot-witch', 'TheWitchMustBeCrazy', 'arcade', 'Velagaming', 'ms', 'wb', '', 'img/mrslotty/velagaming-slot-witch.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1938, 'tpg-baby-pet', 'Baby Pet', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-baby-pet.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1939, 'slotexchange-age-of-jazz', 'Age of jazz', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-age-of-jazz.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1940, 'allwayspin-AWS_5', 'ALICE IN WINTERLAND', 'slot', 'AllwaySpin', 'ms', 'wb', '', 'img/mrslotty/allwayspin-AWS_5.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1941, 'tomhorn-super-keno', 'Super Keno', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-super-keno.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1942, 'draculasgems', 'Dracula\'s Gems', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/draculasgems.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1943, 'bgaming-JourneyFlirt', 'Journey Flirt', 'slot', 'BGaming', 'ms', 'mb', '', 'img/mrslotty/bgaming-JourneyFlirt.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1944, 'wazdan-318', 'Night Club 81', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-318.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1945, 'mascot-cancan_saloon', 'CanCan Saloon', 'slot', 'Mascot', 'ms', 'mb', '', 'img/mrslotty/mascot-cancan_saloon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1946, 'fazi-pyramid', 'Pyramid', 'slot', 'Fazi', 'ms', 'mb', '', 'img/mrslotty/fazi-pyramid.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1947, 'velagaming-slot-kokeshi', 'FushaKokeshi', 'arcade', 'Velagaming', 'ms', 'mb', '', 'img/mrslotty/velagaming-slot-kokeshi.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1948, 'tpg-gemstone-legend', 'Gemstone Legend', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-gemstone-legend.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1949, 'slotexchange-creatures-lab', 'Creatures lab', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-creatures-lab.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1950, 'allwayspin-AWS_6', 'DALANG', 'slot', 'AllwaySpin', 'ms', 'mb', '', 'img/mrslotty/allwayspin-AWS_6.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1951, 'tomhorn-power-keno', 'Power Keno', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-power-keno.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1952, 'draculasgems', 'Dracula\'s Gems', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/draculasgems.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1953, 'bgaming-JourneyFlirt', 'Journey Flirt', 'slot', 'BGaming', 'ms', 'wb', '', 'img/mrslotty/bgaming-JourneyFlirt.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1954, 'wazdan-318', 'Night Club 81', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-318.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1955, 'mascot-cancan_saloon', 'CanCan Saloon', 'slot', 'Mascot', 'ms', 'wb', '', 'img/mrslotty/mascot-cancan_saloon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1956, 'fazi-pyramid', 'Pyramid', 'slot', 'Fazi', 'ms', 'wb', '', 'img/mrslotty/fazi-pyramid.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1957, 'velagaming-slot-kokeshi', 'FushaKokeshi', 'arcade', 'Velagaming', 'ms', 'wb', '', 'img/mrslotty/velagaming-slot-kokeshi.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1958, 'tpg-gemstone-legend', 'Gemstone Legend', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-gemstone-legend.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1959, 'slotexchange-creatures-lab', 'Creatures lab', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-creatures-lab.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1960, 'allwayspin-AWS_6', 'DALANG', 'slot', 'AllwaySpin', 'ms', 'wb', '', 'img/mrslotty/allwayspin-AWS_6.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1961, 'tomhorn-power-keno', 'Power Keno', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-power-keno.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1962, 'godsofegypt', 'Gods Of Egypt', 'slot', 'MrSlotty', 'ms', 'mb', '', 'img/mrslotty/godsofegypt.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1963, 'bgaming-LetItRide', 'Let it Ride', 'slot', 'BGaming', 'ms', 'mb', 'JUME', 'img/mrslotty/bgaming-LetItRide.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1964, 'wazdan-319', 'Crazy Cars', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-319.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1965, 'mascot-the_rite', 'The Rite', 'slot', 'Mascot', 'ms', 'mb', '', 'img/mrslotty/mascot-the_rite.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1966, 'fazi-retro7hot', 'Retro 7 Hot', 'slot', 'Fazi', 'ms', 'mb', '', 'img/mrslotty/fazi-retro7hot.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1967, 'velagaming-slot-zombie', 'ZombieVSDaoZhang', 'arcade', 'Velagaming', 'ms', 'mb', '', 'img/mrslotty/velagaming-slot-zombie.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1968, 'tpg-chinese-zodiac', 'Chinese Zodiac', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-chinese-zodiac.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1969, 'slotexchange-wild-wheels', 'Wild wheels', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-wild-wheels.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1970, 'allwayspin-AWS_7', 'SHISA', 'slot', 'AllwaySpin', 'ms', 'mb', '', 'img/mrslotty/allwayspin-AWS_7.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1971, 'tomhorn-gardener', 'Gardener', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-gardener.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1972, 'godsofegypt', 'Gods Of Egypt', 'slot', 'MrSlotty', 'ms', 'wb', '', 'img/mrslotty/godsofegypt.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1973, 'bgaming-LetItRide', 'Let it Ride', 'slot', 'BGaming', 'ms', 'wb', 'JUME', 'img/mrslotty/bgaming-LetItRide.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1974, 'wazdan-319', 'Crazy Cars', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-319.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1975, 'mascot-the_rite', 'The Rite', 'slot', 'Mascot', 'ms', 'wb', '', 'img/mrslotty/mascot-the_rite.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1976, 'fazi-retro7hot', 'Retro 7 Hot', 'slot', 'Fazi', 'ms', 'wb', '', 'img/mrslotty/fazi-retro7hot.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1977, 'velagaming-slot-zombie', 'ZombieVSDaoZhang', 'arcade', 'Velagaming', 'ms', 'wb', '', 'img/mrslotty/velagaming-slot-zombie.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1978, 'tpg-chinese-zodiac', 'Chinese Zodiac', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-chinese-zodiac.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1979, 'slotexchange-wild-wheels', 'Wild wheels', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-wild-wheels.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1980, 'allwayspin-AWS_7', 'SHISA', 'slot', 'AllwaySpin', 'ms', 'wb', '', 'img/mrslotty/allwayspin-AWS_7.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1981, 'tomhorn-gardener', 'Gardener', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-gardener.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1982, 'truelab-victoria-wild', 'Victoria Wild', 'slot', 'TrueLab', 'ms', 'mb', '', 'img/mrslotty/truelab-victoria-wild.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1983, 'bgaming-LuckyBlue', 'Lucky Blue', 'slot', 'BGaming', 'ms', 'mb', '', 'img/mrslotty/bgaming-LuckyBlue.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1984, 'wazdan-321', 'Bell Wizard', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-321.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1985, 'mascot-venetian_magic', 'Venetian Magic', 'slot', 'Mascot', 'ms', 'mb', '', 'img/mrslotty/mascot-venetian_magic.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1986, 'fazi-roulette', 'Roulette', 'slot', 'Fazi', 'ms', 'mb', '', 'img/mrslotty/fazi-roulette.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1987, 'velagaming-slot-shambhala', 'ParadiseOfShambhala', 'arcade', 'Velagaming', 'ms', 'mb', '', 'img/mrslotty/velagaming-slot-shambhala.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1988, 'tpg-greek-mythology', 'Greek Mythology', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-greek-mythology.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1989, 'slotexchange-silver-bullet', 'Silver bullet', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-silver-bullet.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1990, 'allwayspin-AWS_8', 'FENG SHEN', 'slot', 'AllwaySpin', 'ms', 'mb', '', 'img/mrslotty/allwayspin-AWS_8.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1991, 'tomhorn-wall-street', 'Wall Street', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-wall-street.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1992, 'truelab-victoria-wild', 'Victoria Wild', 'slot', 'TrueLab', 'ms', 'wb', '', 'img/mrslotty/truelab-victoria-wild.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1993, 'bgaming-LuckyBlue', 'Lucky Blue', 'slot', 'BGaming', 'ms', 'wb', '', 'img/mrslotty/bgaming-LuckyBlue.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1994, 'wazdan-321', 'Bell Wizard', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-321.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1995, 'mascot-venetian_magic', 'Venetian Magic', 'slot', 'Mascot', 'ms', 'wb', '', 'img/mrslotty/mascot-venetian_magic.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1996, 'fazi-roulette', 'Roulette', 'slot', 'Fazi', 'ms', 'wb', '', 'img/mrslotty/fazi-roulette.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1997, 'velagaming-slot-shambhala', 'ParadiseOfShambhala', 'arcade', 'Velagaming', 'ms', 'wb', '', 'img/mrslotty/velagaming-slot-shambhala.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1998, 'tpg-greek-mythology', 'Greek Mythology', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-greek-mythology.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(1999, 'slotexchange-silver-bullet', 'Silver bullet', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-silver-bullet.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2000, 'allwayspin-AWS_8', 'FENG SHEN', 'slot', 'AllwaySpin', 'ms', 'wb', '', 'img/mrslotty/allwayspin-AWS_8.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2001, 'tomhorn-wall-street', 'Wall Street', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-wall-street.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2002, 'truelab-illusionist', 'Robby the Illusionist', 'slot', 'TrueLab', 'ms', 'mb', '', 'img/mrslotty/truelab-illusionist.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2003, 'bgaming-LuckyLadyClover', 'Lucky Lady\'s Clover', 'slot', 'BGaming', 'ms', 'mb', '', 'img/mrslotty/bgaming-LuckyLadyClover.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2004, 'wazdan-323', 'Demon Jack 27', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-323.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2005, 'mascot-mermaids_bay', 'Mermaid\'s Bay', 'slot', 'Mascot', 'ms', 'mb', '', 'img/mrslotty/mascot-mermaids_bay.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2006, 'fazi-spaceguardians', 'Space Guardians', 'slot', 'Fazi', 'ms', 'mb', '', 'img/mrslotty/fazi-spaceguardians.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2007, 'velagaming-slot-hyakkiyakou', 'Hyakkiyakou', 'arcade', 'Velagaming', 'ms', 'mb', '', 'img/mrslotty/velagaming-slot-hyakkiyakou.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2008, 'tpg-poker-king', 'Poker King', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-poker-king.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2009, 'slotexchange-alien-jungle', 'Alien jungle', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-alien-jungle.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2010, 'allwayspin-AWS_9', '888 TOWER', 'slot', 'AllwaySpin', 'ms', 'mb', '', 'img/mrslotty/allwayspin-AWS_9.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2011, 'tomhorn-royal-double', 'Royal Double', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-royal-double.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2012, 'truelab-illusionist', 'Robby the Illusionist', 'slot', 'TrueLab', 'ms', 'wb', '', 'img/mrslotty/truelab-illusionist.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2013, 'bgaming-LuckyLadyClover', 'Lucky Lady\'s Clover', 'slot', 'BGaming', 'ms', 'wb', '', 'img/mrslotty/bgaming-LuckyLadyClover.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2014, 'wazdan-323', 'Demon Jack 27', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-323.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2015, 'mascot-mermaids_bay', 'Mermaid\'s Bay', 'slot', 'Mascot', 'ms', 'wb', '', 'img/mrslotty/mascot-mermaids_bay.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2016, 'fazi-spaceguardians', 'Space Guardians', 'slot', 'Fazi', 'ms', 'wb', '', 'img/mrslotty/fazi-spaceguardians.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2017, 'velagaming-slot-hyakkiyakou', 'Hyakkiyakou', 'arcade', 'Velagaming', 'ms', 'wb', '', 'img/mrslotty/velagaming-slot-hyakkiyakou.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2018, 'tpg-poker-king', 'Poker King', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-poker-king.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2019, 'slotexchange-alien-jungle', 'Alien jungle', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-alien-jungle.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2020, 'allwayspin-AWS_9', '888 TOWER', 'slot', 'AllwaySpin', 'ms', 'wb', '', 'img/mrslotty/allwayspin-AWS_9.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2021, 'tomhorn-royal-double', 'Royal Double', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-royal-double.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2022, 'truelab-mining-factory', 'Mining Factory', 'slot', 'TrueLab', 'ms', 'mb', '', 'img/mrslotty/truelab-mining-factory.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2023, 'bgaming-LuckySweets', 'Lucky Sweets', 'slot', 'BGaming', 'ms', 'mb', '', 'img/mrslotty/bgaming-LuckySweets.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2024, 'wazdan-324', 'Beach Party Hot', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-324.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2025, 'mascot-merlins_tower', 'Merlin\'s Tower', 'slot', 'Mascot', 'ms', 'mb', '', 'img/mrslotty/mascot-merlins_tower.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2026, 'fazi-stargems', 'Star Gems', 'slot', 'Fazi', 'ms', 'mb', '', 'img/mrslotty/fazi-stargems.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2027, 'velagaming-slot-otake', '\'Otaku\'\'sHeaven\'', 'arcade', 'Velagaming', 'ms', 'mb', '', 'img/mrslotty/velagaming-slot-otake.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2028, 'tpg-candy-cart', 'Candy Cart', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-candy-cart.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2029, 'slotexchange-future-war', 'Future war', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-future-war.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2030, 'allwayspin-AWS_10', 'RONIN', 'slot', 'AllwaySpin', 'ms', 'mb', '', 'img/mrslotty/allwayspin-AWS_10.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2031, 'tomhorn-red-seven', 'Red Seven', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-red-seven.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2032, 'truelab-mining-factory', 'Mining Factory', 'slot', 'TrueLab', 'ms', 'wb', '', 'img/mrslotty/truelab-mining-factory.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2033, 'bgaming-LuckySweets', 'Lucky Sweets', 'slot', 'BGaming', 'ms', 'wb', '', 'img/mrslotty/bgaming-LuckySweets.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2034, 'wazdan-324', 'Beach Party Hot', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-324.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2035, 'mascot-merlins_tower', 'Merlin\'s Tower', 'slot', 'Mascot', 'ms', 'wb', '', 'img/mrslotty/mascot-merlins_tower.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2036, 'fazi-stargems', 'Star Gems', 'slot', 'Fazi', 'ms', 'wb', '', 'img/mrslotty/fazi-stargems.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2037, 'velagaming-slot-otake', '\'Otaku\'\'sHeaven\'', 'arcade', 'Velagaming', 'ms', 'wb', '', 'img/mrslotty/velagaming-slot-otake.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2038, 'tpg-candy-cart', 'Candy Cart', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-candy-cart.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2039, 'slotexchange-future-war', 'Future war', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-future-war.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2040, 'allwayspin-AWS_10', 'RONIN', 'slot', 'AllwaySpin', 'ms', 'wb', '', 'img/mrslotty/allwayspin-AWS_10.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2041, 'tomhorn-red-seven', 'Red Seven', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-red-seven.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2042, 'truelab-magic-dice', 'Magic Dice', 'slot', 'TrueLab', 'ms', 'mb', '', 'img/mrslotty/truelab-magic-dice.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2043, 'bgaming-MechanicalOrange', 'Mechanical Orange', 'slot', 'BGaming', 'ms', 'mb', '', 'img/mrslotty/bgaming-MechanicalOrange.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2044, 'wazdan-325', 'Vegas Hot 81', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-325.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2045, 'mascot-purple_pills', 'Purple Pills', 'slot', 'Mascot', 'ms', 'mb', '', 'img/mrslotty/mascot-purple_pills.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2046, 'fazi-starlight', 'Star Runner', 'slot', 'Fazi', 'ms', 'mb', '', 'img/mrslotty/fazi-starlight.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2047, 'velagaming-slot-shanhaijing', 'ShanHaiJing', 'arcade', 'Velagaming', 'ms', 'mb', '', 'img/mrslotty/velagaming-slot-shanhaijing.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2048, 'tpg-rome-warrior', 'Rome Warrior', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-rome-warrior.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2049, 'slotexchange-zombie-night', 'Zombie night', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-zombie-night.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2050, 'allwayspin-AWS_11', 'WIENER DONUTS', 'slot', 'AllwaySpin', 'ms', 'mb', '', 'img/mrslotty/allwayspin-AWS_11.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2051, 'tomhorn-wild-bells', 'Wild Bells', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-wild-bells.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2052, 'truelab-magic-dice', 'Magic Dice', 'slot', 'TrueLab', 'ms', 'wb', '', 'img/mrslotty/truelab-magic-dice.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2053, 'bgaming-MechanicalOrange', 'Mechanical Orange', 'slot', 'BGaming', 'ms', 'wb', '', 'img/mrslotty/bgaming-MechanicalOrange.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2054, 'wazdan-325', 'Vegas Hot 81', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-325.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2055, 'mascot-purple_pills', 'Purple Pills', 'slot', 'Mascot', 'ms', 'wb', '', 'img/mrslotty/mascot-purple_pills.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2056, 'fazi-starlight', 'Star Runner', 'slot', 'Fazi', 'ms', 'wb', '', 'img/mrslotty/fazi-starlight.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2057, 'velagaming-slot-shanhaijing', 'ShanHaiJing', 'arcade', 'Velagaming', 'ms', 'wb', '', 'img/mrslotty/velagaming-slot-shanhaijing.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2058, 'tpg-rome-warrior', 'Rome Warrior', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-rome-warrior.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2059, 'slotexchange-zombie-night', 'Zombie night', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-zombie-night.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2060, 'allwayspin-AWS_11', 'WIENER DONUTS', 'slot', 'AllwaySpin', 'ms', 'wb', '', 'img/mrslotty/allwayspin-AWS_11.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2061, 'tomhorn-wild-bells', 'Wild Bells', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-wild-bells.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2062, 'truelab-pirate-bay', 'Pirate Bay', 'slot', 'TrueLab', 'ms', 'mb', '', 'img/mrslotty/truelab-pirate-bay.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2063, 'bgaming-Minesweeper', 'Minesweeper', 'slot', 'BGaming', 'ms', 'mb', '', 'img/mrslotty/bgaming-Minesweeper.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2064, 'wazdan-326', 'Welcome To Hell 81', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-326.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2065, 'mascot-bamboo_bear', 'Bamboo Bear', 'slot', 'Mascot', 'ms', 'mb', '', 'img/mrslotty/mascot-bamboo_bear.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2066, 'fazi-starrunner', 'Starlight', 'slot', 'Fazi', 'ms', 'mb', '', 'img/mrslotty/fazi-starrunner.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2067, 'velagaming-slot-angkor', 'PrincessOfAngkorWat', 'arcade', 'Velagaming', 'ms', 'mb', '', 'img/mrslotty/velagaming-slot-angkor.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2068, 'tpg-selfie', 'Selfie', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-selfie.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2069, 'slotexchange-dawn-of-time', 'Dawn of time', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-dawn-of-time.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2070, 'allwayspin-AWS_12', 'CANDY PLANET', 'slot', 'AllwaySpin', 'ms', 'mb', '', 'img/mrslotty/allwayspin-AWS_12.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2071, 'tomhorn-king-arthur', 'King Arthur', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-king-arthur.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2072, 'truelab-pirate-bay', 'Pirate Bay', 'slot', 'TrueLab', 'ms', 'wb', '', 'img/mrslotty/truelab-pirate-bay.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2073, 'bgaming-Minesweeper', 'Minesweeper', 'slot', 'BGaming', 'ms', 'wb', '', 'img/mrslotty/bgaming-Minesweeper.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2074, 'wazdan-326', 'Welcome To Hell 81', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-326.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2075, 'mascot-bamboo_bear', 'Bamboo Bear', 'slot', 'Mascot', 'ms', 'wb', '', 'img/mrslotty/mascot-bamboo_bear.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2076, 'fazi-starrunner', 'Starlight', 'slot', 'Fazi', 'ms', 'wb', '', 'img/mrslotty/fazi-starrunner.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2077, 'velagaming-slot-angkor', 'PrincessOfAngkorWat', 'arcade', 'Velagaming', 'ms', 'wb', '', 'img/mrslotty/velagaming-slot-angkor.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2078, 'tpg-selfie', 'Selfie', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-selfie.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2079, 'slotexchange-dawn-of-time', 'Dawn of time', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-dawn-of-time.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2080, 'allwayspin-AWS_12', 'CANDY PLANET', 'slot', 'AllwaySpin', 'ms', 'wb', '', 'img/mrslotty/allwayspin-AWS_12.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2081, 'tomhorn-king-arthur', 'King Arthur', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-king-arthur.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2082, 'truelab-tonys-reel', 'Tonys Reel', 'slot', 'TrueLab', 'ms', 'mb', '', 'img/mrslotty/truelab-tonys-reel.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2083, 'bgaming-MultihandBlackjack', 'Multihand Blackjack', 'slot', 'BGaming', 'ms', 'mb', 'JUME', 'img/mrslotty/bgaming-MultihandBlackjack.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2084, 'wazdan-327', 'Jack on Hold', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-327.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2085, 'mascot-the_tomb_det', 'The Tomb: Dragon Emperor\'s Treasure', 'slot', 'Mascot', 'ms', 'mb', '', 'img/mrslotty/mascot-the_tomb_det.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2086, 'fazi-templarsquest', 'Templars Quest', 'slot', 'Fazi', 'ms', 'mb', '', 'img/mrslotty/fazi-templarsquest.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2087, 'velagaming-slot-pharaoh', 'LegendOfPharaoh', 'arcade', 'Velagaming', 'ms', 'mb', '', 'img/mrslotty/velagaming-slot-pharaoh.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2088, 'tpg-journey-to-west', 'Journey To West', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-journey-to-west.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2089, 'slotexchange-dinoland', 'Dinoland', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-dinoland.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2090, 'allwayspin-AWS_13', 'YEEEEE YA', 'slot', 'AllwaySpin', 'ms', 'mb', '', 'img/mrslotty/allwayspin-AWS_13.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2091, 'tomhorn-double-flash', 'Double Flash', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-double-flash.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2092, 'truelab-tonys-reel', 'Tonys Reel', 'slot', 'TrueLab', 'ms', 'wb', '', 'img/mrslotty/truelab-tonys-reel.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2093, 'bgaming-MultihandBlackjack', 'Multihand Blackjack', 'slot', 'BGaming', 'ms', 'wb', 'JUME', 'img/mrslotty/bgaming-MultihandBlackjack.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2094, 'wazdan-327', 'Jack on Hold', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-327.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2095, 'mascot-the_tomb_det', 'The Tomb: Dragon Emperor\'s Treasure', 'slot', 'Mascot', 'ms', 'wb', '', 'img/mrslotty/mascot-the_tomb_det.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2096, 'fazi-templarsquest', 'Templars Quest', 'slot', 'Fazi', 'ms', 'wb', '', 'img/mrslotty/fazi-templarsquest.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2097, 'velagaming-slot-pharaoh', 'LegendOfPharaoh', 'arcade', 'Velagaming', 'ms', 'wb', '', 'img/mrslotty/velagaming-slot-pharaoh.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2098, 'tpg-journey-to-west', 'Journey To West', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-journey-to-west.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2099, 'slotexchange-dinoland', 'Dinoland', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-dinoland.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2100, 'allwayspin-AWS_13', 'YEEEEE YA', 'slot', 'AllwaySpin', 'ms', 'wb', '', 'img/mrslotty/allwayspin-AWS_13.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2101, 'tomhorn-double-flash', 'Double Flash', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-double-flash.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2102, 'platipus-jackpotlab', 'Jackpot Lab', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-jackpotlab.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2103, 'bgaming-OasisPoker', 'Oasis Poker', 'slot', 'BGaming', 'ms', 'mb', 'JUME', 'img/mrslotty/bgaming-OasisPoker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2104, 'wazdan-332', 'Jack\'s Ride', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-332.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2105, 'mascot-blackjack_mg', 'Black Jack', 'slot', 'Mascot', 'ms', 'mb', 'JUME', 'img/mrslotty/mascot-blackjack_mg.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2106, 'fazi-triplecrown', 'Triple Crown', 'slot', 'Fazi', 'ms', 'mb', 'RULF', 'img/mrslotty/fazi-triplecrown.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2107, 'velagaming-slot-northsnow', 'NorthernSnow', 'arcade', 'Velagaming', 'ms', 'mb', '', 'img/mrslotty/velagaming-slot-northsnow.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2108, 'tpg-dessert', 'Dessert', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-dessert.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2109, 'slotexchange-antarctida', 'Antarctida', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-antarctida.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2110, 'allwayspin-AWS_14', 'HANABI', 'slot', 'AllwaySpin', 'ms', 'mb', '', 'img/mrslotty/allwayspin-AWS_14.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2111, 'tomhorn-hammer-of-thor', 'Hammer of Thor', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-hammer-of-thor.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2112, 'platipus-jackpotlab', 'Jackpot Lab', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-jackpotlab.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2113, 'bgaming-OasisPoker', 'Oasis Poker', 'slot', 'BGaming', 'ms', 'wb', 'JUME', 'img/mrslotty/bgaming-OasisPoker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2114, 'wazdan-332', 'Jack\'s Ride', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-332.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2115, 'mascot-blackjack_mg', 'Black Jack', 'slot', 'Mascot', 'ms', 'wb', 'JUME', 'img/mrslotty/mascot-blackjack_mg.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2116, 'fazi-triplecrown', 'Triple Crown', 'slot', 'Fazi', 'ms', 'wb', 'RULF', 'img/mrslotty/fazi-triplecrown.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2117, 'velagaming-slot-northsnow', 'NorthernSnow', 'arcade', 'Velagaming', 'ms', 'wb', '', 'img/mrslotty/velagaming-slot-northsnow.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2118, 'tpg-dessert', 'Dessert', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-dessert.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2119, 'slotexchange-antarctida', 'Antarctida', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-antarctida.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2120, 'allwayspin-AWS_14', 'HANABI', 'slot', 'AllwaySpin', 'ms', 'wb', '', 'img/mrslotty/allwayspin-AWS_14.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2121, 'tomhorn-hammer-of-thor', 'Hammer of Thor', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-hammer-of-thor.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2122, 'platipus-azteccoins', 'Aztec Coins', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-azteccoins.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2123, 'bgaming-PlatinumLightning', 'Platinum Lightning', 'slot', 'BGaming', 'ms', 'mb', '', 'img/mrslotty/bgaming-PlatinumLightning.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2124, 'wazdan-333', 'Mayan Ritual', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-333.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2125, 'mascot-baccarat_mg', 'Baccarat', 'slot', 'Mascot', 'ms', 'mb', 'JUME', 'img/mrslotty/mascot-baccarat_mg.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2126, 'fazi-triplehot', 'Triple Hot', 'slot', 'Fazi', 'ms', 'mb', '', 'img/mrslotty/fazi-triplehot.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2127, 'velagaming-slot-beast', 'FortuneBeast', 'arcade', 'Velagaming', 'ms', 'mb', '', 'img/mrslotty/velagaming-slot-beast.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2128, 'tpg-zombie', 'Zombie', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-zombie.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2129, 'slotexchange-jolly-pirates', 'Jolly pirates', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-jolly-pirates.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2130, 'allwayspin-AWS_15', 'TIKI JENGA', 'slot', 'AllwaySpin', 'ms', 'mb', '', 'img/mrslotty/allwayspin-AWS_15.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2131, 'tomhorn-the-chicago', 'THE Chicago', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-the-chicago.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2132, 'platipus-azteccoins', 'Aztec Coins', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-azteccoins.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2133, 'bgaming-PlatinumLightning', 'Platinum Lightning', 'slot', 'BGaming', 'ms', 'wb', '', 'img/mrslotty/bgaming-PlatinumLightning.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2134, 'wazdan-333', 'Mayan Ritual', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-333.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2135, 'mascot-baccarat_mg', 'Baccarat', 'slot', 'Mascot', 'ms', 'wb', 'JUME', 'img/mrslotty/mascot-baccarat_mg.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2136, 'fazi-triplehot', 'Triple Hot', 'slot', 'Fazi', 'ms', 'wb', '', 'img/mrslotty/fazi-triplehot.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2137, 'velagaming-slot-beast', 'FortuneBeast', 'arcade', 'Velagaming', 'ms', 'wb', '', 'img/mrslotty/velagaming-slot-beast.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2138, 'tpg-zombie', 'Zombie', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-zombie.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2139, 'slotexchange-jolly-pirates', 'Jolly pirates', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-jolly-pirates.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2140, 'allwayspin-AWS_15', 'TIKI JENGA', 'slot', 'AllwaySpin', 'ms', 'wb', '', 'img/mrslotty/allwayspin-AWS_15.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2141, 'tomhorn-the-chicago', 'THE Chicago', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-the-chicago.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2142, 'platipus-chinesetigers', 'Chinese Tigers', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-chinesetigers.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2143, 'bgaming-PlatinumLightningDeluxe', 'Platinum Lightning Deluxe', 'slot', 'BGaming', 'ms', 'mb', '', 'img/mrslotty/bgaming-PlatinumLightningDeluxe.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2144, 'wazdan-336', 'Power of Gods: the Pantheon', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-336.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2145, 'mascot-legioner', 'Legioner', 'slot', 'Mascot', 'ms', 'mb', '', 'img/mrslotty/mascot-legioner.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2146, 'fazi-tropicalhot', 'Tropical Hot', 'slot', 'Fazi', 'ms', 'mb', '', 'img/mrslotty/fazi-tropicalhot.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2147, 'velagaming-slot-1024', 'FortuneBowl V2', 'arcade', 'Velagaming', 'ms', 'mb', '', 'img/mrslotty/velagaming-slot-1024.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2148, 'tpg-face', 'Face', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-face.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2149, 'slotexchange-amazing-illusions', 'Amazing illusions', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-amazing-illusions.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2150, 'allwayspin-AWS_16', 'MONSTER 7', 'slot', 'AllwaySpin', 'ms', 'mb', '', 'img/mrslotty/allwayspin-AWS_16.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2151, 'tomhorn-wheel-of-luck', 'Wheel Of Luck', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-wheel-of-luck.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2152, 'platipus-chinesetigers', 'Chinese Tigers', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-chinesetigers.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2153, 'bgaming-PlatinumLightningDeluxe', 'Platinum Lightning Deluxe', 'slot', 'BGaming', 'ms', 'wb', '', 'img/mrslotty/bgaming-PlatinumLightningDeluxe.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2154, 'wazdan-336', 'Power of Gods: the Pantheon', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-336.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2155, 'mascot-legioner', 'Legioner', 'slot', 'Mascot', 'ms', 'wb', '', 'img/mrslotty/mascot-legioner.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2156, 'fazi-tropicalhot', 'Tropical Hot', 'slot', 'Fazi', 'ms', 'wb', '', 'img/mrslotty/fazi-tropicalhot.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2157, 'velagaming-slot-1024', 'FortuneBowl V2', 'arcade', 'Velagaming', 'ms', 'wb', '', 'img/mrslotty/velagaming-slot-1024.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2158, 'tpg-face', 'Face', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-face.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2159, 'slotexchange-amazing-illusions', 'Amazing illusions', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-amazing-illusions.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2160, 'allwayspin-AWS_16', 'MONSTER 7', 'slot', 'AllwaySpin', 'ms', 'wb', '', 'img/mrslotty/allwayspin-AWS_16.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2161, 'tomhorn-wheel-of-luck', 'Wheel Of Luck', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-wheel-of-luck.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2162, 'platipus-rhinomania', 'Rhino Mania', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-rhinomania.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2163, 'bgaming-Plinko', 'Plinko', 'slot', 'BGaming', 'ms', 'mb', '', 'img/mrslotty/bgaming-Plinko.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2164, 'wazdan-337', 'Lucky Fish', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-337.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2165, 'mascot-wild_spirit', 'Wild Spirit', 'slot', 'Mascot', 'ms', 'mb', '', 'img/mrslotty/mascot-wild_spirit.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2166, 'fazi-turbohot40', 'Turbo Hot 40', 'slot', 'Fazi', 'ms', 'mb', '', 'img/mrslotty/fazi-turbohot40.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2167, 'velagaming-slot-fortune-v2', 'FortuneWheel V2', 'arcade', 'Velagaming', 'ms', 'mb', '', 'img/mrslotty/velagaming-slot-fortune-v2.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2168, 'tpg-soccer', 'Soccer', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-soccer.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2169, 'slotexchange-blood-moon', 'Blood moon', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-blood-moon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2170, 'allwayspin-AWS_17', 'LORD OF THE DEAD', 'slot', 'AllwaySpin', 'ms', 'mb', '', 'img/mrslotty/allwayspin-AWS_17.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2171, 'tomhorn-7-mirrors', '7 Mirrors', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-7-mirrors.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2172, 'platipus-rhinomania', 'Rhino Mania', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-rhinomania.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2173, 'bgaming-Plinko', 'Plinko', 'slot', 'BGaming', 'ms', 'wb', '', 'img/mrslotty/bgaming-Plinko.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2174, 'wazdan-337', 'Lucky Fish', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-337.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2175, 'mascot-wild_spirit', 'Wild Spirit', 'slot', 'Mascot', 'ms', 'wb', '', 'img/mrslotty/mascot-wild_spirit.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2176, 'fazi-turbohot40', 'Turbo Hot 40', 'slot', 'Fazi', 'ms', 'wb', '', 'img/mrslotty/fazi-turbohot40.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2177, 'velagaming-slot-fortune-v2', 'FortuneWheel V2', 'arcade', 'Velagaming', 'ms', 'wb', '', 'img/mrslotty/velagaming-slot-fortune-v2.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2178, 'tpg-soccer', 'Soccer', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-soccer.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2179, 'slotexchange-blood-moon', 'Blood moon', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-blood-moon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2180, 'allwayspin-AWS_17', 'LORD OF THE DEAD', 'slot', 'AllwaySpin', 'ms', 'wb', '', 'img/mrslotty/allwayspin-AWS_17.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2181, 'tomhorn-7-mirrors', '7 Mirrors', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-7-mirrors.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2182, 'platipus-webbyheroes', 'Webby Heroes', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-webbyheroes.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2183, 'bgaming-Pontoon', 'Pontoon', 'slot', 'BGaming', 'ms', 'mb', 'JUME', 'img/mrslotty/bgaming-Pontoon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2184, 'wazdan-338', 'Spectrum', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-338.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2185, 'mascot-fruit_vegas', 'Fruit Vegas', 'slot', 'Mascot', 'ms', 'mb', '', 'img/mrslotty/mascot-fruit_vegas.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2186, 'fazi-twinklinghot40', 'Twinkling Hot 40', 'slot', 'Fazi', 'ms', 'mb', '', 'img/mrslotty/fazi-twinklinghot40.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2187, 'velagaming-slot-beast-v2', 'FortuneBeast V2', 'arcade', 'Velagaming', 'ms', 'mb', '', 'img/mrslotty/velagaming-slot-beast-v2.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2188, 'hub88-torch-of-fire', 'Torch Of Fire', 'slot', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-torch-of-fire.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2189, 'slotexchange-hot-pepper', 'Hot pepper', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-hot-pepper.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2190, 'allwayspin-AWS_18', 'MAHJONG', 'slot', 'AllwaySpin', 'ms', 'mb', '', 'img/mrslotty/allwayspin-AWS_18.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2191, 'tomhorn-scratch-card', 'Scratch Card', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-scratch-card.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2192, 'platipus-webbyheroes', 'Webby Heroes', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-webbyheroes.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2193, 'bgaming-Pontoon', 'Pontoon', 'slot', 'BGaming', 'ms', 'wb', 'JUME', 'img/mrslotty/bgaming-Pontoon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2194, 'wazdan-338', 'Spectrum', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-338.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2195, 'mascot-fruit_vegas', 'Fruit Vegas', 'slot', 'Mascot', 'ms', 'wb', '', 'img/mrslotty/mascot-fruit_vegas.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2196, 'fazi-twinklinghot40', 'Twinkling Hot 40', 'slot', 'Fazi', 'ms', 'wb', '', 'img/mrslotty/fazi-twinklinghot40.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2197, 'velagaming-slot-beast-v2', 'FortuneBeast V2', 'arcade', 'Velagaming', 'ms', 'wb', '', 'img/mrslotty/velagaming-slot-beast-v2.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2198, 'hub88-torch-of-fire', 'Torch Of Fire', 'slot', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-torch-of-fire.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2199, 'slotexchange-hot-pepper', 'Hot pepper', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-hot-pepper.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2200, 'allwayspin-AWS_18', 'MAHJONG', 'slot', 'AllwaySpin', 'ms', 'wb', '', 'img/mrslotty/allwayspin-AWS_18.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2201, 'tomhorn-scratch-card', 'Scratch Card', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-scratch-card.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2202, 'platipus-luckycat', 'Lucky Cat', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-luckycat.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2203, 'bgaming-PrincessOfSky', 'Princess of Sky', 'slot', 'BGaming', 'ms', 'mb', '', 'img/mrslotty/bgaming-PrincessOfSky.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2204, 'wazdan-339', 'Space Spins', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-339.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2205, 'mascot-gryphons_castle', 'Gryphons Castle', 'slot', 'Mascot', 'ms', 'mb', '', 'img/mrslotty/mascot-gryphons_castle.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2206, 'fazi-twinklinghot5', 'Twinkling Hot 5', 'slot', 'Fazi', 'ms', 'mb', '', 'img/mrslotty/fazi-twinklinghot5.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2207, 'velagaming-soccer', 'SoccerFever', 'arcade', 'Velagaming', 'ms', 'mb', '', 'img/mrslotty/velagaming-soccer.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2208, 'hub88-hidden-kingdom', 'Hidden Kingdom', 'slot', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-hidden-kingdom.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2209, 'slotexchange-cheeky-cops', 'Cheeky cops', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-cheeky-cops.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2210, 'allwayspin-AWS_19', 'GACHA BALL', 'slot', 'AllwaySpin', 'ms', 'mb', '', 'img/mrslotty/allwayspin-AWS_19.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2211, 'tomhorn-keno', 'Keno', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-keno.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2212, 'platipus-luckycat', 'Lucky Cat', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-luckycat.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2213, 'bgaming-PrincessOfSky', 'Princess of Sky', 'slot', 'BGaming', 'ms', 'wb', '', 'img/mrslotty/bgaming-PrincessOfSky.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2214, 'wazdan-339', 'Space Spins', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-339.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2215, 'mascot-gryphons_castle', 'Gryphons Castle', 'slot', 'Mascot', 'ms', 'wb', '', 'img/mrslotty/mascot-gryphons_castle.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2216, 'fazi-twinklinghot5', 'Twinkling Hot 5', 'slot', 'Fazi', 'ms', 'wb', '', 'img/mrslotty/fazi-twinklinghot5.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2217, 'velagaming-soccer', 'SoccerFever', 'arcade', 'Velagaming', 'ms', 'wb', '', 'img/mrslotty/velagaming-soccer.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2218, 'hub88-hidden-kingdom', 'Hidden Kingdom', 'slot', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-hidden-kingdom.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2219, 'slotexchange-cheeky-cops', 'Cheeky cops', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-cheeky-cops.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2220, 'allwayspin-AWS_19', 'GACHA BALL', 'slot', 'AllwaySpin', 'ms', 'wb', '', 'img/mrslotty/allwayspin-AWS_19.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2221, 'tomhorn-keno', 'Keno', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-keno.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2222, 'platipus-pharaohsempire', 'Pharaoh\'s Empire', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-pharaohsempire.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2223, 'bgaming-PrincessRoyal', 'Princess Royal', 'slot', 'BGaming', 'ms', 'mb', '', 'img/mrslotty/bgaming-PrincessRoyal.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2224, 'wazdan-341', 'Magic Fruits Deluxe', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-341.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2225, 'mascot-hellsing', 'Hell\'Sing', 'slot', 'Mascot', 'ms', 'mb', '', 'img/mrslotty/mascot-hellsing.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2226, 'fazi-vikinggold', 'Viking Gold', 'slot', 'Fazi', 'ms', 'mb', '', 'img/mrslotty/fazi-vikinggold.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2227, 'velagaming-basketball', 'BasketballFever', 'arcade', 'Velagaming', 'ms', 'mb', '', 'img/mrslotty/velagaming-basketball.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2228, 'hub88-magic-forest', 'Magic Forest', 'slot', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-magic-forest.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2229, 'slotexchange-oriental-tales', 'Oriental tales', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-oriental-tales.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2230, 'allwayspin-AWS_20', 'OH!PARTY!', 'slot', 'AllwaySpin', 'ms', 'mb', '', 'img/mrslotty/allwayspin-AWS_20.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2231, 'tomhorn-ice-agedeleted_1', 'Ice Agedeleted_1', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-ice-agedeleted_1.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2232, 'platipus-pharaohsempire', 'Pharaoh\'s Empire', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-pharaohsempire.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2233, 'bgaming-PrincessRoyal', 'Princess Royal', 'slot', 'BGaming', 'ms', 'wb', '', 'img/mrslotty/bgaming-PrincessRoyal.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2234, 'wazdan-341', 'Magic Fruits Deluxe', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-341.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2235, 'mascot-hellsing', 'Hell\'Sing', 'slot', 'Mascot', 'ms', 'wb', '', 'img/mrslotty/mascot-hellsing.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2236, 'fazi-vikinggold', 'Viking Gold', 'slot', 'Fazi', 'ms', 'wb', '', 'img/mrslotty/fazi-vikinggold.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2237, 'velagaming-basketball', 'BasketballFever', 'arcade', 'Velagaming', 'ms', 'wb', '', 'img/mrslotty/velagaming-basketball.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2238, 'hub88-magic-forest', 'Magic Forest', 'slot', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-magic-forest.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2239, 'slotexchange-oriental-tales', 'Oriental tales', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-oriental-tales.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2240, 'allwayspin-AWS_20', 'OH!PARTY!', 'slot', 'AllwaySpin', 'ms', 'wb', '', 'img/mrslotty/allwayspin-AWS_20.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2241, 'tomhorn-ice-agedeleted_1', 'Ice Agedeleted_1', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-ice-agedeleted_1.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2242, 'platipus-bisontrail', 'Bison Trail', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-bisontrail.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2243, 'bgaming-RocketDice', 'Rocket Dice', 'slot', 'BGaming', 'ms', 'mb', 'JUME', 'img/mrslotty/bgaming-RocketDice.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2244, 'wazdan-342', 'Wild Guns', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-342.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2245, 'mascot-bennys_the_biggest_game', 'Benny\'s the Biggest game', 'slot', 'Mascot', 'ms', 'mb', '', 'img/mrslotty/mascot-bennys_the_biggest_game.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2246, 'fazi-viproulette', 'VIP Roulette', 'slot', 'Fazi', 'ms', 'mb', 'RULF', 'img/mrslotty/fazi-viproulette.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2247, 'velagaming-baseball', 'BaseballFever', 'arcade', 'Velagaming', 'ms', 'mb', '', 'img/mrslotty/velagaming-baseball.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2248, 'hub88-heroes-empire', 'Heroes Empire', 'slot', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-heroes-empire.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2249, 'slotexchange-beach-season', 'Beach season', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-beach-season.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2250, 'allwayspin-AWS_21', 'MONSTER FOOTBALL', 'slot', 'AllwaySpin', 'ms', 'mb', '', 'img/mrslotty/allwayspin-AWS_21.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2251, 'tomhorn-casino-royale', 'Casino Royale', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-casino-royale.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2252, 'platipus-bisontrail', 'Bison Trail', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-bisontrail.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2253, 'bgaming-RocketDice', 'Rocket Dice', 'slot', 'BGaming', 'ms', 'wb', 'JUME', 'img/mrslotty/bgaming-RocketDice.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2254, 'wazdan-342', 'Wild Guns', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-342.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2255, 'mascot-bennys_the_biggest_game', 'Benny\'s the Biggest game', 'slot', 'Mascot', 'ms', 'wb', '', 'img/mrslotty/mascot-bennys_the_biggest_game.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2256, 'fazi-viproulette', 'VIP Roulette', 'slot', 'Fazi', 'ms', 'wb', 'RULF', 'img/mrslotty/fazi-viproulette.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2257, 'velagaming-baseball', 'BaseballFever', 'arcade', 'Velagaming', 'ms', 'wb', '', 'img/mrslotty/velagaming-baseball.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2258, 'hub88-heroes-empire', 'Heroes Empire', 'slot', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-heroes-empire.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2259, 'slotexchange-beach-season', 'Beach season', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-beach-season.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2260, 'allwayspin-AWS_21', 'MONSTER FOOTBALL', 'slot', 'AllwaySpin', 'ms', 'wb', '', 'img/mrslotty/allwayspin-AWS_21.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2261, 'tomhorn-casino-royale', 'Casino Royale', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-casino-royale.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2262, 'platipus-jadevalley', 'Jade Valley', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-jadevalley.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2263, 'bgaming-ScratchDice', 'Scratch Dice', 'slot', 'BGaming', 'ms', 'mb', '', 'img/mrslotty/bgaming-ScratchDice.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2264, 'wazdan-343', 'Mystery Jack Deluxe', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-343.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2265, 'mascot-holdem_mg', 'Casino Hold\'em', 'slot', 'Mascot', 'ms', 'mb', 'JUME', 'img/mrslotty/mascot-holdem_mg.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2266, 'fazi-wildwest', 'Wild West', 'slot', 'Fazi', 'ms', 'mb', '', 'img/mrslotty/fazi-wildwest.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2267, 'velagaming-penalty-king', 'PenaltyKing', 'arcade', 'Velagaming', 'ms', 'mb', '', 'img/mrslotty/velagaming-penalty-king.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2268, 'hub88-enchanted-cash', 'Enchanted Cash', 'slot', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-enchanted-cash.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2269, 'slotexchange-aztec-mystery', 'Aztec mystery', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-aztec-mystery.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2270, 'allwayspin-AWS_22', 'MEDUSA', 'slot', 'AllwaySpin', 'ms', 'mb', '', 'img/mrslotty/allwayspin-AWS_22.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2271, 'tomhorn-oxodeleted_1', 'OXOdeleted_1', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-oxodeleted_1.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2272, 'platipus-jadevalley', 'Jade Valley', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-jadevalley.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2273, 'bgaming-ScratchDice', 'Scratch Dice', 'slot', 'BGaming', 'ms', 'wb', '', 'img/mrslotty/bgaming-ScratchDice.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2274, 'wazdan-343', 'Mystery Jack Deluxe', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-343.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2275, 'mascot-holdem_mg', 'Casino Hold\'em', 'slot', 'Mascot', 'ms', 'wb', 'JUME', 'img/mrslotty/mascot-holdem_mg.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2276, 'fazi-wildwest', 'Wild West', 'slot', 'Fazi', 'ms', 'wb', '', 'img/mrslotty/fazi-wildwest.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2277, 'velagaming-penalty-king', 'PenaltyKing', 'arcade', 'Velagaming', 'ms', 'wb', '', 'img/mrslotty/velagaming-penalty-king.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2278, 'hub88-enchanted-cash', 'Enchanted Cash', 'slot', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-enchanted-cash.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2279, 'slotexchange-aztec-mystery', 'Aztec mystery', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-aztec-mystery.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2280, 'allwayspin-AWS_22', 'MEDUSA', 'slot', 'AllwaySpin', 'ms', 'wb', '', 'img/mrslotty/allwayspin-AWS_22.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2281, 'tomhorn-oxodeleted_1', 'OXOdeleted_1', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-oxodeleted_1.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2282, 'platipus-neonclassic', 'Neon Classic', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-neonclassic.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2283, 'bgaming-ScrollOfAdventure', 'Scroll of Adventure', 'slot', 'BGaming', 'ms', 'mb', '', 'img/mrslotty/bgaming-ScrollOfAdventure.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2284, 'wazdan-344', 'Magic Target Deluxe', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-344.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2285, 'evoplay-egypt-gods', 'Egypt Gods', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-egypt-gods.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2286, 'fazi-wizard', 'Wizard', 'slot', 'Fazi', 'ms', 'mb', '', 'img/mrslotty/fazi-wizard.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2287, 'velagaming-roulette-02', 'Virtual Cricket Roulette', 'arcade', 'Velagaming', 'ms', 'mb', '', 'img/mrslotty/velagaming-roulette-02.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2288, 'hub88-crazy-lab', 'Crazy Lab', 'slot', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-crazy-lab.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2289, 'slotexchange-bug-world', 'Bug world', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-bug-world.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2290, 'allwayspin-AWS_23', 'SNAIL RACING', 'slot', 'AllwaySpin', 'ms', 'mb', '', 'img/mrslotty/allwayspin-AWS_23.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2291, 'tomhorn-jingle-bells', 'Jingle Bells', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-jingle-bells.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2292, 'platipus-neonclassic', 'Neon Classic', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-neonclassic.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2293, 'bgaming-ScrollOfAdventure', 'Scroll of Adventure', 'slot', 'BGaming', 'ms', 'wb', '', 'img/mrslotty/bgaming-ScrollOfAdventure.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2294, 'wazdan-344', 'Magic Target Deluxe', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-344.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2295, 'evoplay-egypt-gods', 'Egypt Gods', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-egypt-gods.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2296, 'fazi-wizard', 'Wizard', 'slot', 'Fazi', 'ms', 'wb', '', 'img/mrslotty/fazi-wizard.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2297, 'velagaming-roulette-02', 'Virtual Cricket Roulette', 'arcade', 'Velagaming', 'ms', 'wb', '', 'img/mrslotty/velagaming-roulette-02.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2298, 'hub88-crazy-lab', 'Crazy Lab', 'slot', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-crazy-lab.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2299, 'slotexchange-bug-world', 'Bug world', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-bug-world.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2300, 'allwayspin-AWS_23', 'SNAIL RACING', 'slot', 'AllwaySpin', 'ms', 'wb', '', 'img/mrslotty/allwayspin-AWS_23.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2301, 'tomhorn-jingle-bells', 'Jingle Bells', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-jingle-bells.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2302, 'platipus-powerofposeidon', 'Power of Poseidon', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-powerofposeidon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2303, 'bgaming-SicBo', 'Sic Bo', 'slot', 'BGaming', 'ms', 'mb', 'JUME', 'img/mrslotty/bgaming-SicBo.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2304, 'wazdan-345', 'BARs&7s', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-345.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2305, 'evoplay-lucky-mahjong-box', 'Lucky Mahjong Box', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-lucky-mahjong-box.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2306, 'redrake-blackjack-vip', 'BLACKJACK VIP', 'slot', 'Red Rake', 'ms', 'mb', 'JUME', 'img/mrslotty/redrake-blackjack-vip.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2307, 'velagaming-baccarat', 'BaccaratDeluxe', 'arcade', 'Velagaming', 'ms', 'mb', '', 'img/mrslotty/velagaming-baccarat.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2308, 'hub88-hungry-chef', 'Hungry Chef', 'slot', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-hungry-chef.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2309, 'slotexchange-postapokalipsis', 'Postapokalipsis', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-postapokalipsis.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2310, 'allwayspin-AWS_24', 'NEON CAT', 'slot', 'AllwaySpin', 'ms', 'mb', '', 'img/mrslotty/allwayspin-AWS_24.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2311, 'evoplay-fluffy-rangers', 'Fluffy Rangers', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-fluffy-rangers.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2312, 'platipus-powerofposeidon', 'Power of Poseidon', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-powerofposeidon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2313, 'bgaming-SicBo', 'Sic Bo', 'slot', 'BGaming', 'ms', 'wb', 'JUME', 'img/mrslotty/bgaming-SicBo.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2314, 'wazdan-345', 'BARs&7s', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-345.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2315, 'evoplay-lucky-mahjong-box', 'Lucky Mahjong Box', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-lucky-mahjong-box.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2316, 'redrake-blackjack-vip', 'BLACKJACK VIP', 'slot', 'Red Rake', 'ms', 'wb', 'JUME', 'img/mrslotty/redrake-blackjack-vip.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2317, 'velagaming-baccarat', 'BaccaratDeluxe', 'arcade', 'Velagaming', 'ms', 'wb', '', 'img/mrslotty/velagaming-baccarat.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2318, 'hub88-hungry-chef', 'Hungry Chef', 'slot', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-hungry-chef.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2319, 'slotexchange-postapokalipsis', 'Postapokalipsis', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-postapokalipsis.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2320, 'allwayspin-AWS_24', 'NEON CAT', 'slot', 'AllwaySpin', 'ms', 'wb', '', 'img/mrslotty/allwayspin-AWS_24.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2321, 'evoplay-fluffy-rangers', 'Fluffy Rangers', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-fluffy-rangers.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2322, 'platipus-greatocean', 'Great Ocean', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-greatocean.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2323, 'bgaming-SicBoMacau', 'Sic Bo Macau', 'slot', 'BGaming', 'ms', 'mb', 'JUME', 'img/mrslotty/bgaming-SicBoMacau.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2324, 'wazdan-346', 'Fenix Play Deluxe', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-346.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2325, 'evoplay-basketball', 'Basketball', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-basketball.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2326, 'redrake-blackjack-fast', 'BLACKJACK FAST', 'slot', 'Red Rake', 'ms', 'mb', 'JUME', 'img/mrslotty/redrake-blackjack-fast.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2327, 'velagaming-dragon-tiger', 'DragonTiger', 'arcade', 'Velagaming', 'ms', 'mb', '', 'img/mrslotty/velagaming-dragon-tiger.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2328, 'hub88-fright-night', 'Fright Night', 'slot', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-fright-night.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2329, 'slotexchange-mysterious-stranger', 'Mysterious stranger', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-mysterious-stranger.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2330, 'allwayspin-AWS_25', 'CHARMING FIVE', 'slot', 'AllwaySpin', 'ms', 'mb', '', 'img/mrslotty/allwayspin-AWS_25.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2331, 'evoplay-midnight-show', 'Midnight Show', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-midnight-show.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2332, 'platipus-greatocean', 'Great Ocean', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-greatocean.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2333, 'bgaming-SicBoMacau', 'Sic Bo Macau', 'slot', 'BGaming', 'ms', 'wb', 'JUME', 'img/mrslotty/bgaming-SicBoMacau.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2334, 'wazdan-346', 'Fenix Play Deluxe', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-346.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2335, 'evoplay-basketball', 'Basketball', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-basketball.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2336, 'redrake-blackjack-fast', 'BLACKJACK FAST', 'slot', 'Red Rake', 'ms', 'wb', 'JUME', 'img/mrslotty/redrake-blackjack-fast.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2337, 'velagaming-dragon-tiger', 'DragonTiger', 'arcade', 'Velagaming', 'ms', 'wb', '', 'img/mrslotty/velagaming-dragon-tiger.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2338, 'hub88-fright-night', 'Fright Night', 'slot', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-fright-night.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2339, 'slotexchange-mysterious-stranger', 'Mysterious stranger', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-mysterious-stranger.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2340, 'allwayspin-AWS_25', 'CHARMING FIVE', 'slot', 'AllwaySpin', 'ms', 'wb', '', 'img/mrslotty/allwayspin-AWS_25.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2341, 'evoplay-midnight-show', 'Midnight Show', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-midnight-show.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2342, 'platipus-megadrago', 'Mega Drago', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-megadrago.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2343, 'bgaming-SlotomonGo', 'Slotomon Go', 'slot', 'BGaming', 'ms', 'mb', '', 'img/mrslotty/bgaming-SlotomonGo.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2344, 'wazdan-347', 'Fenix Play 27 Deluxe', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-347.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2345, 'evoplay-talismans-of-fortune', 'Talismans of Fortune', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-talismans-of-fortune.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2346, 'redrake-blackjack-switch', 'BLACKJACK SWITCH', 'slot', 'Red Rake', 'ms', 'mb', 'JUME', 'img/mrslotty/redrake-blackjack-switch.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2347, 'velagaming-blackjack-01', 'AmericanBlackjack', 'arcade', 'Velagaming', 'ms', 'mb', '', 'img/mrslotty/velagaming-blackjack-01.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2348, 'hub88-madame-fortune', 'Madame Fortune', 'slot', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-madame-fortune.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2349, 'slotexchange-mafia-spin', 'Mafia spin', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-mafia-spin.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2350, 'allwayspin-AWS_26', 'UNDERGROUND THIEVES', 'slot', 'AllwaySpin', 'ms', 'mb', '', 'img/mrslotty/allwayspin-AWS_26.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2351, 'alg-roulette', 'Roulette', 'slot_live', 'Absolute Live Gaming', 'ms', 'mb', '', 'img/mrslotty/alg-roulette.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2352, 'platipus-megadrago', 'Mega Drago', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-megadrago.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2353, 'bgaming-SlotomonGo', 'Slotomon Go', 'slot', 'BGaming', 'ms', 'wb', '', 'img/mrslotty/bgaming-SlotomonGo.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2354, 'wazdan-347', 'Fenix Play 27 Deluxe', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-347.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2355, 'evoplay-talismans-of-fortune', 'Talismans of Fortune', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-talismans-of-fortune.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2356, 'redrake-blackjack-switch', 'BLACKJACK SWITCH', 'slot', 'Red Rake', 'ms', 'wb', 'JUME', 'img/mrslotty/redrake-blackjack-switch.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2357, 'velagaming-blackjack-01', 'AmericanBlackjack', 'arcade', 'Velagaming', 'ms', 'wb', '', 'img/mrslotty/velagaming-blackjack-01.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2358, 'hub88-madame-fortune', 'Madame Fortune', 'slot', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-madame-fortune.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2359, 'slotexchange-mafia-spin', 'Mafia spin', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-mafia-spin.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2360, 'allwayspin-AWS_26', 'UNDERGROUND THIEVES', 'slot', 'AllwaySpin', 'ms', 'wb', '', 'img/mrslotty/allwayspin-AWS_26.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2361, 'alg-roulette', 'Roulette', 'slot_live', 'Absolute Live Gaming', 'ms', 'wb', '', 'img/mrslotty/alg-roulette.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2362, 'platipus-legendofatlantis', 'Legend of Atlantis', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-legendofatlantis.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2363, 'bgaming-SpinAndSpell', 'Spin And Spell', 'slot', 'BGaming', 'ms', 'mb', '', 'img/mrslotty/bgaming-SpinAndSpell.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2364, 'wazdan-348', 'Magic Fruits 4 Deluxe', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-348.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2365, 'evoplay-the-great-wall-treasure', 'The Great Wall Treasure', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-the-great-wall-treasure.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2366, 'redrake-blackjack-double-exposure', 'BLACKJACK DOUBLE EXPOSURE', 'slot', 'Red Rake', 'ms', 'mb', 'JUME', 'img/mrslotty/redrake-blackjack-double-exposure.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2367, 'velagaming-roulette-euro', 'European Roulette', 'arcade', 'Velagaming', 'ms', 'mb', '', 'img/mrslotty/velagaming-roulette-euro.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2368, 'hub88-china-charms', 'China Charms', 'slot', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-china-charms.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2369, 'slotexchange-greedy-leprechauns', 'Greedy leprechauns', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-greedy-leprechauns.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2370, 'hub88-spoils-of-war', 'Spoils of War', 'arcade', 'Green Jade', 'ms', 'mb', '', 'img/mrslotty/hub88-spoils-of-war.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2371, 'simpleplay-mystical-lamp', 'Mystical Lamp', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-mystical-lamp.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2372, 'platipus-legendofatlantis', 'Legend of Atlantis', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-legendofatlantis.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2373, 'bgaming-SpinAndSpell', 'Spin And Spell', 'slot', 'BGaming', 'ms', 'wb', '', 'img/mrslotty/bgaming-SpinAndSpell.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2374, 'wazdan-348', 'Magic Fruits 4 Deluxe', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-348.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2375, 'evoplay-the-great-wall-treasure', 'The Great Wall Treasure', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-the-great-wall-treasure.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2376, 'redrake-blackjack-double-exposure', 'BLACKJACK DOUBLE EXPOSURE', 'slot', 'Red Rake', 'ms', 'wb', 'JUME', 'img/mrslotty/redrake-blackjack-double-exposure.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2377, 'velagaming-roulette-euro', 'European Roulette', 'arcade', 'Velagaming', 'ms', 'wb', '', 'img/mrslotty/velagaming-roulette-euro.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2378, 'hub88-china-charms', 'China Charms', 'slot', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-china-charms.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2379, 'slotexchange-greedy-leprechauns', 'Greedy leprechauns', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-greedy-leprechauns.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2380, 'hub88-spoils-of-war', 'Spoils of War', 'arcade', 'Green Jade', 'ms', 'wb', '', 'img/mrslotty/hub88-spoils-of-war.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2381, 'simpleplay-mystical-lamp', 'Mystical Lamp', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-mystical-lamp.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2382, 'platipus-aztectemple', 'Aztec Temple', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-aztectemple.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2383, 'bgaming-TexasHoldem', 'Texas Holdem', 'slot', 'BGaming', 'ms', 'mb', 'JUME', 'img/mrslotty/bgaming-TexasHoldem.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2384, 'wazdan-350', 'Great Book of Magic Deluxe', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-350.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2385, 'evoplay-robin-hood', 'Robin Hood', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-robin-hood.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2386, 'redrake-blackjack-european', 'BLACKJACK EUROPEAN', 'slot', 'Red Rake', 'ms', 'mb', 'JUME', 'img/mrslotty/redrake-blackjack-european.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2387, 'hub88-lucky-express', 'Lucky Express', 'slot', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-lucky-express.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2388, 'slotexchange-legends-of-rock', 'Legends of rock', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-legends-of-rock.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2389, 'hub88-bingo-samba-rio', 'Bingo Samba Rio', 'video_bingo', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-bingo-samba-rio.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2390, 'simpleplay-cage-fight', 'Cage Fight', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-cage-fight.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2391, 'platipus-aztectemple', 'Aztec Temple', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-aztectemple.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2392, 'bgaming-TexasHoldem', 'Texas Holdem', 'slot', 'BGaming', 'ms', 'wb', 'JUME', 'img/mrslotty/bgaming-TexasHoldem.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2393, 'wazdan-350', 'Great Book of Magic Deluxe', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-350.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2394, 'evoplay-robin-hood', 'Robin Hood', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-robin-hood.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2395, 'redrake-blackjack-european', 'BLACKJACK EUROPEAN', 'slot', 'Red Rake', 'ms', 'wb', 'JUME', 'img/mrslotty/redrake-blackjack-european.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2396, 'hub88-lucky-express', 'Lucky Express', 'slot', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-lucky-express.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2397, 'slotexchange-legends-of-rock', 'Legends of rock', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-legends-of-rock.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2398, 'hub88-bingo-samba-rio', 'Bingo Samba Rio', 'video_bingo', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-bingo-samba-rio.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2399, 'simpleplay-cage-fight', 'Cage Fight', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-cage-fight.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2400, 'platipus-tripledragon', 'Triple Dragon', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-tripledragon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2401, 'bgaming-TreyPoker', 'Trey Poker', 'slot', 'BGaming', 'ms', 'mb', 'JUME', 'img/mrslotty/bgaming-TreyPoker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2402, 'wazdan-351', 'Dracula\'s Castle', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-351.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2403, 'evoplay-chinese-new-year', 'Chinese New Year', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-chinese-new-year.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2404, 'redrake-blackjack-atlantic-city', 'BLACKJACK ATLANTIC CITY', 'slot', 'Red Rake', 'ms', 'mb', 'JUME', 'img/mrslotty/redrake-blackjack-atlantic-city.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2405, 'hub88-lost-saga', 'Lost Saga', 'slot', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-lost-saga.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2406, 'slotexchange-far-island', 'Far island', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-far-island.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2407, 'hub88-side-bet-11k', 'Side bet 11k', 'arcade', 'Green Jade', 'ms', 'mb', '', 'img/mrslotty/hub88-side-bet-11k.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2408, 'simpleplay-dragon-slayer', 'Dragon Slayer', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-dragon-slayer.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2409, 'platipus-tripledragon', 'Triple Dragon', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-tripledragon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2410, 'bgaming-TreyPoker', 'Trey Poker', 'slot', 'BGaming', 'ms', 'wb', 'JUME', 'img/mrslotty/bgaming-TreyPoker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2411, 'wazdan-351', 'Dracula\'s Castle', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-351.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2412, 'evoplay-chinese-new-year', 'Chinese New Year', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-chinese-new-year.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2413, 'redrake-blackjack-atlantic-city', 'BLACKJACK ATLANTIC CITY', 'slot', 'Red Rake', 'ms', 'wb', 'JUME', 'img/mrslotty/redrake-blackjack-atlantic-city.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2414, 'hub88-lost-saga', 'Lost Saga', 'slot', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-lost-saga.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2415, 'slotexchange-far-island', 'Far island', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-far-island.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2416, 'hub88-side-bet-11k', 'Side bet 11k', 'arcade', 'Green Jade', 'ms', 'wb', '', 'img/mrslotty/hub88-side-bet-11k.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2417, 'simpleplay-dragon-slayer', 'Dragon Slayer', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-dragon-slayer.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2418, 'platipus-bookofegypt', 'Book of Egypt', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-bookofegypt.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2419, 'bgaming-WestTown', 'West Town', 'slot', 'BGaming', 'ms', 'mb', '', 'img/mrslotty/bgaming-WestTown.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2420, 'wazdan-352', 'Fruit Mania Deluxe', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-352.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2421, 'evoplay-clash-of-pirates', 'Clash of Pirates', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-clash-of-pirates.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2422, 'redrake-blackjack-vegas-strip', 'BLACKJACK VEGAS STRIP', 'slot', 'Red Rake', 'ms', 'mb', 'JUME', 'img/mrslotty/redrake-blackjack-vegas-strip.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2423, 'hub88-basketball-pro', 'Basketball Pro', 'slot', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-basketball-pro.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2424, 'slotexchange-wild-heat', 'Wild heat', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-wild-heat.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2425, 'hub88-bingo-senor-taco', 'Bingo Señor Taco', 'video_bingo', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-bingo-senor-taco.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2426, 'simpleplay-gold-of-egypt', 'Gold of Egypt', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-gold-of-egypt.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2427, 'platipus-bookofegypt', 'Book of Egypt', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-bookofegypt.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2428, 'bgaming-WestTown', 'West Town', 'slot', 'BGaming', 'ms', 'wb', '', 'img/mrslotty/bgaming-WestTown.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2429, 'wazdan-352', 'Fruit Mania Deluxe', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-352.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2430, 'evoplay-clash-of-pirates', 'Clash of Pirates', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-clash-of-pirates.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2431, 'redrake-blackjack-vegas-strip', 'BLACKJACK VEGAS STRIP', 'slot', 'Red Rake', 'ms', 'wb', 'JUME', 'img/mrslotty/redrake-blackjack-vegas-strip.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2432, 'hub88-basketball-pro', 'Basketball Pro', 'slot', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-basketball-pro.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2433, 'slotexchange-wild-heat', 'Wild heat', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-wild-heat.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2434, 'hub88-bingo-senor-taco', 'Bingo Señor Taco', 'video_bingo', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-bingo-senor-taco.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2435, 'simpleplay-gold-of-egypt', 'Gold of Egypt', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-gold-of-egypt.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2436, 'platipus-monkeysjourney', 'Monkeys Journey', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-monkeysjourney.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2437, 'bgaming-WildTexas', 'Wild Texas', 'slot', 'BGaming', 'ms', 'mb', 'JUME', 'img/mrslotty/bgaming-WildTexas.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2438, 'wazdan-353', 'Magic Stars 3', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-353.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2439, 'evoplay-journey-to-the-west', 'Journey to the West', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-journey-to-the-west.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2440, 'redrake-frenzy-discs-twin-numbers', 'Frenzy Discs - Twin Numbers', 'slot', 'Red Rake', 'ms', 'mb', 'JUME', 'img/mrslotty/redrake-frenzy-discs-twin-numbers.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2441, 'hub88-football-pro', 'Football Pro', 'slot', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-football-pro.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2442, 'slotexchange-cold-blood', 'Cold blood', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-cold-blood.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2443, 'hub88-bingo-saga-loca', 'Bingo Saga Loca', 'video_bingo', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-bingo-saga-loca.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2444, 'simpleplay-spartas-honor', 'Sparta\'s Honor', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-spartas-honor.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2445, 'platipus-monkeysjourney', 'Monkeys Journey', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-monkeysjourney.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2446, 'bgaming-WildTexas', 'Wild Texas', 'slot', 'BGaming', 'ms', 'wb', 'JUME', 'img/mrslotty/bgaming-WildTexas.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2447, 'wazdan-353', 'Magic Stars 3', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-353.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2448, 'evoplay-journey-to-the-west', 'Journey to the West', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-journey-to-the-west.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2449, 'redrake-frenzy-discs-twin-numbers', 'Frenzy Discs - Twin Numbers', 'slot', 'Red Rake', 'ms', 'wb', 'JUME', 'img/mrslotty/redrake-frenzy-discs-twin-numbers.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2450, 'hub88-football-pro', 'Football Pro', 'slot', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-football-pro.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2451, 'slotexchange-cold-blood', 'Cold blood', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-cold-blood.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2452, 'hub88-bingo-saga-loca', 'Bingo Saga Loca', 'video_bingo', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-bingo-saga-loca.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2453, 'simpleplay-spartas-honor', 'Sparta\'s Honor', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-spartas-honor.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2454, 'platipus-sakurawind', 'Sakura Wind', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-sakurawind.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2455, 'bgaming-Avalon', 'Avalon: The Lost Kingdom', 'slot', 'BGaming', 'ms', 'mb', '', 'img/mrslotty/bgaming-Avalon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2456, 'wazdan-354', 'Highschool Manga', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-354.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2457, 'evoplay-the-legend-of-shaolin', 'The Legend of Shaolin', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-the-legend-of-shaolin.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2458, 'redrake-roulette-vip', 'Roulette VIP', 'slot', 'Red Rake', 'ms', 'mb', 'RULF', 'img/mrslotty/redrake-roulette-vip.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2459, 'hub88-dragon-rising', 'Dragon Rising', 'slot', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-dragon-rising.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2460, 'slotexchange-feast-of-football', 'Feast of football', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-feast-of-football.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2461, 'hub88-bingo-senorita-calavera', 'Bingo Señorita Calavera', 'video_bingo', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-bingo-senorita-calavera.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2462, 'simpleplay-hoo-hey-how', 'Hoo Hey How', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-hoo-hey-how.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2463, 'platipus-sakurawind', 'Sakura Wind', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-sakurawind.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2464, 'bgaming-Avalon', 'Avalon: The Lost Kingdom', 'slot', 'BGaming', 'ms', 'wb', '', 'img/mrslotty/bgaming-Avalon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2465, 'wazdan-354', 'Highschool Manga', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-354.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2466, 'evoplay-the-legend-of-shaolin', 'The Legend of Shaolin', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-the-legend-of-shaolin.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2467, 'redrake-roulette-vip', 'Roulette VIP', 'slot', 'Red Rake', 'ms', 'wb', 'RULF', 'img/mrslotty/redrake-roulette-vip.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2468, 'hub88-dragon-rising', 'Dragon Rising', 'slot', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-dragon-rising.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2469, 'slotexchange-feast-of-football', 'Feast of football', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-feast-of-football.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2470, 'hub88-bingo-senorita-calavera', 'Bingo Señorita Calavera', 'video_bingo', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-bingo-senorita-calavera.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2471, 'simpleplay-hoo-hey-how', 'Hoo Hey How', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-hoo-hey-how.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2472, 'platipus-luckymoney', 'Lucky Money', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-luckymoney.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2473, 'bgaming-FourLuckyClover', 'Four Lucky Clover', 'slot', 'BGaming', 'ms', 'mb', '', 'img/mrslotty/bgaming-FourLuckyClover.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2474, 'wazdan-355', 'Dino Reels 81', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-355.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2475, 'evoplay-vegas-nights', 'Vegas Nights', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-vegas-nights.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2476, 'redrake-french-roulette', 'French Roulette', 'slot', 'Red Rake', 'ms', 'mb', 'RULF', 'img/mrslotty/redrake-french-roulette.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2477, 'hub88-pirates-of-fortune', 'Pirates of Fortune', 'slot', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-pirates-of-fortune.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2478, 'slotexchange-steam-lab', 'Steam lab', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-steam-lab.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2479, 'evoplay-animal-quest', 'Animal Quest', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-animal-quest.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2480, 'simpleplay-big-three-dragons', 'Big Three Dragons', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-big-three-dragons.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2481, 'platipus-luckymoney', 'Lucky Money', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-luckymoney.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2482, 'bgaming-FourLuckyClover', 'Four Lucky Clover', 'slot', 'BGaming', 'ms', 'wb', '', 'img/mrslotty/bgaming-FourLuckyClover.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2483, 'wazdan-355', 'Dino Reels 81', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-355.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2484, 'evoplay-vegas-nights', 'Vegas Nights', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-vegas-nights.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2485, 'redrake-french-roulette', 'French Roulette', 'slot', 'Red Rake', 'ms', 'wb', 'RULF', 'img/mrslotty/redrake-french-roulette.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2486, 'hub88-pirates-of-fortune', 'Pirates of Fortune', 'slot', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-pirates-of-fortune.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2487, 'slotexchange-steam-lab', 'Steam lab', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-steam-lab.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2488, 'evoplay-animal-quest', 'Animal Quest', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-animal-quest.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2489, 'simpleplay-big-three-dragons', 'Big Three Dragons', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-big-three-dragons.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2490, 'platipus-loveis', 'Love is', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-loveis.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2491, 'spadegaming-book-of-myth', 'Book of Myth', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-book-of-myth.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2492, 'wazdan-356', 'Cube Mania Deluxe', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-356.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2493, 'evoplay-jewellery-store', 'Jewellery Store', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-jewellery-store.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2494, 'redrake-european-roulette', 'European Roulette', 'slot', 'Red Rake', 'ms', 'mb', 'RULF', 'img/mrslotty/redrake-european-roulette.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2495, 'hub88-gold-fever', 'Gold Fever', 'slot', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-gold-fever.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2496, 'slotexchange-robocity', 'Robocity', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-robocity.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2497, 'evoplay-rocket-stars', 'Rocket Stars', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-rocket-stars.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2498, 'simpleplay-three-star-god-2', 'Three Star God 2', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-three-star-god-2.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2499, 'platipus-loveis', 'Love is', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-loveis.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2500, 'spadegaming-book-of-myth', 'Book of Myth', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-book-of-myth.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2501, 'wazdan-356', 'Cube Mania Deluxe', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-356.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2502, 'evoplay-jewellery-store', 'Jewellery Store', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-jewellery-store.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2503, 'redrake-european-roulette', 'European Roulette', 'slot', 'Red Rake', 'ms', 'wb', 'RULF', 'img/mrslotty/redrake-european-roulette.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2504, 'hub88-gold-fever', 'Gold Fever', 'slot', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-gold-fever.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2505, 'slotexchange-robocity', 'Robocity', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-robocity.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2506, 'evoplay-rocket-stars', 'Rocket Stars', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-rocket-stars.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2507, 'simpleplay-three-star-god-2', 'Three Star God 2', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-three-star-god-2.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2508, 'platipus-cinderella', 'Cinderella', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-cinderella.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2509, 'spadegaming-money-mouse', 'Money Mouse', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-money-mouse.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2510, 'wazdan-357', 'Hot 777 Deluxe', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-357.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2511, 'evoplay-red-cliff', 'Red Cliff', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-red-cliff.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2512, 'redrake-american-roulette', 'American Roulette', 'slot', 'Red Rake', 'ms', 'mb', 'RULA', 'img/mrslotty/redrake-american-roulette.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2513, 'hub88-frozen-fluffies', 'Frozen Fluffies', 'slot', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-frozen-fluffies.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2514, 'slotexchange-demonized', 'Demonized', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-demonized.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2515, 'evoplay-penalty-shoot-out', 'Penalty Shoot Out', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-penalty-shoot-out.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2516, 'simpleplay-six-swords', 'Six Swords', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-six-swords.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2517, 'platipus-cinderella', 'Cinderella', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-cinderella.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2518, 'spadegaming-money-mouse', 'Money Mouse', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-money-mouse.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2519, 'wazdan-357', 'Hot 777 Deluxe', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-357.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2520, 'evoplay-red-cliff', 'Red Cliff', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-red-cliff.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2521, 'redrake-american-roulette', 'American Roulette', 'slot', 'Red Rake', 'ms', 'wb', 'RULA', 'img/mrslotty/redrake-american-roulette.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2522, 'hub88-frozen-fluffies', 'Frozen Fluffies', 'slot', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-frozen-fluffies.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2523, 'slotexchange-demonized', 'Demonized', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-demonized.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2524, 'evoplay-penalty-shoot-out', 'Penalty Shoot Out', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-penalty-shoot-out.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2525, 'simpleplay-six-swords', 'Six Swords', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-six-swords.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2526, 'platipus-crystalsevens', 'Crystal Sevens', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-crystalsevens.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2527, 'spadegaming-three-lucky-stars', 'Three Lucky Stars', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-three-lucky-stars.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2528, 'wazdan-358', 'Corrida Romance Deluxe', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-358.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2529, 'evoplay-indianas-quest', 'Indiana\'s Quest', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-indianas-quest.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2530, 'redrake-secrets-of-the-temple', 'Secrets of the Temple', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-secrets-of-the-temple.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2531, 'hub88-cruise-of-fortune', 'Cruise of Fortune', 'slot', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-cruise-of-fortune.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2532, 'slotexchange-ancient-ruins', 'Ancient ruins', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-ancient-ruins.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2533, 'liw-LuckySix', 'Lucky Six 35/48', 'lottery', 'LiW', 'ms', 'mb', '', 'img/mrslotty/liw-LuckySix.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2534, 'simpleplay-zues', 'Zues', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-zues.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2535, 'platipus-crystalsevens', 'Crystal Sevens', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-crystalsevens.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2536, 'spadegaming-three-lucky-stars', 'Three Lucky Stars', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-three-lucky-stars.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2537, 'wazdan-358', 'Corrida Romance Deluxe', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-358.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2538, 'evoplay-indianas-quest', 'Indiana\'s Quest', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-indianas-quest.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2539, 'redrake-secrets-of-the-temple', 'Secrets of the Temple', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-secrets-of-the-temple.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2540, 'hub88-cruise-of-fortune', 'Cruise of Fortune', 'slot', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-cruise-of-fortune.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2541, 'slotexchange-ancient-ruins', 'Ancient ruins', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-ancient-ruins.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2542, 'liw-LuckySix', 'Lucky Six 35/48', 'lottery', 'LiW', 'ms', 'wb', '', 'img/mrslotty/liw-LuckySix.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2543, 'simpleplay-zues', 'Zues', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-zues.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2544, 'platipus-magicalmirror', 'Magical Mirror', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-magicalmirror.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2545, 'spadegaming-888', '888', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-888.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2546, 'wazdan-359', 'Black Hawk Deluxe', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-359.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2547, 'evoplay-ace-round', 'Ace Round', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-ace-round.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2548, 'redrake-777heist', '777 Heist', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-777heist.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2549, 'hub88-caves-treasures', 'Caves & Treasures', 'slot', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-caves-treasures.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2550, 'slotexchange-space-dwellers', 'Space dwellers', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-space-dwellers.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2551, 'liw-Lotto548', 'Win 5/48', 'lottery', 'LiW', 'ms', 'mb', '', 'img/mrslotty/liw-Lotto548.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2552, 'simpleplay-mayas-miracle', 'Maya\'s Miracle', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-mayas-miracle.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2553, 'platipus-magicalmirror', 'Magical Mirror', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-magicalmirror.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2554, 'spadegaming-888', '888', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-888.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2555, 'wazdan-359', 'Black Hawk Deluxe', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-359.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2556, 'evoplay-ace-round', 'Ace Round', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-ace-round.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2557, 'redrake-777heist', '777 Heist', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-777heist.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2558, 'hub88-caves-treasures', 'Caves & Treasures', 'slot', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-caves-treasures.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2559, 'slotexchange-space-dwellers', 'Space dwellers', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-space-dwellers.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2560, 'liw-Lotto548', 'Win 5/48', 'lottery', 'LiW', 'ms', 'wb', '', 'img/mrslotty/liw-Lotto548.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2561, 'simpleplay-mayas-miracle', 'Maya\'s Miracle', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-mayas-miracle.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2562, 'platipus-powerofgods', 'Power of Gods', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-powerofgods.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2563, 'spadegaming-heroes', 'Heroes', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-heroes.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2564, 'wazdan-360', 'Hot Party Deluxe', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-360.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2565, 'evoplay-elven-princesses', 'Elven Princesses', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-elven-princesses.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2566, 'redrake-solomon-the-king', 'Solomon: the King', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-solomon-the-king.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2567, 'hub88-bangkok-dreams', 'Bangkok Dreams', 'slot', 'Kalamba', 'ms', 'mb', '', 'img/mrslotty/hub88-bangkok-dreams.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2568, 'slotexchange-lights-of-vegas', 'Lights of Vegas', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-lights-of-vegas.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2569, 'liw-Lotto748', 'Win 7/48', 'lottery', 'LiW', 'ms', 'mb', '', 'img/mrslotty/liw-Lotto748.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2570, 'simpleplay-lucky-clovers', 'Lucky Clovers', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-lucky-clovers.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2571, 'platipus-powerofgods', 'Power of Gods', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-powerofgods.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2572, 'spadegaming-heroes', 'Heroes', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-heroes.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2573, 'wazdan-360', 'Hot Party Deluxe', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-360.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2574, 'evoplay-elven-princesses', 'Elven Princesses', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-elven-princesses.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2575, 'redrake-solomon-the-king', 'Solomon: the King', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-solomon-the-king.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2576, 'hub88-bangkok-dreams', 'Bangkok Dreams', 'slot', 'Kalamba', 'ms', 'wb', '', 'img/mrslotty/hub88-bangkok-dreams.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2577, 'slotexchange-lights-of-vegas', 'Lights of Vegas', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-lights-of-vegas.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2578, 'liw-Lotto748', 'Win 7/48', 'lottery', 'LiW', 'ms', 'wb', '', 'img/mrslotty/liw-Lotto748.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2579, 'simpleplay-lucky-clovers', 'Lucky Clovers', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-lucky-clovers.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2580, 'platipus-crazyjelly', 'Crazy Jelly', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-crazyjelly.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2581, 'spadegaming-gold-panther', 'Gold Panther', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-gold-panther.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2582, 'wazdan-363', 'Fruit Fiesta', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-363.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2583, 'evoplay-fruit-burst', 'Fruit Burst', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-fruit-burst.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2584, 'redrake-gustav-minebuster', 'Gustav Minebuster', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-gustav-minebuster.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2585, 'hub88-monkey-god', 'Monkey God', 'slot', 'Kalamba', 'ms', 'mb', '', 'img/mrslotty/hub88-monkey-god.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2586, 'slotexchange-lost-world', 'Lost world', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-lost-world.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2587, 'liw-Special', 'First ball color (Win 5/48)', 'lottery', 'LiW', 'ms', 'mb', '', 'img/mrslotty/liw-Special.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2588, 'simpleplay-pigsy', 'Pigsy', 'slot', 'SimplePlay', 'ms', 'mb', '', 'img/mrslotty/simpleplay-pigsy.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2589, 'platipus-crazyjelly', 'Crazy Jelly', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-crazyjelly.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2590, 'spadegaming-gold-panther', 'Gold Panther', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-gold-panther.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2591, 'wazdan-363', 'Fruit Fiesta', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-363.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2592, 'evoplay-fruit-burst', 'Fruit Burst', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-fruit-burst.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2593, 'redrake-gustav-minebuster', 'Gustav Minebuster', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-gustav-minebuster.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2594, 'hub88-monkey-god', 'Monkey God', 'slot', 'Kalamba', 'ms', 'wb', '', 'img/mrslotty/hub88-monkey-god.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2595, 'slotexchange-lost-world', 'Lost world', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-lost-world.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2596, 'liw-Special', 'First ball color (Win 5/48)', 'lottery', 'LiW', 'ms', 'wb', '', 'img/mrslotty/liw-Special.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2597, 'simpleplay-pigsy', 'Pigsy', 'slot', 'SimplePlay', 'ms', 'wb', '', 'img/mrslotty/simpleplay-pigsy.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2598, '1040', 'MrhatSunshine', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1040_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2599, '1050', 'CosmoMix', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1050_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2600, '1060', 'FancyJungle', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1060_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2601, '1070', 'CaishensTreasure', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1070_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2602, '1080', 'Code243-0', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1080_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2603, '1090', 'Rooster', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1090_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2604, '1110', 'ElFuego', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1110_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2605, '1100', 'LamaGlama', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1100_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2606, '1120', 'Roboland', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1120_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2607, '1130', 'BookOfEon', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1130_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2608, '1270', 'BabyBlue', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1270_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2609, '1200', 'WildCat', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1200_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2610, '1250', 'RLSuperHeroLite', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1250_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2611, '1210', 'BossVegas', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1210_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2612, '1140', 'CalicoJack', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1140_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2613, '1260', 'FruitCube', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1260_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2614, '1220', 'HollyMollyHole', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1220_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2615, '1300', 'PlataOPlomo', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1300_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2616, '1280', 'BingoMachine', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1280_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2617, '1170', 'Poseidon', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1170_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2618, '1380', 'FruitMonsterChristmas', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1380_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2619, '1150', 'FruitFarm', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1150_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2620, '1360', 'TheGoldenEgg', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1360_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2621, '1240', 'HelioLuna', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1240_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2622, '1180', 'FruitMonster', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1180_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2623, '1370', 'Mayanera', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1370_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2624, '1230', 'Mahala', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1230_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2625, '1400', 'TheGoldenEggEaster', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1400_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2626, '1340', 'Rosemary', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1340_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2627, '1390', 'NeedForSpace', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1390_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2628, '1440', 'BookOfSeth', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1440_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2629, '1450', 'FortuneDynasty', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1450_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2630, '1460', 'Freeway', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1460_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2631, '1500', 'SpinCircus', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1500_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2632, '1470', 'DanzaDeLosMuertos', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1470_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2633, '1510', 'NiceCream', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1510_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2634, '1480', 'SpringInvaders', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1480_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2635, '1530', 'ChristmasJoy', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1530_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2636, '1420', 'MadameMoustache', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1420_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2637, '1490', 'Infectus', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1490_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2638, '1570', 'WildValentines', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1570_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2639, '1431', 'Betrick', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1431_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2640, '1550', 'Synthway', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1550_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2641, '1540', 'SushiHouse', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1540_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2642, '1580', 'ShaolinTwins', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1580_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2643, '1600', 'RLSHBonus', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1600_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2644, '1610', 'Starshards', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1610_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2645, '1590', 'Mafioso', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1590_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2646, '1000', 'EgyptianStone', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1000_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2647, '1010', 'Galacnica', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1010_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2648, '1020', 'MrhatBefore', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1020_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2649, '1040', 'MrhatSunshine', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1040_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2650, '1050', 'CosmoMix', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1050_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2651, '1060', 'FancyJungle', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1060_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2652, '1070', 'CaishensTreasure', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1070_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2653, '1080', 'Code243-0', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1080_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2654, '1090', 'Rooster', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1090_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2655, '1110', 'ElFuego', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1110_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2656, '1100', 'LamaGlama', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1100_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2657, '1120', 'Roboland', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1120_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2658, '1130', 'BookOfEon', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1130_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2659, '1270', 'BabyBlue', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1270_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2660, '1200', 'WildCat', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1200_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2661, '1250', 'RLSuperHeroLite', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1250_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2662, '1210', 'BossVegas', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1210_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2663, '1140', 'CalicoJack', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1140_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2664, '1260', 'FruitCube', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1260_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2665, '1220', 'HollyMollyHole', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1220_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2666, '1300', 'PlataOPlomo', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1300_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2667, '1280', 'BingoMachine', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1280_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2668, '1170', 'Poseidon', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1170_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2669, '1380', 'FruitMonsterChristmas', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1380_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2670, '1150', 'FruitFarm', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1150_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2671, '1360', 'TheGoldenEgg', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1360_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2672, '1240', 'HelioLuna', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1240_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2673, '1180', 'FruitMonster', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1180_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2674, '1370', 'Mayanera', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1370_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2675, '1230', 'Mahala', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1230_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2676, '1400', 'TheGoldenEggEaster', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1400_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2677, '1340', 'Rosemary', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1340_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2678, '1390', 'NeedForSpace', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1390_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2679, '1440', 'BookOfSeth', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1440_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2680, '1450', 'FortuneDynasty', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1450_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2681, '1460', 'Freeway', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1460_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2682, '1500', 'SpinCircus', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1500_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2683, '1470', 'DanzaDeLosMuertos', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1470_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2684, '1510', 'NiceCream', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1510_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2685, '1480', 'SpringInvaders', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1480_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2686, '1530', 'ChristmasJoy', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1530_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2687, '1420', 'MadameMoustache', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1420_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2688, '1490', 'Infectus', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1490_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2689, '1570', 'WildValentines', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1570_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2690, '1431', 'Betrick', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1431_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2691, '1550', 'Synthway', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1550_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2692, '1540', 'SushiHouse', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1540_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2693, '1580', 'ShaolinTwins', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1580_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2694, '1600', 'RLSHBonus', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1600_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2695, '1610', 'Starshards', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1610_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2696, '1590', 'Mafioso', 'slot', 'Spinmatic', 'gr', 'wb', '', 'img/spinmatic/1590_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2697, '10100', 'Golden Games', 'virtual', 'GoldenRace', 'gr', 'wb', '', 'img/xpressgaming/10114.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2698, '100168', 'Poker', 'tv', 'HollywoodTv', 'gr', 'mb', '', 'img/hollywoodtv/gr_betpoker.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2699, '100181', 'Lucky5', 'tv', 'HollywoodTv', 'gr', 'wb', '', 'img/hollywoodtv/gr_jokerbet.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2700, '100171', '1Bet', 'tv', 'HollywoodTv', 'gr', 'mb', '', 'img/hollywoodtv/gr_betpoker.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2701, '100171', '1Bet', 'tv', 'HollywoodTv', 'gr', 'wb', '', 'img/hollywoodtv/gr_betpoker.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2702, '100165', '5Bet', 'tv', 'HollywoodTv', 'gr', 'mb', '', 'img/hollywoodtv/gr_betpoker.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2703, '100165', '5Bet', 'tv', 'HollywoodTv', 'gr', 'wb', '', 'img/hollywoodtv/gr_betpoker.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2704, '100172', '7Bet', 'tv', 'HollywoodTv', 'gr', 'mb', '', 'img/hollywoodtv/gr_betpoker.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2705, '100172', '7Bet', 'tv', 'HollywoodTv', 'gr', 'wb', '', 'img/hollywoodtv/gr_betpoker.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2706, '100172', 'Lucky6', 'tv', 'HollywoodTv', 'gr', 'mb', '', 'img/hollywoodtv/gr_betpoker.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2707, '100172', 'Lucky6', 'tv', 'HollywoodTv', 'gr', 'wb', '', 'img/hollywoodtv/gr_betpoker.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2708, '100172', 'War of Element', 'tv', 'HollywoodTv', 'gr', 'mb', '', 'img/hollywoodtv/gr_betpoker.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2709, '10297', 'War of Element', 'tv', 'HollywoodTv', 'gr', 'wb', '', 'img/hollywoodtv/gr_betpoker.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2710, '100175', 'Live Roulette', 'tv', 'HollywoodTv', 'gr', 'wb', '', 'img/hollywoodtv/gr_autowheel.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2711, '100138', 'Keno', 'tv', 'HollywoodTv', 'gr', 'mb', '', 'img/hollywoodtv/gr_liveKeno.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2712, '100157', 'Keno Deluxe', 'tv', 'HollywoodTv', 'gr', 'mb', '', 'img/hollywoodtv/gr_kenoDeluxe.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2713, '100154', 'Mega6', 'tv', 'HollywoodTv', 'gr', 'mb', '', 'img/hollywoodtv/gr_mega6.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2714, '100149', 'Mega7', 'tv', 'HollywoodTv', 'gr', 'mb', '', 'img/hollywoodtv/gr_mega7.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2715, '100155', 'Super7', 'tv', 'HollywoodTv', 'gr', 'mb', '', 'img/hollywoodtv/gr_super7.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2716, '100153', 'Spin2Wheels', 'tv', 'HollywoodTv', 'gr', 'mb', '', 'img/hollywoodtv/gr_spin2wheels.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2717, '100170', 'Super5', 'tv', 'HollywoodTv', 'gr', 'mb', '', 'img/hollywoodtv/gr_super5-logo.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2718, '100175', 'Autowheel', 'tv', 'HollywoodTv', 'gr', 'mb', '', 'img/hollywoodtv/gr_autowheel.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2719, '100169', 'VIP Roulette', 'tv', 'HollywoodTv', 'gr', 'mb', '', 'img/hollywoodtv/gr_roulette.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2720, '100167', 'JokerBet', 'tv', 'HollywoodTv', 'gr', 'mb', '', 'img/hollywoodtv/gr_jokerbet.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2721, '100181', 'Lucky5', 'tv', 'HollywoodTv', 'gr', 'mb', '', 'img/hollywoodtv/gr_betpoker.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2722, '100138', 'Keno', 'tv', 'HollywoodTv', 'gr', 'wb', '', 'img/hollywoodtv/gr_liveKeno.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2723, '100157', 'Keno Deluxe', 'tv', 'HollywoodTv', 'gr', 'wb', '', 'img/hollywoodtv/gr_kenoDeluxe.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2724, '100154', 'Mega6', 'tv', 'HollywoodTv', 'gr', 'wb', '', 'img/hollywoodtv/gr_mega6.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2725, '100149', 'Mega7', 'tv', 'HollywoodTv', 'gr', 'wb', '', 'img/hollywoodtv/gr_mega7.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2726, '100155', 'Super7', 'tv', 'HollywoodTv', 'gr', 'wb', '', 'img/hollywoodtv/gr_super7.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2727, '100153', 'Spin2Wheels', 'tv', 'HollywoodTv', 'gr', 'wb', '', 'img/hollywoodtv/gr_spin2wheels.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2728, '100170', 'Super5', 'tv', 'HollywoodTv', 'gr', 'wb', '', 'img/hollywoodtv/gr_super5-logo.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2729, '100175', 'Wheel', 'tv', 'HollywoodTv', 'gr', 'wb', '', 'img/hollywoodtv/gr_autowheel.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2730, '100169', 'VIP Roulette', 'tv', 'HollywoodTv', 'gr', 'wb', '', 'img/hollywoodtv/gr_roulette.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2731, '100167', 'JokerBet', 'tv', 'HollywoodTv', 'gr', 'wb', '', 'img/hollywoodtv/gr_jokerbet.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2732, '100168', 'Poker', 'tv', 'HollywoodTv', 'gr', 'wb', '', 'img/hollywoodtv/gr_betpoker.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2733, '1000', 'EgyptianStone', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1000_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2734, '1010', 'Galacnica', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1010_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2735, '1020', 'MrhatBefore', 'slot', 'Spinmatic', 'gr', 'mb', '', 'img/spinmatic/1020_s.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2736, 'tvbet', 'tvBet', 'tv', 'tvBet', 'tv', 'wb', '', 'img/tvbet.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2737, '29', 'Mystery Joker', 'slot', 'luckySpins', 'ls', 'wb', '', 'img/ls_29.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2738, 'jackhammer2_touch-gameview_netenthtml', 'Jack Hammer 2 Touch', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_jackhammer2_touch.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2739, 'scruffyduck-gameview_netenthtml', 'Scruffy Duck', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_scruffyduck.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2740, 'vs9catz', 'The Catfather', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs9catz.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2741, 'endorphina2_7up@ENDORPHINA', '7 BONUS UP!', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/7UP.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2742, '30', 'Xmas Joker', 'slot', 'luckySpins', 'ls', 'wb', '', 'img/ls_30.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2743, 'jackhammer_touch-gameview_netenthtml', 'Jack Hammer Touch', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_jackhammer_touch.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2744, 'secretsofatlantis-gameview_netenthtml', 'Secrets of Atlantis', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_secretsofatlantis.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2745, 'vs20cw', 'Sugar Rush Winter', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs20cw.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2746, 'slotexchange-dwarfs-secrets', 'Dwarf\'s secrets', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-dwarfs-secrets.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2747, 'endorphina2_Durga@ENDORPHINA', 'Durga', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/Durga.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2748, '31', 'Prosperity Palace', 'slot', 'luckySpins', 'ls', 'wb', '', 'img/ls_31.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2749, 'jimihendrix-gameview_netenthtml', 'Jimi Hendrix', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_jimihendrix.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2750, 'secretsofchristmas-gameview_netenthtml', 'Secrets of Christmas', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_secretsofchristmas.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2751, 'vs20gg', 'Spooky Fortune', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs20gg.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2752, 'slotexchange-dwarfs-secrets', 'Dwarf\'s secrets', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-dwarfs-secrets.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2753, 'endorphina2_LittlePanda@ENDORPHINA', 'LittlePanda', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/LittlePanda.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2754, '32', 'Firefly Frenzy', 'slot', 'luckySpins', 'ls', 'wb', '', 'img/ls_32.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2755, 'jinglespin-gameview_netenthtml', 'Jingle Spin', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_jinglespin.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2756, 'sparks-gameview_netenthtml', 'Sparks', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_sparks.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2757, 'vs30catz', 'The Catfather Part II', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs30catz.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2758, 'endorphina2_Taboo@ENDORPHINA', 'Taboo', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/Taboo.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2759, '33', 'Planet Fortune', 'slot', 'luckySpins', 'ls', 'wb', '', 'img/ls_33.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2760, 'jokerpro-gameview_netenthtml', 'Joker Pro', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_jokerpro.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2761, 'vs9hockey', 'Hockey League Wild Match', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs9hockey.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2762, 'endorphina2_Tribe@ENDORPHINA', 'Tribe', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/Tribe.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2763, '34', 'The Sword and the Grail', 'slot', 'luckySpins', 'ls', 'wb', '', 'img/ls_34.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2764, 'stickers-gameview_netenthtml', 'Stickers', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_stickers.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2765, 'bjmb', 'American Blackjack', 'slot', 'Pragmatic', 'pg', 'wb', 'JUME', 'img/pg/pg_bjmb.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2766, 'slotexchange-mermaid-secrets', 'Mermaid secret', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-mermaid-secrets.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2767, 'endorphina2_FootballStar@ENDORPHINA', 'Football Star', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/Footballstar.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2768, '35', 'Demon', 'slot', 'luckySpins', 'ls', 'wb', '', 'img/ls_35.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2769, 'junglegames-gameview_netent', 'Jungle Games', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_junglegames.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2770, 'bndt', 'Dragon Tiger', 'slot', 'Pragmatic', 'pg', 'wb', 'JUME', 'img/pg/pg_bndt.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2771, 'slotexchange-mermaid-secrets', 'Mermaid secret', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-mermaid-secrets.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2772, 'endorphina2_LuckyLands@ENDORPHINA', 'Lucky Lands', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/LuckyLands.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2773, '36', 'Wild Rails', 'slot', 'luckySpins', 'ls', 'wb', '', 'img/ls_36.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2774, 'junglespirit-gameview_netenthtml', 'Jungle Spirit: Call of the Wild', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_junglespirit.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2775, 'swipeandroll-gameview_netenthtml', 'Swipe and Roll', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_swipeandroll.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2776, 'bnadvanced', 'Dragon Bonus Baccarat', 'slot', 'Pragmatic', 'pg', 'wb', 'JUME', 'img/pg/pg_bnadvanced.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2777, 'endorphina2_Kamchatka@ENDORPHINA', 'Kamchatka', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/Kamchatka.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2778, '37', 'Xmas Magic', 'slot', 'luckySpins', 'ls', 'wb', '', 'img/ls_37.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2779, 'justjewelsdeluxe-gameview_novomatic', 'Just Jewels Deluxe', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_justjewelsdeluxe.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2780, 'templeofnudges-gameview_netenthtml', 'Temple of Nudges', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_templeofnudges.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2781, 'scwolfgoldai', 'Wolf Gold 1,000,000', 'slot', 'Pragmatic', 'pg', 'wb', 'RAGA', 'img/pg/pg_scwolfgoldai.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2782, 'endorphina2_DiaDeMuertos@ENDORPHINA', 'Dia De Muertos', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/DiaDeLosMuertos.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2783, '38', 'Aztec Idols', 'slot', 'luckySpins', 'ls', 'wb', '', 'img/ls_38.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2784, 'justjewelsdeluxe-gameview_novomatic', 'Just Jewels Deluxe', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_justjewelsdeluxe.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2785, 'theinvisibleman-gameview_netenthtml', 'The Invisible Man', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_theinvisibleman.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2786, 'scsafariai', 'Hot Safari 75,000', 'slot', 'Pragmatic', 'pg', 'wb', 'RAGA', 'img/pg/pg_scsafariai.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2787, 'nsoft-greyhound-races', 'Greyhound Races', 'slot', 'Nsoft', 'ms', 'mb', '', 'img/mrslotty/nsoft-greyhound-races.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2788, 'endorphina2_1LuckyStreak@ENDORPHINA', 'Lucky Streak 1', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/LuckyStreak1.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2789, '39', 'Chinese New Year', 'slot', 'luckySpins', 'ls', 'wb', '', 'img/ls_39.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2790, 'katana-gameview_novomatic', 'Katana', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_katana.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2791, 'themepark-gameview_netenthtml', 'Theme Park', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_themepark.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2792, 'scqogai', 'Queen of Gold 100,000', 'slot', 'Pragmatic', 'pg', 'wb', 'RAGA', 'img/pg/pg_scqogai.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2793, 'nsoft-greyhound-races', 'Greyhound Races', 'slot', 'Nsoft', 'ms', 'wb', '', 'img/mrslotty/nsoft-greyhound-races.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2794, 'endorphina2_SugarGlider@ENDORPHINA', 'Sugar Glider', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/SugarGlider.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2795, '40', 'Doom of Egypt', 'slot', 'luckySpins', 'ls', 'wb', '', 'img/ls_40.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2796, 'katana-gameview_novomatic', 'Katana', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_katana.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2797, 'turnyourfortune-gameview_netenthtml', 'Turn Your Fortune', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_turnyourfortune.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2798, 'scpandai', 'Panda Gold 50,000', 'slot', 'Pragmatic', 'pg', 'wb', 'RAGA', 'img/pg/pg_scpandai.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2799, 'endorphina2_2LuckyStreak@ENDORPHINA', 'Lucky Streak 2', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/LuckyStreak2.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2800, '41', 'Ninja Fruits', 'slot', 'luckySpins', 'ls', 'wb', '', 'img/ls_41.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2801, 'scgoldrushai', 'Gold Rush 500,000', 'slot', 'Pragmatic', 'pg', 'wb', 'RAGA', 'img/pg/pg_scgoldrushai.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2802, 'endorphina2_LuxuryLife@ENDORPHINA', 'Luxury Life', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/LuxuryLife.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2803, '43', 'Rise of Merlin', 'slot', 'luckySpins', 'ls', 'wb', '', 'img/ls_43.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2804, 'kingofcards-gameview_novomatic', 'King of Cards', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_kingofcards.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2805, 'twinspindeluxe-gameview_netenthtml', 'Twin Spin Deluxe', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_twinspindeluxe.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2806, 'scdiamondai', 'Diamond Strike 250,000', 'slot', 'Pragmatic', 'pg', 'wb', 'RAGA', 'img/pg/pg_scdiamondai.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2807, 'nsoft-virtual-greyhound-races', 'Virtual Greyhound Races', 'slot', 'Nsoft', 'ms', 'mb', '', 'img/mrslotty/nsoft-virtual-greyhound-races.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2808, 'endorphina2_3LuckyStreak@ENDORPHINA', 'Lucky Streak 3', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/LuckyStreak3.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2809, '44', 'Legacy of Dead', 'slot', 'luckySpins', 'ls', 'wb', '', 'img/ls_44.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2810, 'kingscrown-gameview_amatic_mobile', 'Kings Crown', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_kingscrown.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2811, 'sc7piggiesai', '7 Piggies 25,000', 'slot', 'Pragmatic', 'pg', 'wb', 'RAGA', 'img/pg/pg_sc7piggiesai.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2812, 'nsoft-virtual-greyhound-races', 'Virtual Greyhound Races', 'slot', 'Nsoft', 'ms', 'wb', '', 'img/mrslotty/nsoft-virtual-greyhound-races.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2813, 'endorphina2_AncientTroy@ENDORPHINA', 'Ancient Troy', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/AncientTroy.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2814, 'sportrace', 'Sport Race', 'virtual', 'SportRace', 'vg', 'wb', '', 'img/bg.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2815, 'kingsofchicago-gameview_netent', 'Kings of Chicago', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_kingsofchicago.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2816, 'scwolfgold', 'Wolf Gold 1 Million', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_scwolfgold.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2817, 'nsoft-virtual-horse-races', 'Virtual Horse Races', 'slot', 'Nsoft', 'ms', 'mb', '', 'img/mrslotty/nsoft-virtual-horse-races.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2818, 'endorphina2_AncientTroyDice@ENDORPHINA', 'Ancient Troy (Dice)', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/AncientTroyDice.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2819, 'sportrace', 'Sport Race', 'virtual', 'SportRace', 'vg', 'mb', '', 'img/bg.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2820, 'koiprincess-gameview_netenthtml', 'Koi Princess', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_koiprincess.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2821, 'warlords-gameview_netenthtml', 'Warlords', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_warlords.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2822, 'scqog', 'Queen of Gold 100,000', 'slot', 'Pragmatic', 'pg', 'wb', 'RAGA', 'img/pg/pg_scqog.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2823, 'nsoft-virtual-horse-races', 'Virtual Horse Races', 'slot', 'Nsoft', 'ms', 'wb', '', 'img/mrslotty/nsoft-virtual-horse-races.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2824, 'endorphina2_1LuckyStreakDice@ENDORPHINA', 'Lucky Streak 1 (Dice)', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/LuckyDice1.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2825, 'evenbet', 'evenBet', 'poker', 'UniversalPoker', 'eb', 'wb', '', 'img/eb.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2826, 'wildbazaar-gameview_netenthtml', 'Wild Bazaar', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_wildbazaar.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2827, 'scpanda', 'Panda Gold 10,000', 'slot', 'Pragmatic', 'pg', 'wb', 'RAGA', 'img/pg/pg_scpanda.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2828, 'nsoft-virtual-motorcycle-speedway', 'Virtual Motorcycle Speedway', 'slot', 'Nsoft', 'ms', 'mb', '', 'img/mrslotty/nsoft-virtual-motorcycle-speedway.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2829, 'endorphina2_2LuckyStreakDice@ENDORPHINA', 'Lucky Streak 2 (Dice)', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/LuckyDice2.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2830, '534-gameview_egt_mobile', '100 Super Dice', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_534.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2831, 'lights-gameview_netent', 'Lights', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_lights.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2832, 'wildotron3000-gameview_netenthtml', 'Wild-O-Tron 3000', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_wildotron3000.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2833, 'scsafari', 'Hot Safari 50,000', 'slot', 'Pragmatic', 'pg', 'wb', 'RAGA', 'img/pg/pg_scsafari.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2834, 'nsoft-virtual-motorcycle-speedway', 'Virtual Motorcycle Speedway', 'slot', 'Nsoft', 'ms', 'wb', '', 'img/mrslotty/nsoft-virtual-motorcycle-speedway.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2835, 'endorphina2_3LuckyStreakDice@ENDORPHINA', 'Lucky Streak 3 (Dice)', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/LuckyDice3.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2836, '527-gameview_egt_mobile', '100 Super Hot', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_527.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2837, 'lights_touch-gameview_netenthtml', 'Lights Touch', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_lights_touch.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2838, 'wildwildwest-gameview_netenthtml', 'Wild Wild West', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_wildwildwest.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2839, 'scgoldrush', 'Gold Rush 250,000', 'slot', 'Pragmatic', 'pg', 'wb', 'RAGA', 'img/pg/pg_scgoldrush.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2840, 'nsoft-slot-car-races', 'Slot Car Races', 'slot', 'Nsoft', 'ms', 'mb', '', 'img/mrslotty/nsoft-slot-car-races.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2841, 'endorphina2_VoodooDice@ENDORPHINA', 'Voodoo (Dice)', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/VoodooDice.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2842, '537-gameview_egt_mobile', '20 Burning Dice', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_537.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2843, 'lordoftheocean-gameview_novomatic', 'Lord of the Ocean', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_lordoftheocean.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2844, 'scdiamond', 'Diamond Strike 100,000', 'slot', 'Pragmatic', 'pg', 'wb', 'RAGA', 'img/pg/pg_scdiamond.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2845, 'nsoft-slot-car-races', 'Slot Car Races', 'slot', 'Nsoft', 'ms', 'wb', '', 'img/mrslotty/nsoft-slot-car-races.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2846, 'endorphina2_AlmightySparta@ENDORPHINA', 'Almighty Sparta', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/AlmightySparta.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2847, '530-gameview_egt_mobile', '20 Burning Hot', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_530.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2848, 'lordoftheocean-gameview_novomatic', 'Lord of the Ocean', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_lordoftheocean.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2849, 'witchcraftacademy-gameview_netenthtml', 'Witchcraft Academy', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_witchcraftacademy.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2850, 'sc7piggies', '7 Piggies 5,000', 'slot', 'Pragmatic', 'pg', 'wb', 'RAGA', 'img/pg/pg_sc7piggies.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2851, 'spadegaming-jokers-treasure', 'Jokers Treasure', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-jokers-treasure.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2852, 'endorphina2_MysteryEldorado@ENDORPHINA', 'Mystery of Eldorado', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/MysteryEldorado.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2853, '854-gameview_egt_mobile', '20 Diamonds', 'slot', 'EGT', 'sl', 'wb', '', 'img/sl_854.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2854, 'lostisland-gameview_netent', 'Lost Island', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_lostisland.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2855, 'wolfcub-gameview_netenthtml', 'Wolf Cub', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_wolfcub.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2856, 'vs25kingdomsnojp', '3 Kingdoms - Battle of Red Cliffs', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs25kingdomsnojp.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2857, 'spadegaming-jokers-treasure', 'Jokers Treasure', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-jokers-treasure.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2858, 'endorphina2_SugarGliderDice@ENDORPHINA', 'Sugar Glider (Dice)', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/SugarGliderDice.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2859, '854-gameview_egt_mobile', '20 Diamonds', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_854.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2860, 'wzdlt-gameview_wazdan', 'Lost Treasure', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdlt.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2861, 'vpfh3', 'Flat Horse Racing', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vpfh3.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2862, 'spadegaming-double-flame', 'Double Flame', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-double-flame.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2863, 'endorphina_JokerWild@ENDORPHINA', 'Joker Wild', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/JokerWild.png', '', 0, 0, 0, 0, 1, 0);
INSERT INTO y_games VALUES
(2864, '843-gameview_egt_mobile', '20 Super Dice', 'slot', 'EGT', 'sl', 'wb', '', 'img/sl_843.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2865, 'ladyluck-gameview_amatic_mobile', 'Lovely Lady', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_ladyluck.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2866, 'allwaysfruits-gameview_amatic_mobile', 'Allways Fruits', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_allwaysfruits.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2867, 'vppso4', 'Penalty Shootout', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vppso4.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2868, 'spadegaming-double-flame', 'Double Flame', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-double-flame.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2869, 'endorphina_DeucesWild@ENDORPHINA', 'Deuces Wild', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/DeucesWild.png', '', 0, 0, 0, 0, 1, 0);
INSERT INTO y_games VALUES
(2870, '843-gameview_egt_mobile', '20 Super Dice', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_843.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2871, 'luckyangler-gameview_netent', 'Lucky Angler', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_luckyangler.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2872, 'allwayswin-gameview_amatic_mobile', 'Allways Win', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_allwayswin.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2873, 'bca', 'Baccarat', 'slot', 'Pragmatic', 'pg', 'wb', 'JUME', 'img/pg/pg_bca.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2874, 'slotexchange-robobase', 'Robobase', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-robobase.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2875, 'endorphina_JacksOrBetter@ENDORPHINA', 'Jacks Or Better', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/JacksOrBetter.png', '', 0, 0, 0, 0, 1, 0);
INSERT INTO y_games VALUES
(2876, '803-gameview_egt_mobile', '20 Super Hot', 'slot', 'EGT', 'sl', 'wb', '', 'img/sl_803.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2877, 'luckyangler_touch-gameview_netenthtml', 'Lucky Angler Touch', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_luckyangler_touch.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2878, 'bellsonfire-gameview_amatic_mobile', 'Bells On Fire', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_bellsonfire.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2879, 'bjma', 'Multihand Blackjack', 'slot', 'Pragmatic', 'pg', 'wb', 'JUME', 'img/pg/pg_bjma.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2880, 'slotexchange-robobase', 'Robobase', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-robobase.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2881, 'endorphina2_BookOfSanta@ENDORPHINA', 'Book Of Santa', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/BookOfSanta.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2882, '803-gameview_egt_mobile', '20 Super Hot', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_803.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2883, 'luckybells-gameview_amatic_mobile', 'Lucky Bells', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_luckybells.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2884, 'kna', 'Keno', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_kna.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2885, 'slotexchange-egyptian-secrets', 'Egyptian secrets', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-egyptian-secrets.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2886, 'endorphina2_CashTank@ENDORPHINA', 'Cash Tank', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/CashTank.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2887, '546-gameview_egt_mobile', '40 Lucky King', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_546.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2888, 'luckycoin-gameview_amatic_mobile', 'Lucky Coin', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_luckycoin.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2889, 'rla', 'Roulette', 'slot', 'Pragmatic', 'pg', 'wb', 'RULF', 'img/pg/pg_rla.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2890, 'slotexchange-egyptian-secrets', 'Egyptian secrets', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-egyptian-secrets.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2891, 'endorphina2_2020HitSlot@ENDORPHINA', '2020 Hit Slot', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/2020HitSlot.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2892, '844-gameview_egt_mobile', '40 Super Dice', 'slot', 'EGT', 'sl', 'wb', '', 'img/sl_844.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2893, 'wzdlf-gameview_wazdan', 'Lucky Fortune', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdlf.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2894, 'vpa', 'Jacks or Better', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vpa.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2895, 'slotexchange-secrets-of-odalisque', 'Secrets of odalisque', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-secrets-of-odalisque.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2896, 'endorphina2_AusDemTal@ENDORPHINA', 'Aus Dem Tal', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/AusDemTal.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2897, '844-gameview_egt_mobile', '40 Super Dice', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_844.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2898, '849-gameview_egt_mobile', 'Lucky Hot', 'slot', 'EGT', 'sl', 'wb', '', 'img/sl_849.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2899, 'billysgame-gameview_amatic_mobile', 'Billys Game', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_billysgame.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2900, 'vs25pyramid', 'Pyramid King', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs25pyramid.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2901, 'slotexchange-secrets-of-odalisque', 'Secrets of odalisque', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-secrets-of-odalisque.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2902, 'endorphina2_TrollHaven@ENDORPHINA', 'Troll Haven', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/TrollHaven.png', '', 0, 0, 0, 0, 1, 0);
INSERT INTO y_games VALUES
(2903, '804-gameview_egt_mobile', '40 Super Hot', 'slot', 'EGT', 'sl', 'wb', '', 'img/sl_804.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2904, '849-gameview_egt_mobile', 'Lucky Hot', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_849.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2905, 'bluedolphin-gameview_amatic_mobile', 'Blue Dolphin', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_bluedolphin.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2906, 'vs5aztecgems', 'Aztec Gems', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs5aztecgems.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2907, 'endorphina2_WindyCity@ENDORPHINA', 'Windy City', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/WindyCity.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2908, '804-gameview_egt_mobile', '40 Super Hot', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_804.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2909, 'luckyladyscharmdeluxe-gameview_novomatic', 'Lucky Ladys Charm Deluxe', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_luckyladyscharmdeluxe.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2910, 'casanova-gameview_amatic_mobile', 'Casanova', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_casanova.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2911, 'vs5joker', 'Joker\'s Jewels', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs5joker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2912, 'endorphina2_Asgardians@ENDORPHINA', 'Asgardians', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/Asgardians.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2913, '810-gameview_egt_mobile', '5 Dazzling Hot', 'slot', 'EGT', 'sl', 'wb', '', 'img/sl_810.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2914, 'luckyladyscharmdeluxe-gameview_novomatic', 'Lucky Ladys Charm Deluxe', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_luckyladyscharmdeluxe.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2915, 'vs20fruitsw', 'Sweet Bonanza', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs20fruitsw.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2916, 'endorphina2_TheRiseOfAI@ENDORPHINA', 'The Rise of AI', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/TheRiseOfAI.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2917, '810-gameview_egt_mobile', '5 Dazzling Hot', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_810.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2918, 'magicforest-gameview_amatic_mobile', 'Magic Forest', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_magicforest.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2919, 'vs1dragon8', '888 Dragons', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs1dragon8.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2920, 'endorphina2_ChanceMachine100@ENDORPHINA', 'Chance Machine 100', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/ChanceMachine100.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2921, '886-gameview_egt_mobile', '5 Hot Dice', 'slot', 'EGT', 'sl', 'wb', '', 'img/sl_886.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2922, 'wzdmf_2-gameview_wazdan', 'Magic Fruits', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdmf_2.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2923, 'diamondsonfire-gameview_amatic_mobile', 'Diamonds On Fire', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_diamondsonfire.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2924, 'vswaysrhino', 'Great Rhino Megaways', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vswaysrhino.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2925, 'endorphina2_LittlePandaDice@ENDORPHINA', 'Little Panda (Dice)', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/LittlePandaDice.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2926, '886-gameview_egt_mobile', '5 Hot Dice', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_886.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2927, 'wzdmf27-gameview_wazdan', 'Magic Fruits 27', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdmf27.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2928, 'vs1600drago', 'Drago - Jewels of Fortune', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs1600drago.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2929, 'endorphina2_AsgardiansDice@ENDORPHINA', 'Asgardians (Dice)', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/AsgardiansDice.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2930, '812-gameview_egt_mobile', 'Age of Troy', 'slot', 'EGT', 'sl', 'wb', '', 'img/sl_812.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2931, 'wzdmf4_2-gameview_wazdan', 'Magic Fruits 4', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdmf4_2.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2932, 'dragonspearl-gameview_amatic_mobile', 'Dragons Pearl', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_dragonspearl.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2933, 'vs75bronco', 'Bronco Spirit', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs75bronco.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2934, 'endorphina2_AlmightySpartaDice@ENDORPHINA', 'Almighty Sparta (Dice)', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/AlmightySpartaDice.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2935, '812-gameview_egt_mobile', 'Age of Troy', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_812.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2936, 'wzdmf4d-gameview_wazdan', 'Magic Fruits 4 Deluxe', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdmf4d.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2937, 'vs5hotburn', 'Hot to Burn', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs5hotburn.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2938, 'endorphina2_RedCap@ENDORPHINA', 'Red Cap', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/RedCap.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2939, 'alienrobots-gameview_netent', 'Alien Robots', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_alienrobots.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2940, 'wzdmf81-gameview_wazdan', 'Magic Fruits 81', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdmf81.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2941, 'fortunasfruits-gameview_amatic_mobile', 'Fortunas Fruits', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_fortunasfruits.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2942, 'vs1ball', 'Lucky Dragon Ball', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs1ball.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2943, 'tpg-funmonkey', 'FunMonkey', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-funmonkey.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2944, 'endorphina2_ChanceMachine20@ENDORPHINA', 'Chance Machine 20', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/ChanceMachine20.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2945, 'aliens-gameview_netent', 'Aliens', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_aliens.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2946, 'wzdmfd-gameview_wazdan', 'Magic Fruits Deluxe', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdmfd.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2947, 'vs20rhino', 'Great Rhino', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs20rhino.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2948, 'tpg-funmonkey', 'FunMonkey', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-funmonkey.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2949, 'endorphina_4OfAKing@ENDORPHINA', '4 of a King', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/4OfAKing.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2950, 'allwaysfruits-gameview_amatic_mobile', 'Allways Fruits', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_allwaysfruits.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2951, 'wzdmh-gameview_wazdan', 'Magic Hot', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdmh.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2952, 'vs1money', 'Money Money Money', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs1money.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2953, 'truelab-sunstrike', 'Sunstrike', 'slot', 'TrueLab', 'ms', 'mb', '', 'img/mrslotty/truelab-sunstrike.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2954, 'endorphina_BlastBoomBang@ENDORPHINA', 'Blast Boom Bang', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/BlastBoomBang.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2955, 'allwayswin-gameview_amatic_mobile', 'Allways Win', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_allwayswin.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2956, 'wzdmh4_2-gameview_wazdan', 'Magic Hot 4', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdmh4_2.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2957, 'vs10threestar', 'Three Star Fortune', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs10threestar.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2958, 'truelab-sunstrike', 'Sunstrike', 'slot', 'TrueLab', 'ms', 'wb', '', 'img/mrslotty/truelab-sunstrike.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2959, 'endorphina_GemsnStones@ENDORPHINA', 'Gems & Stones', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/GemsStones.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2960, 'alohaclusterpays-gameview_netenthtml', 'Aloha! Cluster Pays', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_alohaclusterpays.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2961, 'wzdmh4d-gameview_wazdan', 'Magic Hot 4 Deluxe', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdmh4d.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2962, 'hot7-gameview_amatic_mobile', 'Hot Seven', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_hot7.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2963, 'vs7fire88', 'Fire 88', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs7fire88.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2964, 'mascot-riot', 'Riot', 'slot', 'Mascot', 'ms', 'mb', '', 'img/mrslotty/mascot-riot.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2965, 'endorphina_MongolTreasures@ENDORPHINA', 'Mongol Treasures', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/MongolTreasures.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2966, 'alwayshotcubes-gameview_novomatic', 'Always Hot Cubes', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_alwayshotcubes.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2967, 'magicidol-gameview_amatic_mobile', 'Magic Idol', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_magicidol.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2968, 'vs10firestrike', 'Fire Strike', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs10firestrike.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2969, 'mascot-riot', 'Riot', 'slot', 'Mascot', 'ms', 'wb', '', 'img/mrslotty/mascot-riot.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2970, 'endorphina_TempleCats@ENDORPHINA', 'Temple Cats', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/TempleCats.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2971, 'alwayshotcubes-gameview_novomatic', 'Always Hot Cubes', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_alwayshotcubes.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2972, 'wzdmotr_2-gameview_wazdan', 'Magic of the Ring', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdmotr_2.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2973, 'vs40wildwest', 'Wild West Gold', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs40wildwest.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2974, 'mascot-gemz_grow', 'Gemz Grow', 'slot', 'Mascot', 'ms', 'mb', '', 'img/mrslotty/mascot-gemz_grow.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2975, 'endorphina_UndinesDeep@ENDORPHINA', 'Undine\'s Deep', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/UndinesDeep.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2976, 'alwayshotdeluxe-gameview_novomatic', 'Always Hot Deluxe', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_alwayshotdeluxe.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2977, 'wzdmotrd-gameview_wazdan', 'Magic of the Ring Deluxe', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdmotrd.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2978, 'vs5ultrab', 'Ultra Burn', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs5ultrab.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2979, 'mascot-gemz_grow', 'Gemz Grow', 'slot', 'Mascot', 'ms', 'wb', '', 'img/mrslotty/mascot-gemz_grow.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2980, 'endorphina_ChimneySweep@ENDORPHINA', 'Chimney Sweep', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/ChimneySweep.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2981, 'alwayshotdeluxe-gameview_novomatic', 'Always Hot Deluxe', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_alwayshotdeluxe.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2982, 'magicowl-gameview_amatic_mobile', 'Magic Owl', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_magicowl.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2983, 'hotneon-gameview_amatic_mobile', 'Hot Neon', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_hotneon.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2984, 'vs15diamond', 'Diamond Strike', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs15diamond.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2985, 'mascot-red_horde', 'Red Horde', 'slot', 'Mascot', 'ms', 'mb', '', 'img/mrslotty/mascot-red_horde.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2986, 'endorphina_IcePirates@ENDORPHINA', 'Ice Pirates', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/IcePirates.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2987, 'angrybirds-gameview_novomatic', 'Angry Birds', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_angrybirds.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2988, 'magicportals-gameview_netent', 'Magic Portals', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_magicportals.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2989, 'hotscatter-gameview_amatic_mobile', 'Hot Scatter', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_hotscatter.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2990, 'vs1fortunetree', 'Tree of Riches', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs1fortunetree.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2991, 'mascot-red_horde', 'Red Horde', 'slot', 'Mascot', 'ms', 'wb', '', 'img/mrslotty/mascot-red_horde.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2992, 'endorphina_Shaman@ENDORPHINA', 'Shaman', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/Shaman.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2993, 'angrybirdschristmas-gameview_novomatic', 'Angry Birds Christmas', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_angrybirdschristmas.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2994, 'wzdms-gameview_wazdan', 'Magic Stars', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdms.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2995, 'hottwenty-gameview_amatic_mobile', 'Hot Twenty', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_hottwenty.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2996, 'vs243dancingpar', 'Dance Party', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs243dancingpar.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2997, 'wazdan-black-horse-deluxe', 'Black Horse Deluxe', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-black-horse-deluxe.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2998, 'endorphina_TheKing@ENDORPHINA', 'The King', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/TheKing.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(2999, 'archangels-gameview_netenthtml', 'Archangels: Salvation', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_archangels.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3000, 'wzdms3-gameview_wazdan', 'Magic Stars 3', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdms3.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3001, 'kingscrown-gameview_amatic_mobile', 'Kings Crown', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_kingscrown.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3002, 'vs20doghouse', 'The Dog House', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs20doghouse.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3003, 'wazdan-black-horse-deluxe', 'Black Horse Deluxe', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-black-horse-deluxe.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3004, 'endorphina_Urartu@ENDORPHINA', 'Urartu', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/Urartu.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3005, 'wzdmt_2-gameview_wazdan', 'Magic Target', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdmt_2.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3006, 'vs1masterjoker', 'Master Joker', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs1masterjoker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3007, 'wazdan-sonic-reels', 'Sonic Reels', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-sonic-reels.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3008, 'endorphina_TheVampires@ENDORPHINA', 'The Vampires', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/TheVampires.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3009, 'asgardianstones-gameview_netenthtml', 'Asgardian Stones', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_asgardianstones.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3010, 'wzdmt2_2-gameview_wazdan', 'Magic Target Deluxe', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdmt2_2.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3011, 'ladyluck-gameview_amatic_mobile', 'Lovely Lady', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_ladyluck.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3012, 'vs10bookoftut', 'Book of Tut', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs10bookoftut.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3013, 'wazdan-sonic-reels', 'Sonic Reels', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-sonic-reels.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3014, 'endorphina_TheEmirate@ENDORPHINA', 'The Emirate', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/TheEmirates.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3015, 'attraction-gameview_netent', 'Attraction', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_attraction.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3016, '830-gameview_egt_mobile', 'Majestic Forest', 'slot', 'EGT', 'sl', 'wb', '', 'img/sl_830.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3017, 'luckybells-gameview_amatic_mobile', 'Lucky Bells', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_luckybells.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3018, 'vs243lionsgold', '5 Lions Gold', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs243lionsgold.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3019, 'wazdan-reel-hero', 'Reel Hero', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-reel-hero.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3020, 'endorphina_Pachamama@ENDORPHINA', 'Pachamama', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/Pachamama.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3021, '511-gameview_egt_mobile', 'Aztec Glory', 'slot', 'EGT', 'sl', 'wb', '', 'img/sl_511.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3022, '830-gameview_egt_mobile', 'Majestic Forest', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_830.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3023, 'luckycoin-gameview_amatic_mobile', 'Lucky Coin', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_luckycoin.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3024, 'vs25wolfgold', 'Wolf Gold', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs25wolfgold.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3025, 'wazdan-reel-hero', 'Reel Hero', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-reel-hero.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3026, 'endorphina_Geisha@ENDORPHINA', 'Geisha', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/Geisha.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3027, '511-gameview_egt_mobile', 'Aztec Glory', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_511.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3028, 'masterofmystery-gameview_netent', 'Master of Mystery', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_masterofmystery.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3029, 'magicforest-gameview_amatic_mobile', 'Magic Forest', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_magicforest.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3030, 'vs243caishien', 'Caishen\'s Cash', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs243caishien.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3031, 'wazdan-9-tigers', '9 Tigers', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-9-tigers.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3032, 'endorphina_FreshFruits@ENDORPHINA', 'Fresh Fruits', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/FreshFruits.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3033, 'wzdbt70_2-gameview_wazdan', 'Back to the 70s', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdbt70_2.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3034, 'magicidol-gameview_amatic_mobile', 'Magic Idol', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_magicidol.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3035, 'vs20eightdragons', '8 Dragons', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs20eightdragons.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3036, 'wazdan-9-tigers', '9 Tigers', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-9-tigers.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3037, 'endorphina_MoreFreshFruits@ENDORPHINA', 'More Fresh Fruits', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/MoreFreshFruits.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3038, 'beach-gameview_netent', 'Beach', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_beach.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3039, 'megajoker-gameview_novomatic', 'Mega Joker', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_megajoker.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3040, 'magicowl-gameview_amatic_mobile', 'Magic Owl', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_magicowl.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3041, 'vs20sbxmas', 'Sweet Bonanza Xmas', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs20sbxmas.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3042, 'wazdan-lucky-9', 'Lucky 9', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-lucky-9.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3043, 'endorphina_SparklingFresh@ENDORPHINA', 'Sparkling Fresh', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/SparklingFresh.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3044, 'wzdbph-gameview_wazdan', 'Beach Party Hot', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdbph.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3045, 'megajoker-gameview_novomatic', 'Mega Joker', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_megajoker.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3046, 'mermaidsgold-gameview_amatic_mobile', 'Mermaids Gold', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_mermaidsgold.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3047, 'vs9madmonkey', 'Monkey Madness', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs9madmonkey.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3048, 'wazdan-lucky-9', 'Lucky 9', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-lucky-9.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3049, 'endorphina_StoneAge@ENDORPHINA', 'Stone Age', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/StoneAge.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3050, 'beartracks-gameview_novomatic', 'Bear Tracks', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_beartracks.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3051, 'mermaidsgold-gameview_amatic_mobile', 'Mermaids Gold', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_mermaidsgold.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3052, 'merryfruits-gameview_amatic_mobile', 'Merry Fruits', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_merryfruits.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3053, 'cs5triple8gold', '888 Gold', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_cs5triple8gold.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3054, 'wazdan-sic-bo-dragons', 'Sic Bo Dragons', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-sic-bo-dragons.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3055, 'endorphina_UltraFresh@ENDORPHINA', 'Ultra Fresh', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/UltraFresh.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3056, 'bellsonfire-gameview_amatic_mobile', 'Bells On Fire', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_bellsonfire.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3057, 'merryfruits-gameview_amatic_mobile', 'Merry Fruits', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_merryfruits.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3058, 'partytime-gameview_amatic_mobile', 'Party Time', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_partytime.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3059, 'vs25mmouse', 'Money Mouse', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs25mmouse.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3060, 'wazdan-sic-bo-dragons', 'Sic Bo Dragons', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-sic-bo-dragons.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3061, 'endorphina_Gladiators@ENDORPHINA', 'Gladiators', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/Gladiators.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3062, 'berryburst-gameview_netenthtml', 'Berry Burst', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_berryburst.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3063, 'vs25scarabqueen', 'John Hunter and the Tomb of the Scarab Queen', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs25scarabqueen.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3064, 'wazdan-infinity-hero', 'Infinity Hero', 'slot', 'Wazdan', 'ms', 'mb', '', 'img/mrslotty/wazdan-infinity-hero.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3065, 'endorphina_Football@ENDORPHINA', 'Football', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/Football.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3066, 'berryburstmax-gameview_netenthtml', 'Berry Burst Max', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_berryburstmax.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3067, 'muse-gameview_netent', 'Muse', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_muse.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3068, 'vs25mustang', 'Mustang Gold', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs25mustang.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3069, 'wazdan-infinity-hero', 'Infinity Hero', 'slot', 'Wazdan', 'ms', 'wb', '', 'img/mrslotty/wazdan-infinity-hero.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3070, 'endorphina_WildFruits@ENDORPHINA', 'Wild Fruits', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/WildFruits.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3071, 'mansion-gameview_netent', 'Mystery at the mansion', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_mansion.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3072, 'vs25goldpig', 'Golden Pig', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs25goldpig.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3073, 'spadegaming-fiery-sevens', 'Fiery Sevens', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-fiery-sevens.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3074, 'endorphina_FairyTale@ENDORPHINA', 'Fairy Tale', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/FairyTale.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3075, 'bigbang-gameview_netent', 'Big Bang', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_bigbang.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3076, 'mysterystar-gameview_novomatic', 'Mystery Star', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_mysterystar.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3077, 'tweetybirds-gameview_amatic_mobile', 'Tweety Birds', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_tweetybirds.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3078, 'vs20kraken', 'Release the Kraken', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs20kraken.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3079, 'spadegaming-fiery-sevens', 'Fiery Sevens', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-fiery-sevens.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3080, 'endorphina_Retromania@ENDORPHINA', 'Retromania', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/Retromania.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3081, 'bigbang_touch-gameview_netenthtml', 'Big Bang Touch', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_bigbang_touch.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3082, 'mythicmaiden-gameview_netent', 'Mythic Maiden', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_mythicmaiden.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3083, 'vs243lions', '5 Lions', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs243lions.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3084, 'evoplay-jelly-boom', 'Jelly Boom', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-jelly-boom.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3085, 'endorphina_Safari@ENDORPHINA', 'Safari', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/Safari.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3086, '861-gameview_egt_mobile', 'Neon Dice', 'slot', 'EGT', 'sl', 'wb', '', 'img/sl_861.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3087, 'ultraseven-gameview_amatic_mobile', 'Ultra Seven', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_ultraseven.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3088, 'vs243fortune', 'Caishen\'s Gold', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs243fortune.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3089, 'evoplay-jelly-boom', 'Jelly Boom', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-jelly-boom.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3090, 'endorphina_Sushi@ENDORPHINA', 'Sushi', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/Sushi.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3091, 'billysgame-gameview_amatic_mobile', 'Billys Game', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_billysgame.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3092, '861-gameview_egt_mobile', 'Neon Dice', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_861.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3093, 'wild7-gameview_amatic_mobile', 'Wild 7', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_wild7.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3094, 'vs1tigers', 'Triple Tigers', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs1tigers.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3095, 'redrake-million-dracula', 'Million Dracula', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-million-dracula.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3096, 'endorphina_Origami@ENDORPHINA', 'Origami', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/Origami.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3097, 'wzdbh_2-gameview_wazdan', 'Black Hawk', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdbh_2.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3098, 'neonstaxx-gameview_netenthtml', 'Neon Staxx', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_neonstaxx.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3099, 'wilddragon-gameview_amatic_mobile', 'Wild Dragon', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_wilddragon.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3100, 'vs5super7', 'Super 7s', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs5super7.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3101, 'redrake-million-dracula', 'Million Dracula', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-million-dracula.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3102, 'endorphina_Macaroons@ENDORPHINA', 'Macaroons', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/Macaroons.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3103, 'wzdbhd-gameview_wazdan', 'Black Hawk Deluxe', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdbhd.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3104, 'wzdnc81-gameview_wazdan', 'Night Club 81', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdnc81.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3105, 'wildshark-gameview_amatic_mobile', 'Wild Shark', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_wildshark.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3106, 'vs25pandagold', 'Panda\'s Fortune', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs25pandagold.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3107, 'fazi-crystalwin', 'Crystal Win', 'slot', 'Fazi', 'ms', 'mb', '', 'img/mrslotty/fazi-crystalwin.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3108, 'endorphina_Minotaur@ENDORPHINA', 'Minotaur', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/Minotaur.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3109, 'bloodsuckers-gameview_netent', 'Blood Suckers', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_bloodsuckers.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3110, 'nrvna-gameview_netent', 'Nrvna', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_nrvna.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3111, 'wolfmoon-gameview_amatic_mobile', 'Wolf Moon', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_wolfmoon.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3112, 'vs10egyptcls', 'Ancient Egypt Classic', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs10egyptcls.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3113, 'fazi-crystalwin', 'Crystal Win', 'slot', 'Fazi', 'ms', 'wb', '', 'img/mrslotty/fazi-crystalwin.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3114, 'endorphina_SatoshisSecret@ENDORPHINA', 'Satoshi\'s Secret', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/SatoshisSecret.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3115, 'bloodsuckers2-gameview_netenthtml', 'Blood Suckers 2', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_bloodsuckers2.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3116, 'nrvna_touch-gameview_netenthtml', 'Nrvna Touch', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_nrvna_touch.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3117, 'bookoffate-gameview_novomatic', 'Book of Fate', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_bookoffate.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3118, 'vs7776aztec', 'Aztec Bonanza', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs7776aztec.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3119, 'allwayspin-AWS_27', 'ALLWAY HEROES', 'slot', 'AllwaySpin', 'ms', 'mb', '', 'img/mrslotty/allwayspin-AWS_27.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3120, 'endorphina_Ninja@ENDORPHINA', 'Ninja', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/Ninja.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3121, 'bloodsuckers_touch-gameview_netenthtml', 'Blood Suckers Touch', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_bloodsuckers_touch.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3122, '819-gameview_egt_mobile', 'Olympus Glory', 'slot', 'EGT', 'sl', 'wb', '', 'img/sl_819.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3123, 'bookofradeluxe6-gameview_novomatic', 'Book of Ra Deluxe 6', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_bookofradeluxe6.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3124, 'vs75empress', 'Golden Beauty', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs75empress.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3125, 'allwayspin-AWS_27', 'ALLWAY HEROES', 'slot', 'AllwaySpin', 'ms', 'wb', '', 'img/mrslotty/allwayspin-AWS_27.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3126, 'endorphina_Vikings@ENDORPHINA', 'Vikings', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/Vikings.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3127, 'bluedolphin-gameview_amatic_mobile', 'Blue Dolphin', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_bluedolphin.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3128, '819-gameview_egt_mobile', 'Olympus Glory', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_819.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3129, 'bookofstars-gameview_novomatic', 'Book of Stars', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_bookofstars.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3130, 'vs25newyear', 'Lucky New Year', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs25newyear.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3131, 'allwayspin-AWS_28', 'OH YA FRUIT', 'slot', 'AllwaySpin', 'ms', 'mb', '', 'img/mrslotty/allwayspin-AWS_28.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3132, 'endorphina_Jetsetter@ENDORPHINA', 'Jetsetter', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/Jetsetter.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3133, 'bollywoodstory-gameview_netenthtml', 'Bollywood Story', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_bollywoodstory.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3134, 'orca-gameview_novomatic', 'Orca', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_orca.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3135, 'captainsbooty-gameview_novomatic', 'Captains Booty', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_captainsbooty.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3136, 'vs5spjoker', 'Super Joker', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs5spjoker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3137, 'allwayspin-AWS_28', 'OH YA FRUIT', 'slot', 'AllwaySpin', 'ms', 'wb', '', 'img/mrslotty/allwayspin-AWS_28.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3138, 'endorphina_Twerk@ENDORPHINA', 'Twerk', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/Twerk.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3139, 'bookofaztec-gameview_amatic_mobile', 'Book of Aztec', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_bookofaztec.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3140, 'orca-gameview_novomatic', 'Orca', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_orca.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3141, 'coinofapollo-gameview_novomatic', 'Coin of Apollo', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_coinofapollo.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3142, 'vs243mwarrior', 'Monkey Warrior', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs243mwarrior.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3143, 'allwayspin-AWS_29', 'BUNNY CIRCUS', 'slot', 'AllwaySpin', 'ms', 'mb', '', 'img/mrslotty/allwayspin-AWS_29.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3144, 'endorphina2_2016Gladiators@ENDORPHINA', '2016 Gladiators', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/2016Gladiators.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3145, 'bookofaztec-gameview_amatic_mobile', 'Book of Aztec', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_bookofaztec.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3146, 'pacificattack-gameview_netent', 'Pacific Attack', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_pacificattack.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3147, 'eyeofthedragon-gameview_novomatic', 'Eye of the Dragon', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_eyeofthedragon.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3148, 'vs4096bufking', 'Buffalo King', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs4096bufking.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3149, 'allwayspin-AWS_29', 'BUNNY CIRCUS', 'slot', 'AllwaySpin', 'ms', 'wb', '', 'img/mrslotty/allwayspin-AWS_29.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3150, 'endorphina2_2027ISS@ENDORPHINA', '2027 ISS', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/2027ISS.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3151, 'bookoffate-gameview_novomatic', 'Book of Fate', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_bookoffate.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3152, 'pandorasbox-gameview_netent', 'Pandoras Box', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_pandorasbox.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3153, 'faust-gameview_novomatic', 'Faust', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_faust.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3154, 'vs10fruity2', 'Extra Juicy', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs10fruity2.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3155, 'allwayspin-AWS_30', 'ETERNAL DINO', 'slot', 'AllwaySpin', 'ms', 'mb', '', 'img/mrslotty/allwayspin-AWS_30.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3156, 'endorphina2_Voodoo@ENDORPHINA', 'Voodoo', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/Voodoo.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3157, 'bookoffortune-gameview_amatic_mobile', 'Book of Fortune', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_bookoffortune.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3158, 'partytime-gameview_amatic_mobile', 'Party Time', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_partytime.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3159, 'gryphonsgolddeluxe-gameview_novomatic', 'Gryphons Gold Deluxe', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_gryphonsgolddeluxe.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3160, 'vs40pirate', 'Pirate Gold', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs40pirate.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3161, 'allwayspin-AWS_30', 'ETERNAL DINO', 'slot', 'AllwaySpin', 'ms', 'wb', '', 'img/mrslotty/allwayspin-AWS_30.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3162, 'endorphina2_Chunjie@ENDORPHINA', 'Chunjie', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/Chunjie.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3163, 'bookoffortune-gameview_amatic_mobile', 'Book of Fortune', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_bookoffortune.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3164, 'pharaohsring-gameview_novomatic', 'Pharaohs Ring', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_pharaohsring.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3165, 'helena-gameview_novomatic', 'Helena', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_helena.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3166, 'vs20vegasmagic', 'Vegas Magic', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs20vegasmagic.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3167, 'mascot-robin_of_loxley', 'Robin of Loxley', 'slot', 'Mascot', 'ms', 'mb', '', 'img/mrslotty/mascot-robin_of_loxley.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3168, 'endorphina2_DiamondVapor@ENDORPHINA', 'Diamond Vapor', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/DiamondVapor.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3169, '826-gameview_egt_mobile', 'Book of Magic', 'slot', 'EGT', 'sl', 'wb', '', 'img/sl_826.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3170, 'pharaohstomb-gameview_novomatic', 'Pharaohs Tomb', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_pharaohstomb.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3171, 'vs25journey', 'Journey to the West', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs25journey.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3172, 'mascot-robin_of_loxley', 'Robin of Loxley', 'slot', 'Mascot', 'ms', 'wb', '', 'img/mrslotty/mascot-robin_of_loxley.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3173, 'endorphina2_Slotomoji@ENDORPHINA', 'Slotomoji', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/Slotomoji.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3174, '826-gameview_egt_mobile', 'Book of Magic', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_826.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3175, 'kingofcards-gameview_novomatic', 'King of Cards', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_kingofcards.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3176, 'vs25chilli', 'Chilli Heat', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs25chilli.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3177, 'platipus-wildspin', 'Wild Spin', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-wildspin.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3178, 'endorphina2_Cuckoo@ENDORPHINA', 'Cuckoo', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/Cuckoo.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3179, 'bookofmaya-gameview_novomatic', 'Book of Maya', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_bookofmaya.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3180, 'piggyriches-gameview_netent', 'Piggy Riches', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_piggyriches.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3181, 'pharaohsring-gameview_novomatic', 'Pharaohs Ring', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_pharaohsring.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3182, 'vs20godiva', 'Lady Godiva', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs20godiva.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3183, 'platipus-wildspin', 'Wild Spin', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-wildspin.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3184, 'endorphina2_InJazz@ENDORPHINA', 'In Jazz', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/InJazz.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3185, 'bookofra-gameview_novomatic', 'Book of Ra', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_bookofra.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3186, 'piggyriches_touch-gameview_netenthtml', 'Piggy Riches Touch', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_piggyriches_touch.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3187, 'plentyontwentyiihot-gameview_novomatic', 'Plenty on Twenty II Hot', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_plentyontwentyiihot.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3188, 'vs25kingdoms', '3 Kingdoms - Battle of Red Cliffs', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs25kingdoms.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3189, 'nsoft-keno', 'Keno', 'slot', 'Nsoft', 'ms', 'mb', '', 'img/mrslotty/nsoft-keno.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3190, 'endorphina2_7up@ENDORPHINA', '7 BONUS UP!', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/7UP.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3191, 'bookofra-gameview_novomatic', 'Book of Ra', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_bookofra.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3192, 'plentyoffruit20-gameview_novomatic', 'Plenty of Fruit 20', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_plentyoffruit20.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3193, 'queencleopatra-gameview_novomatic', 'Queen Cleopatra', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_queencleopatra.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3194, 'vs18mashang', 'Treasure Horse', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs18mashang.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3195, 'nsoft-keno', 'Keno', 'slot', 'Nsoft', 'ms', 'wb', '', 'img/mrslotty/nsoft-keno.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3196, 'endorphina2_Durga@ENDORPHINA', 'Durga', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/Durga.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3197, 'bookofradeluxe-gameview_novomatic', 'Book of Ra Deluxe', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_bookofradeluxe.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3198, 'plentyoffruit20-gameview_novomatic', 'Plenty of Fruit 20', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_plentyoffruit20.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3199, 'redhot20-gameview_novomatic', 'Red Hot 20', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_redhot20.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3200, 'vs9chen', 'Master Chen\'s Fortune', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs9chen.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3201, 'hub88-sky-hunters', 'Sky Hunters', 'slot', 'Kalamba', 'ms', 'mb', '', 'img/mrslotty/hub88-sky-hunters.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3202, 'endorphina2_LittlePanda@ENDORPHINA', 'LittlePanda', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/LittlePanda.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3203, 'bookofradeluxe-gameview_novomatic', 'Book of Ra Deluxe', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_bookofradeluxe.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3204, 'plentyoffruit20hot-gameview_novomatic', 'Plenty of Fruit 20 hot', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_plentyoffruit20hot.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3205, 'redlady-gameview_novomatic', 'Red Lady', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_redlady.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3206, 'vs4096mystery', 'Mysterious', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs4096mystery.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3207, 'hub88-sky-hunters', 'Sky Hunters', 'slot', 'Kalamba', 'ms', 'wb', '', 'img/mrslotty/hub88-sky-hunters.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3208, 'endorphina2_Taboo@ENDORPHINA', 'Taboo', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/Taboo.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3209, 'bookofradeluxe6-gameview_novomatic', 'Book of Ra Deluxe 6', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_bookofradeluxe6.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3210, 'plentyoffruit20hot-gameview_novomatic', 'Plenty of Fruit 20 hot', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_plentyoffruit20hot.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3211, 'roaringwilds-gameview_novomatic', 'Roaring Wilds', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_roaringwilds.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3212, 'vs25wildspells', 'Wild Spells', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs25wildspells.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3213, 'hub88-rct-new-fruit', 'RCT - New Fruit', 'slot', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-rct-new-fruit.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3214, 'endorphina2_Tribe@ENDORPHINA', 'Tribe', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/Tribe.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3215, 'bookofstars-gameview_novomatic', 'Book of Stars', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_bookofstars.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3216, 'plentyoffruit40-gameview_novomatic', 'Plenty of Fruit 40', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_plentyoffruit40.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3217, 'royallotus-gameview_novomatic', 'Royal Lotus', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_royallotus.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3218, 'cs5moneyroll', 'Money Roll', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_cs5moneyroll.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3219, 'hub88-rct-new-fruit', 'RCT - New Fruit', 'slot', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-rct-new-fruit.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3220, 'endorphina2_FootballStar@ENDORPHINA', 'Football Star', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/Footballstar.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3221, 'boombrothers-gameview_netent', 'Boom Brothers', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_boombrothers.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3222, 'plentyoffruit40-gameview_novomatic', 'Plenty of Fruit 40', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_plentyoffruit40.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3223, 'sahara-gameview_novomatic', 'Sahara', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_sahara.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3224, 'vs25goldrush', 'Gold Rush', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs25goldrush.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3225, 'hub88-rct-rio-de-janeiro', 'RCT - Rio de Janeiro', 'slot', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-rct-rio-de-janeiro.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3226, 'endorphina2_LuckyLands@ENDORPHINA', 'Lucky Lands', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/LuckyLands.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3227, '842-gameview_egt_mobile', 'Burning Dice', 'slot', 'EGT', 'sl', 'wb', '', 'img/sl_842.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3228, 'plentyofjewels20hot-gameview_novomatic', 'Plenty of Jewels 20 hot', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_plentyofjewels20hot.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3229, 'sizzlinggems-gameview_novomatic', 'Sizzling Gems', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_sizzlinggems.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3230, 'vs20egypttrs', 'Egyptian Fortunes', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs20egypttrs.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3231, 'hub88-rct-rio-de-janeiro', 'RCT - Rio de Janeiro', 'slot', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-rct-rio-de-janeiro.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3232, 'endorphina2_Kamchatka@ENDORPHINA', 'Kamchatka', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/Kamchatka.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3233, '842-gameview_egt_mobile', 'Burning Dice', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_842.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3234, 'therealkinggoldrecords-gameview_novomatic', 'The Real King Gold Records', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_therealkinggoldrecords.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3235, 'vs3train', 'Gold Train', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs3train.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3236, 'hub88-rct-reis-do-egito', 'RCT - Reis do Egito', 'slot', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-rct-reis-do-egito.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3237, 'endorphina2_DiaDeMuertos@ENDORPHINA', 'Dia De Muertos', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/DiaDeLosMuertos.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3238, '801-gameview_egt_mobile', 'Burning Hot', 'slot', 'EGT', 'sl', 'wb', '', 'img/sl_801.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3239, 'plentyontwenty-gameview_novomatic', 'Plenty on Twenty', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_plentyontwenty.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3240, 'vs25dragonkingdom', 'Dragon Kingdom', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs25dragonkingdom.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3241, 'hub88-rct-reis-do-egito', 'RCT - Reis do Egito', 'slot', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-rct-reis-do-egito.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3242, 'endorphina2_1LuckyStreak@ENDORPHINA', 'Lucky Streak 1', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/LuckyStreak1.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3243, '801-gameview_egt_mobile', 'Burning Hot', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_801.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3244, 'plentyontwenty-gameview_novomatic', 'Plenty on Twenty', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_plentyontwenty.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3245, 'vs5trdragons', 'Triple Dragons', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs5trdragons.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3246, 'bgaming-ElvisFrogInVegas', 'Elvis Frog in Vegas', 'slot', 'BGaming', 'ms', 'mb', '', 'img/mrslotty/bgaming-ElvisFrogInVegas.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3247, 'endorphina2_SugarGlider@ENDORPHINA', 'Sugar Glider', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/SugarGlider.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3248, 'wzdbr_2-gameview_wazdan', 'Burning Reels', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdbr_2.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3249, 'plentyontwentyiihot-gameview_novomatic', 'Plenty on Twenty II Hot', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_plentyontwentyiihot.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3250, 'vs50kingkong', 'Mighty Kong', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs50kingkong.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3251, 'bgaming-ElvisFrogInVegas', 'Elvis Frog in Vegas', 'slot', 'BGaming', 'ms', 'wb', '', 'img/mrslotty/bgaming-ElvisFrogInVegas.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3252, 'endorphina2_2LuckyStreak@ENDORPHINA', 'Lucky Streak 2', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/LuckyStreak2.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3253, 'wzdbs-gameview_wazdan', 'Burning Stars', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdbs.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3254, 'powerstars-gameview_novomatic', 'Power Stars', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_powerstars.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3255, 'vs9hotroll', 'Hot Chilli', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs9hotroll.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3256, 'tpg-lucky-leprechaun', 'Lucky Leprechaun', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-lucky-leprechaun.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3257, 'endorphina2_LuxuryLife@ENDORPHINA', 'Luxury Life', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/LuxuryLife.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3258, 'butterflystaxx-gameview_netenthtml', 'Butterfly Staxx', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_butterflystaxx.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3259, 'pyramid-gameview_netenthtml', 'Pyramid', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_pyramid.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3260, 'vs25asgard', 'Asgard', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs25asgard.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3261, 'tpg-lucky-leprechaun', 'Lucky Leprechaun', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-lucky-leprechaun.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3262, 'endorphina2_3LuckyStreak@ENDORPHINA', 'Lucky Streak 3', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/LuckyStreak3.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3263, 'captainsbooty-gameview_novomatic', 'Captains Booty', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_captainsbooty.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3264, 'queencleopatra-gameview_novomatic', 'Queen Cleopatra', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_queencleopatra.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3265, 'vs20chicken', 'The Great Chicken Escape', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs20chicken.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3266, 'truelab-dan', 'Day and Night', 'slot', 'TrueLab', 'ms', 'mb', '', 'img/mrslotty/truelab-dan.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3267, 'endorphina2_AncientTroy@ENDORPHINA', 'Ancient Troy', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/AncientTroy.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3268, 'casanova-gameview_amatic_mobile', 'Casanova', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_casanova.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3269, 'randomrunner15-gameview_novomatic', 'Random Runner 15', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_randomrunner15.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3270, 'vs25safari', 'Hot Safari', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs25safari.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3271, 'truelab-dan', 'Day and Night', 'slot', 'TrueLab', 'ms', 'wb', '', 'img/mrslotty/truelab-dan.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3272, 'endorphina2_AncientTroyDice@ENDORPHINA', 'Ancient Troy (Dice)', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/AncientTroyDice.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3273, '850-gameview_egt_mobile', 'Casino Mania', 'slot', 'EGT', 'sl', 'wb', '', 'img/sl_850.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3274, 'cs3irishcharms', 'Irish Charms', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_cs3irishcharms.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3275, 'allwayspin-AWS_31', 'ALCHEMY', 'slot', 'AllwaySpin', 'ms', 'mb', '', 'img/mrslotty/allwayspin-AWS_31.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3276, 'endorphina2_1LuckyStreakDice@ENDORPHINA', 'Lucky Streak 1 (Dice)', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/LuckyDice1.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3277, '850-gameview_egt_mobile', 'Casino Mania', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_850.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3278, 'redhot20-gameview_novomatic', 'Red Hot 20', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_redhot20.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3279, 'vs1024lionsd', '5 Lions Dance', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs1024lionsd.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3280, 'allwayspin-AWS_31', 'ALCHEMY', 'slot', 'AllwaySpin', 'ms', 'wb', '', 'img/mrslotty/allwayspin-AWS_31.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3281, 'endorphina2_2LuckyStreakDice@ENDORPHINA', 'Lucky Streak 2 (Dice)', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/LuckyDice2.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3282, 'redlady-gameview_novomatic', 'Red Lady', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_redlady.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3283, 'vs9aztecgemsdx', 'Aztec Gems Deluxe', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs9aztecgemsdx.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3284, 'allwayspin-AWS_32', 'GREEN LEPRECHAUN', 'slot', 'AllwaySpin', 'ms', 'mb', '', 'img/mrslotty/allwayspin-AWS_32.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3285, 'endorphina2_3LuckyStreakDice@ENDORPHINA', 'Lucky Streak 3 (Dice)', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/LuckyDice3.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3286, 'cindereela-gameview_novomatic', 'Cindereela', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_cindereela.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3287, 'reelking-gameview_novomatic', 'Reel King', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_reelking.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3288, 'vswayswerewolf', 'Curse of the Werewolf Megaways', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vswayswerewolf.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3289, 'allwayspin-AWS_32', 'GREEN LEPRECHAUN', 'slot', 'AllwaySpin', 'ms', 'wb', '', 'img/mrslotty/allwayspin-AWS_32.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3290, 'endorphina2_VoodooDice@ENDORPHINA', 'Voodoo (Dice)', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/VoodooDice.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3291, 'coinofapollo-gameview_novomatic', 'Coin of Apollo', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_coinofapollo.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3292, 'reelrush-gameview_netent', 'Reel Rush', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_reelrush.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3293, 'vs20eking', 'Emerald King', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs20eking.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3294, 'evoplay-forest-dreams', 'Forest Dreams', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-forest-dreams.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3295, 'endorphina2_AlmightySparta@ENDORPHINA', 'Almighty Sparta', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/AlmightySparta.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3296, 'wzdctc_2-gameview_wazdan', 'Colin the Cat', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdctc_2.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3297, 'reelrush_touch-gameview_netenthtml', 'Reel Rush Touch', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_reelrush_touch.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3298, 'vs20goldfever', 'Gems Bonanza', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs20goldfever.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3299, 'evoplay-forest-dreams', 'Forest Dreams', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-forest-dreams.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3300, 'endorphina2_MysteryEldorado@ENDORPHINA', 'Mystery of Eldorado', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/MysteryEldorado.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3301, 'columbusdeluxe-gameview_novomatic', 'Columbus Deluxe', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_columbusdeluxe.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3302, 'reelsteal-gameview_netent', 'Reel Steal', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_reelsteal.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3303, 'vs20rhinoluxe', 'Great Rhino Deluxe', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs20rhinoluxe.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3304, 'slotexchange-deadly-battle', 'Deadly battle', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-deadly-battle.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3305, 'endorphina2_SugarGliderDice@ENDORPHINA', 'Sugar Glider (Dice)', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/SugarGliderDice.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3306, 'columbusdeluxe-gameview_novomatic', 'Columbus Deluxe', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_columbusdeluxe.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3307, 'reelsteal_touch-gameview_netenthtml', 'Reel Steal Touch', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_reelsteal_touch.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3308, 'vs20gorilla', 'Jungle Gorilla', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs20gorilla.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3309, 'slotexchange-deadly-battle', 'Deadly battle', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-deadly-battle.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3310, 'endorphina_JokerWild@ENDORPHINA', 'Joker Wild', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/JokerWild.png', '', 0, 0, 0, 0, 1, 0);
INSERT INTO y_games VALUES
(3311, 'copycats-gameview_netenthtml', 'Copy Cats', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_copycats.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3312, '806-gameview_egt_mobile', 'Rise of Ra', 'slot', 'EGT', 'sl', 'wb', '', 'img/sl_806.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3313, 'vs20pblinders', 'Peaky Blinders', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs20pblinders.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3314, 'slotexchange-african-savannah', 'African savannah', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-african-savannah.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3315, 'endorphina_DeucesWild@ENDORPHINA', 'Deuces Wild', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/DeucesWild.png', '', 0, 0, 0, 0, 1, 0);
INSERT INTO y_games VALUES
(3316, '514-gameview_egt_mobile', 'Coral Island', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_514.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3317, '806-gameview_egt_mobile', 'Rise of Ra', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_806.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3318, 'vs25samurai', 'Rise of Samurai', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs25samurai.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3319, 'slotexchange-african-savannah', 'African savannah', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-african-savannah.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3320, 'endorphina_JacksOrBetter@ENDORPHINA', 'Jacks Or Better', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/JacksOrBetter.png', '', 0, 0, 0, 0, 1, 0);
INSERT INTO y_games VALUES
(3321, 'wzdcrm-gameview_wazdan', 'Corrida Romance', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdcrm.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3322, 'roaringforties-gameview_novomatic', 'Roaring Forties', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_roaringforties.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3323, 'vswayshive', 'Star Bounty', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vswayshive.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3324, 'slotexchange-battle-bot', 'Battle bot', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-battle-bot.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3325, 'endorphina2_BookOfSanta@ENDORPHINA', 'Book Of Santa', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/BookOfSanta.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3326, 'creaturefromtheblacklagoon-gameview_netent', 'Creature From The Black Lagoon', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_creaturefromtheblacklagoon.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3327, 'roaringforties-gameview_novomatic', 'Roaring Forties', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_roaringforties.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3328, 'vswaysdogs', 'The Dog House Megaways', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vswaysdogs.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3329, 'slotexchange-battle-bot', 'Battle bot', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-battle-bot.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3330, 'endorphina2_CashTank@ENDORPHINA', 'Cash Tank', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/CashTank.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3331, 'crimescene-gameview_netent', 'Crime Scene', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_crimescene.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3332, 'roaringwilds-gameview_novomatic', 'Roaring Wilds', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_roaringwilds.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3333, 'vs25tigerwar', 'The Tiger Warrior', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs25tigerwar.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3334, 'slotexchange-gold-joust', 'Gold joust', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-gold-joust.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3335, 'endorphina2_2020HitSlot@ENDORPHINA', '2020 Hit Slot', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/2020HitSlot.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3336, 'crusadeoffortune-gameview_netent', 'Crusade of Fortune', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_crusadeoffortune.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3337, 'robinhood-gameview_netent', 'Robin Hood', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_robinhood.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3338, 'vs40madwheel', 'The Wild Machine', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs40madwheel.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3339, 'slotexchange-gold-joust', 'Gold joust', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-gold-joust.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3340, 'endorphina2_AusDemTal@ENDORPHINA', 'Aus Dem Tal', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/AusDemTal.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3341, '860-gameview_egt_mobile', 'Rolling Dice', 'slot', 'EGT', 'sl', 'wb', '', 'img/sl_860.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3342, 'vs25pyramid', 'Pyramid King', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs25pyramid.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3343, 'vs5ultra', 'Ultra Hold and Spin', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs5ultra.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3344, 'slotexchange-stone-captivity', 'Stone captivity', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-stone-captivity.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3345, 'endorphina2_TrollHaven@ENDORPHINA', 'Troll Haven', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/TrollHaven.png', '', 0, 0, 0, 0, 1, 0);
INSERT INTO y_games VALUES
(3346, 'dazzleme-gameview_netent', 'Dazzle Me', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_dazzleme.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3347, '860-gameview_egt_mobile', 'Rolling Dice', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_860.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3348, 'vs5aztecgems', 'Aztec Gems', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs5aztecgems.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3349, 'vs25walker', 'Wild Walker', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs25walker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3350, 'slotexchange-stone-captivity', 'Stone captivity', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-stone-captivity.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3351, 'endorphina2_WindyCity@ENDORPHINA', 'Windy City', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/WindyCity.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3352, 'dazzlingdiamonds-gameview_novomatic', 'Dazzling Diamonds', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_dazzlingdiamonds.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3353, '883-gameview_egt_mobile', 'Royal Gardens', 'slot', 'EGT', 'sl', 'wb', '', 'img/sl_883.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3354, 'vs5joker', 'Joker\'s Jewels', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs5joker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3355, 'vs576treasures', 'Wild Wild Riches', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs576treasures.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3356, 'slotexchange-pool-game', 'Pool game', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-pool-game.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3357, 'endorphina2_Asgardians@ENDORPHINA', 'Asgardians', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/Asgardians.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3358, 'dazzlingdiamonds-gameview_novomatic', 'Dazzling Diamonds', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_dazzlingdiamonds.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3359, '883-gameview_egt_mobile', 'Royal Gardens', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_883.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3360, 'vs20fruitsw', 'Sweet Bonanza', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs20fruitsw.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3361, 'vs10vampwolf', 'Vampires vs Wolves', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs10vampwolf.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3362, 'slotexchange-pool-game', 'Pool game', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-pool-game.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3363, 'endorphina2_TheRiseOfAI@ENDORPHINA', 'The Rise of AI', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/TheRiseOfAI.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3364, 'deadoralive_touch-gameview_netenthtml', 'Dead of Alive Touch', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_deadoralive_touch.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3365, 'royallotus-gameview_novomatic', 'Royal Lotus', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_royallotus.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3366, 'vs1dragon8', '888 Dragons', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs1dragon8.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3367, 'cs3w', 'Diamonds are Forever 3 Lines', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_cs3w.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3368, 'fazi-crystalhot40deluxe', 'Crystal Hot 40 Deluxe', 'slot', 'Fazi', 'ms', 'mb', '', 'img/mrslotty/fazi-crystalhot40deluxe.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3369, 'endorphina2_ChanceMachine100@ENDORPHINA', 'Chance Machine 100', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/ChanceMachine100.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3370, 'deadoralive-gameview_netent', 'Dead or Alive', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_deadoralive.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3371, '809-gameview_egt_mobile', 'Royal Secrets', 'slot', 'EGT', 'sl', 'wb', '', 'img/sl_809.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3372, 'vswaysrhino', 'Great Rhino Megaways', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vswaysrhino.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3373, 'vs243fortseren', 'Greek Gods', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs243fortseren.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3374, 'fazi-crystalhot40deluxe', 'Crystal Hot 40 Deluxe', 'slot', 'Fazi', 'ms', 'wb', '', 'img/mrslotty/fazi-crystalhot40deluxe.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3375, 'endorphina2_LittlePandaDice@ENDORPHINA', 'Little Panda (Dice)', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/LittlePandaDice.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3376, 'demolitionsquad-gameview_netent', 'Demolition Squad', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_demolitionsquad.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3377, '809-gameview_egt_mobile', 'Royal Secrets', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_809.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3378, 'vs1600drago', 'Drago - Jewels of Fortune', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs1600drago.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3379, 'vs20bl', 'Busy Bees', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs20bl.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3380, 'fazi-crystalhot80', 'Crystal Hot 80', 'slot', 'Fazi', 'ms', 'mb', '', 'img/mrslotty/fazi-crystalhot80.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3381, 'endorphina2_AsgardiansDice@ENDORPHINA', 'Asgardians (Dice)', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/AsgardiansDice.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3382, 'wzddj27-gameview_wazdan', 'Demon Jack 27', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzddj27.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3383, 'sahara-gameview_novomatic', 'Sahara', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_sahara.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3384, 'vs75bronco', 'Bronco Spirit', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs75bronco.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3385, 'vs20honey', 'Honey Honey Honey', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs20honey.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3386, 'fazi-crystalhot80', 'Crystal Hot 80', 'slot', 'Fazi', 'ms', 'wb', '', 'img/mrslotty/fazi-crystalhot80.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3387, 'endorphina2_AlmightySpartaDice@ENDORPHINA', 'Almighty Sparta (Dice)', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/AlmightySpartaDice.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3388, 'devilsdelight-gameview_netent', 'Devils Delight', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_devilsdelight.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3389, 'scruffyduck-gameview_netenthtml', 'Scruffy Duck', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_scruffyduck.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3390, 'vs5hotburn', 'Hot to Burn', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs5hotburn.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3391, 'vs7776secrets', 'Aztec Treasure', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs7776secrets.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3392, 'fazi-luckytwister', 'Lucky Twister', 'slot', 'Fazi', 'ms', 'mb', '', 'img/mrslotty/fazi-luckytwister.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3393, 'endorphina2_RedCap@ENDORPHINA', 'Red Cap', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/RedCap.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3394, 'diamond7-gameview_novomatic', 'Diamond 7', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_diamond7.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3395, 'seasirens-gameview_novomatic', 'Sea Sirens', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_seasirens.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3396, 'vs1ball', 'Lucky Dragon Ball', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs1ball.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3397, 'vs20fruitparty', 'Fruit Party', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs20fruitparty.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3398, 'fazi-luckytwister', 'Lucky Twister', 'slot', 'Fazi', 'ms', 'wb', '', 'img/mrslotty/fazi-luckytwister.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3399, 'endorphina2_ChanceMachine20@ENDORPHINA', 'Chance Machine 20', 'slot', 'Endorphina', 'ed', 'mb', '', 'img/ed/ChanceMachine20.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3400, 'vs20rhino', 'Great Rhino', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs20rhino.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3401, 'vs1fufufu', 'Fu Fu Fu', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs1fufufu.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3402, 'kiron-steeple-chase', 'Steeple Chase', 'slot', 'Kiron', 'ms', 'mb', '', 'img/mrslotty/kiron-steeple-chase.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3403, 'diamonddogs-gameview_netent', 'Diamond Dogs', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_diamonddogs.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3404, 'vs1money', 'Money Money Money', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs1money.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3405, 'vs117649starz', 'Starz Megaways', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs117649starz.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3406, 'kiron-steeple-chase', 'Steeple Chase', 'slot', 'Kiron', 'ms', 'wb', '', 'img/mrslotty/kiron-steeple-chase.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3407, 'diamondsonfire-gameview_amatic_mobile', 'Diamonds On Fire', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_diamondsonfire.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3408, 'secretcode-gameview_netent', 'Secret Code', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_secretcode.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3409, 'vs10threestar', 'Three Star Fortune', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs10threestar.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3410, 'vs40streetracer', 'Street Racer', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs40streetracer.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3411, 'tomhorn-243-crystal-fruits-reversed', '243 Crystal Fruits Reversed', 'slot', 'Tomhorn', 'ms', 'mb', '', 'img/mrslotty/tomhorn-243-crystal-fruits-reversed.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3412, '824-gameview_egt_mobile', 'Dice and Roll', 'slot', 'EGT', 'sl', 'wb', '', 'img/sl_824.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3413, 'secretofthestones-gameview_netent', 'Secret of the Stones', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_secretofthestones.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3414, 'vs7fire88', 'Fire 88', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs7fire88.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3415, 'vs8magicjourn', 'Magic Journey', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs8magicjourn.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3416, 'tomhorn-243-crystal-fruits-reversed', '243 Crystal Fruits Reversed', 'slot', 'Tomhorn', 'ms', 'wb', '', 'img/mrslotty/tomhorn-243-crystal-fruits-reversed.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3417, '824-gameview_egt_mobile', 'Dice and Roll', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_824.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3418, 'secretofthestones_touch-gameview_netenthtml', 'Secret of the Stones Touch', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_secretofthestones_touch.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3419, 'vs10firestrike', 'Fire Strike', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs10firestrike.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3420, 'vs20hercpeg', 'Hercules and Pegasus', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs20hercpeg.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3421, 'platipus-dynastywarriors', 'Dynasty Warriors', 'slot', 'Platipus', 'ms', 'mb', '', 'img/mrslotty/platipus-dynastywarriors.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3422, '856-gameview_egt_mobile', 'Dice of Ra', 'slot', 'EGT', 'sl', 'wb', '', 'img/sl_856.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3423, 'secretsofatlantis-gameview_netenthtml', 'Secrets of Atlantis', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_secretsofatlantis.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3424, 'vs40wildwest', 'Wild West Gold', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs40wildwest.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3425, 'vs25peking', 'Peking Luck', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs25peking.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3426, 'platipus-dynastywarriors', 'Dynasty Warriors', 'slot', 'Platipus', 'ms', 'wb', '', 'img/mrslotty/platipus-dynastywarriors.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3427, '856-gameview_egt_mobile', 'Dice of Ra', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_856.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3428, 'secretsofchristmas-gameview_netenthtml', 'Secrets of Christmas', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_secretsofchristmas.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3429, 'vs5ultrab', 'Ultra Burn', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs5ultrab.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3430, 'vs40frrainbow', 'Fruit Rainbow', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs40frrainbow.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3431, 'hub88-beers-on-reels', 'Beers on Reels', 'slot', 'Kalamba', 'ms', 'mb', '', 'img/mrslotty/hub88-beers-on-reels.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3432, 'wzddr81_2-gameview_wazdan', 'Dino Reels 81', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzddr81_2.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3433, 'secretsofhorus-gameview_netent', 'Secrets of Horus', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_secretsofhorus.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3434, 'vs15diamond', 'Diamond Strike', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs15diamond.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3435, 'vs50chinesecharms', 'Lucky Dragons', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs50chinesecharms.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3436, 'hub88-beers-on-reels', 'Beers on Reels', 'slot', 'Kalamba', 'ms', 'wb', '', 'img/mrslotty/hub88-beers-on-reels.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3437, 'discospins-gameview_netent', 'Disco Spins', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_discospins.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3438, 'vs1fortunetree', 'Tree of Riches', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs1fortunetree.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3439, 'vs50amt', 'Aladdin\'s Treasure', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs50amt.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3440, 'hub88-banana-keno', 'Banana Keno', 'slot', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-banana-keno.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3441, 'sharky-gameview_novomatic', 'Sharky', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_sharky.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3442, 'vs243dancingpar', 'Dance Party', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs243dancingpar.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3443, 'vs50pixie', 'Pixie Wings', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs50pixie.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3444, 'hub88-banana-keno', 'Banana Keno', 'slot', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-banana-keno.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3445, 'dolphinspearldeluxe-gameview_novomatic', 'Dolphins Pearl Deluxe', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_dolphinspearldeluxe.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3446, '831-gameview_egt_mobile', 'Shining Crown', 'slot', 'EGT', 'sl', 'wb', '', 'img/sl_831.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3447, 'vs20doghouse', 'The Dog House', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs20doghouse.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3448, 'vs7monkeys', '7 Monkeys', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs7monkeys.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3449, 'hub88-jungle-keno', 'Jungle Keno', 'slot', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-jungle-keno.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3450, 'dolphinspearldeluxe-gameview_novomatic', 'Dolphins Pearl Deluxe', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_dolphinspearldeluxe.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3451, '831-gameview_egt_mobile', 'Shining Crown', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_831.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3452, 'vs1masterjoker', 'Master Joker', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs1masterjoker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3453, 'vs20cm', 'Sugar Rush', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs20cm.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3454, 'hub88-jungle-keno', 'Jungle Keno', 'slot', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-jungle-keno.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3455, 'dolphinspearl-gameview_novomatic', 'Dolphins Pearl', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_dolphinspearl.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3456, 'silentrun-gameview_netent', 'Silent Run', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_silentrun.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3457, 'vs10bookoftut', 'Book of Tut', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs10bookoftut.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3458, 'vs10madame', 'Madame Destiny', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs10madame.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3459, 'hub88-magical-keno', 'Magical Keno', 'slot', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-magical-keno.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3460, 'dolphinspearl-gameview_novomatic', 'Dolphins Pearl', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_dolphinspearl.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3461, 'simsalabim-gameview_netent', 'Simsalabim', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_simsalabim.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3462, 'vs243lionsgold', '5 Lions Gold', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs243lionsgold.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3463, 'vs5trjokers', 'Triple Jokers', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs5trjokers.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3464, 'hub88-magical-keno', 'Magical Keno', 'slot', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-magical-keno.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3465, 'doublestacks-gameview_netenthtml', 'Double Stacks', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_doublestacks.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3466, 'wzds777_2-gameview_wazdan', 'Sizzling 777', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzds777_2.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3467, 'vs25wolfgold', 'Wolf Gold', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs25wolfgold.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3468, 'vs7pigs', '7 Piggies', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs7pigs.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3469, 'apollo-sizzlefire', 'Sizzle Fire', 'slot', 'Apollo', 'ms', 'mb', '', 'img/mrslotty/apollo-sizzlefire.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3470, 'dracula-gameview_netent', 'Dracula', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_dracula.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3471, 'wzds777d-gameview_wazdan', 'Sizzling 777 Deluxe', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzds777d.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3472, 'vs243caishien', 'Caishen\'s Cash', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs243caishien.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3473, 'vs15fairytale', 'Fairytale Fortune', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs15fairytale.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3474, 'apollo-sizzlefire', 'Sizzle Fire', 'slot', 'Apollo', 'ms', 'wb', '', 'img/mrslotty/apollo-sizzlefire.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3475, 'dracula_touch-gameview_netenthtml', 'Dracula Touch', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_dracula_touch.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3476, 'sizzlinggems-gameview_novomatic', 'Sizzling Gems', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_sizzlinggems.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3477, 'vs20eightdragons', '8 Dragons', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs20eightdragons.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3478, 'vs25dwarves_new', 'Dwarven Gold Deluxe', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs25dwarves_new.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3479, 'apollo-slotbirds81', 'Slot Birds 81', 'slot', 'Apollo', 'ms', 'mb', '', 'img/mrslotty/apollo-slotbirds81.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3480, 'wzddc-gameview_wazdan', 'Draculas Castle', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzddc.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3481, 'sizzlinghotdeluxe-gameview_novomatic', 'Sizzling Hot Deluxe', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_sizzlinghotdeluxe.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3482, 'vs20sbxmas', 'Sweet Bonanza Xmas', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs20sbxmas.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3483, 'vs25sea', 'Great Reef', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs25sea.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3484, 'apollo-slotbirds81', 'Slot Birds 81', 'slot', 'Apollo', 'ms', 'wb', '', 'img/mrslotty/apollo-slotbirds81.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3485, 'dragonisland-gameview_netent', 'Dragon Island', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_dragonisland.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3486, 'sizzlinghotdeluxe-gameview_novomatic', 'Sizzling Hot Deluxe', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_sizzlinghotdeluxe.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3487, 'vs9madmonkey', 'Monkey Madness', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs9madmonkey.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3488, 'vs25davinci', 'Da Vinci\'s Treasure', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs25davinci.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3489, 'apollo-occultum81', 'Occultum 81', 'slot', 'Apollo', 'ms', 'mb', '', 'img/mrslotty/apollo-occultum81.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3490, 'sizzling6-gameview_novomatic', 'Sizzling6', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_sizzling6.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3491, 'cs5triple8gold', '888 Gold', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_cs5triple8gold.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3492, 'vs40beowulf', 'Beowulf', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs40beowulf.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3493, 'apollo-occultum81', 'Occultum 81', 'slot', 'Apollo', 'ms', 'wb', '', 'img/mrslotty/apollo-occultum81.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3494, 'dragonspearl-gameview_amatic_mobile', 'Dragons Pearl', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_dragonspearl.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3495, 'sizzling6-gameview_novomatic', 'Sizzling6', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_sizzling6.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3496, 'vs25mmouse', 'Money Mouse', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs25mmouse.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3497, 'vs10egypt', 'Ancient Egypt', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs10egypt.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3498, 'apollo-bonusjoker', 'Bonus Joker', 'slot', 'Apollo', 'ms', 'mb', '', 'img/mrslotty/apollo-bonusjoker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3499, 'drivemultipliermayhem-gameview_netenthtml', 'Drive Multiplier Mayhem', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_drivemultipliermayhem.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3500, 'southpark-gameview_netent', 'South Park', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_southpark.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3501, 'vs25scarabqueen', 'John Hunter and the Tomb of the Scarab Queen', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs25scarabqueen.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3502, 'vs20leprexmas', 'Leprechaun Carol', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs20leprexmas.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3503, 'apollo-bonusjoker', 'Bonus Joker', 'slot', 'Apollo', 'ms', 'wb', '', 'img/mrslotty/apollo-bonusjoker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3504, 'spacewars-gameview_netent', 'Space Wars', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_spacewars.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3505, 'vs25mustang', 'Mustang Gold', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs25mustang.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3506, 'vs20wildpix', 'Wild Pixies', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs20wildpix.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3507, 'apollo-turboslots81', 'Turbo Slots 81', 'slot', 'Apollo', 'ms', 'mb', '', 'img/mrslotty/apollo-turboslots81.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3508, 'spacewars_touch-gameview_netenthtml', 'Space Wars Touch', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_spacewars_touch.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3509, 'vs25goldpig', 'Golden Pig', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs25goldpig.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3510, 'vs25queenofgold', 'Queen of Gold', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs25queenofgold.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3511, 'apollo-turboslots81', 'Turbo Slots 81', 'slot', 'Apollo', 'ms', 'wb', '', 'img/mrslotty/apollo-turboslots81.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3512, 'sparks-gameview_netenthtml', 'Sparks', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_sparks.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3513, 'vs20kraken', 'Release the Kraken', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs20kraken.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3514, 'vs50aladdin', '3 Genie Wishes', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs50aladdin.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3515, 'apollo-slotjoker', 'Slot Joker', 'slot', 'Apollo', 'ms', 'mb', '', 'img/mrslotty/apollo-slotjoker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3516, 'eggomatic_touch-gameview_netenthtml', 'Eggomatic Touch', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_eggomatic_touch.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3517, 'spellcast-gameview_netent', 'Spellcast', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_spellcast.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3518, 'vs243lions', '5 Lions', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs243lions.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3519, 'vs25vegas', 'Vegas Nights', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs25vegas.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3520, 'apollo-slotjoker', 'Slot Joker', 'slot', 'Apollo', 'ms', 'wb', '', 'img/mrslotty/apollo-slotjoker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3521, 'egyptianheroes-gameview_netent', 'Egyptian Heroes', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_egyptianheroes.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3522, 'spellcast_touch-gameview_netenthtml', 'Spellcast Touch', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_spellcast_touch.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3523, 'vs243fortune', 'Caishen\'s Gold', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs243fortune.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3524, 'vs1024butterfly', 'Jade Butterfly', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs1024butterfly.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3525, 'apollo-crystalminers', 'Crystal Miners', 'slot', 'Apollo', 'ms', 'mb', '', 'img/mrslotty/apollo-crystalminers.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3526, 'elements-gameview_netent', 'Elements', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_elements.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3527, 'spinatagrande-gameview_netent', 'Spinata Grande', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_spinatagrande.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3528, 'vs1tigers', 'Triple Tigers', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs1tigers.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3529, 'vs20santa', 'Santa', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs20santa.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3530, 'apollo-crystalminers', 'Crystal Miners', 'slot', 'Apollo', 'ms', 'wb', '', 'img/mrslotty/apollo-crystalminers.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3531, 'emojiplanet-gameview_netenthtml', 'Emoji Planet', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_emojiplanet.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3532, 'spinatagrande_touch-gameview_netenthtml', 'Spinata Grande Touch', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_spinatagrande_touch.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3533, 'vs5super7', 'Super 7s', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs5super7.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3534, 'vs25gladiator', 'Wild Gladiator', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs25gladiator.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3535, 'apollo-smilingjoker', 'Smiling Joker', 'slot', 'Apollo', 'ms', 'mb', '', 'img/mrslotty/apollo-smilingjoker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3536, 'vs25pandagold', 'Panda\'s Fortune', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs25pandagold.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3537, 'vs13ladyofmoon', 'Lady of the Moon', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs13ladyofmoon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3538, 'apollo-smilingjoker', 'Smiling Joker', 'slot', 'Apollo', 'ms', 'wb', '', 'img/mrslotty/apollo-smilingjoker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3539, 'evolution-gameview_netent', 'Evolution', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_evolution.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3540, 'starburst-gameview_netent', 'Starburst', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_starburst.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3541, 'vs10egyptcls', 'Ancient Egypt Classic', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs10egyptcls.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3542, 'vs20rome', 'Glorious Rome', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs20rome.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3543, 'apollo-winandrace', 'Winand Race', 'slot', 'Apollo', 'ms', 'mb', '', 'img/mrslotty/apollo-winandrace.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3544, 'excalibur-gameview_netent', 'Excalibur', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_excalibur.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3545, 'starburst_touch-gameview_netenthtml', 'Starburst Touch', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_starburst_touch.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3546, 'vs7776aztec', 'Aztec Bonanza', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs7776aztec.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3547, 'vs20leprechaun', 'Leprechaun Song', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs20leprechaun.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3548, 'apollo-winandrace', 'Winand Race', 'slot', 'Apollo', 'ms', 'wb', '', 'img/mrslotty/apollo-winandrace.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3549, 'excalibur_touch-gameview_netenthtml', 'Excalibur Touch', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_excalibur_touch.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3550, 'steamtower-gameview_netent', 'Steam Tower', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_steamtower.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3551, 'vs75empress', 'Golden Beauty', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs75empress.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3552, 'vs20aladdinsorc', 'Aladdin and the Sorcerer', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs20aladdinsorc.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3553, 'apollo-mysteryapollo', 'Mystery Apollo', 'slot', 'Apollo', 'ms', 'mb', '', 'img/mrslotty/apollo-mysteryapollo.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3554, '857-gameview_egt_mobile', 'Extra Joker', 'slot', 'EGT', 'sl', 'wb', '', 'img/sl_857.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3555, 'steamtower_touch-gameview_netenthtml', 'Steam Tower Touch', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_steamtower_touch.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3556, 'vs25newyear', 'Lucky New Year', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs25newyear.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3557, 'vs25dwarves', 'Dwarven Gold', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs25dwarves.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3558, 'apollo-mysteryapollo', 'Mystery Apollo', 'slot', 'Apollo', 'ms', 'wb', '', 'img/mrslotty/apollo-mysteryapollo.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3559, '857-gameview_egt_mobile', 'Extra Joker', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_857.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3560, 'stickers-gameview_netenthtml', 'Stickers', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_stickers.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3561, 'vs5spjoker', 'Super Joker', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs5spjoker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3562, 'vs4096jurassic', 'Jurassic Giants', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs4096jurassic.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3563, 'apollo-spacegame', 'Space Game', 'slot', 'Apollo', 'ms', 'mb', '', 'img/mrslotty/apollo-spacegame.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3564, '822-gameview_egt_mobile', 'Extra Stars', 'slot', 'EGT', 'sl', 'wb', '', 'img/sl_822.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3565, 'vs243mwarrior', 'Monkey Warrior', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs243mwarrior.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3566, 'vs243crystalcave', 'Magic Crystals', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs243crystalcave.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3567, 'apollo-spacegame', 'Space Game', 'slot', 'Apollo', 'ms', 'wb', '', 'img/mrslotty/apollo-spacegame.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3568, '822-gameview_egt_mobile', 'Extra Stars', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_822.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3569, 'subtopia-gameview_netent', 'Subtopia', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_subtopia.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3570, 'vs4096bufking', 'Buffalo King', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs4096bufking.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3571, 'vs13g', 'Devil\'s 13', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs13g.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3572, 'apollo-goldenage', 'Golden Age', 'slot', 'Apollo', 'ms', 'mb', '', 'img/mrslotty/apollo-goldenage.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3573, 'eyeofthedragon-gameview_novomatic', 'Eye of the Dragon', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_eyeofthedragon.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3574, '897-gameview_egt_mobile', 'Super 20', 'slot', 'EGT', 'sl', 'wb', '', 'img/sl_897.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3575, 'vs10fruity2', 'Extra Juicy', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs10fruity2.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3576, 'vs50safariking', 'Safari King', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs50safariking.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3577, 'apollo-goldenage', 'Golden Age', 'slot', 'Apollo', 'ms', 'wb', '', 'img/mrslotty/apollo-goldenage.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3578, '897-gameview_egt_mobile', 'Super 20', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_897.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3579, 'vs40pirate', 'Pirate Gold', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs40pirate.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3580, 'vs25pantherqueen', 'Panther Queen', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs25pantherqueen.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3581, 'apollo-bloodrevival', 'Blood Revival', 'slot', 'Apollo', 'ms', 'mb', '', 'img/mrslotty/apollo-bloodrevival.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3582, 'faust-gameview_novomatic', 'Faust', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_faust.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3583, 'supereighties-gameview_netent', 'Super Eighties', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_supereighties.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3584, 'vs20vegasmagic', 'Vegas Magic', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs20vegasmagic.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3585, 'vs50hercules', 'Hercules Son of Zeus', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs50hercules.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3586, 'apollo-bloodrevival', 'Blood Revival', 'slot', 'Apollo', 'ms', 'wb', '', 'img/mrslotty/apollo-bloodrevival.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3587, 'wzdfp_2-gameview_wazdan', 'Fenix Play', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdfp_2.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3588, 'wzdsh-gameview_wazdan', 'Super Hot', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdsh.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3589, '2014', 'Crash', 'iq', 'IqGames', 'dg', 'wb', '', 'img/dg/crash.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3590, 'vs25journey', 'Journey to the West', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs25journey.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3591, 'vs20egypt', 'Tales of Egypt', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs20egypt.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3592, 'fazi-spincards', 'Spin Cards', 'slot', 'Fazi', 'ms', 'mb', '', 'img/mrslotty/fazi-spincards.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3593, 'wzdfp27_2-gameview_wazdan', 'Fenix Play 27', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdfp27_2.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3594, '845-gameview_egt_mobile', 'Supreme Dice', 'slot', 'EGT', 'sl', 'wb', '', 'img/sl_845.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3595, '2013', 'Crash', 'iq', 'IqGames', 'dg', 'mb', '', 'img/dg/crash.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3596, 'vs25chilli', 'Chilli Heat', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs25chilli.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3597, 'vs15b', 'Crazy 7s', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs15b.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3598, 'fazi-spincards', 'Spin Cards', 'slot', 'Fazi', 'ms', 'wb', '', 'img/mrslotty/fazi-spincards.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3599, 'wzdfp27d-gameview_wazdan', 'Fenix Play 27 Deluxe', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdfp27d.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3600, '845-gameview_egt_mobile', 'Supreme Dice', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_845.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3601, '5236', 'HiLo', 'iq', 'IqGames', 'dg', 'wb', '', 'img/dg/hilo.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3602, 'vs20godiva', 'Lady Godiva', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs20godiva.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3603, 'vs20hockey', 'Hockey League', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs20hockey.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3604, 'wzdfpd-gameview_wazdan', 'Fenix Play Deluxe', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdfpd.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3605, '821-gameview_egt_mobile', 'Supreme Hot', 'slot', 'EGT', 'sl', 'wb', '', 'img/sl_821.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3606, '5237', 'HiLo', 'iq', 'IqGames', 'dg', 'mb', '', 'img/dg/hilo.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3607, 'vs25kingdoms', '3 Kingdoms - Battle of Red Cliffs', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs25kingdoms.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3608, 'vs1024atlantis', 'Queen of Atlantis', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs1024atlantis.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3609, 'wzdfb-gameview_wazdan', 'Fire Bird', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdfb.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3610, '821-gameview_egt_mobile', 'Supreme Hot', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_821.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3611, '19', 'Keno 8', 'iq', 'IqGames', 'dg', 'wb', '', 'img/dg/keno.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3612, 'vs18mashang', 'Treasure Horse', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs18mashang.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3613, 'vs15ktv', 'KTV', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs15ktv.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3614, 'evoplay-raccoon-tales', 'Raccoon Tales', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-raccoon-tales.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3615, 'fisticuffs-gameview_netent', 'Fisticuffs', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_fisticuffs.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3616, 'swipeandroll-gameview_netenthtml', 'Swipe and Roll', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_swipeandroll.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3617, '2010', 'Keno 10', 'iq', 'IqGames', 'dg', 'wb', '', 'img/dg/keno.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3618, 'vs9chen', 'Master Chen\'s Fortune', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs9chen.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3619, 'vs25h', 'Fruity Blast', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs25h.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3620, 'evoplay-raccoon-tales', 'Raccoon Tales', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-raccoon-tales.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3621, 'flamedancer-gameview_novomatic', 'Flame Dancer', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_flamedancer.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3622, 'talesofkrakow-gameview_netent', 'Tales of Krakow', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_talesofkrakow.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3623, '5730', 'Penalty', 'iq', 'IqGames', 'dg', 'wb', '', 'img/dg/penalty.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3624, 'vs4096mystery', 'Mysterious', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs4096mystery.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3625, 'vs25champ', 'The Champions', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs25champ.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3626, 'wearecasino-raise-the-bar', 'Raise The Bar', 'slot', 'Wearecasino', 'ms', 'mb', '', 'img/mrslotty/wearecasino-raise-the-bar.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3627, 'flamedancer-gameview_novomatic', 'Flame Dancer', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_flamedancer.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3628, 'templeofnudges-gameview_netenthtml', 'Temple of Nudges', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_templeofnudges.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3629, '2011', 'Keno 10', 'iq', 'IqGames', 'dg', 'mb', '', 'img/dg/penalty.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3630, 'vs25wildspells', 'Wild Spells', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs25wildspells.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3631, 'vs20cms', 'Sugar Rush Summer Time', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs20cms.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3632, 'wearecasino-raise-the-bar', 'Raise The Bar', 'slot', 'Wearecasino', 'ms', 'wb', '', 'img/mrslotty/wearecasino-raise-the-bar.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3633, '509-gameview_egt_mobile', 'Flaming Dice', 'slot', 'EGT', 'sl', 'wb', '', 'img/sl_509.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3634, '5510', 'Rocketon', 'iq', 'IqGames', 'dg', 'wb', '', 'img/dg/rocketon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3635, 'cs5moneyroll', 'Money Roll', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_cs5moneyroll.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3636, 'vs25romeoandjuliet', 'Romeo and Juliet', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs25romeoandjuliet.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3637, 'redrake-wild-animals', 'Wild Animals', 'slot', 'Red Rake', 'ms', 'mb', '', 'img/mrslotty/redrake-wild-animals.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3638, '509-gameview_egt_mobile', 'Flaming Dice', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_509.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3639, '885-gameview_egt_mobile', 'The Great Egypt', 'slot', 'EGT', 'sl', 'wb', '', 'img/sl_885.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3640, '2012', 'Keno 8', 'iq', 'IqGames', 'dg', 'mb', '', 'img/dg/rocketon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3641, 'vs25goldrush', 'Gold Rush', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs25goldrush.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3642, 'vs20cmv', 'Sugar Rush Valentine\'s Day', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs20cmv.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3643, 'redrake-wild-animals', 'Wild Animals', 'slot', 'Red Rake', 'ms', 'wb', '', 'img/mrslotty/redrake-wild-animals.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3644, '805-gameview_egt_mobile', 'Flaming Hot', 'slot', 'EGT', 'sl', 'wb', '', 'img/sl_805.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3645, '885-gameview_egt_mobile', 'The Great Egypt', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_885.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3646, '5337', 'SicBo', 'iq', 'IqGames', 'dg', 'wb', '', 'img/dg/sicbo.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3647, 'vs20egypttrs', 'Egyptian Fortunes', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs20egypttrs.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3648, 'vs9catz', 'The Catfather', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs9catz.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3649, 'eagaming-wild-protectors', 'Wild Protectors', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-wild-protectors.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3650, '805-gameview_egt_mobile', 'Flaming Hot', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_805.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3651, 'theinvisibleman-gameview_netenthtml', 'The Invisible Man', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_theinvisibleman.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3652, 'vs3train', 'Gold Train', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs3train.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3653, 'vs20cw', 'Sugar Rush Winter', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs20cw.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3654, 'eagaming-wild-protectors', 'Wild Protectors', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-wild-protectors.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3655, 'flowers-gameview_netent', 'Flowers', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_flowers.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3656, 'themoneygame-gameview_novomatic', 'The Money Game', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_themoneygame.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3657, 'vs25dragonkingdom', 'Dragon Kingdom', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs25dragonkingdom.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3658, 'vs20gg', 'Spooky Fortune', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs20gg.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3659, 'eagaming-cluster-mania', 'Cluster Mania', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-cluster-mania.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3660, 'flowerschristmas-gameview_netenthtml', 'Flowers Christmas Edition', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_flowerschristmas.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3661, 'phantomoftheopera-gameview_netenthtml', 'The Phantoms Curse', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_phantomoftheopera.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3662, 'vs5trdragons', 'Triple Dragons', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs5trdragons.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3663, 'vs30catz', 'The Catfather Part II', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs30catz.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3664, 'eagaming-cluster-mania', 'Cluster Mania', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-cluster-mania.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3665, 'flowers_touch-gameview_netenthtml', 'Flowers Touch', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_flowers_touch.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3666, 'therealking-gameview_novomatic', 'The Real King Aloha Hawaii', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_therealking.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3667, 'vs50kingkong', 'Mighty Kong', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs50kingkong.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3668, 'vs9hockey', 'Hockey League Wild Match', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs9hockey.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3669, 'eagaming-mythical-sand', 'Mythical Sand', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-mythical-sand.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3670, 'football-gameview_netenthtml', 'Football: Champions Cup', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_football.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3671, 'therealkinggoldrecords-gameview_novomatic', 'The Real King Gold Records', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_therealkinggoldrecords.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3672, 'vs9hotroll', 'Hot Chilli', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs9hotroll.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3673, 'bjmb', 'American Blackjack', 'slot', 'Pragmatic', 'pg', 'mb', 'JUME', 'img/pg/pg_bjmb.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3674, 'eagaming-mythical-sand', 'Mythical Sand', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-mythical-sand.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3675, 'fortunasfruits-gameview_amatic_mobile', 'Fortunas Fruits', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_fortunasfruits.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3676, 'theroyals-gameview_novomatic', 'The Royals', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_theroyals.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3677, 'vs25asgard', 'Asgard', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs25asgard.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3678, 'bndt', 'Dragon Tiger', 'slot', 'Pragmatic', 'pg', 'mb', 'JUME', 'img/pg/pg_bndt.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3679, 'eagaming-joker-madness', 'Joker Madness', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-joker-madness.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3680, '820-gameview_egt_mobile', 'Fortune Spells', 'slot', 'EGT', 'sl', 'wb', '', 'img/sl_820.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3681, '863-gameview_egt_mobile', 'The White Wolf', 'slot', 'EGT', 'sl', 'wb', '', 'img/sl_863.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3682, 'vs20chicken', 'The Great Chicken Escape', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs20chicken.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3683, 'bnadvanced', 'Dragon Bonus Baccarat', 'slot', 'Pragmatic', 'pg', 'mb', 'JUME', 'img/pg/pg_bnadvanced.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3684, 'eagaming-joker-madness', 'Joker Madness', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-joker-madness.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3685, '820-gameview_egt_mobile', 'Fortune Spells', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_820.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3686, '863-gameview_egt_mobile', 'The White Wolf', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_863.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3687, 'vs25safari', 'Hot Safari', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs25safari.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3688, 'scwolfgoldai', 'Wolf Gold 1,000,000', 'slot', 'Pragmatic', 'pg', 'mb', 'RAGA', 'img/pg/pg_scwolfgoldai.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3689, 'eagaming-blackbeard-legacy', 'Blackbeard Legacy', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-blackbeard-legacy.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3690, 'fortuneteller-gameview_netent', 'Fortune Teller', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_fortuneteller.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3691, 'themepark-gameview_netenthtml', 'Theme Park', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_themepark.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3692, 'cs3irishcharms', 'Irish Charms', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_cs3irishcharms.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3693, 'scsafariai', 'Hot Safari 75,000', 'slot', 'Pragmatic', 'pg', 'mb', 'RAGA', 'img/pg/pg_scsafariai.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3694, 'eagaming-blackbeard-legacy', 'Blackbeard Legacy', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-blackbeard-legacy.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3695, 'frankenstein-gameview_netent', 'Frankenstein', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_frankenstein.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3696, 'thief-gameview_netent', 'Thief', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_thief.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3697, 'vs1024lionsd', '5 Lions Dance', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs1024lionsd.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3698, 'scqogai', 'Queen of Gold 100,000', 'slot', 'Pragmatic', 'pg', 'mb', 'RAGA', 'img/pg/pg_scqogai.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3699, 'eagaming-the-four-inventions', 'The Four Inventions', 'slot', 'EAGaming', 'ms', 'mb', '', 'img/mrslotty/eagaming-the-four-inventions.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3700, 'frankenstein_touch-gameview_netenthtml', 'Frankenstein Touch', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_frankenstein_touch.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3701, 'thrillspin-gameview_netent', 'Thrill Spin', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_thrillspin.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3702, 'vs9aztecgemsdx', 'Aztec Gems Deluxe', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs9aztecgemsdx.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3703, 'scpandai', 'Panda Gold 50,000', 'slot', 'Pragmatic', 'pg', 'mb', 'RAGA', 'img/pg/pg_scpandai.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3704, 'eagaming-the-four-inventions', 'The Four Inventions', 'slot', 'EAGaming', 'ms', 'wb', '', 'img/mrslotty/eagaming-the-four-inventions.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3705, 'fruitcase-gameview_netent', 'Fruit Case', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_fruitcase.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3706, 'thunderfist-gameview_netent', 'Thunderfist', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_thunderfist.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3707, 'vswayswerewolf', 'Curse of the Werewolf Megaways', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vswayswerewolf.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3708, 'scgoldrushai', 'Gold Rush 500,000', 'slot', 'Pragmatic', 'pg', 'mb', 'RAGA', 'img/pg/pg_scgoldrushai.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3709, 'hub88-lucky-k', 'Lucky K', 'slot', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-lucky-k.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3710, 'fruitfarm-gameview_novomatic', 'Fruit Farm', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_fruitfarm.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3711, 'tornado-gameview_netent', 'Tornado', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_tornado.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3712, 'vs20eking', 'Emerald King', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs20eking.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3713, 'scdiamondai', 'Diamond Strike 250,000', 'slot', 'Pragmatic', 'pg', 'mb', 'RAGA', 'img/pg/pg_scdiamondai.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3714, 'hub88-lucky-k', 'Lucky K', 'slot', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-lucky-k.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3715, 'fruitfarm-gameview_novomatic', 'Fruit Farm', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_fruitfarm.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3716, 'trolls-gameview_netent', 'Trolls', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_trolls.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3717, 'vs20goldfever', 'Gems Bonanza', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs20goldfever.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3718, 'sc7piggiesai', '7 Piggies 25,000', 'slot', 'Pragmatic', 'pg', 'mb', 'RAGA', 'img/pg/pg_sc7piggiesai.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3719, 'hub88-candy-prize-big', 'Candy Prize Big', 'slot', 'Green Jade', 'ms', 'mb', '', 'img/mrslotty/hub88-candy-prize-big.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3720, 'wzdff-gameview_wazdan', 'Fruit Fiesta', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdff.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3721, 'turnyourfortune-gameview_netenthtml', 'Turn Your Fortune', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_turnyourfortune.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3722, 'vs20rhinoluxe', 'Great Rhino Deluxe', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs20rhinoluxe.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3723, 'scwolfgold', 'Wolf Gold 1 Million', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_scwolfgold.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3724, 'hub88-candy-prize-big', 'Candy Prize Big', 'slot', 'Green Jade', 'ms', 'wb', '', 'img/mrslotty/hub88-candy-prize-big.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3725, 'fruitsensation-gameview_novomatic', 'Fruit Sensation', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_fruitsensation.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3726, 'tweetybirds-gameview_amatic_mobile', 'Tweety Birds', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_tweetybirds.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3727, 'vs20gorilla', 'Jungle Gorilla', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs20gorilla.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3728, 'scqog', 'Queen of Gold 100,000', 'slot', 'Pragmatic', 'pg', 'mb', 'RAGA', 'img/pg/pg_scqog.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3729, 'spadegaming-crazy-bomber', 'Crazy Bomber', 'slot', 'Spadegaming', 'ms', 'mb', '', 'img/mrslotty/spadegaming-crazy-bomber.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3730, 'fruitsensation-gameview_novomatic', 'Fruit Sensation', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_fruitsensation.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3731, 'twinspin-gameview_netent', 'Twin Spin', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_twinspin.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3732, 'vs20pblinders', 'Peaky Blinders', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs20pblinders.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3733, 'scpanda', 'Panda Gold 10,000', 'slot', 'Pragmatic', 'pg', 'mb', 'RAGA', 'img/pg/pg_scpanda.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3734, 'spadegaming-crazy-bomber', 'Crazy Bomber', 'slot', 'Spadegaming', 'ms', 'wb', '', 'img/mrslotty/spadegaming-crazy-bomber.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3735, 'fruitshop-gameview_netent', 'Fruit Shop', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_fruitshop.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3736, 'twinspindeluxe-gameview_netenthtml', 'Twin Spin Deluxe', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_twinspindeluxe.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3737, 'vs25samurai', 'Rise of Samurai', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs25samurai.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3738, 'scsafari', 'Hot Safari 50,000', 'slot', 'Pragmatic', 'pg', 'mb', 'RAGA', 'img/pg/pg_scsafari.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3739, 'hub88-candy-prize', 'Candy Prize', 'slot', 'Green Jade', 'ms', 'mb', '', 'img/mrslotty/hub88-candy-prize.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3740, 'fruitshop_touch-gameview_netenthtml', 'Fruit Shop Touch', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_fruitshop_touch.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3741, 'twinspin_touch-gameview_netenthtml', 'Twin Spin Touch', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_twinspin_touch.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3742, 'vswayshive', 'Star Bounty', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vswayshive.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3743, 'scgoldrush', 'Gold Rush 250,000', 'slot', 'Pragmatic', 'pg', 'mb', 'RAGA', 'img/pg/pg_scgoldrush.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3744, 'hub88-candy-prize', 'Candy Prize', 'slot', 'Green Jade', 'ms', 'wb', '', 'img/mrslotty/hub88-candy-prize.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3745, 'fruitspin-gameview_netenthtml', 'Fruit Spin', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_fruitspin.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3746, '802-gameview_egt_mobile', 'Ultimate Hot', 'slot', 'EGT', 'sl', 'wb', '', 'img/sl_802.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3747, 'vswaysdogs', 'The Dog House Megaways', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vswaysdogs.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3748, 'scdiamond', 'Diamond Strike 100,000', 'slot', 'Pragmatic', 'pg', 'mb', 'RAGA', 'img/pg/pg_scdiamond.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3749, 'hub88-pick-a-pinata', 'Pick A Pinata', 'slot', 'Green Jade', 'ms', 'mb', '', 'img/mrslotty/hub88-pick-a-pinata.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3750, 'fruitilicious-gameview_novomatic', 'Fruitilicious', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_fruitilicious.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3751, '802-gameview_egt_mobile', 'Ultimate Hot', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_802.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3752, 'vs25tigerwar', 'The Tiger Warrior', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs25tigerwar.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3753, 'sc7piggies', '7 Piggies 5,000', 'slot', 'Pragmatic', 'pg', 'mb', 'RAGA', 'img/pg/pg_sc7piggies.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3754, 'hub88-pick-a-pinata', 'Pick A Pinata', 'slot', 'Green Jade', 'ms', 'wb', '', 'img/mrslotty/hub88-pick-a-pinata.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3755, 'fruitilicious-gameview_novomatic', 'Fruitilicious', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_fruitilicious.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3756, 'ultrahotdeluxe-gameview_novomatic', 'Ultra Hot Deluxe', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_ultrahotdeluxe.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3757, 'vs40madwheel', 'The Wild Machine', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs40madwheel.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3758, 'vs25kingdomsnojp', '3 Kingdoms - Battle of Red Cliffs', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vs25kingdomsnojp.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3759, 'evoplay-wheel-of-time', 'Wheel Of Time', 'slot', 'Evoplay', 'ms', 'mb', 'JUME', 'img/mrslotty/evoplay-wheel-of-time.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3760, '811-gameview_egt_mobile', 'Fruits Kingdom', 'slot', 'EGT', 'sl', 'wb', '', 'img/sl_811.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3761, 'ultrahotdeluxe-gameview_novomatic', 'Ultra Hot Deluxe', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_ultrahotdeluxe.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3762, 'vs5ultra', 'Ultra Hold and Spin', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs5ultra.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3763, 'vpfh3', 'Flat Horse Racing', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vpfh3.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3764, 'evoplay-wheel-of-time', 'Wheel Of Time', 'slot', 'Evoplay', 'ms', 'wb', 'JUME', 'img/mrslotty/evoplay-wheel-of-time.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3765, '811-gameview_egt_mobile', 'Fruits Kingdom', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_811.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3766, 'ultraseven-gameview_amatic_mobile', 'Ultra Seven', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_ultraseven.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3767, 'vs25walker', 'Wild Walker', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs25walker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3768, 'vppso4', 'Penalty Shootout', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vppso4.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3769, 'hub88-betina-bingo', 'Betina Bingo', 'video_bingo', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-betina-bingo.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3770, 'fruitsnroyals-gameview_novomatic', 'Fruitsn Royals', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_fruitsnroyals.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3771, 'wzdval_2-gameview_wazdan', 'Valhalla', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdval_2.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3772, 'vs576treasures', 'Wild Wild Riches', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs576treasures.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3773, 'bca', 'Baccarat', 'slot', 'Pragmatic', 'pg', 'mb', 'JUME', 'img/pg/pg_bca.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3774, 'hub88-betina-bingo', 'Betina Bingo', 'video_bingo', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-betina-bingo.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3775, 'wzdvh-gameview_wazdan', 'Vegas Hot', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdvh.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3776, 'vs10vampwolf', 'Vampires vs Wolves', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs10vampwolf.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3777, 'bjma', 'Multihand Blackjack', 'slot', 'Pragmatic', 'pg', 'mb', 'JUME', 'img/pg/pg_bjma.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3778, 'hub88-crystal-unicorn', 'Crystal Unicorn', 'slot', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-crystal-unicorn.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3779, 'fruitshopchristmas-gameview_netenthtml', 'Fruitshop Christmas Edition', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_fruitshopchristmas.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3780, '818-gameview_egt_mobile', 'Versailles Gold', 'slot', 'EGT', 'sl', 'wb', '', 'img/sl_818.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3781, 'cs3w', 'Diamonds are Forever 3 Lines', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_cs3w.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3782, 'kna', 'Keno', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_kna.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3783, 'hub88-crystal-unicorn', 'Crystal Unicorn', 'slot', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-crystal-unicorn.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3784, 'funkyseventies-gameview_netent', 'Funky Seventies', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_funkyseventies.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3785, '818-gameview_egt_mobile', 'Versailles Gold', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_818.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3786, 'vs243fortseren', 'Greek Gods', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs243fortseren.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3787, 'rla', 'Roulette', 'slot', 'Pragmatic', 'pg', 'mb', 'RULF', 'img/pg/pg_rla.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3788, 'hub88-ocean-richies', 'Ocean Richies', 'slot', 'Caleta', 'ms', 'mb', '', 'img/mrslotty/hub88-ocean-richies.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3789, 'victorious-gameview_netent', 'Victorious', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_victorious.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3790, 'vs20bl', 'Busy Bees', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs20bl.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3791, 'vpa', 'Jacks or Better', 'slot', 'Pragmatic', 'pg', 'mb', '', 'img/pg/pg_vpa.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3792, 'hub88-ocean-richies', 'Ocean Richies', 'slot', 'Caleta', 'ms', 'wb', '', 'img/mrslotty/hub88-ocean-richies.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3793, 'ghostpirates-gameview_netent', 'Ghost Pirates', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_ghostpirates.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3794, 'vikingstreasure-gameview_netent', 'Vikings Treasure', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_vikingstreasure.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3795, 'vs20honey', 'Honey Honey Honey', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs20honey.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3796, 'truelab-chains-code', 'Chains Code', 'slot', 'TrueLab', 'ms', 'mb', '', 'img/mrslotty/truelab-chains-code.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3797, 'evoplay-forgotten-fable', 'Forgotten Fable', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-forgotten-fable.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3798, 'ghostpirates_touch-gameview_netenthtml', 'Ghost Pirates Touch', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_ghostpirates_touch.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3799, 'voodoovibes-gameview_netent', 'Voodoo Vibes', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_voodoovibes.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3800, 'vs7776secrets', 'Aztec Treasure', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs7776secrets.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3801, 'truelab-chains-code', 'Chains Code', 'slot', 'TrueLab', 'ms', 'wb', '', 'img/mrslotty/truelab-chains-code.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3802, 'evoplay-forgotten-fable', 'Forgotten Fable', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-forgotten-fable.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3803, 'glow-gameview_netent', 'Glow', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_glow.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3804, 'warlords-gameview_netenthtml', 'Warlords', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_warlords.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3805, 'vs20fruitparty', 'Fruit Party', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs20fruitparty.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3806, 'betsolutions-backgammon', 'Backgammon', 'slot', 'Betsolutions', 'ms', 'mb', 'JUME', 'img/mrslotty/betsolutions-backgammon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3807, 'evoplay-jolly-treasures', 'Jolly Treasures', 'slot', 'Evoplay', 'ms', 'mb', '', 'img/mrslotty/evoplay-jolly-treasures.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3808, 'gobananas-gameview_netent', 'Go Bananas!', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_gobananas.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3809, 'whenpigsfly-gameview_netent', 'When Pigs Fly', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_whenpigsfly.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3810, 'vs1fufufu', 'Fu Fu Fu', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs1fufufu.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3811, 'betsolutions-backgammon', 'Backgammon', 'slot', 'Betsolutions', 'ms', 'wb', 'JUME', 'img/mrslotty/betsolutions-backgammon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3812, 'evoplay-jolly-treasures', 'Jolly Treasures', 'slot', 'Evoplay', 'ms', 'wb', '', 'img/mrslotty/evoplay-jolly-treasures.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3813, 'wild7-gameview_amatic_mobile', 'Wild 7', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_wild7.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3814, 'vs117649starz', 'Starz Megaways', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs117649starz.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3815, 'betsolutions-okey', 'Okey', 'slot', 'Betsolutions', 'ms', 'mb', 'JUME', 'img/mrslotty/betsolutions-okey.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3816, 'slotexchange-mysterious-house', 'Mysterious house', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-mysterious-house.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3817, 'goldenark-gameview_novomatic', 'Golden Ark', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_goldenark.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3818, 'wildbazaar-gameview_netenthtml', 'Wild Bazaar', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_wildbazaar.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3819, 'vs40streetracer', 'Street Racer', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs40streetracer.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3820, 'betsolutions-okey', 'Okey', 'slot', 'Betsolutions', 'ms', 'wb', 'JUME', 'img/mrslotty/betsolutions-okey.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3821, 'slotexchange-mysterious-house', 'Mysterious house', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-mysterious-house.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3822, 'goldenark-gameview_novomatic', 'Golden Ark', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_goldenark.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3823, 'wilddragon-gameview_amatic_mobile', 'Wild Dragon', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_wilddragon.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3824, 'vs8magicjourn', 'Magic Journey', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs8magicjourn.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3825, 'betsolutions-bura', 'Bura', 'slot', 'Betsolutions', 'ms', 'mb', 'JUME', 'img/mrslotty/betsolutions-bura.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3826, 'slotexchange-cyber-zone', 'Cyber zone', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-cyber-zone.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3827, 'goldengrimoire-gameview_netenthtml', 'Golden Grimoire', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_goldengrimoire.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3828, 'wzdwj-gameview_wazdan', 'Wild Jack', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdwj.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3829, 'vs20hercpeg', 'Hercules and Pegasus', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs20hercpeg.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3830, 'betsolutions-bura', 'Bura', 'slot', 'Betsolutions', 'ms', 'wb', 'JUME', 'img/mrslotty/betsolutions-bura.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3831, 'slotexchange-cyber-zone', 'Cyber zone', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-cyber-zone.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3832, 'wzdgs-gameview_wazdan', 'Golden Sphinx', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdgs.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3833, 'wzdwj81-gameview_wazdan', 'Wild Jack 81', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdwj81.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3834, 'vs25peking', 'Peking Luck', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs25peking.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3835, 'betsolutions-dominoes', 'Dominoes', 'slot', 'Betsolutions', 'ms', 'mb', 'JUME', 'img/mrslotty/betsolutions-dominoes.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3836, 'hub88-neon-lights', 'Neon Lights', 'slot', 'Green Jade', 'ms', 'mb', '', 'img/mrslotty/hub88-neon-lights.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3837, 'wildrockets-gameview_netent', 'Wild Rockets', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_wildrockets.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3838, 'vs40frrainbow', 'Fruit Rainbow', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs40frrainbow.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3839, 'betsolutions-dominoes', 'Dominoes', 'slot', 'Betsolutions', 'ms', 'wb', 'JUME', 'img/mrslotty/betsolutions-dominoes.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3840, 'hub88-neon-lights', 'Neon Lights', 'slot', 'Green Jade', 'ms', 'wb', '', 'img/mrslotty/hub88-neon-lights.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3841, 'gonzosquest-gameview_netent', 'Gonzos Quest', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_gonzosquest.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3842, 'wildshark-gameview_amatic_mobile', 'Wild Shark', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_wildshark.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3843, 'vs50chinesecharms', 'Lucky Dragons', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs50chinesecharms.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3844, 'betsolutions-seka', 'Seka', 'slot', 'Betsolutions', 'ms', 'mb', 'JUME', 'img/mrslotty/betsolutions-seka.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3845, 'endorphina_4OfAKing@ENDORPHINA', '4 of a King', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/4OfAKing.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3846, 'gonzosquest_touch-gameview_netenthtml', 'Gonzos Quest Touch', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_gonzosquest_touch.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3847, 'wildturkey-gameview_netent', 'Wild Turkey', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_wildturkey.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3848, 'vs50amt', 'Aladdin\'s Treasure', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs50amt.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3849, 'betsolutions-seka', 'Seka', 'slot', 'Betsolutions', 'ms', 'wb', 'JUME', 'img/mrslotty/betsolutions-seka.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3850, 'endorphina_BlastBoomBang@ENDORPHINA', 'Blast Boom Bang', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/BlastBoomBang.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3851, 'gorilla-gameview_novomatic', 'Gorilla', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_gorilla.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3852, 'wildwater-gameview_netent', 'Wild Water', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_wildwater.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3853, 'vs50pixie', 'Pixie Wings', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs50pixie.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3854, 'tpg-dragon-tiger', 'Dragon Tiger', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-dragon-tiger.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3855, 'endorphina_GemsnStones@ENDORPHINA', 'Gems & Stones', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/GemsStones.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3856, 'gorilla-gameview_novomatic', 'Gorilla', 'slot', 'Novomatic', 'sl', 'mb', '', 'img/sl_gorilla.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3857, 'wildwater_touch-gameview_netenthtml', 'Wild Water Touch', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_wildwater_touch.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3858, 'vs7monkeys', '7 Monkeys', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs7monkeys.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3859, 'tpg-dragon-tiger', 'Dragon Tiger', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-dragon-tiger.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3860, 'endorphina_MongolTreasures@ENDORPHINA', 'Mongol Treasures', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/MongolTreasures.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3861, '817-gameview_egt_mobile', 'Grace of Cleopatra', 'slot', 'EGT', 'sl', 'wb', '', 'img/sl_817.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3862, 'wildwildwest-gameview_netenthtml', 'Wild Wild West', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_wildwildwest.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3863, 'vs20cm', 'Sugar Rush', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs20cm.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3864, 'tpg-go-gold-fishing-360', 'Go Gold Fishing 360°', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-go-gold-fishing-360.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3865, 'endorphina_TempleCats@ENDORPHINA', 'Temple Cats', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/TempleCats.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3866, '817-gameview_egt_mobile', 'Grace of Cleopatra', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_817.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3867, 'wildwitches-gameview_netent', 'Wild Witches', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_wildwitches.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3868, 'vs10madame', 'Madame Destiny', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs10madame.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3869, 'tpg-go-gold-fishing-360', 'Go Gold Fishing 360°', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-go-gold-fishing-360.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3870, 'endorphina_UndinesDeep@ENDORPHINA', 'Undine\'s Deep', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/UndinesDeep.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3871, 'grandmonarch-gameview', 'Grand Monarch', 'slot', 'IGT', 'sl', 'wb', '', 'img/sl_grandmonarch.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3872, 'wildotron3000-gameview_netenthtml', 'Wild-O-Tron 3000', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_wildotron3000.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3873, 'vs5trjokers', 'Triple Jokers', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs5trjokers.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3874, 'tpg-fishing-fortune-360', 'Fishing Fortune 360°', 'slot', 'Tpg', 'ms', 'mb', '', 'img/mrslotty/tpg-fishing-fortune-360.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3875, 'endorphina_ChimneySweep@ENDORPHINA', 'Chimney Sweep', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/ChimneySweep.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3876, 'wzdgbom-gameview_wazdan', 'Great Book of Magic', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdgbom.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3877, 'wishmaster-gameview_netent', 'Wish Master', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_wishmaster.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3878, 'vs7pigs', '7 Piggies', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs7pigs.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3879, 'tpg-fishing-fortune-360', 'Fishing Fortune 360°', 'slot', 'Tpg', 'ms', 'wb', '', 'img/mrslotty/tpg-fishing-fortune-360.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3880, 'endorphina_IcePirates@ENDORPHINA', 'Ice Pirates', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/IcePirates.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3881, 'wzdgbomd-gameview_wazdan', 'Great Book of Magic Deluxe', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdgbomd.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3882, 'witchcraftacademy-gameview_netenthtml', 'Witchcraft Academy', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_witchcraftacademy.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3883, 'vs15fairytale', 'Fairytale Fortune', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs15fairytale.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3884, 'hub88-age-of-dragons', 'Age of Dragons', 'slot', 'Kalamba', 'ms', 'mb', '', 'img/mrslotty/hub88-age-of-dragons.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3885, 'endorphina_Shaman@ENDORPHINA', 'Shaman', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/Shaman.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3886, 'groovysixties-gameview_netent', 'Groovy Sixties', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_groovysixties.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3887, '815-gameview_egt_mobile', 'Witches Charm', 'slot', 'EGT', 'sl', 'wb', '', 'img/sl_815.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3888, 'vs25dwarves_new', 'Dwarven Gold Deluxe', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs25dwarves_new.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3889, 'hub88-age-of-dragons', 'Age of Dragons', 'slot', 'Kalamba', 'ms', 'wb', '', 'img/mrslotty/hub88-age-of-dragons.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3890, 'endorphina_TheKing@ENDORPHINA', 'The King', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/TheKing.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3891, 'gryphonsgolddeluxe-gameview_novomatic', 'Gryphons Gold Deluxe', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_gryphonsgolddeluxe.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3892, '815-gameview_egt_mobile', 'Witches Charm', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_815.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3893, 'alohaclusterpays-gameview_netenthtml', 'Aloha! Cluster Pays', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_alohaclusterpays.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3894, 'vs25sea', 'Great Reef', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs25sea.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3895, 'hub88-dino-odyssey', 'Dino Odyssey', 'slot', 'Kalamba', 'ms', 'mb', '', 'img/mrslotty/hub88-dino-odyssey.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3896, 'endorphina_Urartu@ENDORPHINA', 'Urartu', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/Urartu.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3897, 'gunsandroses-gameview_netenthtml', 'Guns N Roses', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_gunsandroses.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3898, 'wolfcub-gameview_netenthtml', 'Wolf Cub', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_wolfcub.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3899, 'archangels-gameview_netenthtml', 'Archangels: Salvation', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_archangels.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3900, 'vs25davinci', 'Da Vinci\'s Treasure', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs25davinci.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3901, 'hub88-dino-odyssey', 'Dino Odyssey', 'slot', 'Kalamba', 'ms', 'wb', '', 'img/mrslotty/hub88-dino-odyssey.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3902, 'endorphina_TheVampires@ENDORPHINA', 'The Vampires', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/TheVampires.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3903, 'wolfmoon-gameview_amatic_mobile', 'Wolf Moon', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_wolfmoon.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3904, 'asgardianstones-gameview_netenthtml', 'Asgardian Stones', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_asgardianstones.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3905, 'vs40beowulf', 'Beowulf', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs40beowulf.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3906, 'hub88-double-joker', 'Double Joker', 'slot', 'Kalamba', 'ms', 'mb', '', 'img/mrslotty/hub88-double-joker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3907, 'endorphina_TheEmirate@ENDORPHINA', 'The Emirate', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/TheEmirate.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3908, 'halloweenjack-gameview_netenthtml', 'Halloween Jack', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_halloweenjack.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3909, 'wolfrun-gameview', 'Wolf Run', 'slot', 'IGT', 'sl', 'wb', '', 'img/sl_wolfrun.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3910, 'berryburst-gameview_netenthtml', 'Berry Burst', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_berryburst.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3911, 'vs10egypt', 'Ancient Egypt', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs10egypt.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3912, 'hub88-double-joker', 'Double Joker', 'slot', 'Kalamba', 'ms', 'wb', '', 'img/mrslotty/hub88-double-joker.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3913, 'endorphina_Pachamama@ENDORPHINA', 'Pachamama', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/Pachamama.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3914, 'helena-gameview_novomatic', 'Helena', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_helena.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3915, 'wonkywabbits-gameview_netent', 'Wonky Wabbits', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_wonkywabbits.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3916, 'berryburstmax-gameview_netenthtml', 'Berry Burst Max', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_berryburstmax.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3917, 'vs20leprexmas', 'Leprechaun Carol', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs20leprexmas.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3918, 'hub88-fire-eagle', 'Fire Eagle', 'slot', 'Kalamba', 'ms', 'mb', '', 'img/mrslotty/hub88-fire-eagle.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3919, 'endorphina_Geisha@ENDORPHINA', 'Geisha', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/Geisha.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3920, '1', 'Legacy of Egypt', 'slot', 'luckySpins', 'ls', 'wb', '', 'img/ls_1.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3921, '807-gameview_egt_mobile', 'Zodiac Wheel', 'slot', 'EGT', 'sl', 'wb', '', 'img/sl_807.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3922, 'bloodsuckers2-gameview_netenthtml', 'Blood Suckers 2', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_bloodsuckers2.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3923, 'vs20wildpix', 'Wild Pixies', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs20wildpix.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3924, 'hub88-fire-eagle', 'Fire Eagle', 'slot', 'Kalamba', 'ms', 'wb', '', 'img/mrslotty/hub88-fire-eagle.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3925, 'endorphina_FreshFruits@ENDORPHINA', 'Fresh Fruits', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/FreshFruits.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3926, '2', 'Golden Caravan', 'slot', 'luckySpins', 'ls', 'wb', '', 'img/ls_2.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3927, 'wzdhm_2-gameview_wazdan', 'Highschool Manga', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdhm_2.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3928, '807-gameview_egt_mobile', 'Zodiac Wheel', 'slot', 'EGT', 'sl', 'mb', '', 'img/sl_807.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3929, 'bollywoodstory-gameview_netenthtml', 'Bollywood Story', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_bollywoodstory.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3930, 'vs25queenofgold', 'Queen of Gold', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs25queenofgold.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3931, 'hub88-gates-of-babylon', 'Gates of Babylon', 'slot', 'Kalamba', 'ms', 'mb', '', 'img/mrslotty/hub88-gates-of-babylon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3932, 'endorphina_MoreFreshFruits@ENDORPHINA', 'More Fresh Fruits', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/MoreFreshFruits.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3933, '3', 'Sails of Gold', 'slot', 'luckySpins', 'ls', 'wb', '', 'img/ls_3.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3934, 'wzdhth-gameview_wazdan', 'Highway to Hell', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdhth.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3935, 'zombies-gameview_netent', 'Zombies', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_zombies.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3936, 'butterflystaxx-gameview_netenthtml', 'Butterfly Staxx', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_butterflystaxx.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3937, 'vs50aladdin', '3 Genie Wishes', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs50aladdin.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3938, 'hub88-gates-of-babylon', 'Gates of Babylon', 'slot', 'Kalamba', 'ms', 'wb', '', 'img/mrslotty/hub88-gates-of-babylon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3939, 'endorphina_SparklingFresh@ENDORPHINA', 'Sparkling Fresh', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/SparklingFresh.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3940, '4', 'Gold Trophy 2', 'slot', 'luckySpins', 'ls', 'wb', '', 'img/ls_4.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3941, 'wzdhthd-gameview_wazdan', 'Highway to Hell Deluxe', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdhthd.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3942, 'copycats-gameview_netenthtml', 'Copy Cats', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_copycats.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3943, 'vs25vegas', 'Vegas Nights', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs25vegas.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3944, 'hub88-goldfire7s', 'Goldfire7s', 'slot', 'Kalamba', 'ms', 'mb', '', 'img/mrslotty/hub88-goldfire7s.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3945, 'endorphina_StoneAge@ENDORPHINA', 'Stone Age', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/StoneAge.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3946, '5', 'Myth', 'slot', 'luckySpins', 'ls', 'wb', '', 'img/ls_5.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3947, 'holdyourhorses-gameview_novomatic', 'Hold Your Horses', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_holdyourhorses.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3948, 'vs1024butterfly', 'Jade Butterfly', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs1024butterfly.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3949, 'hub88-goldfire7s', 'Goldfire7s', 'slot', 'Kalamba', 'ms', 'wb', '', 'img/mrslotty/hub88-goldfire7s.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3950, 'endorphina_UltraFresh@ENDORPHINA', 'Ultra Fresh', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/UltraFresh.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3951, '6', 'Pearl Lagoon', 'slot', 'luckySpins', 'ls', 'wb', '', 'img/ls_6.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3952, 'hollywoodstar-gameview_novomatic', 'Hollywood Star', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_hollywoodstar.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3953, 'doublestacks-gameview_netenthtml', 'Double Stacks', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_doublestacks.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3954, 'vs20santa', 'Santa', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs20santa.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3955, 'hub88-ruby-hunter', 'Ruby Hunter', 'slot', 'Kalamba', 'ms', 'mb', '', 'img/mrslotty/hub88-ruby-hunter.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3956, 'endorphina_Gladiators@ENDORPHINA', 'Gladiators', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/Gladiators.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3957, '7', 'Star Joker', 'slot', 'luckySpins', 'ls', 'wb', '', 'img/ls_7.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3958, 'hooksheroes-gameview_netenthtml', 'Hooks Heroes', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_hooksheroes.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3959, 'drivemultipliermayhem-gameview_netenthtml', 'Drive Multiplier Mayhem', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_drivemultipliermayhem.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3960, 'vs25gladiator', 'Wild Gladiator', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs25gladiator.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3961, 'hub88-ruby-hunter', 'Ruby Hunter', 'slot', 'Kalamba', 'ms', 'wb', '', 'img/mrslotty/hub88-ruby-hunter.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3962, 'endorphina_Football@ENDORPHINA', 'Football', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/Football.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3963, '8', 'Holiday Season', 'slot', 'luckySpins', 'ls', 'wb', '', 'img/ls_8.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3964, 'hotcubes-gameview_novomatic', 'Hot Cubes', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_hotcubes.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3965, 'eggomatic_touch-gameview_netenthtml', 'Eggomatic Touch', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_eggomatic_touch.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3966, 'vs13ladyofmoon', 'Lady of the Moon', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs13ladyofmoon.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3967, 'hub88-desert-gem', 'Desert Gem', 'slot', 'Kalamba', 'ms', 'mb', '', 'img/mrslotty/hub88-desert-gem.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3968, 'endorphina_WildFruits@ENDORPHINA', 'Wild Fruits', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/WildFruits.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3969, '9', 'Pimped', 'slot', 'luckySpins', 'ls', 'wb', '', 'img/ls_9.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3970, 'emojiplanet-gameview_netenthtml', 'Emoji Planet', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_emojiplanet.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3971, 'vs20rome', 'Glorious Rome', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs20rome.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3972, 'hub88-desert-gem', 'Desert Gem', 'slot', 'Kalamba', 'ms', 'wb', '', 'img/mrslotty/hub88-desert-gem.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3973, 'endorphina_FairyTale@ENDORPHINA', 'Fairy Tale', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/FairyTale.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3974, '10', 'Royal Masquerade', 'slot', 'luckySpins', 'ls', 'wb', '', 'img/ls_10.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3975, 'hotneon-gameview_amatic_mobile', 'Hot Neon', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_hotneon.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3976, 'flowerschristmas-gameview_netenthtml', 'Flowers Christmas Edition', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_flowerschristmas.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3977, 'vs20leprechaun', 'Leprechaun Song', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs20leprechaun.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3978, 'hub88-joker-3600', 'Joker 3600', 'slot', 'Kalamba', 'ms', 'mb', '', 'img/mrslotty/hub88-joker-3600.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3979, 'endorphina_Retromania@ENDORPHINA', 'Retromania', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/Retromania.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3980, '11', 'European Roulette Pro', 'slot', 'luckySpins', 'ls', 'wb', 'RULF', 'img/ls_11.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3981, 'wzdhp_2-gameview_wazdan', 'Hot Party', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdhp_2.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3982, 'football-gameview_netenthtml', 'Football: Champions Cup', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_football.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3983, 'vs20aladdinsorc', 'Aladdin and the Sorcerer', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs20aladdinsorc.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3984, 'hub88-joker-3600', 'Joker 3600', 'slot', 'Kalamba', 'ms', 'wb', '', 'img/mrslotty/hub88-joker-3600.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3985, 'endorphina_Safari@ENDORPHINA', 'Safari', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/Safari.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3986, '12', 'Jewel Box', 'slot', 'luckySpins', 'ls', 'wb', '', 'img/ls_12.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3987, 'wzdhpd-gameview_wazdan', 'Hot Party Deluxe', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdhpd.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3988, 'fruitshopchristmas-gameview_netenthtml', 'Fruitshop Christmas Edition', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_fruitshopchristmas.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3989, 'vs25dwarves', 'Dwarven Gold', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs25dwarves.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3990, 'hub88-atlantis-thunder-st-patricks-edition', 'Atlantis Thunder St Patrick\'s Edition', 'slot', 'Kalamba', 'ms', 'mb', '', 'img/mrslotty/hub88-atlantis-thunder-st-patricks-edition.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3991, 'endorphina_Sushi@ENDORPHINA', 'Sushi', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/Sushi.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3992, '13', 'Lady of Fortune', 'slot', 'luckySpins', 'ls', 'wb', '', 'img/ls_13.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3993, 'hotscatter-gameview_amatic_mobile', 'Hot Scatter', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_hotscatter.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3994, 'fruitspin-gameview_netenthtml', 'Fruit Spin', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_fruitspin.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3995, 'vs4096jurassic', 'Jurassic Giants', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs4096jurassic.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3996, 'hub88-atlantis-thunder-st-patricks-edition', 'Atlantis Thunder St Patrick\'s Edition', 'slot', 'Kalamba', 'ms', 'wb', '', 'img/mrslotty/hub88-atlantis-thunder-st-patricks-edition.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3997, 'endorphina_Origami@ENDORPHINA', 'Origami', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/Origami.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3998, '14', 'Merry Xmas', 'slot', 'luckySpins', 'ls', 'wb', '', 'img/ls_14.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(3999, 'hot7-gameview_amatic_mobile', 'Hot Seven', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_hot7.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4000, 'goldengrimoire-gameview_netenthtml', 'Golden Grimoire', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_goldengrimoire.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4001, 'vs243crystalcave', 'Magic Crystals', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs243crystalcave.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4002, 'hub88-burning-diamonds', 'Burning Diamonds', 'slot', 'Kalamba', 'ms', 'mb', '', 'img/mrslotty/hub88-burning-diamonds.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4003, 'endorphina_Macaroons@ENDORPHINA', 'Macaroons', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/Macaroons.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4004, '15', 'Samba Carnival', 'slot', 'luckySpins', 'ls', 'wb', '', 'img/ls_15.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4005, 'hotstar-gameview_amatic_mobile', 'Hot Star', 'slot', 'Amatic', 'sl', 'mb', '', 'img/sl_hotstar.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4006, 'vs13g', 'Devil\'s 13', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs13g.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4007, 'hub88-burning-diamonds', 'Burning Diamonds', 'slot', 'Kalamba', 'ms', 'wb', '', 'img/mrslotty/hub88-burning-diamonds.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4008, 'endorphina_Minotaur@ENDORPHINA', 'Minotaur', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/Minotaur.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4009, '16', 'Happy Halloween', 'slot', 'luckySpins', 'ls', 'wb', '', 'img/ls_16.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4010, 'gunsandroses-gameview_netenthtml', 'Guns N Roses', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_gunsandroses.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4011, 'vs50safariking', 'Safari King', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs50safariking.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4012, 'endorphina_SatoshisSecret@ENDORPHINA', 'Satoshi\'s Secret', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/SatoshisSecret.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4013, '17', 'Golden Legend', 'slot', 'luckySpins', 'ls', 'wb', '', 'img/ls_17.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4014, 'hottwenty-gameview_amatic_mobile', 'Hot Twenty', 'slot', 'Amatic', 'sl', 'wb', '', 'img/sl_hottwenty.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4015, 'halloweenjack-gameview_netenthtml', 'Halloween Jack', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_halloweenjack.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4016, 'vs25pantherqueen', 'Panther Queen', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs25pantherqueen.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4017, 'endorphina_Ninja@ENDORPHINA', 'Ninja', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/Ninja.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4018, '18', 'Aztec Warrior Princess', 'slot', 'luckySpins', 'ls', 'wb', '', 'img/ls_18.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4019, 'hotline-gameview_netenthtml', 'Hotline', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_hotline.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4020, 'hooksheroes-gameview_netenthtml', 'Hooks Heroes', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_hooksheroes.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4021, 'vs50hercules', 'Hercules Son of Zeus', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs50hercules.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4022, 'endorphina_Vikings@ENDORPHINA', 'Vikings', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/Vikings.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4023, '19', 'Fortune Teller', 'slot', 'luckySpins', 'ls', 'wb', '', 'img/ls_19.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4024, 'wzdhs_2-gameview_wazdan', 'Hungry Shark', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzdhs_2.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4025, 'hotline-gameview_netenthtml', 'Hotline', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_hotline.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4026, 'vs20egypt', 'Tales of Egypt', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs20egypt.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4027, 'endorphina_Jetsetter@ENDORPHINA', 'Jetsetter', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/Jetsetter.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4028, '20', 'Dragon Ship', 'slot', 'luckySpins', 'ls', 'wb', '', 'img/ls_20.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4029, 'jimihendrix-gameview_netenthtml', 'Jimi Hendrix', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_jimihendrix.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4030, 'vs15b', 'Crazy 7s', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs15b.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4031, 'endorphina_Twerk@ENDORPHINA', 'Twerk', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/Twerk.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4032, '21', 'Riches Of Ra', 'slot', 'luckySpins', 'ls', 'wb', '', 'img/ls_21.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4033, 'wzditf_2-gameview_wazdan', 'In the Forest', 'slot', 'Wazdan', 'sl', 'mb', '', 'img/sl_wzditf_2.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4034, 'jinglespin-gameview_netenthtml', 'Jingle Spin', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_jinglespin.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4035, 'vs20hockey', 'Hockey League', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs20hockey.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4036, 'endorphina2_2016Gladiators@ENDORPHINA', '2016 Gladiators', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/2016Gladiators.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4037, '22', 'Phoenix Reborn', 'slot', 'luckySpins', 'ls', 'wb', '', 'img/ls_22.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4038, 'indianspirit-gameview_novomatic', 'Indian Spirit', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_indianspirit.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4039, 'jokerpro-gameview_netenthtml', 'Joker Pro', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_jokerpro.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4040, 'vs1024atlantis', 'Queen of Atlantis', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs1024atlantis.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4041, 'hub88-mammoth-chase-easter-edition', 'Mammoth Chase Easter Edition', 'slot', 'Kalamba', 'ms', 'mb', '', 'img/mrslotty/hub88-mammoth-chase-easter-edition.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4042, 'endorphina2_2027ISS@ENDORPHINA', '2027 ISS', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/2027ISS.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4043, '23', 'Matsuri', 'slot', 'luckySpins', 'ls', 'wb', '', 'img/ls_23.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4044, 'junglespirit-gameview_netenthtml', 'Jungle Spirit: Call of the Wild', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_junglespirit.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4045, 'vs15ktv', 'KTV', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs15ktv.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4046, 'hub88-mammoth-chase-easter-edition', 'Mammoth Chase Easter Edition', 'slot', 'Kalamba', 'ms', 'wb', '', 'img/mrslotty/hub88-mammoth-chase-easter-edition.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4047, 'endorphina2_Voodoo@ENDORPHINA', 'Voodoo', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/Voodoo.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4048, '24', 'Jolly Roger', 'slot', 'luckySpins', 'ls', 'wb', '', 'img/ls_24.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4049, 'inferno-gameview_novomatic', 'Inferno', 'slot', 'Novomatic', 'sl', 'wb', '', 'img/sl_inferno.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4050, 'koiprincess-gameview_netenthtml', 'Koi Princess', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_koiprincess.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4051, 'vs25h', 'Fruity Blast', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs25h.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4052, 'endorphina2_Chunjie@ENDORPHINA', 'Chunjie', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/Chunjie.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4053, '25', 'Cats and Cash', 'slot', 'luckySpins', 'ls', 'wb', '', 'img/ls_25.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4054, 'vs25champ', 'The Champions', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs25champ.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4055, 'endorphina2_DiamondVapor@ENDORPHINA', 'Diamond Vapor', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/DiamondVapor.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4056, '26', 'Photo Safari', 'slot', 'luckySpins', 'ls', 'wb', '', 'img/ls_26.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4057, 'jackandthebeanstalk-gameview_netent', 'Jack and the Beanstalk', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_jackandthebeanstalk.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4058, 'neonstaxx-gameview_netenthtml', 'Neon Staxx', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_neonstaxx.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4059, 'vs20cms', 'Sugar Rush Summer Time', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs20cms.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4060, 'slotexchange-dads-garage', 'Dad\'s garage', 'slot', 'Slotexchange', 'ms', 'mb', '', 'img/mrslotty/slotexchange-dads-garage.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4061, 'endorphina2_Slotomoji@ENDORPHINA', 'Slotomoji', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/Slotomoji.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4062, '27', 'Space Race', 'slot', 'luckySpins', 'ls', 'wb', '', 'img/ls_27.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4063, 'jackhammer-gameview_netent', 'Jack Hammer', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_jackhammer.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4064, 'phantomoftheopera-gameview_netenthtml', 'The Phantoms Curse', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_phantomoftheopera.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4065, 'vs25romeoandjuliet', 'Romeo and Juliet', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs25romeoandjuliet.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4066, 'slotexchange-dads-garage', 'Dad\'s garage', 'slot', 'Slotexchange', 'ms', 'wb', '', 'img/mrslotty/slotexchange-dads-garage.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4067, 'endorphina2_Cuckoo@ENDORPHINA', 'Cuckoo', 'slot', 'Endorphina', 'ed', 'wb', '', 'img/ed/Cuckoo.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4068, '28', 'Cops\'n\'Robbers', 'slot', 'luckySpins', 'ls', 'wb', '', 'img/ls_28.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4069, 'jackhammer2-gameview_netent', 'Jack Hammer 2', 'slot', 'Netent', 'sl', 'wb', '', 'img/sl_jackhammer2.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4070, 'pyramid-gameview_netenthtml', 'Pyramid', 'slot', 'Netent', 'sl', 'mb', '', 'img/sl_pyramid.jpg', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4071, 'vs20cmv', 'Sugar Rush Valentine\'s Day', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg/pg_vs20cmv.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4072, 'vs25bkofkngdm', 'Book Of Kingdoms', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg_vs25bkofkngdm.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4073, 'vs10returndead', 'Return of the Dead', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg_vs10returndead.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4074, 'vs1024dtiger', 'The Dragon Tiger', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg_vs1024dtiger.png', '', 0, 0, 0, 0, 1, 1);
INSERT INTO y_games VALUES
(4075, 'vs7monkeys_jp', '7 Monkeys JP', 'slot', 'Pragmatic', 'pg', 'wb', '', 'img/pg_vs7monkeys_jp.png', '', 0, 0, 0, 0, 1, 1);
