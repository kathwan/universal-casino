CREATE TABLE IF NOT EXISTS `y_gtypes` (
  `GTY_AutoNum` int(7) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Control de registros.',
  `GTY_Codigo` char(5) COLLATE latin1_spanish_ci NOT NULL DEFAULT '' COMMENT 'Código del Tipo de Juego.',
  `GTY_Desc` char(30) COLLATE latin1_spanish_ci NOT NULL DEFAULT '' COMMENT 'Descripción (Nombre por defecto) del Tipo de Juego.',
  `GTY_Pos` int(7) unsigned NOT NULL DEFAULT 0 COMMENT 'Orden de posicionamiento en pantalla.',
  `GTY_Top` tinyint(1) unsigned NOT NULL DEFAULT 0 COMMENT 'Indica posición del juego respecto al banner. 1: Antes / 0: Después',
  `GTY_Activo` tinyint(1) unsigned NOT NULL DEFAULT 1 COMMENT 'Indica si está Activo (1) o Inactivo (0).',
  PRIMARY KEY (`GTY_AutoNum`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci ROW_FORMAT=FIXED COMMENT='Games Types - Tipos de Juego';

INSERT INTO `y_gtypes` (`GTY_AutoNum`, `GTY_Codigo`, `GTY_Desc`, `GTY_Pos`, `GTY_Top`, `GTY_Activo`) VALUES
	(1, 'POPU', 'JUEGOS POPULARES', 1, 1, 1);
INSERT INTO `y_gtypes` (`GTY_AutoNum`, `GTY_Codigo`, `GTY_Desc`, `GTY_Pos`, `GTY_Top`, `GTY_Activo`) VALUES
	(2, 'NUEV', 'JUEGOS NUEVOS', 2, 1, 1);
INSERT INTO `y_gtypes` (`GTY_AutoNum`, `GTY_Codigo`, `GTY_Desc`, `GTY_Pos`, `GTY_Top`, `GTY_Activo`) VALUES
	(3, 'EAST', 'JUEGOS DE TEMPORADA', 3, 0, 1);
INSERT INTO `y_gtypes` (`GTY_AutoNum`, `GTY_Codigo`, `GTY_Desc`, `GTY_Pos`, `GTY_Top`, `GTY_Activo`) VALUES
	(4, 'JUME', 'JUEGOS DE MESA', 4, 0, 1);
INSERT INTO `y_gtypes` (`GTY_AutoNum`, `GTY_Codigo`, `GTY_Desc`, `GTY_Pos`, `GTY_Top`, `GTY_Activo`) VALUES
	(5, 'RAGA', 'JUEGOS RASPA Y GANA', 5, 0, 1);
INSERT INTO `y_gtypes` (`GTY_AutoNum`, `GTY_Codigo`, `GTY_Desc`, `GTY_Pos`, `GTY_Top`, `GTY_Activo`) VALUES
	(6, 'RULA', 'RULETA AMERICANA', 6, 0, 1);
INSERT INTO `y_gtypes` (`GTY_AutoNum`, `GTY_Codigo`, `GTY_Desc`, `GTY_Pos`, `GTY_Top`, `GTY_Activo`) VALUES
	(7, 'RULE', 'RULETA EUROPEA', 7, 0, 1);
