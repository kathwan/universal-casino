<?php
ys_ChangeClientIP();

function ys_ChangeClientIP()
 {
 //--- URL de API
 $ys_url = "https://test.apiuniversalsoft.com/api/client/security/address";
 $ys_query = json_encode(['phrase' => "C00243766056TestLocal", 'ip' => "209.45.60.42"], true);
 $ys_curl = curl_init();
 curl_setopt($ys_curl, CURLOPT_URL, $ys_url);
 curl_setopt($ys_curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
 curl_setopt($ys_curl, CURLOPT_CUSTOMREQUEST, "PUT");
 curl_setopt($ys_curl, CURLOPT_RETURNTRANSFER, true); 
 curl_setopt($ys_curl, CURLOPT_POSTFIELDS, $ys_query);
 $ys_retval = curl_exec($ys_curl);
 curl_close($ys_curl);
 return $ys_retval;
 }
?>
