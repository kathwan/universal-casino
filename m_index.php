<?php
include_once("include_sessions.php");
include_once("ys_sql_funciones.php");
include_once("ys_leer_api.php");
?>
<!DOCTYPE html>
<html lang="<?php echo WEB_LANG; ?>">
<head>
<?php include_once("include_meta.php"); ?>
<?php include_once("include_meta_css.php"); ?>
<link rel="stylesheet" href="css/style-movil.css?=v20">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.css" id="theme-styles">
<?php include_once("include_meta_js.php"); ?>
<script language="Javascript" type="text/javascript" src="js/ys_js_ajax.js"></script>
</head>

<body class="body-bg1" OnLoad="ys_js_GetGames(ys_sel='C', ys_cat='slot');ys_js_GetUserData(ys_d=2);ys_js_GetUserData(ys_d=3);">
 <div class="header">
  <a href="home.php" style="width: 145px;"><img class="img-fluid" src="images/logo.png" alt="logo"></img>
  </a>
  <!-- SALDO -->
  <div class="user-div" data-toggle="modal" data-target="#user-perfil">
   <i class="far fa-user"></i>
   <span class="money-user">
   <span id="usu_moneda"></span>
   &nbsp;
   <span id="usu_balance">0.00</span>
   </span>
  </div>
  <!-- FIN DE SALDO -->
 </div>

 <!-- LISTA DE SALAS DE JUEGO (CATEGORIAS) -->
 <div class="bar-top">
  <div class="row no-gutters">
   <div class="col-12">
    <div class="owl-carousel carousel1">
<?php echo ys_ShowCategories(); ?>
    </div>
   </div>
  </div>
 </div>
 <!-- FIN DE SALAS DE JUEGO (CATEGORIAS) -->

 <div class="promo-bg">
  <div class="owl-carousel carousel2">
   <div class="item">
    <img class="img-fluid" src="images/casino/slider2.png" atl="promo">
   </div>
   <div class="item">
    <img class="img-fluid" src="images/casino/slider1.png" atl="promo">
   </div>
  </div>
 </div>

 <div class="jackpot-bg">

  <!-- TIPOS DE JUEGO -->

  <div class="new-carousel dragscroll">
    <ul id="ys_showtipos" class="new-carousel-ul">
      
    </ul>
  </div>
  <!-- FIN DE TIPOS DE JUEGO -->

  <div class="row no-gutters">

   <!-- FAVORITOS -->
   <div class="col-6">
    <div class="favorito-btn" OnClick="ys_js_ShowFavs();">
     <i class="far fa-heart icon-heart" aria-hidden="true"></i>
     <span>Favoritos</span>
    </div>
   </div>
   <!-- FIN DE FAVORITOS -->
   <!-- PROVEEDORES -->
   <div class="col-6">
    <div class="jackpot-list-2 dropdown-toggle">
     <a role="button" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Proveedores</a>
     <div id="ys_showprov" class="dropdown-menu drop-casino" aria-labelledby="navbarDropdown">
     </div>
    </div>
   </div>
   <!-- PROVEEDORES -->
  </div>
 </div>

 <div id="game-list">
 <?php
 //--- Recorrer las capas para despliegue de juegos.
 $SQL = "";
 $SQL .= "SELECT";
 $SQL .= " *";
 $SQL .= " FROM";
 $SQL .= " ".TB_GTYPES;
 $SQL .= " WHERE";
 $SQL .= " GTY_Activo";
 $SQL .= " ORDER BY";
 $SQL .= " GTY_Pos ASC";
 $ys_rs=YQuery($SQL,"1");
 if($ys_rs)
  {
  $ys_rs= YQuery($SQL);
  //--- Desplegar las capas superiores encontradas.
  while ($ys_file = mysqli_fetch_assoc($ys_rs))
   {ys_ShowSection($ys_file["GTY_Codigo"]);}
  } //--- if $ys_rs
?>
 </div>

 <!-- Modal -->
 <div class="modal fade" id="user-perfil" tabindex="-1" role="dialog" aria-labelledby="userperfil" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
   <div class="modal-content">
    <div class="modal-body">
     <div class="row">
      <div class="col-6">
       <p class="moda-p">Artigel # 1000074</p>
      </div>
      <div class="col-6">
       <a class="modal-salir" href="">Cerrar Sesi�n <i class="fas fa-sign-out-alt"></i></a>
      </div>
     </div>
     <hr class="mt-0">
     <div class="row">
      <div class="col-6">
       <p class="moda-p">Saldo :</p>
      </div>
      <div class="col-6">
       <div class="modal-money">
        <p class="moda-p mb-3">10520 PEN</p>
        <div class="mb-3"><i class="fas fa-sync-alt"></i></div>
       </div>
      </div>
     </div>
     <hr class="mt-0">
     <div class="row">
      <div class="col-6">
       <p class="moda-p">Saldo Bono :</p>
      </div>
      <div class="col-6">
       <div class="modal-money">
        <p class="moda-p mb-3">10520 PEN</p>
        <div class="mb-3"><i class="fas fa-sync-alt"></i></div>
       </div>
      </div>
     </div>
     <hr class="mt-0 mb-0">
     <div class="row">
      <div class="col-12">
       <a href="deposit.php" role="button" class="btn btn-success btn-block mt-3 mb-3 ">Depositar</a>
      </div>
     </div>
     <hr class="mt-0">

     <div class="row">

      <div class="col-6">
       <a class="modal-text moda-p" href="#" data-dismiss="modal" data-toggle="modal" data-target="#programas">
        <i class="fas fa-book mb-3 mr-3"></i>
        <p class="mb-3">Programas</p>
       </a>
      </div>
      <div class="col-6">
       <a class="modal-text moda-p" href="#">
        <i class="fas fa-horse-head mb-3 mr-3"></i>
        <p class="mb-3">Retirados</p>
       </a>
      </div>
      <div class="col-6">
       <a class="modal-text moda-p" href="#" data-dismiss="modal" data-toggle="modal" data-target="#user-resultado">
        <i class="fas fa-clipboard-list mb-3 mr-3"></i>
        <p class="mb-3">Resultados</p>
       </a>
      </div>
      <div class="col-6">
       <a class="modal-text moda-p" href="mybets.php">
        <i class="fas fa-clipboard-check mb-3 mr-3"></i>
        <p class="mb-3">Mis Apuestas</p>
       </a>
      </div>
      <div class="col-6">
       <a class="modal-text moda-p" href="payment-history.php">
        <i class="fas fa-search-dollar mb-3 mr-3"></i>
        <p class="mb-3">Movimientos</p>
       </a>
      </div>
      <div class="col-6">
       <a class="modal-text moda-p" href="retirement.php">
        <i class="fas fa-university mb-3 mr-3"></i>
        <p class="mb-3">Retiros</p>
       </a>
      </div>
      <div class="col-6">
       <a class="modal-text moda-p" href="#" data-dismiss="modal" data-toggle="modal" data-target="#user-clave">
        <i class="fas fa-unlock-alt mb-3 mr-3"></i>
        <p class="mb-3">Cambiar Clave</p>
      </div>
      </a>
      <div class="col-6">
       <a class="modal-text moda-p" href="profile.php">
        <i class="fas fa-user-shield mb-3 mr-3"></i>
        <p class="mb-3">Mi Cuenta</p>
       </a>
      </div>
      <div class="col-6">

      </div>

     </div>
    </div>
    <div class="modal-footer">
     <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Cerrar</button>
    </div>
   </div>
  </div>
 </div>

 <div class="modal-slot">
    <div class="body-slot">
        <iframe src="javascript:void(0);" id="iframe" frameborder="0" style="flex: 1 1 0%; width: 100%; height: 100%;"></iframe>
        <button class="btn btn-danger btn-programs modal-prog"><i class="fas fa-times" aria-hidden="true"></i></button>
    </div>
</div>
 <a class="scroll-top" href="#scrolltop" style="display: none;"><i class="fas fa-chevron-up"></i></a>
 <!-- loader -->
 <div id="ftco-loader" class="show fullscreen">
  <div class="espere-logo"><img class="img-fluid" src="images/espere.gif" alt=""></div>
  <!-- <svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg> -->
 </div>

 <script src="https://kit.fontawesome.com/a076d05399.js"></script>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 <script src="js/jquery-migrate-3.0.1.min.js"></script>
 <script src="js/popper.min.js"></script>
 <script src="js/bootstrap.min.js"></script>
 <script src="js/jquery.easing.1.3.js"></script>
 <script src="js/jquery.waypoints.min.js"></script>
 <script src="js/jquery.stellar.min.js"></script>
 <script src="js/jquery.animateNumber.min.js"></script>
 <script src="js/bootstrap-datepicker.js"></script>
 <script src="js/jquery.timepicker.min.js"></script>
 <script src="js/owl.carousel.min.js"></script>
 <script src="js/jquery.magnific-popup.min.js"></script>
 <script src="js/scrollax.min.js"></script>
 <script src="js/main.js"></script>
 <script src="js/mystyles_movil.js"></script>
 <script src="js/dragscroll.js"></script>
</body>
</html>
