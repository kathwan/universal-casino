<?php
include_once("include_sessions.php");
include_once("ys_sql_funciones.php");

//--- Actualizar los valores de Clicks en Categories / Brands/  Games
//--- El nombre del Script es ys_ajax_setc.php: "SET Clicks"

//--- Variables que se usar�n
$ys_CatCod = "";   //--- Cod de Category para AJAX.
$ys_BraCod = "";   //--- Cod de Brand para AJAX.
$ys_GamCod = "";   //--- Cod de Game para AJAX.
$ys_ModCod = "";   //--- Recibe modo de despliegue: wb: Web / mb: Mobile
$ys_CliFav = "";   //--- Tipo de contador. c: Clicks / f: Favoritos

//--- Recibir par�metro. Id de la Categor�a. (Valor por defecto: "")
if(isset($_GET["c"]))  {$ys_CatCod = $_GET["c"];}
if(isset($_GET["b"]))  {$ys_BraCod = $_GET["b"];}
if(isset($_GET["g"]))  {$ys_GamCod = $_GET["g"];}
if(isset($_GET["m"]))  {$ys_ModCod = $_GET["m"];}
if(isset($_GET["cf"])) {$ys_CliFav = $_GET["cf"];}

//--- Control de errores
$ys_CatCodError = 0;
$ys_BraCodError = 0;
$ys_GamCodError = 0;
$ys_ModCodError = 0;
$ys_CliFavError = 0;

//----------------------------------------
//--- Recorrer Categorie / Brand / Game
//----------------------------------------
$ys_ArrTipoCBG[] = "C";
$ys_ArrTipoCBG[] = "B";
$ys_ArrTipoCBG[] = "G";
$ys_CodCBG       = "";
$ys_TipoCBG      = "";

foreach($ys_ArrTipoCBG as $ys_TipoCBG)
 {
 //--- Buscar Registros
 if($ys_TipoCBG=="C") {$ys_CodCBG=$ys_CatCod;}
 if($ys_TipoCBG=="B") {$ys_CodCBG=$ys_BraCod;}
 if($ys_TipoCBG=="G") {$ys_CodCBG=$ys_GamCod;}

 //--- Cuando se trata de Favoritos:
 //--- 0: Registro NO existe. Se crea con valor 1.
 //--- 1: Registro SI existe. Es Fav para la BD y SI ES Fav para el usuario.
 //--- 2: Registro SI existe. Es Fav para la BD y NO ES Fav para el usuario.
 if(ys_searchdata($ys_tabla=TB_USUCF, $ys_condicion="UCF_UsuLogin='".$_SESSION['ULOG']."' AND UCF_TipoCBG='".$ys_TipoCBG."' AND UCF_CodCBG='".$ys_CodCBG."'", $ys_campo="", $ys_0Campo_1Contar_2AutoNum_3Max=YSEARCH_CONTAR))
  {
  //--- Existe el registro.
  $SQL  = "";
  $SQL .= "UPDATE";
  $SQL .= " ".TB_USUCF;
  $SQL .= " SET";
  if($ys_CliFav)
   {
   //--- Se incrementan los Clicks por igual para C, B y G.
   $SQL .= " UCF_Clicks = UCF_Clicks+1";
   }
  else
   {
   //--- Se incrementan los Favs.
   if($ys_TipoCBG=="G")
    {
    //--- Cuando SI son Games no se incrementan los Favs,
    //--- s�lo se intercambian entre 1 y 2.
    $SQL .= " UCF_Favs = if(UCF_Favs = 1, 2, 1)";
    }
   else
    {
    //--- Cuando NO son Games  se incrementan los Favs
    //--- de acuerdo al estado del Fav de Games.
    //--- Se debe tomar en cuenta que la validaci�n de "C" y "B" viene ANTES de "G",
    //--- por lo tanto, una vez que se incrementen los Favs de "C" y "B"
    //--- cambiar� el estado del Fav de "G".
    //--- Es decir, se incrementar� el Fav de "C" y "B" cuando el estado de Fav de "G" sea 0 � 2,
    //--- debido a que cambiar� a 1 cuando se validen los "G".
    $SQL .= " UCF_Favs = UCF_Favs + ";
    if(ys_searchdata($ys_tabla=TB_USUCF, $ys_condicion="UCF_UsuLogin='".$_SESSION['ULOG']."' AND UCF_TipoCBG='G' AND UCF_CodCBG='".$ys_GamCod."'", $ys_campo="UCF_Favs", $ys_0Campo_1Contar_2AutoNum_3Max=YSEARCH_CAMPO)<>1)
     {$SQL .= "1";}
    else
     {$SQL .= "0";}
    }
   }
  $SQL .= " WHERE";
  $SQL .= " UCF_UsuLogin = '".$_SESSION['ULOG']."'";
  $SQL .= " AND";
  $SQL .= " UCF_TipoCBG = '".$ys_TipoCBG."'";
  $SQL .= " AND";
  $SQL .= " UCF_CodCBG = '".$ys_CodCBG."'";
  }
 else
  {
  //--- NO existe el registro: Crear el registro e inciar el contador en 1 de lo que corresponda.
  $SQL = "";
  $SQL .= "INSERT INTO";
  $SQL .= " ".TB_USUCF;
  $SQL .= " VALUES";
  $SQL .= " (";
  $SQL .= " 0"                       .",";
  $SQL .= " '".$_SESSION['ULOG'] ."'".",";
  $SQL .= " '".$ys_TipoCBG         ."'".",";
  $SQL .= " '".$ys_CodCBG        ."'".",";
  if($ys_CliFav)
   {
   //--- Incrementar Clicks.
   $SQL .= " "."1"                   .",";
   $SQL .= " "."0"                       ;
   }
  else
   {
   //--- Incrementar Favs.
   $SQL .= " "."0"                   .",";
   $SQL .= " "."1"                       ;
   }
  $SQL .= " )";
  } //--- if ys_searchdata
 YQuery($SQL);
 }  //--- foreach

 //--- Totalizar en tablas de CBG los Clicks y los Favs.
 if($ys_CliFav)
  {
  //--- Totaliza Clicks en Categories.
  $SQL  = "";
  $SQL .= "UPDATE";
  $SQL .= " ".TB_CATEGORIES;
  $SQL .= " SET";
  $SQL .= " CAT_Clicks = "."(SELECT SUM(UCF_Clicks) FROM ".TB_USUCF." WHERE UCF_TipoCBG='C' AND UCF_CodCBG='".$ys_CatCod."')";
  $SQL .= " WHERE";
  $SQL .= " CAT_Codigo = '".$ys_CatCod."'";
  YQuery($SQL);

  //--- Totaliza Clicks de Brands.
  $SQL  = "";
  $SQL .= "UPDATE";
  $SQL .= " ".TB_BRANDS;
  $SQL .= " SET";
  $SQL .= " BRA_Clicks = "."(SELECT SUM(UCF_Clicks) FROM ".TB_USUCF." WHERE UCF_TipoCBG='B' AND UCF_CodCBG='".$ys_BraCod."')";
  $SQL .= " WHERE";
  $SQL .= " BRA_Codigo = '".$ys_BraCod."'";
  YQuery($SQL);

  //--- Totaliza Clicks de Games.
  $SQL  = "";
  $SQL .= "UPDATE";
  $SQL .= " ".TB_GAMES;
  $SQL .= " SET";
  $SQL .= " GAM_Clicks = "."(SELECT SUM(UCF_Clicks) FROM ".TB_USUCF." WHERE UCF_TipoCBG='G' AND UCF_CodCBG='".$ys_GamCod."')";
  $SQL .= " WHERE";
  $SQL .= " GAM_Codigo = '".$ys_GamCod."'";
  YQuery($SQL);
  }
 else
  {
  //--- Totaliza Favs de Categories.
  $SQL  = "";
  $SQL .= "UPDATE";
  $SQL .= " ".TB_CATEGORIES;
  $SQL .= " SET";
  $SQL .= " CAT_Favs = "."(SELECT SUM(UCF_Favs) FROM ".TB_USUCF." WHERE UCF_TipoCBG='C' AND UCF_CodCBG='".$ys_CatCod."')";
  $SQL .= " WHERE";
  $SQL .= " CAT_Codigo = '".$ys_CatCod."'";
  YQuery($SQL);

  //--- Totaliza Favs de Brands.
  $SQL  = "";
  $SQL .= "UPDATE";
  $SQL .= " ".TB_BRANDS;
  $SQL .= " SET";
  $SQL .= " BRA_Favs = "."(SELECT SUM(UCF_Favs) FROM ".TB_USUCF." WHERE UCF_TipoCBG='B' AND UCF_CodCBG='".$ys_BraCod."')";
  $SQL .= " WHERE";
  $SQL .= " BRA_Codigo = '".$ys_BraCod."'";
  YQuery($SQL);

  //--- Totaliza Favs de Games.
  $SQL  = "";
  $SQL .= "UPDATE";
  $SQL .= " ".TB_GAMES;
  $SQL .= " SET";
  $SQL .= " GAM_Favs = "."(SELECT SUM(UCF_Favs) FROM ".TB_USUCF." WHERE UCF_TipoCBG='G' AND UCF_CodCBG='".$ys_GamCod."')";
  $SQL .= " WHERE";
  $SQL .= " GAM_Codigo = '".$ys_GamCod."'";
  YQuery($SQL);
  }
?>
