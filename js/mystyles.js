// cambiar de metodo de carreras
$(".event-carrera-btn").click(function(e){
  $(".event-carrera-btn").removeClass("active");
  $(this).addClass("active");
})

// cambiar de tipo de apuesta
$(".bar-top-border-2").click(function(e){
  $(".bar-top-border-2").removeClass("active");
  $(this).addClass("active");
})

// al dar click a los eventos
$(".list-hipodro-home").click(function(e){
  $(".list-hipodro-home").removeClass("active");
  $(this).addClass("active");
})

// al dar click a los eventos
$(".flex-space-hipo").click(function(e){
  $(".flex-space-hipo").removeClass("active");
  $(this).addClass("active");
})

// al dar click a las promociones
$(".promo-div-nav").click(function(e){
  $(".promo-div-nav").removeClass("active");
  $(this).addClass("active");
})

// al dar click a las promociones
$(".header-btn").click(function(e){
  $(".header-btn").removeClass("active");
  $(this).addClass("active");
})

// al dar click a las promociones
$(".filter-icons").click(function(e){
  $(".filter-icons").removeClass("active");
  $(this).addClass("active");
})

// al dar click a las promociones
$(".pagos-img").click(function(e){
  $(".pagos-img").removeClass("active");
  $(this).addClass("active");
})

// al dar click a las promociones
$(".pagos-cant").click(function(e){
  $(".pagos-cant").removeClass("active");
  $(this).addClass("active");
})

$(".result-acor").click(function(e){
  $(".result-acor").removeClass("active");
  $(this).addClass("active");
})

// $("#check1").on('click',function(){

//   $(".progress-home").css({"width": "15%"});
// })
// $("#check2").on('click',function(){
  
//   $(".progress-home").css({"width": "40%"});
// })
// $("#check3").on('click',function(){
  
//   $(".progress-home").css({"width": "62%"});
// })
// $("#check4").on('click',function(){
  
//   $(".progress-home").css({"width": "85%"});
// })

var owl = $('.owl-carousel');
  owl.owlCarousel({
    margin: 10,
    loop: true,
    items:1,
    autoplay:true,
    autoplayTimeout:5000,
    dots:false
  })

  $(".lazo-soon").click(function(){
      Swal.fire({
        title: 'Muy Pronto',
        html:'<ul class="tablafire"><li>Polla Hipica</li><li>Tabla Fija</li><li>Tabla Dinamica</li><li>Puestos</li></ul>',
        icon: 'warning',
        confirmButtonText: 'Cerrar',
        confirmButtonColor:'#ed3234',
        customClass: {
          popup: 'popup-ph',
        }
      })
  })
  $("#retirados").click(function(){
    Swal.fire({
      title: 'Muy Pronto',
      icon: 'warning',
      confirmButtonText: 'Cerrar',
      confirmButtonColor:'#ed3234'
    })
  })
  
  $("#searchcasino").on("keyup", function() {

    var patron = $(this).val();
  
    // si el campo est� vac�o
    if (patron === "") {

      $(".col-item.img-grande").removeClass("sm")
      $(".col-item.img-grande").addClass("lg")
      $(".game_inner.img-grande").removeClass("game-sm")
      $(".game_inner.img-grande").addClass("game-lg")
      // mostrar todos los elementos
      $(".col-item").show();  
  
      // si tiene valores, realizar la b�squeda
    } else {
      
        $(".col-item").css("display", "none");
        $(".col-item").removeClass("lg")
        $(".col-item").addClass("sm")
        $(".game_inner").removeClass("game-lg")
        $(".game_inner").addClass("game-sm")

      // atravesar la lista
      $(".col-item").each(function() {
        if ($(this).find(".game_title p").html().toUpperCase().indexOf(patron.toUpperCase()) < 0) {
          // si el texto NO contiene el patr�n de b�squeda, esconde el elemento
          $(this).hide();
        } else {
          // si el texto S� contiene el patr�n de b�squeda, muestra el elemento
          $(this).show();
        }
  
      });
    }
  
  });
// promociones ....................................................................

$(".top-tab .promo-div-nav").click(function(){
  $(".filter .col-4").hide(200);
  $(".filter .col-4[data-filter=" + $(this).data('menu') + "]").delay(200).show(200);
});
$(".top-tab .promo-div-nav-all").click(function(){
  var value = $(this).attr('data-filter');
  
  if(value == "all"){
    $('.filter-all-img').show('1000');
  }
});

$(".tab-programa .btn-programa").click(function(){
  $(".prog-filter .col-3").hide(200);
  $(".prog-filter .col-3[data-filter=" + $(this).data('programa') + "]").delay(200).show(200);
});

$(".tab-programa .btn-programa-all").click(function(){
  var value = $(this).attr('data-filter');
  
  if(value == "all-prog"){
    $('.filter-all-prog').delay(200).show(200);
  }
});

$(".filter-icons").click(function(){
  $(".filter-casinolive .col-2").hide(200);
  $(".filter-casinolive .col-2[data-filter=" + $(this).data('online') + "]").delay(200).show(200);
});

$(".filter-icons-all").click(function(){
  var value = $(this).attr('data-filter');
  
  if(value == "all-online"){
    $('.filter-all-online').delay(200).show(200);
  }
});

/// idioma...........................................................................

$(document).ready(function(){
  // funcion para cambiar las banderas del idioma

  $('#inputPais').on('change',function(){
    var inputpaiss =  $("#inputPais option:selected").val()
    $('#flat-img').attr('src','images/Banderas/'+ inputpaiss + '.png');  
    
    var lan = inputpaiss; 

    switch(lan){
      case 'es':
        $(".Fheader1").html("Promociones");
        $(".Fheader2").html("Programas");
        $(".Fheader3").html("Resultados");
        $(".Fheader4").html("Retirados");
        $(".Fheader5").html("Hola");
        $(".Fheader6").html("Saldo :");
        $(".Fheader7").html("Saldo Bono :");
        $(".Fheader8").html("Deposito");
        $(".Fheader9").html("Salir <i class='fas fa-sign-out-alt'></i>");
        $(".Fheader10").html("Apuestas Realizadas");
        $(".Fheader11").html("Movimientos");
        $(".Fheader12").html("Retiros");
        $(".Fheader13").html("Cambio de clave");
        $(".Fheader14").html("Mi cuenta");
        $(".Fsub-header1").html("H�PICA");
        $(".Fsub-header2").html("CASINO");
        $(".Fsub-header3").html("CASINO EN VIVO");
        $(".Fsub-header4").html("JUEGOS VIRTUALES");
        $(".Fsub-header5").html("DEPORTES VR");
        $(".Fsub-header6").html("LOTERIA");
        $(".Fsub-header7").html("POKER");
        $(".Fsub-header8").html("JUEGOS TV");
        $(".Fsectionleft1").html("Seleccione su P�is e Hip�dromo");
        $(".Fsectionleft2").html("Pr�ximos Eventos");
        $(".Fsectionleft3").html("Tiempo");
        $(".Fsectionleft4").html("Hip�dromo");
        $(".Fsectionleft5").html("Carrera");
        $(".Fsectionleft6").html("Tipo");
        $(".Fsectionleft7").html("Min");
        $(".Fsectionleft8").html("Max");
        $(".Fsectioncenter1").html("SELECCIONE SU APUESTA");
        $(".Fsectioncenter2").html("Hora de inicio :");
        $(".Fsectioncenter3").html("Tiempo Restante :");
        $(".Fsectioncenter4").html("Fecha :");
        $(".Fsectioncenter5").html("Carreras");
        $(".Fsectionright1").html("Talon de apuestas");
        $(".Fsectionright2").html("Caballo");
        $(".Fsectionright3").html("Tipo");
        $(".Fsectionright4").html("Monto");
        $(".Fsectionright5").html("Accion");
        $(".Fsectionright6").html("Total :");
        $(".Fsectionright7").html("Jugadas :");
        $(".Fsectionright8").html("Realizar Apuesta");
        $(".Ffooter1").html("Todos los derechos reservados");
        $(".Ffooter2").html("Terminos y condiciones");
        $(".Ffooter3").html("Politica de privacidad");
        $(".Fmodaluser1").html("DATOS PERSONALES");
        $(".Fmodaluser2").html("Usuario");
        $(".Fmodaluser3").html("Nombre");
        $(".Fmodaluser4").html("Tel�fono M�vil");
        $(".Fmodaluser5").html("Pais");
        $(".Fmodaluser6").html("E-mail");
        $(".Fmodaluser7").html("Fecha de nacimiento");
        $(".Fmodaluser8").html("Tipo de documento");
        $(".Fmodaluser9").html("N� de documento");
        $(".Fmodaluser10").html("Estado, Provincia � Region");
        $(".Fmodaluser11").html("Ciudad");
        $(".Fmodaluser12").html("Direcci�n");
        $(".Fmodaluser13").html("Cerrar");
        $(".Fmodaluser14").html("Guardar");
        $(".Fmodalclave1").html("Contrase�a Actual");
        $(".Fmodalclave2").html("Nueva Contrase�a");
        $(".Fmodalclave3").html("Repetir Contrase�a");
        $(".Fmodalclave4").html("Cambio de contrase�a");
        $(".Fmodalretiro1").html("Ingrese el monto a retirar");
        $(".Fmodalretiro2").attr("placeholder","Ingrese el monto");
        $(".Fmodalretiro3").html("Retiro minimo de 50");
        $(".Fmodalretiro4").html("Nombre Completo");
        $(".Fmodalretiro5").html("Documento de Identidad");
        $(".Fmodalretiro6").html("Banco");
        $(".Fmodalretiro7").html("Numero de cuenta");
        $(".Fmodalretiro8").html("Informaci�n adicional");
        $(".Fmodalretiro9").html("Solicitar Retiro");
        $(".Fmodalretiro10").html("Registrar");
        $(".Fmodalretiro11").html("Retirar su saldo");
        $(".Fmodaldeposito1").html("Seleccione una cuenta");
        $(".Fmodaldeposito2").html("Seleccione su cantidad");
        $(".Fmodaldeposito3").html("Cantidad");
        $(".Fmodaldeposito4").html("Nombre de la cuenta :");
        $(".Fmodaldeposito5").html("N�mero de cuenta :");
        $(".Fmodaldeposito6").html("CCI :");
        $(".Fmodaldeposito7").html("Monto :");
        $(".Fmodaldeposito8").html("Foto del deposito :");
        $(".Fmodaldeposito9").html("Declaro que los fondos utilizados en esta <br>operaci�n, tienen su origen y destino l�cito.");
        $(".Fmodaldeposito10").html("IMPORTANTE");
        $(".Fmodaldeposito11").html("Acepto que al enviar el formulario estoy de acuerdo <br>con los t�rminos y condiciones de la P�gina web");
        $(".Fmodaldeposito12").html("Depositar");
        $(".Fmodaldeposito13").html("M�todos De Pagos");
        $(".Fmodalprogram1").html("Actualizar");
        $(".Fmodalprogram2").html("Todos");
        $(".Fmodalmovi1").html("MOVIMIENTOS DE SU CUENTA");
        $(".Fmodalmovi2").html("Desde :");
        $(".Fmodalmovi3").html("Hasta :");
        $(".Fmodalmovi4").html("Filtros :");
        $(".Fmodalmovi5").html("Consultar");
        $(".Fmodalmovi6").html("Fecha");
        $(".Fmodalmovi7").html("Hora");
        $(".Fmodalmovi8").html("Concepto");
        $(".Fmodalmovi9").html("Monto");
        $(".Fmodalmovi10").html("Saldo");
        $(".Fmodalmovi11").html("Serial/referencia");
        $(".Fmodalmovi12").html("N� referencia");
        $(".Fmodalbets1").html("MIS APUESTAS");
        $(".Fmodalbets2").html("Estado");
        $(".Fmodalbets3").html("Hip�dromo");
        $(".Fmodalbets4").html("Fecha :");
        $(".Fmodalbets5").html("Consultar");
        $(".Fmodalbets6").html("ID");
        $(".Fmodalbets7").html("Numero");
        $(".Fmodalbets8").html("Monto");
        $(".Fmodalbets9").html("Premio");
        $(".Fmodalbets10").html("Devolucion");
        $(".Fmodalbets11").html("Fecha Creado");
        $(".Fmodalbets12").html("Fecha Pagado");
        $(".Fmodalbets13").html("Estado");
        $(".Fmodalbets14").html("Acciones");
        $(".Fmodalbets15").html("En Curso");
        $(".Fmodalbets16").html("Devoluci�n");
        $(".Fmodalbets17").html("Pagado");
        $(".Fmodalbets18").html("Perdio");
        $(".Fmodalres1").html("RESULTADOS");
        $(".Fmodalres2").html("Fecha");
        $(".Fmodalres3").html("Busqueda");
        $(".Fmodalres4").html("Hip�dromo");
        $(".Fcasino1").html("Todos los Juegos");
        $(".Fcasino2").html("Raspa y Gana");
        $(".Fcasino3").html("Torneo");
        $(".Fcasino4").html("Easter");
        $(".Fcasino5").html("Nuevo Juegos");
        $(".Fcasino6").html("Juegos Populares");
        $(".Fcasino7").html("Juegos de Mesa");
        $(".Fcasino8").html("Ruleta");
        $(".Fcasino9").html("Proveedores");
        $(".Fcasino10").html("Favoritos");
        
        break;
      case 'eu' :
        $(".Fheader1").html("Promotions");
        $(".Fheader2").html("Programs");
        $(".Fheader3").html("Results");
        $(".Fheader4").html("Retired");
        $(".Fheader5").html("Hello");
        $(".Fheader6").html("Balance :");
        $(".Fheader7").html("Balance Bonus :");
        $(".Fheader8").html("Deposit");
        $(".Fheader9").html("Log off <i class='fas fa-sign-out-alt'></i>");
        $(".Fheader10").html("Placed Bets");
        $(".Fheader11").html("Transactions");
        $(".Fheader12").html("Withdrawals");
        $(".Fheader13").html("Key change");
        $(".Fheader14").html("My account");
        $(".Fsub-header1").html("HORSE RACE");
        $(".Fsub-header2").html("CASINO");
        $(".Fsub-header3").html("LIVE CASINO");
        $(".Fsub-header4").html("VIRTUAL GAMES");
        $(".Fsub-header5").html("VR SPORTS");
        $(".Fsub-header6").html("LOTTERY");
        $(".Fsub-header7").html("POKER");
        $(".Fsub-header8").html("TV GAMES");
        $(".Fsectionleft1").html("Select your country and race track");
        $(".Fsectionleft2").html("Upcoming events");
        $(".Fsectionleft3").html("Weather");
        $(".Fsectionleft4").html("Race Track");
        $(".Fsectionleft5").html("Race");
        $(".Fsectionleft6").html("Type");
        $(".Fsectionleft7").html("Min");
        $(".Fsectionleft8").html("Max");
        $(".Fsectioncenter1").html("SELECT YOUR BETS");
        $(".Fsectioncenter2").html("Start Time :");
        $(".Fsectioncenter3").html("Time remaining :");
        $(".Fsectioncenter4").html("Race date :");
        $(".Fsectioncenter5").html("Race's");
        $(".Fsectionright1").html("Betting stub");
        $(".Fsectionright2").html("Horse");
        $(".Fsectionright3").html("Type");
        $(".Fsectionright4").html("Amount");
        $(".Fsectionright5").html("Action");
        $(".Fsectionright6").html("Total :");
        $(".Fsectionright7").html("Plays :");
        $(".Fsectionright8").html("Place Bet");
        $(".Ffooter1").html("All rights reserved");
        $(".Ffooter2").html("Terms and Conditions");
        $(".Ffooter3").html("Privacy Policy");

        $(".Fmodaluser1").html("PERSONAL INFORMATION");
        $(".Fmodaluser2").html("User");
        $(".Fmodaluser3").html("Names");
        $(".Fmodaluser4").html("Cell Number");
        $(".Fmodaluser5").html("Country");
        $(".Fmodaluser6").html("E-mail");
        $(".Fmodaluser7").html("Birthdate");
        $(".Fmodaluser8").html("Document type");
        $(".Fmodaluser9").html("Identification document");
        $(".Fmodaluser10").html("State, province or region");
        $(".Fmodaluser11").html("City");
        $(".Fmodaluser12").html("Address");
        $(".Fmodaluser13").html("Cerrar");
        $(".Fmodaluser14").html("Save");

        $(".Fmodalclave1").html("Current password");
        $(".Fmodalclave2").html("New Password");
        $(".Fmodalclave3").html("Re enter Password");
        $(".Fmodalclave4").html("Change of password");

        $(".Fmodalretiro1").html("Enter the amount to withdraw");
        $(".Fmodalretiro2").attr("placeholder","Enter the amount");
        $(".Fmodalretiro3").html("Minimum");
        $(".Fmodalretiro4").html("Full Name");
        $(".Fmodalretiro5").html("ID Document");
        $(".Fmodalretiro6").html("Bank");
        $(".Fmodalretiro7").html("Account number");
        $(".Fmodalretiro8").html("Additional Information");
        $(".Fmodalretiro9").html("Request withdrawal");
        $(".Fmodalretiro10").html("Register account");
        $(".Fmodalretiro11").html("Withdrawal to bank account");

        $(".Fmodaldeposito1").html("Select an account");
        $(".Fmodaldeposito2").html("Select your quantity");
        $(".Fmodaldeposito3").html("Quantity");
        $(".Fmodaldeposito4").html("Account name :");
        $(".Fmodaldeposito5").html("Account number :");
        $(".Fmodaldeposito6").html("CCI :");
        $(".Fmodaldeposito7").html("Amount :");
        $(".Fmodaldeposito8").html("Deposit photo :");
        $(".Fmodaldeposito9").html("I declare that the funds used in this <br> operation have their lawful origin and destination");
        $(".Fmodaldeposito10").html("IMPORTANT");
        $(".Fmodaldeposito11").html("I accept that by submitting the form I agree <br>to the terms and conditions of the Website");
        $(".Fmodaldeposito12").html("Deposit");
        $(".Fmodaldeposito13").html("Payment Methods");

        $(".Fmodalprogram1").html("To update");
        $(".Fmodalprogram2").html("All");

        $(".Fmodalmovi1").html("MOVEMENTS OF YOUR ACCOUNT");
        $(".Fmodalmovi2").html("From :");
        $(".Fmodalmovi3").html("To :");
        $(".Fmodalmovi4").html("Filter :");
        $(".Fmodalmovi5").html("Consult");
        $(".Fmodalmovi6").html("Date");
        $(".Fmodalmovi7").html("Time");
        $(".Fmodalmovi8").html("Concept");
        $(".Fmodalmovi9").html("Amount");
        $(".Fmodalmovi10").html("Balance");
        $(".Fmodalmovi11").html("Serial/reference");
        $(".Fmodalmovi12").html("N� referencia");

        $(".Fmodalbets1").html("MY BETS");
        $(".Fmodalbets2").html("Status");
        $(".Fmodalbets3").html("Race Track");
        $(".Fmodalbets4").html("Date :");
        $(".Fmodalbets5").html("Consult");
        $(".Fmodalbets6").html("ID");
        $(".Fmodalbets7").html("Number");
        $(".Fmodalbets8").html("Amount");
        $(".Fmodalbets9").html("Award");
        $(".Fmodalbets10").html("Repayment");
        $(".Fmodalbets11").html("Date Created");
        $(".Fmodalbets12").html("Date Paid");
        $(".Fmodalbets13").html("Status");
        $(".Fmodalbets14").html("Action");
        $(".Fmodalbets15").html("In progress");
        $(".Fmodalbets16").html("Repayment");
        $(".Fmodalbets17").html("Paid out");
        $(".Fmodalbets18").html("Lose");

        $(".Fmodalres1").html("RESULTS");
        $(".Fmodalres2").html("Date");
        $(".Fmodalres3").html("Search");
        $(".Fmodalres4").html("Race Track");

        $(".Fcasino1").html("All Games");
        $(".Fcasino2").html("Scratch and win");
        $(".Fcasino3").html("Tournament");
        $(".Fcasino4").html("Easter");
        $(".Fcasino5").html("New Games");
        $(".Fcasino6").html("Popular games");
        $(".Fcasino7").html("Table games");
        $(".Fcasino8").html("Roulette");
        $(".Fcasino9").html("Providers");
        $(".Fcasino10").html("Favorites");
        
        break;
      case 'fran' : 
        $(".Fheader1").html("Promotions");
        $(".Fheader2").html("Programmes");
        $(".Fheader3").html("R�sultats");
        $(".Fheader4").html("Retrait�");
        $(".Fheader5").html("Bonjour");
        $(".Fheader6").html("Solde :");
        $(".Fheader7").html("Solde Bonus :");
        $(".Fheader8").html("D�p�t");
        $(".Fheader9").html("Sortir <i class='fas fa-sign-out-alt'></i>");
        $(".Fheader10").html("Paris plac�s");
        $(".Fheader11").html("Mouvements");
        $(".Fheader12").html("Retraits");
        $(".Fheader13").html("Changement de mot de passe");
        $(".Fheader14").html("Mon compte");

        $(".Fsub-header1").html("HIPPIQUE");
        $(".Fsub-header2").html("CASINO");
        $(".Fsub-header3").html("CASINO EN DIRECT");
        $(".Fsub-header4").html("JEUX VIRTUELS");
        $(".Fsub-header5").html("VR SPORTS");
        $(".Fsub-header6").html("LOTERIE");
        $(".Fsub-header7").html("POKER");
        $(".Fsub-header8").html("JEUX TV");
        
        $(".Fsectionleft1").html("S�lectionnez votre pays et votre hippodrome");
        $(".Fsectionleft2").html("�v�nements � venir");
        $(".Fsectionleft3").html("Temps");
        $(".Fsectionleft4").html("Hippodrome");
        $(".Fsectionleft5").html("Course");
        $(".Fsectionleft6").html("Type");
        $(".Fsectionleft7").html("Min");
        $(".Fsectionleft8").html("Max");

        $(".Fsectioncenter1").html("S�LECTIONNEZ VOZ PARIS");
        $(".Fsectioncenter2").html("Heure de d�but :");
        $(".Fsectioncenter3").html("Temps restant :");
        $(".Fsectioncenter4").html("Date de la Course :");
        $(".Fsectioncenter5").html("Course");

        $(".Fsectionright1").html("Talon de pari");
        $(".Fsectionright2").html("Cheval");
        $(".Fsectionright3").html("Type");
        $(".Fsectionright4").html("Montant");
        $(".Fsectionright5").html("Action");
        $(".Fsectionright6").html("Total :");
        $(".Fsectionright7").html("Pi�ces :");
        $(".Fsectionright8").html("Parier");

        $(".Ffooter1").html("Tous les droits sont r�serv�s");
        $(".Ffooter2").html("Termes et Conditions");
        $(".Ffooter3").html("Politique de confidentialit�");

        $(".Fmodaluser1").html("INFORMATIONS PERSONNELLES");
        $(".Fmodaluser2").html("Utilisateur");
        $(".Fmodaluser3").html("Noms");
        $(".Fmodaluser4").html("T�l�phone portable");
        $(".Fmodaluser5").html("Pays");
        $(".Fmodaluser6").html("Courrier");
        $(".Fmodaluser7").html("Date de naissance");
        $(".Fmodaluser8").html("Type de document");
        $(".Fmodaluser9").html("Carte d'identit�");
        $(".Fmodaluser10").html("�tat, province ou r�gion");
        $(".Fmodaluser11").html("Ville");
        $(".Fmodaluser12").html("Adresse");
        $(".Fmodaluser13").html("Fermer");
        $(".Fmodaluser14").html("sauvegarder");

        $(".Fmodalclave1").html("Mot de passe actuel");
        $(".Fmodalclave2").html("Nouveau mot de passe");
        $(".Fmodalclave3").html("R�p�ter le mot de passe");
        $(".Fmodalclave4").html("Changement de mot de passe");

        $(".Fmodalretiro1").html("Entrez le montant � retirer");
        $(".Fmodalretiro2").attr("placeholder","Entrez le montant");
        $(".Fmodalretiro3").html("Minimum");
        $(".Fmodalretiro4").html("Nom complet");
        $(".Fmodalretiro5").html("Carte d'identit�");
        $(".Fmodalretiro6").html("Banque");
        $(".Fmodalretiro7").html("Num�ro de compte");
        $(".Fmodalretiro8").html("Information compl�mentaire");
        $(".Fmodalretiro9").html("Demande de retrait");
        $(".Fmodalretiro10").html("Cr�er un compte");
        $(".Fmodalretiro11").html("Retrait sur un compte bancaire");

        $(".Fmodaldeposito1").html("S�lectionnez un compte");
        $(".Fmodaldeposito2").html("S�lectionnez votre quantit�");
        $(".Fmodaldeposito3").html("Quantit�");
        $(".Fmodaldeposito4").html("Nom du compte :");
        $(".Fmodaldeposito5").html("Num�ro de compte :");
        $(".Fmodaldeposito6").html("CCI :");
        $(".Fmodaldeposito7").html("Total :");
        $(".Fmodaldeposito8").html("D�p�t de photo :");
        $(".Fmodaldeposito9").html("Je d�clare que les fonds utilis�s dans cette<br> op�ration ont leur origine et destination l�gales.");
        $(".Fmodaldeposito10").html("IMPORTANT");
        $(".Fmodaldeposito11").html("J'accepte qu'en soumettant le formulaire <br> j'accepte les termes et conditions du site Internet ");
        $(".Fmodaldeposito12").html("D�poser");
        $(".Fmodaldeposito13").html("M�thodes de payement");

        $(".Fmodalprogram1").html("Mettre � jour");
        $(".Fmodalprogram2").html("Tout");

        $(".Fmodalmovi1").html("MOUVEMENTS DE VOTRE COMPTE");
        $(".Fmodalmovi2").html("Depuis :");
        $(".Fmodalmovi3").html("Jusqu'� :");
        $(".Fmodalmovi4").html("Filtres :");
        $(".Fmodalmovi5").html("Consulter");
        $(".Fmodalmovi6").html("Date");
        $(".Fmodalmovi7").html("Heure");
        $(".Fmodalmovi8").html("Concept");
        $(".Fmodalmovi9").html("Montant");
        $(".Fmodalmovi10").html("Solde");
        $(".Fmodalmovi11").html("S�rie/R�f�rence");
        $(".Fmodalmovi12").html("N� R�f�rence");

        $(".Fmodalbets1").html("MIS APUESTAS");
        $(".Fmodalbets2").html("�tat");
        $(".Fmodalbets3").html("Hippodrome");
        $(".Fmodalbets4").html("Date :");
        $(".Fmodalbets5").html("Consulter");
        $(".Fmodalbets6").html("ID");
        $(".Fmodalbets7").html("Num�ro");
        $(".Fmodalbets8").html("Montant");
        $(".Fmodalbets9").html("Prix");
        $(".Fmodalbets10").html("Revenir");
        $(".Fmodalbets11").html("Date cr��e");
        $(".Fmodalbets12").html("Date de paiement");
        $(".Fmodalbets13").html("�tat");
        $(".Fmodalbets14").html("action");
        $(".Fmodalbets15").html("En cours");
        $(".Fmodalbets16").html("Revenir");
        $(".Fmodalbets17").html("Pay�");
        $(".Fmodalbets18").html("Perdu")

        $(".Fmodalres1").html("R�SULTATS");
        $(".Fmodalres2").html("Date");
        $(".Fmodalres3").html("Chercher");
        $(".Fmodalres4").html("Hippodrome");

        $(".Fcasino1").html("Tous les jeux");
        $(".Fcasino2").html("Grattez et gagnez");
        $(".Fcasino3").html("Tournoi");
        $(".Fcasino4").html("P�ques");
        $(".Fcasino5").html("Nouveaux jeux");
        $(".Fcasino6").html("Jeux populaires");
        $(".Fcasino7").html("Jeux de soci�t�");
        $(".Fcasino8").html("Roulette");
        $(".Fcasino9").html("Fournisseurs");
        $(".Fcasino10").html("Favoris");
        break;
      case 'bra' : 
        $(".Fheader1").html("Promo��es");
        $(".Fheader2").html("Programas");
        $(".Fheader3").html("Resultados");
        $(".Fheader4").html("Aposentado");
        $(".Fheader5").html("Ol�");
        $(".Fheader6").html("Saldo :");
        $(".Fheader7").html("Saldo de B�nus :");
        $(".Fheader8").html("Dep�sito ");
        $(".Fheader9").html("Saia <i class='fas fa-sign-out-alt'></i>");
        $(".Fheader10").html("Apostas");
        $(".Fheader11").html("Movimientos");
        $(".Fheader12").html("Saques ");
        $(".Fheader13").html("mudan�a de chave");
        $(".Fheader14").html("Minha conta");

        $(".Fsub-header1").html("H�PICA");
        $(".Fsub-header2").html("CASSINO");
        $(".Fsub-header3").html("CASINO AO VIVO");
        $(".Fsub-header4").html("JOGOS VIRTUAIS");
        $(".Fsub-header5").html("VR ESPORTES");
        $(".Fsub-header6").html("LOTERIA");
        $(".Fsub-header7").html("P�QUER");
        $(".Fsub-header8").html("JOGOS TV");

        $(".Fsectionleft1").html("Selecione seu pa�s e pista de corrida");
        $(".Fsectionleft2").html("Pr�ximos Eventos");
        $(".Fsectionleft3").html("Tempo");
        $(".Fsectionleft4").html("Hip�dromo");
        $(".Fsectionleft5").html("Carreira");
        $(".Fsectionleft6").html("Tipo");
        $(".Fsectionleft7").html("Min");
        $(".Fsectionleft8").html("Max");

        $(".Fsectioncenter1").html("SELECCIONE SU APUESTA");
        $(".Fsectioncenter2").html("Hora de in�cio :");
        $(".Fsectioncenter3").html("Tempo restante :");
        $(".Fsectioncenter4").html("data da corrida :");
        $(".Fsectioncenter5").html("Corrida");

        $(".Fsectionright1").html("Tiket de aposta");
        $(".Fsectionright2").html("Cavalo");
        $(".Fsectionright3").html("Tipo");
        $(".Fsectionright4").html("Montante");
        $(".Fsectionright5").html("A�ao");
        $(".Fsectionright6").html("Total :");
        $(".Fsectionright7").html("Jogadas :");
        $(".Fsectionright8").html("Fazer Aposta");

        $(".Ffooter1").html("Todos os direitos reservados");
        $(".Ffooter2").html("termos e Condi��es");
        $(".Ffooter3").html("Pol�tica de Privacidade");

        $(".Fmodaluser1").html("DADOS PESSOAIS");
        $(".Fmodaluser2").html("Do utilizador");
        $(".Fmodaluser3").html("Nome");
        $(".Fmodaluser4").html("Telefone celular");
        $(".Fmodaluser5").html("Pa�s");
        $(".Fmodaluser6").html("E-mail");
        $(".Fmodaluser7").html("Data de nascimento");
        $(".Fmodaluser8").html("Tipo de documento");
        $(".Fmodaluser9").html("N�mero do documento");
        $(".Fmodaluser10").html("Estado, Prov�ncia ou Regi�o");
        $(".Fmodaluser11").html("Cidade");
        $(".Fmodaluser12").html("Endere�o");
        $(".Fmodaluser13").html("Fechar");
        $(".Fmodaluser14").html("Salve");

        $(".Fmodalclave1").html("Senha atual");
        $(".Fmodalclave2").html("Nova senha");
        $(".Fmodalclave3").html("Repetir a senha");
        $(".Fmodalclave4").html("Mudan�a de senha");

        $(".Fmodalretiro1").html("Insira o valor a retirar");
        $(".Fmodalretiro2").attr("placeholder","Insira o valor");
        $(".Fmodalretiro3").html("retirada m�nima");
        $(".Fmodalretiro4").html("Nome completo");
        $(".Fmodalretiro5").html("Identidade");
        $(".Fmodalretiro6").html("Banco");
        $(".Fmodalretiro7").html("Numero de conta");
        $(".Fmodalretiro8").html("Informa��o adicional");
        $(".Fmodalretiro9").html("Solicitar retirada");
        $(".Fmodalretiro10").html("Registrar");
        $(".Fmodalretiro11").html("Retirar seu saldo");

        $(".Fmodaldeposito1").html("Selecione uma conta");
        $(".Fmodaldeposito2").html("Selecione sua quantidade");
        $(".Fmodaldeposito3").html("Quantidade");
        $(".Fmodaldeposito4").html("Nome da conta :");
        $(".Fmodaldeposito5").html("Numero de conta :");
        $(".Fmodaldeposito6").html("CCI :");
        $(".Fmodaldeposito7").html("Montante :");
        $(".Fmodaldeposito8").html("Foto do tanque :");
        $(".Fmodaldeposito9").html("Declaro que os fundos utilizados nesta<br> opera��o t�m origem e destino legais.");
        $(".Fmodaldeposito10").html("IMPORTANTE");
        $(".Fmodaldeposito11").html("Aceito que, ao enviar o formul�rio, concordo<br> com os termos e condi��es do site");
        $(".Fmodaldeposito12").html("Depositar");
        $(".Fmodaldeposito13").html("M�todos de Pagamento");

        $(".Fmodalprogram1").html("Atualizar");
        $(".Fmodalprogram2").html("Todos");

        $(".Fmodalmovi1").html("MOVIMENTOS DA SUA CONTA");
        $(".Fmodalmovi2").html("Desde a :");
        $(".Fmodalmovi3").html("At� :");
        $(".Fmodalmovi4").html("Filtros :");
        $(".Fmodalmovi5").html("Consultar");
        $(".Fmodalmovi6").html("Encontro");
        $(".Fmodalmovi7").html("Hora");
        $(".Fmodalmovi8").html("Conceito");
        $(".Fmodalmovi9").html("Montante");
        $(".Fmodalmovi10").html("Saldo");
        $(".Fmodalmovi11").html("Serial/refer�ncia");
        $(".Fmodalmovi12").html("N� refer�ncia");

        $(".Fmodalbets1").html("MINHAS APOSTAS");
        $(".Fmodalbets2").html("Estado");
        $(".Fmodalbets3").html("Pista de corridas");
        $(".Fmodalbets4").html("Encontro :");
        $(".Fmodalbets5").html("Consultar");
        $(".Fmodalbets6").html("ID");
        $(".Fmodalbets7").html("N�mero");
        $(".Fmodalbets8").html("Montante");
        $(".Fmodalbets9").html("Pr�mio");
        $(".Fmodalbets10").html("Retorna");
        $(".Fmodalbets11").html("Data Criada");
        $(".Fmodalbets12").html("Data do pagamento");
        $(".Fmodalbets13").html("Estado");
        $(".Fmodalbets14").html("A��es");
        $(".Fmodalbets15").html("No curso");
        $(".Fmodalbets16").html("Retorna");
        $(".Fmodalbets17").html("Pago");
        $(".Fmodalbets18").html("Perdido");

        $(".Fmodalres1").html("RESULTADO");
        $(".Fmodalres2").html("Encontro");
        $(".Fmodalres3").html("Procurar");
        $(".Fmodalres4").html("Pista de corridas");

        $(".Fcasino1").html("Todos os jogos");
        $(".Fcasino2").html("Arranhar e ganhar");
        $(".Fcasino3").html("Torneio");
        $(".Fcasino4").html("P�scoa");
        $(".Fcasino5").html("Novos jogos");
        $(".Fcasino6").html("Jogos populares");
        $(".Fcasino7").html("Jogos de tabuleiro");
        $(".Fcasino8").html("Roleta");
        $(".Fcasino9").html("Fornecedores");
        $(".Fcasino10").html("Favoritos");
        break;
    }
    
  });

});

function abrir_modal(href)
 {
 const modal_tag = '.modal-slot';
 $(modal_tag).toggleClass('active');
 if( $(modal_tag).find("#iframe").length ) {$(modal_tag).find("#iframe").remove();}
 $(modal_tag).find('.body-slot').append("<iframe src='"+href+"' id='iframe' frameborder='0' style='flex: 1 1 0%; width: 100%; height: 100%;'></iframe>");
 $('.modal-prog').click(function()
  {
  if($(modal_tag).find("#iframe").length)
   {
   $(".modal-slot").removeClass('active');
   $(".modal-slot").find("#iframe").remove();
   }
  ys_js_GetUserData(ys_d=3);
  });
 }

function no_jackpot()
 {
 $(".body-casino").css("padding-top","3rem");
 $("#main-jackpot").hide();
 }

function hover_tv()
 {
  $(".header-btn").hover(function(e)
  {
  e.preventDefault();
  var tipo = e.type;
  $(this).addClass(tipo);
  }, function()
      {$('.header-btn').removeClass('mouseenter');})
 }

$(".search-casino").submit(function(e)
 {
 e.preventDefault();
 var name = $("#searchcasino").val();
 list_games({page:1,g:name});
});

function likes(val)
 {
 //--- Encender o apagar indicador de Fav.
 //--- Recibe el c�digo del juego.
 var likes = document.getElementById(escape(val));
 if(likes.classList.contains("far"))
  {
  likes.classList.add("fas");
  likes.classList.remove("far");
  }
 else
  {
  likes.classList.add("far");
  likes.classList.remove("fas");
  }
 }

function ys_js_Contadores(ys_c='', ys_p='', ys_b='', ys_g='', ys_m='', ys_cf='', ys_n=0)
 {
 //--- Establecer el indicador de Favs.
 GetAjax('ys_ajax_setc.php?c='+ys_c+'&b='+ys_b+'&g='+ys_g+'&m='+ys_m+'&cf='+ys_cf, '', 'GET');

 //--- Incrementar contador de Clicks y Fav.
 ys_sum = 1;  //--- Incremento de contador de Fav s�lo cuando el usuario nunca haya pulsado Fav a este Game.
 if(ys_n<0)
  {
  //--- El usuario ya puls� Fav a este Game. No se incrementar�.
  ys_sum = 0;
  }
 ys_n = Math.abs(ys_n) + ys_sum;
 ys_g = ys_g.replace("@", "_");
 if(!ys_cf) {$('#f-'+ys_g).html(ys_n);}
 }

// Scroll to top button appear
$(document).on('scroll', function() {
  var scrollDistance = $(this).scrollTop();
  if (scrollDistance > 100) {
    $('.scroll-top').fadeIn();
  } else {
    $('.scroll-top').fadeOut(100);
  }
});

  function scrollWin(elementSelector, offset, time){
    if(typeof(offset)=="undefined")
        offset = 0;
    if(typeof(time)=="undefined")
        time = 1000;
    $('body,html').stop().animate({scrollTop: $(elementSelector).offset().top+offset}, time);
  }
  function ys_name(name){
      console.log("1");
      console.log(name);
      scrollWin("#ys_sec"+name, 0, 1000);
      return false;
  }

  $("a[href='#scrolltop']").click(function() {
    scrollWin("body", 0, 1000);
    return false;
  });

//  juegos_array("slot","POPU","wb");
//  juegos_array("slot","NUEV","wb");
//  juegos_array("slot","EAST","wb");
//  juegos_array("slot","JUME","wb");
//  juegos_array("slot","RAGA","wb");
//  juegos_array("slot","RULA","wb");
//  juegos_array("slot","RULE","wb");
//  juegos_array("slot","CLAS","wb");

let current_page = [];

juegos_tipos("slot")

 function juegos_tipos(categoria)
    {
      
    $.ajax({
      url: "https://www.lobbyuniversalsoft.net/gameTypes",
      type: "POST",
      contentType: "application/json",
      error: (a, b, c) => { console.log(a, b, c);},
      success: function(data_tipos) {
        if(data_tipos == "" || data_tipos == "undefined" || data_tipos.status == 0){
          return;
        }
          console.log("success",data_tipos)
          for(i=0;i<=data_tipos.registros-1;i++)
          {
            current_page.push(data_tipos["tipos"][i].ind);
            juegos_array(categoria,data_tipos["tipos"][i].cod,"wb",data_tipos["tipos"][i].ind);
          }
      }
  });
   
 }
 
 function juegos_array(categoria,tipos,devices,indices){
  
    current_page[indices]=1;

    $.ajax({
      url: "https://www.lobbyuniversalsoft.net/gamePager",
      type: "POST",
      dataType: "json",
      data: JSON.stringify({ cat:categoria , type:tipos, device:devices }),
      contentType: "application/json",
      success: function(data_pager) {
          console.log("success",data_pager)

          if(data_pager == "" || data_pager == "undefined" || data_pager.status == 0){
              $("#ys_sec"+tipos).css("display","none")
              $('#ys_show'+tipos).find(".col-item").remove();
              $("#vermas"+tipos).hide()
                return
            }
          
          $("#ys_sec"+tipos).css("display","block")
          $('#ys_show'+tipos).find(".col-item").remove();
          
        
          var ini = ((current_page[indices]-1) * data_pager.limite);
          var fin = ((data_pager.limite * current_page[indices])-1); 

          $("#vermas"+tipos).show()
            
          if(current_page[indices] >= data_pager.paginas){
            $("#vermas"+tipos).hide()
            $("#vermenos"+tipos).hide()
          }

          game_contruction(data_pager["juegos"],tipos,ini,fin,data_pager.limite,data_pager.paginas,indices);

      },
      error: function(error){
        console.log("error",error)
      } 
  });

 }
 function vermas(data_mas,tipo,limite,paginas,indice){
    current_page[indice]++;

    if(current_page[indice] >= paginas ){
      current_page[indice] = paginas;
      $("#vermas"+tipo).hide();
      // $("#vermenos"+tipo).show();
    }else{
      $("#vermas"+tipo).show();
    }

    var ini = ((current_page[indice]-1) * limite);
    var fin = ((limite * current_page[indice])-1);

    game_contruction(data_mas,tipo,ini,fin,limite,paginas,indice);
 }

 function vermenos(data,tipo,limite,paginas,indice){

  current_page[indice]--;
  if(current_page[indice] <= 1 ){
    current_page[indice] = 1;
    $("#vermenos"+tipo).hide();
    $("#vermas"+tipo).show();
  }else{
    $("#vermas"+tipo).show();
  }

  let ini = ((current_page[indice]-1) * limite);
  let fin = ((limite * current_page[indice])-1);
  $('#ys_show'+tipo).find(".col-item").remove();
  $("#vermas"+tipo).show();
  game_contruction(data,tipo,ini,fin,limite,paginas,indice);
}

 function game_contruction(data,tipo,ini,fin,limite,paginas,indice){
    for(i=ini;i<=fin;i++){
      if(i >= data.length){
        break
      }
      var id = data[i].id;
      var nombre = data[i].name;
      var categoria = data[i].category;
      var provee = data[i].brand;
      var control = data[i].control;
      var url_img = data[i].image;
      var device = data[i].device;
      var favs_total = data[i].favtot;
      var favs_estado = data[i].favest;

      var div = ` 
        <div id="p-${id}" class="col-item sm">
          <div class="row no-gutters">
            <div class="col-12 effect-padd2">
              <div class="effect-padd">
                <div class="new-text-div">
                  <p class="title-game">${nombre}</p>
                </div>
                <div class="game_inner game-sm my-effect" style="background-image:url(https://test.apiuniversalsoft.com/${url_img})">
          <div class="fav_holder">
            <div class="like-game">
            <i class="${favs_estado} fa-heart" style="display:block" id="${id}" aria-hidden="true" onclick="ys_js_Contadores(ys_c='${categoria}', ys_p='${control}', ys_b='${provee}', ys_g='${id}', ys_m='${device}', ys_cf=0, ys_n=${favs_total});likes('${id}')"></i>
            <span id="f-${id}" class="span-casino">${Math.abs(favs_total)}</span>
            </div>
          </div>
          <div class="game_overlay">
            <div class="play">
            <div class="circle-play"></div>
              <!-- URL DEL JUEGO -->
              <a href="javascript:void(0)" onclick="ys_js_LaunchGame(ys_c='${categoria}', ys_p='${control}', ys_b='${provee}', ys_g='${id}', ys_m='${device}');"><i class="fas fa-play" aria-hidden="true"></i></a>
              <!-- FIN DE URL DEL JUEGO -->
            </div>
            <div class="game_title"><img class="img-fluid" src="https://demo.apiuniversalsoft.com/images/casino/providers/${provee}.png" alt=""></div>
            </div>
            </div>
          </div>
          </div>
          </div>
        </div>`
      
      $('#ys_show'+tipo).append(div);
      // likes(id)
      
    }
    $("#vermas"+tipo).on('click',function(){
      vermas(data,tipo,limite,paginas,indice)
    })

    // $("#vermenos"+tipo).on('click',function(){
    //   vermenos(data,tipo,limite,paginas,indice)
    // })
    
    
    // $('#ys_show'+tipo).append(btn_mas_menos);
    
 }

 