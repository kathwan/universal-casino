/* Programación AJAX  */
/* (c) 2020 - YOLED   */
/* http://www.yoled.com */

function CreateAjax()
 {
 var ys_objetoAjax = false;
 /* Para navegadores distintos a Internet Explorer */
 try
  {ys_objetoAjax = new ActiveXObject("Msxml2.XMLHTTP");}

 /* Para Internet Explorer */
 catch (e)
  {
  try
   {ys_objetoAjax = new ActiveXObject("Microsoft.XMLHTTP");} 
  catch (E)
   {ys_objetoAjax = false;}
  }

 if (!ys_objetoAjax && typeof XMLHttpRequest!='undefined')
  {ys_objetoAjax = new XMLHttpRequest();}

 return ys_objetoAjax;
 }

function GetAjax(ys_url='', ys_capa='', ys_metodo='GET', ys_sync=true)
 {
 var ys_AJAX = CreateAjax();
 var ys_readyState;
 var ys_retval = [];
 ys_retval.push("Procesando...");      //--- 0
 ys_retval.push("Iniciando...");       //--- 1
 ys_retval.push("Conectado...");       //--- 2
 ys_retval.push("Recibiendo datos...");//--- 3
 ys_retval.push("");                   //--- 4
 if(ys_capa!='')
  {var ys_capaContenedora = document.getElementById(ys_capa);}
 /* Creamos y ejecutamos la instancia si el ys_metodo elegido es GET */
 if (ys_metodo.toUpperCase()=='GET')
  {
  ys_AJAX.open('GET', ys_url, true);
  ys_AJAX.onreadystatechange = function()
                               {
                               //--- readyState
                               //--- 0 UNSENT            Client has been created. open() not called yet.
                               //--- 1 OPENED            open() has been called.
                               //--- 2 HEADERS_RECEIVED  send() has been called, and headers and status are available.
                               //--- 3 LOADING           Downloading; responseText holds partial data.
                               //--- 4 DONE              The operation is complete.
                               ys_readyState = ys_AJAX.readyState;
                               if (ys_readyState>=0 && ys_readyState<=3)
                                {if(ys_capa!='') {ys_capaContenedora.innerHTML = ys_retval[ys_readyState];}}
                               else if (ys_readyState==4)
                                {
                                if(ys_AJAX.status==200)
                                 {
                                 //--- Procesamiento correcto. Status 200: Ok.
                                 ys_retval[4] = ys_AJAX.responseText;
                                 if(ys_capa!='') {ys_capaContenedora.innerHTML = ys_retval[4];}
                                 }
                                else
                                 {
                                 //--- Error en el procesamiento del URL.
                                 ys_retval[4] = "Error: ".ys_AJAX.status;
                                 if(ys_capa!='') {ys_capaContenedora.innerHTML = ys_retval[4];}
                                 }     //--- if ys_AJAX status 200
                                } //--- if ys_AJAX readyState 1
                               }
  ys_AJAX.setRequestHeader('Content-Type', 'text/plain');
  ys_AJAX.setRequestHeader('Content-Language', 'es-VE');
  ys_AJAX.send(null);
  return ys_retval[4];
  }
 }
